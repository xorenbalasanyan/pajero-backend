DROP TRIGGER file__after_insert;
DROP TRIGGER file__after_update;
DROP TRIGGER file__before_delete;
DROP TABLE file__log;

DROP TRIGGER file_marker__after_insert;
DROP TRIGGER file_marker__after_update;
DROP TRIGGER file_marker__before_delete;
DROP TABLE file_marker__log;

DROP TRIGGER file_type__after_insert;
DROP TRIGGER file_type__after_update;
DROP TRIGGER file_type__before_delete;
DROP TABLE file_type__log;

DROP TRIGGER good_after_insert;
DROP TRIGGER good_after_update;
DROP TRIGGER good_before_delete;
DROP TABLE good_log;

DROP TRIGGER info_message__after_insert;
DROP TRIGGER info_message__after_update;
DROP TRIGGER info_message__before_delete;
DROP TABLE info_message__log;

DROP TRIGGER shop_discount__after_insert;
DROP TRIGGER shop_discount__after_update;
DROP TRIGGER shop_discount__before_delete;
DROP TABLE shop_discount__log;

