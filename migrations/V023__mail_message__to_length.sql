ALTER TABLE mail_message
    MODIFY `to` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
    MODIFY cc TEXT COLLATE utf8mb4_unicode_ci NULL,
    MODIFY bcc TEXT COLLATE utf8mb4_unicode_ci NULL;

ALTER TABLE mail_message_log
    MODIFY `to` TEXT COLLATE utf8mb4_unicode_ci NULL,
    MODIFY cc TEXT COLLATE utf8mb4_unicode_ci NULL,
    MODIFY bcc TEXT COLLATE utf8mb4_unicode_ci NULL;
