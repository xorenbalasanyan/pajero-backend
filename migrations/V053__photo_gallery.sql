CREATE TABLE photo_gallery
(
    id              INT PRIMARY KEY AUTO_INCREMENT                                      NOT NULL,
    accountId       INT                                                                 NOT NULL,
    fileId          INT                                                                 NOT NULL,
    createdAt       TIMESTAMP DEFAULT CURRENT_TIMESTAMP()                               NOT NULL,
    ownerId         INT                                                                 NOT NULL,
    CONSTRAINT fk__photo_gallery__file FOREIGN KEY (fileId) REFERENCES file (id) ON DELETE RESTRICT,
    CONSTRAINT fk__photo_gallery__user FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__photo_gallery__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT
);
