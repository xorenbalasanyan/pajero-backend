-- TABLE mail

CREATE TABLE mail_message
(
    id          INT PRIMARY KEY AUTO_INCREMENT          NOT NULL,
    `from`      VARCHAR(300) COLLATE utf8mb4_unicode_ci NULL,
    `to`        VARCHAR(300) COLLATE utf8mb4_unicode_ci NOT NULL,
    cc          VARCHAR(300) COLLATE utf8mb4_unicode_ci,
    bcc         VARCHAR(300) COLLATE utf8mb4_unicode_ci,
    subject     VARCHAR(300) COLLATE utf8mb4_unicode_ci NOT NULL,
    text        TEXT COLLATE utf8mb4_unicode_ci         NOT NULL,
    fileIds     TEXT                                    NULL,
    status      ENUM ('CREATED', 'SENDED', 'ERROR')     NOT NULL,
    errorCount  INT       DEFAULT 0                     NOT NULL,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()   NOT NULL,
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL
);

ALTER TABLE mail_message
    AUTO_INCREMENT = 30000;

CREATE TABLE mail_message_log
(
    id          INT,
    `from`      VARCHAR(300) COLLATE utf8mb4_unicode_ci,
    `to`        VARCHAR(300) COLLATE utf8mb4_unicode_ci,
    cc          VARCHAR(300) COLLATE utf8mb4_unicode_ci,
    bcc         VARCHAR(300) COLLATE utf8mb4_unicode_ci,
    subject     VARCHAR(300) COLLATE utf8mb4_unicode_ci,
    text        TEXT COLLATE utf8mb4_unicode_ci,
    fileIds     TEXT,
    status      ENUM ('CREATED', 'SENDED', 'ERROR'),
    errorCount  INT,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER mail_message_after_insert
    AFTER INSERT
    ON mail_message
    FOR EACH ROW
BEGIN
    INSERT INTO mail_message_log
    VALUES (NEW.id, NEW.`from`, NEW.`to`, NEW.cc, NEW.bcc, NEW.subject, NEW.text, NEW.fileIds,
            NEW.status, NEW.errorCount, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId,
            1);
END;

CREATE TRIGGER mail_message_after_update
    AFTER UPDATE
    ON mail_message
    FOR EACH ROW
BEGIN
    INSERT INTO mail_message_log
    VALUES (NEW.id, NEW.`from`, NEW.`to`, NEW.cc, NEW.bcc, NEW.subject, NEW.text, NEW.fileIds,
            NEW.status, NEW.errorCount, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId,
            2);
END;

CREATE TRIGGER mail_message_before_delete
    BEFORE DELETE
    ON mail_message
    FOR EACH ROW
BEGIN
    INSERT INTO mail_message_log
    VALUES (OLD.id, OLD.`from`, OLD.`to`, OLD.cc, OLD.bcc, OLD.subject, OLD.text, OLD.fileIds,
            OLD.status, OLD.errorCount, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId,
            3);
END;
