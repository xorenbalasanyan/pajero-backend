DROP TRIGGER IF EXISTS checklist_task_to_shop__after_insert;
DROP TRIGGER IF EXISTS checklist_task_to_shop__after_update;
DROP TRIGGER IF EXISTS checklist_task_to_shop__before_delete;
DROP TABLE checklist_task_to_shop__log;

DROP TABLE checklist_task_to_shop;

DROP TRIGGER IF EXISTS checklist_task__after_insert;
DROP TRIGGER IF EXISTS checklist_task__after_update;
DROP TRIGGER IF EXISTS checklist_task__before_delete;
DROP TABLE checklist_task__log;

DROP TABLE checklist_task;

