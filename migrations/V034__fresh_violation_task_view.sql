CREATE VIEW fresh_violation_task_view AS
SELECT *
FROM violation_task vt
WHERE (DATE_ADD(vt.updatedAt, INTERVAL vt.executionPeriodDays + 30 DAY) > CURRENT_DATE);
