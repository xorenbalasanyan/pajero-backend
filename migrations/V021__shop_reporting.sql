CREATE TABLE shop_reporting
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    accountId   INT                                     NOT NULL,
    shopId      INT                                     NOT NULL,
    reportType  ENUM ('EMPLOYEE_COUNT')                 NOT NULL,
    date        DATE                                    NOT NULL,
    value       TEXT COLLATE utf8mb4_unicode_ci         NOT NULL,
    isReported  TINYINT                                 NOT NULL DEFAULT 0,
    createdAt   TIMESTAMP                               NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    ownerId     INT                                     NOT NULL,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NULL,
    moderatorId INT                                     NULL,
    CONSTRAINT fk__shop_reporting__shop FOREIGN KEY (shopId) REFERENCES shop (id) ON DELETE RESTRICT,
    CONSTRAINT fk__shop_reporting__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__shop_reporting__moderator FOREIGN KEY (moderatorId) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE shop_reporting__log
(
    id          INT,
    accountId   INT,
    shopId      INT,
    reportType  ENUM ('EMPLOYEE_COUNT'),
    date        DATE,
    value       TEXT COLLATE utf8mb4_unicode_ci,
    isReported  TINYINT,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(),
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER shop_reporting__after_insert
    AFTER INSERT
    ON shop_reporting
    FOR EACH ROW
BEGIN
    INSERT INTO shop_reporting__log
    VALUES (NEW.id, NEW.accountId, NEW.shopId, NEW.reportType, NEW.date, NEW.value, NEW.isReported, NEW.createdAt,
            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER shop_reporting__after_update
    AFTER UPDATE
    ON shop_reporting
    FOR EACH ROW
BEGIN
    INSERT INTO shop_reporting__log
    VALUES (NEW.id, NEW.accountId, NEW.shopId, NEW.reportType, NEW.date, NEW.value, NEW.isReported, NEW.createdAt,
                NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER shop_reporting__before_delete
    BEFORE DELETE
    ON shop_reporting
    FOR EACH ROW
BEGIN
    INSERT INTO shop_reporting__log
    VALUES (OLD.id, OLD.accountId, OLD.shopId, OLD.reportType, OLD.date, OLD.value, OLD.isReported, OLD.createdAt,
                OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;
