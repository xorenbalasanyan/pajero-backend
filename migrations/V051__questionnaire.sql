CREATE TABLE questionnaire
(
    id                  INT PRIMARY KEY AUTO_INCREMENT                                      NOT NULL,
    accountId           INT                                                                 NOT NULL,
    title               VARCHAR(200) COLLATE utf8mb4_unicode_ci                             NOT NULL,
    enabled             TINYINT                                                             NOT NULL,
    questionnaireConfig JSON COLLATE utf8mb4_unicode_ci                                     NOT NULL,
    addresseesShopIds   JSON COLLATE utf8mb4_unicode_ci                                     NOT NULL,
    reportConfig        JSON COLLATE utf8mb4_unicode_ci                                     NOT NULL,
    executionConfig     JSON COLLATE utf8mb4_unicode_ci                                     NOT NULL,
    isDeleted           TINYINT   DEFAULT 0                                                 NOT NULL,
    createdAt           TIMESTAMP DEFAULT CURRENT_TIMESTAMP()                               NOT NULL,
    ownerId             INT                                                                 NOT NULL,
    updatedAt           TIMESTAMP DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP() NOT NULL,
    moderatorId         INT                                                                 NULL,
    CONSTRAINT fk__quest__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT fk__quest__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__quest__moderator FOREIGN KEY (moderatorid) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE questionnaire_execution
(
    id              INT PRIMARY KEY AUTO_INCREMENT                                                                                                                 NOT NULL,
    questionnaireId INT                                                                                                                                            NOT NULL,
    status          ENUM ('IDLE', 'SCHEDULED', 'RUNNING', 'STOPPED', 'REPORTED', 'FINISHED', 'CANCELED') DEFAULT 'IDLE'                                            NOT NULL,
    startAt         TIMESTAMP                                                                            DEFAULT '0000-00-00 00:00:00'                             NOT NULL,
    finishAt        TIMESTAMP                                                                            DEFAULT '0000-00-00 00:00:00'                             NOT NULL,
    addresseesCount INT                                                                                                                                            NOT NULL,
    answeredCount   INT                                                                                                                                            NOT NULL,
    createdAt       TIMESTAMP                                                                            DEFAULT CURRENT_TIMESTAMP()                               NOT NULL,
    ownerId         INT                                                                                                                                            NOT NULL,
    updatedAt       TIMESTAMP                                                                            DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP() NOT NULL,
    moderatorId     INT                                                                                                                                            NULL,
    CONSTRAINT fk__questexec__quest FOREIGN KEY (questionnaireId) REFERENCES questionnaire (id) ON DELETE RESTRICT,
    CONSTRAINT fk__questexec__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__questexec__moderator FOREIGN KEY (moderatorid) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE questionnaire_execution_to_shop
(
    id                       INT PRIMARY KEY AUTO_INCREMENT                                                                   NOT NULL,
    questionnaireId          INT                                                                                              NOT NULL,
    questionnaireExecutionId INT                                                                                              NOT NULL,
    shopId                   INT                                                                                              NOT NULL,
    status                   ENUM ('RUNNING', 'FINISHED', 'FAILED', 'CANCELED') DEFAULT 'RUNNING'                             NOT NULL,
    startAt                  TIMESTAMP                                                                                        NOT NULL,
    finishAt                 TIMESTAMP                                                                                        NOT NULL,
    respondingUserId         INT                                                                                              NULL,
    respondingData           JSON COLLATE utf8mb4_unicode_ci                                                                  NULL,
    respondingAt             TIMESTAMP                                                                                        NULL,
    createdAt                TIMESTAMP                              DEFAULT CURRENT_TIMESTAMP()                               NOT NULL,
    ownerId                  INT                                                                                              NOT NULL,
    updatedAt                TIMESTAMP                              DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP() NOT NULL,
    moderatorId              INT                                                                                              NULL,
    CONSTRAINT fk__questexec2shop__quest FOREIGN KEY (questionnaireId) REFERENCES questionnaire (id) ON DELETE RESTRICT,
    CONSTRAINT fk__questexec2shop__questexec FOREIGN KEY (questionnaireExecutionId) REFERENCES questionnaire_execution (id) ON DELETE RESTRICT,
    CONSTRAINT fk__questexec2shop__shop FOREIGN KEY (shopId) REFERENCES shop (id) ON DELETE RESTRICT,
    CONSTRAINT fk__questexec2shop__respuser FOREIGN KEY (respondingUserId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__questexec2shop__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__questexec2shop__moderator FOREIGN KEY (moderatorid) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE questionnaire_history
(
    id                       INT PRIMARY KEY AUTO_INCREMENT        NOT NULL,
    questionnaireId          INT                                   NOT NULL,
    questionnaireExecutionId INT                                   NOT NULL,
    addresseesCount          INT                                   NOT NULL,
    answeredCount            INT                                   NOT NULL,
    finishedAt               TIMESTAMP                             NOT NULL,
    reportData               JSON COLLATE utf8mb4_unicode_ci       NOT NULL,
    reportFileId             INT                                   NOT NULL,
    createdAt                TIMESTAMP DEFAULT CURRENT_TIMESTAMP() NOT NULL,
    ownerId                  INT                                   NOT NULL,
    CONSTRAINT fk__questhistory__quest FOREIGN KEY (questionnaireId) REFERENCES questionnaire (id) ON DELETE RESTRICT,
    CONSTRAINT fk__questhistory__reportfile FOREIGN KEY (reportFileId) REFERENCES file (id) ON DELETE RESTRICT,
    CONSTRAINT fk__questhistory__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT
);

