ALTER TABLE shop_reporting
    MODIFY COLUMN reportType VARCHAR(50) NOT NULL;

ALTER TABLE shop_reporting__log
    MODIFY COLUMN reportType VARCHAR(50) NOT NULL;
