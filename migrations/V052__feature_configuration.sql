CREATE TABLE feature_configuration
(
    id          INT PRIMARY KEY AUTO_INCREMENT                                      NOT NULL,
    accountId   INT                                                                 NOT NULL,
    name        VARCHAR(255) COLLATE utf8mb4_unicode_ci                             NOT NULL,
    title       VARCHAR(255) COLLATE utf8mb4_unicode_ci                             NOT NULL,
    enabledAll  TINYINT   DEFAULT 0                                                 NOT NULL,
    shopIds     JSON                                                                NULL,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()                               NOT NULL,
    ownerId     INT                                                                 NOT NULL,
    updatedAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP() NOT NULL,
    moderatorId INT                                                                 NULL,
    CONSTRAINT fk__feature_configuration__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT fk__feature_configuration_ownerId__user FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__feature_configuration_moderatorId__user FOREIGN KEY (moderatorId) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE feature_configuration__log
(
    id          INT,
    accountId   INT,
    name        VARCHAR(255) COLLATE utf8mb4_unicode_ci,
    title       VARCHAR(255) COLLATE utf8mb4_unicode_ci,
    enabledAll  TINYINT,
    shopIds     JSON,
    createdAt   TIMESTAMP,
    ownerId     INT,
    updatedAt   TIMESTAMP,
    moderatorId INT,
    __type      TINYINT NOT NULL
);

CREATE TRIGGER feature_configuration__after_insert
    AFTER INSERT
    ON feature_configuration
    FOR EACH ROW
BEGIN
    INSERT INTO feature_configuration__log
    VALUES (NEW.id, NEW.accountId, NEW.name, NEW.title, NEW.enabledAll, NEW.shopIds, NEW.createdAt,
            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER feature_configuration__after_update
    AFTER UPDATE
    ON feature_configuration
    FOR EACH ROW
BEGIN
    INSERT INTO feature_configuration__log
    VALUES (NEW.id, NEW.accountId, NEW.name, NEW.title, NEW.enabledAll, NEW.shopIds, NEW.createdAt,
            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER feature_configuration__before_delete
    BEFORE DELETE
    ON feature_configuration
    FOR EACH ROW
BEGIN
    INSERT INTO feature_configuration__log
    VALUES (OLD.id, OLD.accountId, OLD.name, OLD.title, OLD.enabledAll, OLD.shopIds, OLD.createdAt,
            OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;
