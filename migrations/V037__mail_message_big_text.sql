ALTER TABLE mail_message
    MODIFY text MEDIUMTEXT COLLATE utf8mb4_unicode_ci NOT NULL;

ALTER TABLE mail_message_log
    MODIFY text MEDIUMTEXT COLLATE utf8mb4_unicode_ci NULL;
