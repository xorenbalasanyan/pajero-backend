CREATE TABLE checklist_task
(
    id                      INT PRIMARY KEY AUTO_INCREMENT                                          NOT NULL,
    accountId               INT                                                                     NOT NULL,
    shopId                  INT                                                                     NOT NULL,
    taskId                  VARCHAR(255)                                                            NOT NULL,
    status                  ENUM('PLANNED', 'STARTED', 'COMPLETED')                                 NOT NULL,
    taskCreatedAt           TIMESTAMP                                                               NOT NULL,
    departmentName          VARCHAR(255)                                                            NULL,
    checkListName           VARCHAR(255)                                                            NULL,
    checkListProcessName    VARCHAR(255)                                                            NULL,
    checkListQuestion       TEXT                                                                    NOT NULL,
    creatorComment          TEXT                                                                    NULL,
    questionPhotos          JSON                                                                    NULL,
    isPhotoRequired         TINYINT DEFAULT 0                                                       NULL,
    isCommentRequired       TINYINT DEFAULT 0                                                       NULL,
    taskDeadlineAt          TIMESTAMP                                                               NOT NULL,
    creatorFullname         VARCHAR(255)                                                            NOT NULL,
    creatorPhone            VARCHAR(255)                                                            NULL,
    isNeedUpdateMedia       TINYINT                                                                 NOT NULL,
    taskClosedAt            TIMESTAMP                                                               NULL,
    executorUserId          INT                                                                     NULL,
    executorShopId          INT                                                                     NULL,
    executorComment         TEXT                                                                    NULL,
    executorPhotoIds        JSON                                                                    NULL,
    isExpired               TINYINT                                                                 NULL,
    isReportSent            TINYINT                                                                 NULL,
    createdAt               TIMESTAMP DEFAULT CURRENT_TIMESTAMP()                                   NOT NULL,
    ownerId                 INT                                                                     NOT NULL,
    updatedAt               TIMESTAMP DEFAULT CURRENT_TIMESTAMP() ON UPDATE  CURRENT_TIMESTAMP()    NOT NULL,
    moderatorId             INT                                                                     NULL,

    CONSTRAINT fk__checklist_task__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT fk__checklist_task__shop FOREIGN KEY (shopId) REFERENCES shop (id) ON DELETE RESTRICT,
    CONSTRAINT fk__checklist_task__executor_user FOREIGN KEY (executorUserId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__checklist_task__executor_shop FOREIGN KEY (executorShopId) REFERENCES shop (id) ON DELETE RESTRICT,
    CONSTRAINT fk__checklist_task__owner_user FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__checklist_task__moderator_user FOREIGN KEY (moderatorId) REFERENCES user (id) ON DELETE RESTRICT
);

CREATE TABLE checklist_task__log
(
    id                      INT,
    accountId               INT,
    shopId                  INT,
    taskId                  VARCHAR(255),
    status                  ENUM('PLANNED', 'STARTED', 'COMPLETED'),
    taskCreatedAt           TIMESTAMP,
    departmentName          VARCHAR(255),
    checkListName           VARCHAR(255),
    checkListProcessName    VARCHAR(255),
    checkListQuestion       TEXT,
    creatorComment          TEXT,
    questionPhotos          JSON,
    isPhotoRequired         TINYINT,
    isCommentRequired       TINYINT,
    taskDeadlineAt          TIMESTAMP,
    creatorFullName         VARCHAR(255),
    creatorPhone            VARCHAR(255),
    isNeedUpdateMedia       TINYINT,
    taskClosedAt            TIMESTAMP,
    executorUserId          INT,
    executorShopId          INT,
    executorComment         TEXT,
    executorPhotoIds        JSON,
    isExpired               TINYINT,
    isReportSent            TINYINT,
    createdAt               TIMESTAMP,
    ownerId                 INT,
    updatedAt               TIMESTAMP,
    moderatorId             INT,
    __type                  TINYINT NOT NULL
);

CREATE TRIGGER checklist_task__after_insert
    AFTER INSERT
    ON checklist_task
    FOR EACH ROW
BEGIN
    INSERT INTO checklist_task__log
    VALUES (NEW.id, NEW.accountId, NEW.shopId, NEW.taskId, NEW.status, NEW.taskCreatedAt, NEW.departmentName,
            NEW.checkListName, NEW.checkListProcessName, NEW.checkListQuestion, NEW.creatorComment, NEW.questionPhotos,
            NEW.isPhotoRequired, NEW.isCommentRequired, NEW.taskDeadlineAt, NEW.creatorFullName, NEW.creatorPhone,
            NEW.isNeedUpdateMedia, NEW.taskClosedAt, NEW.executorUserId, NEW.executorShopId, NEW.executorComment,
            NEW.executorPhotoIds, NEW.isExpired, NEW.isReportSent, NEW.createdAt, NEW.ownerId, NEW.updatedAt,
            NEW.moderatorId, 1);
END;

CREATE TRIGGER checklist_task__after_update
    AFTER UPDATE
    ON checklist_task
    FOR EACH ROW
BEGIN
    INSERT INTO checklist_task__log
    VALUES (NEW.id, NEW.accountId, NEW.shopId, NEW.taskId, NEW.status, NEW.taskCreatedAt, NEW.departmentName,
            NEW.checkListName, NEW.checkListProcessName, NEW.checkListQuestion, NEW.creatorComment, NEW.questionPhotos,
            NEW.isPhotoRequired, NEW.isCommentRequired, NEW.taskDeadlineAt, NEW.creatorFullName, NEW.creatorPhone,
            NEW.isNeedUpdateMedia, NEW.taskClosedAt, NEW.executorUserId, NEW.executorShopId, NEW.executorComment,
            NEW.executorPhotoIds, NEW.isExpired, NEW.isReportSent, NEW.createdAt, NEW.ownerId, NEW.updatedAt,
            NEW.moderatorId, 2);
END;

CREATE TRIGGER checklist_task__before_delete
    BEFORE DELETE
    ON checklist_task
    FOR EACH ROW
BEGIN
    INSERT INTO checklist_task__log
    VALUES (OLD.id, OLD.accountId, OLD.shopId, OLD.taskId, OLD.status, OLD.taskCreatedAt, OLD.departmentName,
            OLD.checkListName, OLD.checkListProcessName, OLD.checkListQuestion, OLD.creatorComment, OLD.questionPhotos,
            OLD.isPhotoRequired, OLD.isCommentRequired, OLD.taskDeadlineAt, OLD.creatorFullName, OLD.creatorPhone,
            OLD.isNeedUpdateMedia, OLD.taskClosedAt, OLD.executorUserId, OLD.executorShopId, OLD.executorComment,
            OLD.executorPhotoIds, OLD.isExpired, OLD.isReportSent, OLD.createdAt, OLD.ownerId, OLD.updatedAt,
            OLD.moderatorId, 3);
END;
