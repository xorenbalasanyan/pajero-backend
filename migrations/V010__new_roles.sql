
-- поменялся список ролей:
-- роль FILIAL заменить на DF, роль SHOP -> DM
-- добавить роль SUPPORT, DT, DT и ZDM

-- удаляем триггеры для таблицы user
DROP TRIGGER user_after_insert;
DROP TRIGGER user_after_update;
DROP TRIGGER user_before_delete;

-- добавляем новые роли
ALTER TABLE user
    MODIFY role ENUM ('ADMIN', 'ROBOT', 'SUPPORT', 'DT', 'DF', 'TM', 'UPF', 'DM', 'ZDM', 'FILIAL', 'SHOP') NOT NULL,
    ADD COLUMN isDeleted TINYINT DEFAULT 0 NOT NULL AFTER comment;

ALTER TABLE user_log
    MODIFY role ENUM ('ADMIN', 'ROBOT', 'SUPPORT', 'DT', 'DF', 'TM', 'UPF', 'DM', 'ZDM', 'FILIAL', 'SHOP') NOT NULL,
    ADD COLUMN isDeleted TINYINT DEFAULT 0 NOT NULL AFTER comment;

-- обновляем старые роли на новыые
UPDATE user
SET role = 'DF'
WHERE role = 'FILIAL';

UPDATE user
SET role = 'DM'
WHERE role = 'SHOP';

UPDATE user_log
SET role = 'DF'
WHERE role = 'FILIAL';

UPDATE user_log
SET role = 'DM'
WHERE role = 'SHOP';

-- скидываем индексы
UPDATE user
SET tmId  = NULL,
    upfId = NULL
WHERE tmId IS NOT NULL
   OR upfId IS NOT NULL;

-- перекидываем флаг
UPDATE user
SET isDeleted=1
WHERE isDisabled = 1;

-- меняем поля
ALTER TABLE user
    DROP CONSTRAINT user_filial_fk,
    DROP CONSTRAINT user_tm_fk,
    DROP CONSTRAINT user_upf_fk;

ALTER TABLE user
    MODIFY role ENUM ('ADMIN', 'ROBOT', 'SUPPORT', 'DT', 'DF', 'TM', 'UPF', 'DM', 'ZDM') NOT NULL,
    DROP COLUMN filialId,
    DROP COLUMN isDisabled,
    ADD COLUMN dfId INT AFTER accountId,
    ADD CONSTRAINT user_dfId__user_id__fk FOREIGN KEY (dfId) REFERENCES user (id) ON DELETE RESTRICT,
    ADD CONSTRAINT user_tmId__user_id__fk FOREIGN KEY (tmId) REFERENCES user (id) ON DELETE RESTRICT,
    ADD CONSTRAINT user_upfId__user_id__fk FOREIGN KEY (upfId) REFERENCES user (id) ON DELETE RESTRICT;

ALTER TABLE user_log
    MODIFY role ENUM ('ADMIN', 'ROBOT', 'SUPPORT', 'DT', 'DF', 'TM', 'UPF', 'DM', 'ZDM') NOT NULL,
    DROP COLUMN filialId,
    DROP COLUMN isDisabled,
    ADD COLUMN dfId INT AFTER accountId;

-- создаем триггеры
CREATE TRIGGER user_after_insert
    AFTER INSERT
    ON user
    FOR EACH ROW
BEGIN
    INSERT INTO user_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.dfId, NEW.tmId, NEW.upfId, NEW.shopId, NEW.role, NEW.username,
            NEW.passwordHash, NEW.key, NEW.firstName, NEW.lastName, NEW.middleName, NEW.email, NEW.phone, NEW.skype,
            NEW.telegram, NEW.comment, NEW.isDeleted, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER user_after_update
    AFTER UPDATE
    ON user
    FOR EACH ROW
BEGIN
    INSERT INTO user_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.dfId, NEW.tmId, NEW.upfId, NEW.shopId, NEW.role, NEW.username,
            NEW.passwordHash, NEW.key, NEW.firstName, NEW.lastName, NEW.middleName, NEW.email, NEW.phone, NEW.skype,
            NEW.telegram, NEW.comment, NEW.isDeleted, NEW.createdAt, NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER user_before_delete
    BEFORE DELETE
    ON user
    FOR EACH ROW
BEGIN
    INSERT INTO user_log
    VALUES (OLD.id, OLD.externalId, OLD.accountId, OLD.dfId, OLD.tmId, OLD.upfId, OLD.shopId, OLD.role, OLD.username,
            OLD.passwordHash, OLD.key, OLD.firstName, OLD.lastName, OLD.middleName, OLD.email, OLD.phone, OLD.skype,
            OLD.telegram, OLD.comment, OLD.isDeleted, OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- удаляем триггеры для таблицы shop
DROP TRIGGER shop_after_insert;
DROP TRIGGER shop_after_update;
DROP TRIGGER shop_before_delete;

-- скидываем индексы
UPDATE shop
SET tmId  = NULL,
    upfId = NULL
WHERE tmId IS NOT NULL
   OR upfId IS NOT NULL;

-- меняем поля
ALTER TABLE shop
    DROP CONSTRAINT shop_filial_fk,
    DROP CONSTRAINT shop_tm_fk,
    DROP CONSTRAINT shop_upf_fk;

ALTER TABLE shop
    DROP COLUMN filialId,
    ADD COLUMN dfId      INT AFTER accountId,
    ADD COLUMN region    VARCHAR(100) COLLATE utf8mb4_unicode_ci AFTER name,
    ADD COLUMN isDeleted TINYINT DEFAULT 0 NOT NULL AFTER timeOffset,
    ADD CONSTRAINT shop_dfId__user_id__fk FOREIGN KEY (dfId) REFERENCES user (id) ON DELETE RESTRICT,
    ADD CONSTRAINT shop_tmId__user_id__fk FOREIGN KEY (tmId) REFERENCES user (id) ON DELETE RESTRICT,
    ADD CONSTRAINT shop_upfId__user_id__fk FOREIGN KEY (upfId) REFERENCES user (id) ON DELETE RESTRICT;

ALTER TABLE shop_log
    DROP COLUMN filialId,
    ADD COLUMN dfId      INT AFTER accountId,
    ADD COLUMN region    VARCHAR(100) COLLATE utf8mb4_unicode_ci AFTER name,
    ADD COLUMN isDeleted TINYINT NOT NULL AFTER timeOffset;

-- создаем триггеры
CREATE TRIGGER shop_after_insert
    AFTER INSERT
    ON shop
    FOR EACH ROW
BEGIN
    INSERT INTO shop_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.dfId, NEW.tmId, NEW.upfId, NEW.name, NEW.region, NEW.city,
            NEW.address, NEW.phone, NEW.email, NEW.description, NEW.timeOffset, NEW.isDeleted, NEW.createdAt,
            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER shop_after_update
    AFTER UPDATE
    ON shop
    FOR EACH ROW
BEGIN
    INSERT INTO shop_log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.dfId, NEW.tmId, NEW.upfId, NEW.name, NEW.region, NEW.city,
            NEW.address, NEW.phone, NEW.email, NEW.description, NEW.timeOffset, NEW.isDeleted, NEW.createdAt,
            NEW.ownerId, NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER shop_before_delete
    BEFORE DELETE
    ON shop
    FOR EACH ROW
BEGIN
    INSERT INTO shop_log
    VALUES (OLD.id, OLD.externalId, OLD.accountId, OLD.dfId, OLD.tmId, OLD.upfId, OLD.name, OLD.region, OLD.city,
            OLD.address, OLD.phone, OLD.email, OLD.description, OLD.timeOffset, OLD.isDeleted, OLD.createdAt,
            OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- дропаем лишние таблицы
DROP TABLE upf;
DROP TABLE upf_log;
DROP TABLE tm;
DROP TABLE tm_log;
DROP TABLE filial;
DROP TABLE filial_log;
