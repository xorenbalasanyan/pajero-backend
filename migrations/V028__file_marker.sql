-- TABLE file_marker

CREATE TABLE file_marker
(
    id        INT AUTO_INCREMENT PRIMARY KEY,
    fileId    INT                                     NOT NULL,
    marker    ENUM ('BAD', 'MATCH', 'LATER')          NOT NULL,
    createdAt TIMESTAMP                               NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    updatedAt TIMESTAMP ON UPDATE CURRENT_TIMESTAMP() NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    CONSTRAINT fk__file_marker__file FOREIGN KEY (fileId) REFERENCES file (id) ON DELETE RESTRICT
);

CREATE TABLE file_marker__log
(
    id        INT,
    fileId    INT,
    marker    ENUM ('BAD', 'MATCH', 'LATER'),
    createdAt TIMESTAMP,
    updatedAt TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(),
    __type    TINYINT NOT NULL
);

CREATE TRIGGER file_marker__after_insert
    AFTER INSERT
    ON file_marker
    FOR EACH ROW
BEGIN
    INSERT INTO file_marker__log
    VALUES (NEW.id, NEW.fileId, NEW.marker, NEW.createdAt, NEW.updatedAt, 1);
END;

CREATE TRIGGER file_marker__after_update
    AFTER UPDATE
    ON file_marker
    FOR EACH ROW
BEGIN
    INSERT INTO file_marker__log
    VALUES (NEW.id, NEW.fileId, NEW.marker, NEW.createdAt, NEW.updatedAt, 2);
END;

CREATE TRIGGER file_marker__before_delete
    BEFORE DELETE
    ON file_marker
    FOR EACH ROW
BEGIN
    INSERT INTO file_marker__log
    VALUES (OLD.id, OLD.fileId, OLD.marker, OLD.createdAt, OLD.updatedAt, 3);
END;
