-- TABLE incident_type

DROP TRIGGER incident_type_after_insert;
DROP TRIGGER incident_type_after_update;
DROP TRIGGER incident_type_before_delete;

ALTER TABLE incident_type
    ADD COLUMN version INT NOT NULL DEFAULT 1 AFTER type,
    ADD UNIQUE (type, version);

ALTER TABLE incident_type_log
    ADD COLUMN version INT NOT NULL AFTER type;

CREATE TRIGGER incident_type_after_insert
    AFTER INSERT
    ON incident_type
    FOR EACH ROW
BEGIN
    INSERT INTO incident_type_log
    VALUES (NEW.id, NEW.accountId, NEW.name, NEW.type, NEW.version, NEW.data, NEW.createdAt, NEW.ownerId,
            NEW.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER incident_type_after_update
    AFTER UPDATE
    ON incident_type
    FOR EACH ROW
BEGIN
    INSERT INTO incident_type_log
    VALUES (NEW.id, NEW.accountId, NEW.name, NEW.type, NEW.version, NEW.data, NEW.createdAt, NEW.ownerId,
            NEW.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER incident_type_before_delete
    BEFORE DELETE
    ON incident_type
    FOR EACH ROW
BEGIN
    INSERT INTO incident_type_log
    VALUES (OLD.id, OLD.accountId, OLD.name, OLD.type, OLD.version, OLD.data, OLD.createdAt, OLD.ownerId,
            OLD.updatedAt, OLD.moderatorId, 3);
END;

-- TABLE incident

DROP TRIGGER incident_after_insert;
DROP TRIGGER incident_after_update;
DROP TRIGGER incident_before_delete;

ALTER TABLE incident
    ADD COLUMN incidentTypeVersion INT NOT NULL DEFAULT 1 AFTER incidentTypeId;

ALTER TABLE incident_log
    ADD COLUMN incidentTypeVersion INT NOT NULL AFTER incidentTypeId;

CREATE TRIGGER incident_after_insert
    AFTER INSERT
    ON incident
    FOR EACH ROW
BEGIN
    INSERT INTO incident_log
    VALUES (NEW.id, NEW.externalId, NEW.shopId, NEW.incidentTypeId, NEW.incidentTypeVersion, NEW.goodId, NEW.date,
            NEW.state, NEW.inputData, NEW.report, NEW.isReported, NEW.createdAt, NEW.ownerId, NEW.updatedAt,
            NEW.moderatorId, 1);
END;

CREATE TRIGGER incident_after_update
    AFTER UPDATE
    ON incident
    FOR EACH ROW
BEGIN
    INSERT INTO incident_log
    VALUES (NEW.id, NEW.externalId, NEW.shopId, NEW.incidentTypeId, NEW.incidentTypeVersion, NEW.goodId, NEW.date,
            NEW.state, NEW.inputData, NEW.report, NEW.isReported, NEW.createdAt, NEW.ownerId, NEW.updatedAt,
            NEW.moderatorId, 2);
END;

CREATE TRIGGER incident_before_delete
    BEFORE DELETE
    ON incident
    FOR EACH ROW
BEGIN
    INSERT INTO incident_log
    VALUES (OLD.id, OLD.externalId, OLD.shopId, OLD.incidentTypeId, OLD.incidentTypeVersion, OLD.goodId, OLD.date,
            OLD.state, OLD.inputData, OLD.report, OLD.isReported, OLD.createdAt, OLD.ownerId, OLD.updatedAt,
            OLD.moderatorId, 3);
END;
