CREATE TABLE checklist_task_import
(
    id                INT AUTO_INCREMENT PRIMARY KEY,
    accountId         INT       NOT NULL,
    fileId            INT       NULL,
    logId             INT       NULL,
    status            ENUM ('OK', 'WARN', 'ERROR') DEFAULT 'OK' NOT NULL,
    createdAt         TIMESTAMP NOT NULL           DEFAULT CURRENT_TIMESTAMP(),
    ownerId           INT       NOT NULL,
    CONSTRAINT fk__checklist_tasks_import__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT,
    CONSTRAINT fk__checklist_tasks_import__file    FOREIGN KEY (fileId)    REFERENCES file (id)    ON DELETE RESTRICT,
    CONSTRAINT fk__checklist_tasks_import__log     FOREIGN KEY (logId)     REFERENCES log (id)     ON DELETE RESTRICT,
    CONSTRAINT fk__checklist_tasks_import__owner   FOREIGN KEY (ownerId)   REFERENCES user (id)    ON DELETE RESTRICT
);
