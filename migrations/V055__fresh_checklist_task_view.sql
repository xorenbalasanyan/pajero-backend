CREATE VIEW fresh_checklist_task_view AS
SELECT *
FROM checklist_task ct
WHERE (DATE_ADD(ct.updatedAt, INTERVAL ct.executionPeriodDays + 7 DAY) > CURRENT_DATE);
