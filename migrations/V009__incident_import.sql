-- update incident_import
DROP TRIGGER incident_import_after_insert;
DROP TRIGGER incident_import_after_update;
DROP TRIGGER incident_import_before_delete;

ALTER TABLE incident_import DROP COLUMN type;
ALTER TABLE incident_import ADD COLUMN incidentTypeId INT NOT NULL AFTER `date`;
ALTER TABLE incident_import_log DROP COLUMN type;
ALTER TABLE incident_import_log ADD COLUMN incidentTypeId INT NOT NULL AFTER `date`;

UPDATE incident_import SET incidentTypeId = 200 WHERE accountId = 100;

ALTER TABLE incident_import
    ADD CONSTRAINT incident_import__incident_type__fk FOREIGN KEY (incidentTypeId) REFERENCES incident_type (id) ON DELETE RESTRICT;

CREATE TRIGGER incident_import_after_insert
    AFTER INSERT
    ON incident_import
    FOR EACH ROW
BEGIN
    INSERT INTO incident_import_log
    VALUES (NEW.id, NEW.accountId, NEW.fileId, NEW.logId, NEW.date, NEW.incidentTypeId, NEW.state, NEW.addedCount, NEW.warnCount,
            NEW.createdAt, NEW.ownerId, NEw.updatedAt, NEW.moderatorId, 1);
END;

CREATE TRIGGER incident_import_after_update
    AFTER UPDATE
    ON incident_import
    FOR EACH ROW
BEGIN
    INSERT INTO incident_import_log
    VALUES (NEW.id, NEW.accountId, NEW.fileId, NEW.logId, NEW.date, NEW.incidentTypeId, NEW.state, NEW.addedCount, NEW.warnCount,
            NEW.createdAt, NEW.ownerId, NEw.updatedAt, NEW.moderatorId, 2);
END;

CREATE TRIGGER incident_import_before_delete
    BEFORE DELETE
    ON incident_import
    FOR EACH ROW
BEGIN
    INSERT INTO incident_import_log
    VALUES (OLD.id, OLD.accountId, OLD.fileId, OLD.logId, OLD.date, OLD.incidentTypeId, OLD.state, OLD.addedCount, OLD.warnCount,
            OLD.createdAt, OLD.ownerId, OLD.updatedAt, OLD.moderatorId, 3);
END;

-- update index in incident
ALTER TABLE incident
    DROP INDEX incident_unique_index,
    ADD UNIQUE incident_unique_index (shopId, goodId, date, incidentTypeId);
