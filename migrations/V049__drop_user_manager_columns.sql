DROP TRIGGER IF EXISTS user_after_insert;
DROP TRIGGER IF EXISTS user_after_update;
DROP TRIGGER IF EXISTS user_before_delete;

-- drop migrated fields and improve role enum
ALTER TABLE user
    DROP FOREIGN KEY user_dfId__user_id__fk,
    DROP COLUMN dfId,
    DROP FOREIGN KEY user_tmId__user_id__fk,
    DROP COLUMN tmId,
    DROP FOREIGN KEY user_upfId__user_id__fk,
    DROP COLUMN upfId,
    DROP FOREIGN KEY user_shop_fk,
    DROP COLUMN shopId,
    MODIFY role ENUM ('ADMIN', 'ROBOT', 'SUPPORT', 'DT', 'DF', 'TM', 'UPF', 'DM', 'ZDM', 'CHIEF', 'MANAGER') NOT NULL;

RENAME TABLE user_log TO user__log;

ALTER TABLE user__log
    DROP COLUMN dfId,
    DROP COLUMN tmId,
    DROP COLUMN upfId,
    DROP COLUMN shopId,
    MODIFY role VARCHAR(20) NULL;

-- recreate triggers
CREATE TRIGGER user__after_insert
    AFTER INSERT
    ON user
    FOR EACH ROW
BEGIN
    INSERT INTO user__log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.role, NEW.username, NEW.passwordHash,
            NEW.key, NEW.firstName, NEW.lastName, NEW.middleName, NEW.email, NEW.phone, NEW.skype,
            NEW.telegram, NEW.comment, NEW.isDeleted, NEW.createdAt, NEW.ownerId, NEW.updatedAt,
            NEW.moderatorId, 1);
END;

CREATE TRIGGER user__after_update
    AFTER UPDATE
    ON user
    FOR EACH ROW
BEGIN
    INSERT INTO user__log
    VALUES (NEW.id, NEW.externalId, NEW.accountId, NEW.role, NEW.username, NEW.passwordHash,
            NEW.key, NEW.firstName, NEW.lastName, NEW.middleName, NEW.email, NEW.phone, NEW.skype,
            NEW.telegram, NEW.comment, NEW.isDeleted, NEW.createdAt, NEW.ownerId, NEW.updatedAt,
            NEW.moderatorId, 2);
END;

CREATE TRIGGER user__before_delete
    BEFORE DELETE
    ON user
    FOR EACH ROW
BEGIN
    INSERT INTO user__log
    VALUES (OLD.id, OLD.externalId, OLD.accountId, OLD.role, OLD.username, OLD.passwordHash,
            OLD.key, OLD.firstName, OLD.lastName, OLD.middleName, OLD.email, OLD.phone, OLD.skype,
            OLD.telegram, OLD.comment, OLD.isDeleted, OLD.createdAt, OLD.ownerId, OLD.updatedAt,
            OLD.moderatorId, 3);
END;

-- update roles for exist users
UPDATE user
SET role='MANAGER'
WHERE role IN ('DF', 'TM', 'UPF', 'DM', 'ZDM');

UPDATE user
SET role='CHIEF'
WHERE role IN ('DT');

-- update role enum
ALTER TABLE user
    MODIFY role ENUM ('ADMIN', 'ROBOT', 'SUPPORT', 'CHIEF', 'MANAGER') NOT NULL;
