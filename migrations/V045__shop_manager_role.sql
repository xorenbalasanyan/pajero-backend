-- shop_manager_role
CREATE TABLE shop_manager_role
(
    id            INT PRIMARY KEY AUTO_INCREMENT,
    accountId     INT                                                                 NOT NULL,
    title         VARCHAR(100) COLLATE utf8mb4_unicode_ci                             NOT NULL,
    shortTitle    VARCHAR(10) COLLATE utf8mb4_unicode_ci                              NOT NULL,
    engShortTitle VARCHAR(10)                                                         NOT NULL,
    subLevel      INT                                                                 NOT NULL,
    isShopUser    TINYINT                                                             NOT NULL,
    createdAt     TIMESTAMP DEFAULT CURRENT_TIMESTAMP()                               NOT NULL,
    ownerId       INT                                                                 NOT NULL,
    updatedAt     TIMESTAMP DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP() NOT NULL,
    moderatorId   INT                                                                 NULL,
    CONSTRAINT fk__shop_manager_role__account FOREIGN KEY (accountId) REFERENCES account (id) ON DELETE RESTRICT
);
