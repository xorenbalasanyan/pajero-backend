-- table: link shop to role and user
CREATE TABLE shop_manager
(
    id          INT PRIMARY KEY AUTO_INCREMENT,
    shopId      INT                                                                 NOT NULL,
    roleId      INT                                                                 NOT NULL,
    userId      INT                                                                 NOT NULL,
    createdAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP()                               NOT NULL,
    ownerId     INT                                                                 NOT NULL,
    updatedAt   TIMESTAMP DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP() NOT NULL,
    moderatorId INT                                                                 NULL,
    CONSTRAINT fk__shop_manager__shop FOREIGN KEY (shopId) REFERENCES shop (id) ON DELETE RESTRICT,
    CONSTRAINT fk__shop_manager__role FOREIGN KEY (roleId) REFERENCES shop_manager_role (id) ON DELETE RESTRICT,
    CONSTRAINT fk__shop_manager__user FOREIGN KEY (userId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__shop_manager__owner FOREIGN KEY (ownerId) REFERENCES user (id) ON DELETE RESTRICT,
    CONSTRAINT fk__shop_manager__moderator FOREIGN KEY (moderatorId) REFERENCES user (id) ON DELETE RESTRICT
);
