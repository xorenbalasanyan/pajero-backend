DROP TRIGGER incident_import_after_insert;
DROP TRIGGER incident_import_after_update;
DROP TRIGGER incident_import_before_delete;

DROP TABLE incident_import_log;

ALTER TABLE incident_import
    DROP COLUMN updatedAt,
    DROP COLUMN moderatorId;
