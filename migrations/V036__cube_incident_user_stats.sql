CREATE TABLE cube_incident_shop_stats
(
    id             INT AUTO_INCREMENT PRIMARY KEY,
    accountId      INT UNSIGNED         NOT NULL,
    managerId      INT UNSIGNED         NULL,
    shopId         INT UNSIGNED         NULL,
    incidentTypeId INT UNSIGNED         NULL,
    state          ENUM ('TODO','DONE') NULL,
    completed      INT UNSIGNED         NOT NULL,
    allCount       INT UNSIGNED         NOT NULL,
    createdAt      TIMESTAMP            NOT NULL DEFAULT CURRENT_TIMESTAMP()
);

CREATE TABLE cube_incident_user_stats
(
    id             INT AUTO_INCREMENT PRIMARY KEY,
    accountId      INT UNSIGNED         NOT NULL,
    managerId      INT UNSIGNED         NULL,
    userId         INT UNSIGNED         NULL,
    incidentTypeId INT UNSIGNED         NULL,
    state          ENUM ('TODO','DONE') NULL,
    completed      INT UNSIGNED         NOT NULL,
    allCount       INT UNSIGNED         NOT NULL,
    createdAt      TIMESTAMP            NOT NULL DEFAULT CURRENT_TIMESTAMP()
);
