DROP TRIGGER IF EXISTS violation_task_to_shop__after_insert;
DROP TRIGGER IF EXISTS violation_task_to_shop__after_update;
DROP TRIGGER IF EXISTS violation_task_to_shop__before_delete;
DROP TABLE violation_task_to_shop__log;

DROP TABLE violation_task_to_shop;

DROP TRIGGER IF EXISTS violation_task__after_insert;
DROP TRIGGER IF EXISTS violation_task__after_update;
DROP TRIGGER IF EXISTS violation_task__before_delete;
DROP TABLE violation_task__log;

DROP TABLE violation_task;

DROP VIEW fresh_violation_task_view;
