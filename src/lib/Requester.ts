import https, { RequestOptions } from 'https';
import { IncomingMessage, OutgoingHttpHeaders } from 'http';

type CallCallback = (error: string | Error | null, traceId: string, response?: IncomingMessage, responseBody?: Buffer) => void;

export default class HttpsRequester
{
	private readonly hostname: string;
	private readonly port: number;

	constructor(hostname: string, port: number = 443) {
		this.hostname = hostname;
		this.port = port;
	}

	/**
	 * Универсальный метод отправки запроса.
	 * @param method - GET, POST, PUT etc
	 * @param path - url
	 * @param headers
	 * @param body
	 * @param callback
	 */
	call = (method: string, path: string, headers: OutgoingHttpHeaders, body: any, callback: CallCallback) => {
		const traceId = Math.random().toString(36).substr(2, 5);

		const options: RequestOptions = {
			hostname: this.hostname,
			port: this.port,
			method,
			path,
			headers,
		};

		const request = https.request(options, (response: IncomingMessage) => {
			const buffers: Buffer[] = [];
			response.on('data', (data: Buffer) => buffers.push(data));
			response.on('end', () => {
				callback(null, traceId, response, Buffer.concat(buffers));
			});
		});

		request.on('error', error => {
			callback(error, traceId);
		});

		if (body) {
			request.write(body);
		}

		request.end();
	}

	/**
	 * GET запрос
	 * @param path
	 * @param headers
	 * @param callback
	 */
	get = (path: string, headers: OutgoingHttpHeaders, callback: CallCallback) => {
		return this.call('GET', path, headers, null, callback);
	}

	/**
	 * POST запрос
	 * @param path
	 * @param headers
	 * @param body
	 * @param callback
	 */
	post = (path: string, headers: OutgoingHttpHeaders, body: any, callback: CallCallback) => {
		return this.call('POST', path, headers, body, callback);
	}

	/**
	 * POST запрос с телом JSON
	 * @param path
	 * @param headers
	 * @param data
	 * @param callback
	 */
	postJson = (path: string, headers: OutgoingHttpHeaders | undefined, data: object, callback: CallCallback) => {
		const body = JSON.stringify(data);
		if (!headers) {
			headers = {};
		}
		headers['Content-Type'] = 'application/json';
		headers['Content-Length'] = body.length;
		return this.call('POST', path, headers, body, callback);
	}

	/**
	 * PUT запрос
	 * @param path
	 * @param headers
	 * @param body
	 * @param callback
	 */
	put = (path: string, headers: OutgoingHttpHeaders, body: any, callback: CallCallback) => {
		return this.call('PUT', path, headers, body, callback);
	}

	/**
	 * DELETE запрос
	 * @param path
	 * @param headers
	 * @param callback
	 */
	delete = (path: string, headers: OutgoingHttpHeaders, callback: CallCallback) => {
		return this.call('DELETE', path, headers, null, callback);
	}
}
