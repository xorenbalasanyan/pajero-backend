import fs from 'fs';
import path from 'path';
import { Sequelize } from 'sequelize-typescript';
import { QueryTypes, Transaction } from 'sequelize';
import { AppLogger } from '~lib/AppLogger';
import { initSequelize } from '~lib/sequelize';
import { fillError } from '~utils/errors';

const MIGRATIONS_TABLE_NAME = 'migration_schema';

export default class MigrationApplier
{
	private readonly logger = new AppLogger(MigrationApplier.name);
	private readonly db: Sequelize;

	constructor() {
		// создает новое подключение к базе, которое позвояет выполнять скрипты
		this.db = initSequelize({
			logging: (msg: string, time: number, ...obj: any[]) => {
				this.logger.debug(`(time: ${time}ms) ${msg}`);
			},
			logQueryParameters: true, // print query replacements
			benchmark: true, // в logging вернет время выполнения запроса в ms
			dialectOptions: {
				multipleStatements: true,
			},
		});
	}

	private async select(sqlQuery: string): Promise<any[]> {
		return await this.db.query(sqlQuery, {
			type: QueryTypes.SELECT,
			plain: false,
		});
	}

	private async query(sqlQuery: string, transaction?: Transaction) {
		return this.db.query(sqlQuery, {
			type: QueryTypes.RAW,
			raw: true,
			transaction,
		});
	}

	/**
	 * Проверяет какие миграции уже были применены в БД.
	 * Если таблица миграций не существует, то создает ее.
	 */
	private findAppliedMigrationSet = async (): Promise<Set<string>> => {
		// проверяем наличие таблицы миграций
		const tableNames = await this.select(`SELECT table_name
                                              FROM information_schema.tables
                                              WHERE table_type = 'BASE TABLE'
                                                and table_name = '${MIGRATIONS_TABLE_NAME}'`);
		if (!tableNames.length) {
			// таблица не существует, создает таблицу для истории миграций
			await this.query(`
                CREATE TABLE migration_schema
                (
                    id        INT AUTO_INCREMENT PRIMARY KEY,
                    name      VARCHAR(100)                          NOT NULL,
                    createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP() NOT NULL,
                    CONSTRAINT ${MIGRATIONS_TABLE_NAME}__name__key UNIQUE (name)
                );`)
				.catch(error1 => {
					this.logger.error(fillError(Error, '[Ошибка создания таблицы migration_schema]', error1));
					process.exit(1);
				});
			// если соединения с бд нет, то вылетит из приложения с ошибкой
			this.logger.info('Создана таблица хранения истории миграций');
			return new Set();
		} else {
			// таблица миграция существует, вытаскиваем список ранее выполненных миграций
			const migrations = await this.select(`SELECT name
                                                  FROM ${MIGRATIONS_TABLE_NAME}`);
			return new Set(migrations.map(m => m.name));
		}
	}

	/**
	 * Выбирает файлы версионных миграций, которых еще нет в таблице миграций
	 * @param migrationSet
	 */
	private findAndFilterVersionMigrationFiles = (migrationSet: Set<string>): string[] => fs
		.readdirSync('./migrations')
		.filter(Boolean)
		.filter(fn => fn[0] === 'V')
		.filter(fn => !migrationSet.has(fn))

	/**
	 * Выбирает файлы тестовых миграций, которых еще нет в таблице миграций
	 * @param migrationSet
	 */
	findAndFilterTestMigrationFiles(migrationSet: Set<string>): string[] {
		return fs.readdirSync('./migrations')
			.filter(Boolean)
			.filter(fn => fn[0] === 'T')
			.filter(fn => !migrationSet.has(fn));
	}

	/**
	 * Сверяет имена файлов миграций с шаблоном.
	 * Если имя файла не соответствует шаблону, то кидает исключение.
	 */
	private checkMigrationFileNames = (fileNames: string[]): string[] => {
		const regex = /^[VT]\d{2,}__\w+\.sql$/;
		const badFileName = fileNames.find(fn => !regex.test(fn));
		if (badFileName) {
			throw new Error(`Имя файла миграции "${badFileName}" должно соответствовать шаблону "${regex}"`);
		}
		return fileNames;
	}

	private async executeSqlFile(fileName: string): Promise<void> {
		return await this.db.transaction(async tr => {
			const filePath = path.resolve('migrations', fileName);
			const sql = fs.readFileSync(filePath).toString();
			await this.query(sql, tr);
			await this.query(`INSERT INTO ${MIGRATIONS_TABLE_NAME} (name)
                              VALUES (${this.db.escape(fileName)})`, tr);
		})
			.catch((error: any) => {
				throw fillError(Error,
					`Не удалось выполнить скрипт миграции "${fileName}"`,
					error);
			});
	}

	/**
	 * Получает на вход список файлов с новыми миграциями.
	 * @param fileNames
	 */
	private applyMigrations = async (fileNames: string[]): Promise<void> => {
		fileNames.sort();
		for (const fileName of fileNames) {
			await this.executeSqlFile(fileName);
		}
	}

	/**
	 * Выполняет версионные миграции
	 */
	migrateVersionFiles = async (): Promise<void> => this
		.findAppliedMigrationSet()
		.then(this.findAndFilterVersionMigrationFiles)
		.then(this.checkMigrationFileNames)
		.then(this.applyMigrations)

	/**
	 * Выполняет миграции для тестов
	 */
	migrateTestFiles = async (): Promise<void> => this
		.findAppliedMigrationSet()
		.then(this.findAndFilterTestMigrationFiles)
		.then(this.checkMigrationFileNames)
		.then(this.applyMigrations)

	/**
	 * Закрывает соединение с СУБД
	 */
	close = async (): Promise<void> => await this.db.close();
}
