import merge from 'deepmerge';
import nodeFetch from 'node-fetch';
import util from 'util';
import ResponseError from '~errors/ResponseError';

/**
 * Класс для выполнения запросов к API.
 */
export default class NodeFetcher
{
	private _basePath: any;
	private _headers: any;

	constructor(basePath) {
		this._basePath = basePath;
		this._headers = {
			Accept: 'application/json',
		};
	}

	setAuthToken = authToken => {
		this._headers.cookie = `auth-token=${authToken}`;
	}

	fetch = (method: string, url: string, body?: any, inputHeaders?: any) => {
		url = `${this._basePath}${url}`;
		method = method.toUpperCase();
		const headers: any = merge({}, this._headers);
		const isContentTypeJson = !!body;
		if (isContentTypeJson) {
			headers['Content-Type'] = 'application/json';
		}
		if (inputHeaders) Object.assign(headers, inputHeaders);
		return nodeFetch(url, {
			method,
			headers,
			body: isContentTypeJson ? JSON.stringify(body) : body,
		});
	}

	fetchWithJson = (method, url, ...args: any[]) => this
		.fetch(method, url, ...args)
		.then(async (res: any) => [res, await res.json()])
		.then(([res, json]) => {
			if (res.status >= 400) {
				// @ts-ignore
				throw new ResponseError(res.statusText, res.status,
					util.format('Ошибка при запросе %s %o\n%o', method, `${this._basePath}${url}`, json),
				);
			}
			return json;
		})

	get = (url, ...args: any[]) => this.fetch('get', url, ...args);

	getWithJson = (url, ...args: any[]) => this.fetchWithJson('get', url, ...args);

	post = (url, ...args: any[]) => this.fetch('post', url, ...args);

	postWithJson = (url, ...args: any[]) => this.fetchWithJson('post', url, ...args);

	put = (url, ...args: any[]) => this.fetch('put', url, ...args);

	putWithJson = (url, ...args: any[]) => this.fetchWithJson('put', url, ...args);

	delete = (url, ...args: any[]) => this.fetch('delete', url, ...args);

	login = (username, password) => this
		.post('/api/login', { username, password })
		.then(async res => {
			if (res.status !== 200) {
				const json = await res.json();
				// tslint:disable-next-line:no-console
				console.error('[Ошибка во время логина]', json);
			}
			expect(res.status).toEqual(200);
			const cookieHeader: any = res.headers.get('set-cookie');
			expect(cookieHeader).not.toBeNull();
			const authToken: any = cookieHeader.match(/auth-token=([^;]+);/)[1];
			expect(authToken).not.toBeFalsy();
			this.setAuthToken(authToken);
		})
}
