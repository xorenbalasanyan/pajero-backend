import Sequelize from 'sequelize';
import {
	AuthCredential,
	File,
	Good,
	Incident,
	IncidentDaylyReport,
	IncidentImport,
	IncidentType,
	InfoMessage,
	InfoMessageToAddressee,
	Log,
	Shop,
	ShopManager,
	ShopManagerRole,
	StructureImport,
	Task,
	TaskToShop,
	User,
	ChecklistTask,
} from '../stores/db/entity';
import { FreshTask } from '../stores/db/views';

type JsonObject = any | any[];

type EntityType = {
	id: number,
	fk_owner?: EntityType,
	fk_executor?: EntityType,
	fk_rejector?: EntityType,
	fk_shop?: EntityType,
	fk_good?: EntityType,
	fk_file?: EntityType,
	fk_fileType?: EntityType,
	fk_incidentType?: EntityType,
	fk_taskToShops?: EntityType[],
	fk_infoMessageToAddressees?: EntityType[],
	fk_addressee?: EntityType,
	fk_log?: EntityType,
	authCredential?: EntityType,
	shopManagers?: EntityType[],
	shopManagerRole?: EntityType,
	user?: EntityType,
	infoMessageId?: number,
	addresseeUserId?: number,
	toDto?: () => JsonObject,
};

type MetaCollection = {
	authCredentialMap?: Map<number, typeof AuthCredential>,
	shopMap?: Map<number, typeof Shop>,
	goodMap?: Map<number, typeof Good>,
	userMap?: Map<number, typeof User>,
	shopManagerMap?: Map<number, typeof ShopManager>,
	shopManagerRoleMap?: Map<number, typeof ShopManagerRole>,
	taskMap?: Map<number, typeof Task>,
	ChecklistTaskMap?: Map<number, typeof ChecklistTask>,
	fileMap?: Map<number, typeof File>,
	logMap?: Map<number, typeof Log>,
	incidentTypeMap?: Map<number, typeof IncidentType>,
	infoMessageToAddresseeMap?: Map<string, typeof InfoMessageToAddressee>,
};

/**
 * Получает на вход объект или массив, которые надо подготовить к передаче в Response.
 * Дополнительно перед конвертацией Entity в Dto вырезает другие Entity и размещает в Meta.
 * @param value
 * @returns {[JsonObject, MetaCollection]}
 */
export default function jsoner(value: EntityType | EntityType[]): [JsonObject, JsonObject] {
	const meta: MetaCollection = {};
	const result = convertValue(value, meta);
	// конвертируем каждый Map из meta в Json
	const metaKeys = Object.keys(meta);
	const jsonMeta = metaKeys.reduce((json, metaKey) => {
		json[metaKey] = {};
		Array.from(meta[metaKey].keys()).forEach(mapKey => {
			json[metaKey][mapKey] = meta[metaKey].get(mapKey);
		});
		return json;
	}, {});
	return [result, jsonMeta];
}

function convertValue(value: EntityType | EntityType[], meta: MetaCollection): JsonObject {
	if (value instanceof Array) {
		return value.map(i => convertValue(i, meta));
	} else if (value instanceof Sequelize.Model) {
		return entityMapper(value, meta);
	} else {
		return value;
	}
}

/**
 * Вырезает подчиненные Entity в Entity, складывает их в Meta.
 * Для входящего entity выполняет конвертацию в Dto.
 * @param entity
 * @param {MetaCollection} meta
 */
const entityMapper = (entity: EntityType, meta: MetaCollection) => {
	if (entity.fk_fileType) {
		if (entity instanceof File) {
			addFileType(entity.fk_fileType, meta);
			delete entity.fk_fileType;
		}
	}
	if (entity.shopManagers?.length) {
		if (entity instanceof Shop || entity instanceof User) {
			entity.shopManagers.forEach(shopManager => {
				addShopManager(shopManager, meta);
			});
			delete entity.shopManagers;
		}
	}
	if (entity.authCredential) {
		if (entity instanceof User) {
			addAuthCredential(entity.authCredential, meta);
			delete entity.authCredential;
		}
	}
	if (entity.shopManagerRole) {
		if (entity instanceof ShopManager) {
			addShopManagerRole(entity.shopManagerRole, meta);
			delete entity.shopManagerRole;
		}
	}
	if (entity.user) {
		if (entity instanceof ShopManager) {
			addUser(entity.user, meta);
			delete entity.user;
		}
	}
	if (entity.fk_owner) {
		if (entity instanceof Task || entity instanceof FreshTask
			|| entity instanceof ChecklistTask || entity instanceof IncidentImport || entity instanceof StructureImport) {
			addUser(entity.fk_owner, meta);
			delete entity.fk_owner;
		}
	}
	if (entity.fk_executor) {
		if (entity instanceof TaskToShop) {
			addUser(entity.fk_executor, meta);
			delete entity.fk_executor;
		} else if (entity instanceof ChecklistTask) {
			addUser(entity.fk_executor, meta);
			delete entity.fk_executor;
		}
	}
	if (entity.fk_rejector) {
		if (entity instanceof TaskToShop) {
			addUser(entity.fk_rejector, meta);
			delete entity.fk_rejector;
		}
	}
	if (entity.fk_shop) {
		if (entity instanceof User || entity instanceof TaskToShop) {
			addShop(entity.fk_shop, meta);
			delete entity.fk_shop;
		}
	}
	if (entity.fk_good) {
		if (entity instanceof Incident) {
			addGood(entity.fk_good, meta);
			delete entity.fk_good;
		}
	}
	if (entity.fk_incidentType) {
		if (entity instanceof IncidentImport || entity instanceof Incident) {
			addIncidentType(entity.fk_incidentType, meta);
			delete entity.fk_incidentType;
		}
	}
	if (entity.fk_file) {
		if (entity instanceof IncidentImport || entity instanceof StructureImport || entity instanceof IncidentDaylyReport) {
			addFile(entity.fk_file, meta);
			delete entity.fk_file;
		}
	}
	if (entity.fk_log) {
		if (entity instanceof IncidentImport || entity instanceof StructureImport) {
			addLog(entity.fk_log, meta);
			delete entity.fk_log;
		}
	}
	if (entity.fk_taskToShops) {
		if (entity instanceof Task || entity instanceof FreshTask) {
			entity.fk_taskToShops.forEach(i => addTaskToShop(i, meta));
			delete entity.fk_taskToShops;
		}
	}
	if (entity.fk_infoMessageToAddressees) {
		if (entity instanceof InfoMessage) {
			entity.fk_infoMessageToAddressees
				?.forEach(i => addInfoMessageToAddressee(i, meta));
			delete entity.fk_infoMessageToAddressees;
		}
	}
	if (entity.fk_addressee) {
		if (entity instanceof InfoMessageToAddressee) {
			addUser(entity.fk_addressee, meta);
			delete entity.fk_addressee;
		}
	}
	// конвератция entity в Dto
	if (entity.toDto instanceof Function) {
		return entity.toDto();
	} else {
		return undefined;
	}
};

function addItemToMetaMap(mapKey: string, entity: EntityType, meta: MetaCollection) {
	if (!meta[mapKey]) meta[mapKey] = new Map();
	if (!meta[mapKey].has(entity.id)) {
		meta[mapKey].set(entity.id, convertValue(entity, meta));
	}
}

const addFileType = addItemToMetaMap.bind(null, 'fileTypeMap');

const addShop = addItemToMetaMap.bind(null, 'shopMap');

const addGood = addItemToMetaMap.bind(null, 'goodMap');

const addUser = addItemToMetaMap.bind(null, 'userMap');

const addShopManager = addItemToMetaMap.bind(null, 'shopManagerMap');

const addAuthCredential = addItemToMetaMap.bind(null, 'authCredentialMap');

const addShopManagerRole = addItemToMetaMap.bind(null, 'shopManagerRoleMap');

const addFile = addItemToMetaMap.bind(null, 'fileMap');

const addLog = addItemToMetaMap.bind(null, 'logMap');

const addIncidentType = addItemToMetaMap.bind(null, 'incidentTypeMap');

const addTaskToShop = addItemToMetaMap.bind(null, 'taskToShopMap');

// У InfoMessageToAddressee нет id, поэтому используем составной ключ
function addInfoMessageToAddressee(entity: EntityType, meta: MetaCollection) {
	if (!meta.infoMessageToAddresseeMap) meta.infoMessageToAddresseeMap = new Map();
	const key = `${entity.infoMessageId}_${entity.addresseeUserId}`;
	if (!meta.infoMessageToAddresseeMap.has(key)) meta.infoMessageToAddresseeMap.set(key, convertValue(entity, meta));
}
