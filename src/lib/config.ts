import fs from 'fs';
import nconf from 'nconf';
import path from 'path';
import util from 'util';
import { validateInteger, validateIsBoolean, validateString } from '~utils/validate';

function init() {
	// проверка наличия файла конфигурации
	const configFilePath = process.env.APP_CONFIG_FILE || 'config.json';
	if (!fs.existsSync(configFilePath)) {
		// tslint:disable-next-line:no-console
		console.error(`[Ошибка запуска] Config file '${configFilePath}' not exists. Process will exit.`);
		process.exit(1);
	}

	const config = nconf
		// .argv()
		// .env()
		.file({ file: configFilePath });

	// проверка параметров конфигурации
	const validatePort = validateInteger(30, 65500);
	const validateHost = validateString(1, 100);
	const errorsObject = {
		port: config.get('port') ? validatePort(config.get('port')) : null,
		host: config.get('host') ? validateHost(config.get('host')) : null,
		'mobileApp:minAndroidVersion': validateString(1, 10)(config.get('mobileApp:minAndroidVersion')),
		'mobileApp:minIosVersion': validateString(1, 10)(config.get('mobileApp:minIosVersion')),
		'paths:userdata': validateString(1)(config.get('paths:userdata')),
		'paths:userdata-deleted': validateString(1)(config.get('paths:userdata-deleted')),
		'mysql:host': validateString(1)(config.get('mysql:host')),
		'mysql:port': validatePort(config.get('mysql:port')),
		'mysql:database': validateString(1)(config.get('mysql:database')),
		'mysql:username': validateString(1)(config.get('mysql:username')),
		'mysql:password': validateString(1)(config.get('mysql:password')),
		'smtp-noreply:transport': config.get('smtp-noreply:transport')
			? typeof config.get('smtp-noreply:transport') === 'object'
				? null
				: 'Нужно указать объект'
			: null,
		'smtp-noreply:from': config.get('smtp-noreply:transport') ? validateString(1)(config.get('smtp-noreply:from')) : null,
		inforeader: config.get('inforeader')?.length > 0
			&& config.get('inforeader').map(
				({
					accountId,
					adminUserId,
					readQuantityAtOneTime,
					transport,
				}) => ({
					accountId: validateInteger(1)(accountId),
					adminUserId: validateInteger(1)(adminUserId),
					readQuantityAtOneTime: validateInteger(1)(readQuantityAtOneTime),
					transport: typeof transport === 'object' ? null : 'Нужно указать объект',
				})),
		schedulerJobs: config.get('schedulerJobs')?.length > 0
			&& config.get('schedulerJobs').map(({ jobClassName, enabled, executeOnStart }) => ({
				jobClassName: validateString(1)(jobClassName),
				enabled: validateIsBoolean()(enabled),
				executeOnStart: validateIsBoolean()(executeOnStart),
			})),
	};

	checkErrors(errorsObject);

	// проверки прошли, резолвим пути
	config.set('paths:userdata', path.resolve(config.get('paths:userdata')));
	config.set('paths:userdata-deleted', path.resolve(config.get('paths:userdata-deleted')));

	// второй круг проверок
	checkErrors({
		'paths:userdata': !fs.existsSync(config.get('paths:userdata')) && `Путь "${config.get('paths:userdata')}" не существует`,
		'paths:userdata-deleted': !fs.existsSync(config.get('paths:userdata-deleted')) && `Путь "${config.get('paths:userdata-deleted')}" не существует`,
	});

	// замена значений по умолчанию
	if (!config.get('port')) config.set('port', 3000);
	if (!config.get('host')) config.set('host', '127.0.0.1'); // по умолчанию закрываем доступ извне

	return config;
}

function checkErrors(errorsObject) {
	const errors: string[] = [];

	const foo = (o, ...keys) => {
		if (o instanceof Array) {
			o.forEach((value, index) => foo(value, ...keys, index));
		} else if (o instanceof Object) {
			Object.keys(o).forEach(key => foo(o[key], ...keys, key));
		} else if (o !== undefined && o !== null && o !== false) {
			errors.push(util.format('Параметр "%s" указан с ошибкой: %s', keys.join(':'), o));
		}
	};

	foo(errorsObject);

	if (errors.length) {
		// tslint:disable-next-line:no-console
		console.error('🚨 Ошибки конфигурации:\n - ' + errors.join('\n - '));
		process.exit(1);
	}
}

const mainConfig = init();

export default mainConfig;
