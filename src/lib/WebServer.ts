import fs from 'fs';
import path from 'path';
import chalk from 'chalk';
import express, { Request, Response } from 'express';
import bodyParser from 'body-parser'; // json body
import cookieParser from 'cookie-parser';
import http from 'http';
import { isProd } from '~utils/env';
import config from '~lib/config';
import initFirebaseAdmin from '~lib/initFirebaseAdmin';
import initRoutes from '~lib/initRoutes';
import MigrationApplier from '~lib/MigrationApplier';
import {
	injectResponseMethods,
	morganLogger,
	notFoundRoute,
	readRequestBodyAsCsvFile,
	responseWriter,
} from '~middlewares';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import PageInfo from '~utils/PageInfo';
import HttpError from '~errors/HttpError';
import User from '../stores/db/entity/User';
import { UserAuth } from '../stores/db/entity';
import Account from '../stores/db/entity/Account';

const START = Date.now();

export type ExRequest = Request & {
	requestId: string,
	getLogger: (parentLogger: AppLogger) => AppLogger,
	getAuthorizedUser: () => typeof User,
	getAuthorizedUserAuth?:() => typeof UserAuth,
	__authorizedUserAccount?: typeof Account,
	getAuthorizedUserAccount?: () => Promise<typeof Account>,
	doNotLogNormalStatus?: boolean,
};

export type ExResponse = Response & {
	sendOk: (value?: any, pageInfo?: PageInfo) => void,
	sendHttpError: (error: HttpError, jsonFields?: any) => void,
};

export default class WebServer
{
	private _inited = false;
	private declare _logger: AppLogger;
	private declare _app: express.Express;
	private declare _PORT: number;
	private declare _HOST: string;
	private declare _started: boolean;
	private declare _httpServer: http.Server;

	init = async () => {
		if (this._inited) return undefined;
		// модуль используется только в разработке
		const errorhandler = isProd ? undefined : require('errorhandler');
		const logger = new AppLogger('WebServer');
		this._logger = logger;
		logger.info(`🌎 Initializing WebServer...\n`
			+ `Mode: ${process.env.NODE_ENV}\n`
			+ `Config file: ${process.env.APP_CONFIG_FILE}\n`
			+ `Package version: ${process.env.PACKAGE_NAME} v${process.env.PACKAGE_VERSION}`);
		// накатываем миграции
		const migrationApplier = new MigrationApplier();
		await migrationApplier.migrateVersionFiles();
		// для тестов накатываются дополнительно тестовые миграции
		if (process.env.NODE_ENV === 'test') {
			await migrationApplier.migrateTestFiles();
		}
		await migrationApplier.close();
		if (process.env.FIREBASE_CONFIG) {
			logger.info(`🌎 Firebase config: ${process.env.FIREBASE_CONFIG}`);
			initFirebaseAdmin(process.env.FIREBASE_CONFIG);
		}
		// отлов ошибок
		process.on('uncaughtException', (err) => {
			logger.error(fillError(Error, chalk.red(`[UNCAUGHT EXCEPTION] ${err}`), err));
		});
		// отлов ошибок Promise на случай Unhandled rejection
		process.on('unhandledRejection', (err: Error) => {
			logger.error(fillError(Error, chalk.red(`[UNHANDLED REJECTION IN PROMISE] ${String(err)}`), err));
		});
		const app = express();
		// simple
		app.get('/api/uptime', (req, res) => {
			const uptime = Date.now() - START;
			res.status(200).json({
				started: new Date(START),
				nodeEnv: process.env.NODE_ENV,
				serverEnv: process.env.SERVER_ENV,
				version: process.env.PACKAGE_VERSION,
				uptime: {
					ms: uptime,
					sec: uptime / 1_000,
					min: uptime / 60_000,
					hour: uptime / 3_600_000,
					day: uptime / 86_400_000,
				},
			});
		});
		/**
		 * Для каждого запроса генерится уникальный requestId.
		 * Расширяем request: добавляем метод для инициализации дочернего модульного логгера.
		 */
		app.use((req: Request, res: Response, next) => {
			Object.assign(req, {
				requestId: Math.random().toString(36).substr(2, 6),
				getLogger: (parentLogger: AppLogger): AppLogger => parentLogger.child({ req }),
			});
			next();
		});
		// use middle wares
		app.use(responseWriter); // запоминаем тело ответа
		app.use(morganLogger(logger)); // логгер запросов
		app.use(bodyParser.json({ limit: '20mb' })); // парсер request body, 20 mb limit for JSON
		app.use(bodyParser.urlencoded({ limit: '100mb', extended: false }));
		app.use(cookieParser());
		app.use(readRequestBodyAsCsvFile); // парсер для csv файлов
		app.use(injectResponseMethods);
		// use routes
		app.use(initRoutes);
		// use not found and error middle wares
		app.use((req, res, next) => {
			// в случае, если роут не был найден, пробуем вернуть index.html
			if (req.accepts('html')) {
				fs.readFile(path.join('./public/index.html'), 'utf-8', (err, page) => {
					if (err) {
						next();
					}
					// сообщаем браузеру, что требуемый запрос перенаправлен
					res.writeHead(303, { 'Content-Type': 'text/html' });
					res.write(page);
					res.end();
				});
			} else {
				next();
			}
		});
		app.use(notFoundRoute);
		const errorHandlerWare = (err, req, res, next) => {
			const { method, originalUrl, headers, query } = req;
			const text = [String(new Date()), `${method} ${originalUrl}`];
			const headersText = Object.keys(headers).map(key => `. ${key}: ${headers[key]}`);
			text.push(`Request headers:\n${headersText.join('\n')}`);
			const queryText = Object.keys(query).map(key => `. ${key}: ${query[key]}`);
			if (queryText.length) text.push(`Request query:\n${queryText.join('\n')}`);
			text.push(err.stack || String(err));
			logger.error(fillError(Error, chalk.red(text.join('\n'))));
			if (errorhandler) errorhandler()(err, req, res, next);
		};
		app.use(errorHandlerWare);
		this._app = app;
		this._PORT = config.get('port');
		this._HOST = config.get('host');
		this._inited = true;
	}

	get url() {
		return `http://${this._HOST}:${this._PORT}`;
	}

	start = async () => {
		if (!this._inited) {
			throw new Error('Сначала надо вызвать метод init');
		}
		if (this._started) {
			return undefined;
		}
		return new Promise(resolve => {
			this._httpServer = this._app.listen(
				this._PORT,
				this._HOST,
				() => {
					this._logger.info(`🌎 Web server listening to http://${this._HOST}:${this._PORT}`);
					this._started = true;
					resolve(undefined);
				},
			);
		});
	}

	stop = async () => {
		if (!this._started) {
			throw new Error('Сначала надо вызвать метод start');
		}
		return new Promise(resolve => {
			this._httpServer.close(() => {
				this._logger.info('🌎 WebServer stopped');
				this._started = false;
				resolve(undefined);
			});
		});
	}
}
