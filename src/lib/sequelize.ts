import { Sequelize } from 'sequelize-typescript';
import { QueryTypes, Transaction } from 'sequelize';
import { BindOrReplacements } from 'sequelize/types/dialects/abstract/query-interface';
import merge from 'deepmerge';
import mysql2 from 'mysql2';
import config from '~lib/config';
import { isProd } from '~utils/env';
import chalk from 'chalk';

export function initSequelize(options: any): Sequelize {
	options = merge({
		host: config.get('mysql:host'),
		port: config.get('mysql:port'),
		database: config.get('mysql:database'),
		username: config.get('mysql:username'),
		password: config.get('mysql:password'),
		dialect: 'mysql',
		dialectModule: mysql2,
		define: {
			// если сделать false, то в полях createdAt и updatedAt данных не будет в методах toDto
			timestamps: true,
		},
		pool: {
			min: config.get('mysql:pool:min') || 1,
			max: config.get('mysql:pool:max') || 5,
			idle: config.get('mysql:pool:idle') || 10_000,
			acquire: config.get('mysql:pool:acquire') || 30_000,
		},
		logging: false,
	}, options || {});
	return new Sequelize(options);
}

const mainSequelize = initSequelize({
	logging: isProd ? undefined : (msg: string, time: number, ...obj: any[]) => {
		const colFoo = time > 100 ? chalk.red : chalk.cyanBright;
		// tslint:disable-next-line:no-console
		console.debug(colFoo(`(time: ${time}ms) `) + chalk.gray(msg));
	},
	logQueryParameters: true, // print query replacements
	benchmark: true, // в logging вернет время выполнения запроса в ms
});
mainSequelize.authenticate();

export default mainSequelize;

export async function select(sqlQuery: string, replacements?: BindOrReplacements, transaction?: Transaction): Promise<any[]> {
	return await mainSequelize.query(sqlQuery, {
		replacements,
		type: QueryTypes.SELECT,
		transaction,
	});
}

export async function insert(sqlQuery: string, replacements?: BindOrReplacements, transaction?: Transaction): Promise<[number, number]> {
	return await mainSequelize.query(sqlQuery, {
		replacements,
		type: QueryTypes.INSERT,
		transaction,
	});
}

export async function update(sqlQuery: string, replacements?: BindOrReplacements, transaction?: Transaction): Promise<[undefined, number]> {
	return await mainSequelize.query(sqlQuery, {
		replacements,
		type: QueryTypes.UPDATE,
		transaction,
	});
}

export async function deleteQuery(sqlQuery: string, replacements?: BindOrReplacements, transaction?: Transaction): Promise<void> {
	return await mainSequelize.query(sqlQuery, {
		replacements,
		type: QueryTypes.DELETE,
		transaction,
	});
}
