import util from 'util';
import md5 from 'md5';
import { Op, Transaction } from 'sequelize';
import { ONE_DAY_MS } from '~enums/DateEnum';
import {
	HEADER_HTTP_AUTH_TOKEN,
	HEADER_HTTP_REFRESH_TOKEN,
	HEADER_WEB_AUTH_TOKEN,
	HEADER_WEB_REFRESH_TOKEN,
} from '~enums/HeaderEnum';
import ResponseError, { AUTH_AS_CURRENT_USER, DATABASE_WRITE_ERROR } from '~errors/ResponseError';
import { UserRoles } from '~enums/UserRoleEnum';
import { toUTCSqlDateTime } from '~utils/date';
import guid from '~utils/guid';
import { userStore } from '~stores';
import {
	AuthCredentialService,
	ShopManagerRoleService,
	ShopManagerService,
	ShopService,
} from '~services';
import BaseEntityService from './BaseEntityService';
import { AuthCredential, Shop, ShopManager, User, UserAuth } from '../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';
import { select } from '~lib/sequelize';
import { arrayToMapById, distinctListByField } from '~utils/list';
import { fillError } from '~utils/errors';
import ShopManagerRole from '../stores/db/entity/ShopManagerRole';

const moduleLogger = new AppLogger('UserService');

export default class UserService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, userStore);
	}

	/* findOneDmByExternalId = async (externalId, transaction?: Transaction) => this.findOne({
		where: { externalId, role: USER_ROLE_DM },
	}, transaction) */

	/* findOneZdmByExternalId = async (externalId, transaction?: Transaction) => this.findOne({
		where: { externalId, role: USER_ROLE_ZDM },
	}, transaction) */

	/* findOneByUsername = async (username, transaction?: Transaction) => this
		.findOne({ where: { username } }, transaction); */

	/* findAllBySupervisor = async (superVisorUser: User, transaction?: Transaction): Promise<User[]> => {
		switch (superVisorUser.role) {
			case USER_ROLE_DF:
				return this.findAllByDfId(superVisorUser.id, transaction);
			case USER_ROLE_TM:
				return this.findAllByTmId(superVisorUser.id, transaction);
			case USER_ROLE_UPF:
				return this.findAllByUpfId(superVisorUser.id, transaction);
			default:
				throw new Error(`Cannot find users with supervisor id #${superVisorUser.id} with unknown role`);
		}
	} */

	findAllByEmails = async emails => this.findAll({ where: { email: { [Op.in]: emails } } });

	/* findAllDmAndZdmByShopIds = async shopIds => this.findAll({ where: { shopId: { [Op.in]: shopIds } } }); */

	/**
	 * Возвращает список подчиненных магазинов.
	 * Подчиненные магазины могут быть только у шефа и менеджеров, не ДМ.
	 * Список строится вниз по каждой роли.
	 * @param user
	 * @param transaction
	 */
	getSubordinateShopList = async (user: typeof User, transaction?: Transaction) => {
		// админу, роботу и шефу подчиняются все магазины
		const shopService = new ShopService(this.req);
		if (user.isAdmin || user.isRobot || user.isChief) {
			return shopService.findAll({ where: {} }, transaction); // поиск всех
		}
		// определяем список id магазинов
		const shops = await select(
			`SELECT sm.shopId
             FROM shop_manager sm
                      INNER JOIN shop_manager_role smr ON smr.id = sm.roleId
             WHERE sm.userId = :userId
               AND NOT smr.isShopUser
             GROUP BY sm.shopId`,
			{ userId: user.id },
			transaction,
		);
		const shopIds = distinctListByField(shops, 'shopId');
		return shopService.findAll({ where: { id: shopIds } }, transaction);
	}

	/**
	 * Возвращает дерево подчиненных магазинов.
	 * Подчиненные магазины могут быть только у менеджеров с ролями: ДТ, ДФ, ТМ, УПФ.
	 * Список строится вниз по каждой роли.
	 * @param user
	 */
	getSubordinateShopTree = async (user: typeof User) => {
		const shopManagerRoleService = new ShopManagerRoleService(this.req);
		const roles = await shopManagerRoleService.findAllWithAccountId({ where: {} });
		const rolesMap = arrayToMapById(roles);
		const shopService = new ShopService(this.req);
		const selectAllTree = user.isAdmin || user.isSupport || user.isChief;
		// узнаем все подчиненные магазины и менеджеров этого пользователя
		let subLinks: any[];
		if (selectAllTree) {
			subLinks = await select(
				`SELECT sm.shopId, sm.userId, sm.roleId, smr.subLevel
                 FROM shop_manager sm
                          INNER JOIN shop_manager_role smr ON smr.id = sm.roleId
                 WHERE NOT smr.isShopUser
                 GROUP BY sm.shopId, sm.userId, sm.roleId, smr.subLevel`);
		} else {
			subLinks = await select(
				`SELECT sm.shopId, sm.userId, sm.roleId, smr.subLevel
                 FROM shop_manager sm
                          INNER JOIN shop_manager_role smr ON smr.id = sm.roleId
                 WHERE NOT smr.isShopUser
                   AND sm.shopId IN (
                     SELECT DISTINCT sm.shopId
                     FROM shop_manager sm
                              INNER JOIN shop_manager_role smr ON smr.id = sm.roleId
                     WHERE sm.userId = :userId
                       AND NOT smr.isShopUser
                 )
                 GROUP BY sm.shopId, sm.userId, sm.roleId, smr.subLevel`,
				{ userId: user.id },
			);
		}
		// вытаскиваем и загружаем магазины и менеджеров из этого списка
		const shopIds = distinctListByField(subLinks, 'shopId');
		const shops = await shopService.findAllByIds(shopIds);
		const shopMap = arrayToMapById(shops);
		const users = await this.findAllByIds(distinctListByField(subLinks, 'userId'));
		const usersMap = arrayToMapById(users);
		const subordinateUsers: typeof User[] = [];
		const subordinateShops: typeof Shop[] = [];
		// для каждого магазина составляем список менеджеров
		const mansByShop = subLinks.reduce((map, subLink) => {
			if (!map.has(subLink.shopId)) {
				map.set(subLink.shopId, []);
			}
			map.get(subLink.shopId).push(subLink);
			return map;
		}, new Map<number, any[]>());
		// для каждого магазина из предыдущего списка сортируем менеджеров по уровню
		for (const [shopId, subLinkList] of mansByShop) {
			// какая роль у текущего пользователя в магазине
			const userLink = subLinkList.find(i => i.userId === user.id);
			// eslint-disable-next-line no-continue
			if (!userLink && !selectAllTree) continue;
			const userRole = userLink && rolesMap.get(userLink.roleId);
			if (!userRole && !selectAllTree) {
				// TODO так быть не должно
				// eslint-disable-next-line no-continue
				continue;
			}
			subLinkList.sort((a, b) => b.subLevel - a.subLevel);
			let subUserList: typeof User[] = subordinateUsers;
			// tslint:disable-next-line:prefer-for-of
			for (let i = 0; i < subLinkList.length; i++) {
				const subUser = usersMap.get(subLinkList[i].userId);
				if (subUser) {
					const subUserRole = rolesMap.get(subLinkList[i].roleId);
					if (selectAllTree || userRole.subLevel > subUserRole.subLevel) {
						if (subLinkList[i].userId !== user.id) {
							if (!subUserList.find(u => u.id === subUser.id)) {
								subUserList.push(subUser);
								subUser.shopManagerRole = rolesMap.get(subLinkList[i].roleId);
							}
							if (!subUser.subordinateUsers) {
								subUser.subordinateUsers = [];
							}
							subUserList = subUser.subordinateUsers;
						}
					}
					if (i === subLinkList.length - 1) {
						const shop = shopMap.get(shopId);
						// магазин может быть помечен как isDeleted
						if (shop) {
							if (subLinkList[i].userId !== user.id) {
								if (!subUser.subordinateShops) {
									subUser.subordinateShops = [];
								}
								subUser.subordinateShops.push(shop);
							} else {
								subordinateShops.push(shop);
							}
						}
					}
				}
			}
		}
		return { subordinateShops, subordinateUsers };
	}

	addMeta = async (options, pageInfo) => {
		// нужно ли прикрепить username
		if (pageInfo.meta?.has('username')) {
			if (!options.include) options.include = [];
			// required позволяет делать LEFT JOIN вместо INNER JOIN
			options.include.push({
				model: AuthCredential,
				attributes: ['id', 'userId', 'username'],
				required: false,
			});
		}
		// нужно ли прикрепить роли
		if (pageInfo.meta?.has('shoproles')) {
			if (!options.include) options.include = [];
			// required позволяет делать LEFT JOIN вместо INNER JOIN
			options.include.push({
				model: ShopManager,
				attributes: ['id', 'userId', 'roleId'],
				required: false,
				include: [{
					model: ShopManagerRole,
					as: 'shopManagerRole',
					attributes: ['id', 'title', 'shortTitle', 'engShortTitle', 'subLevel', 'isShopUser'],
					required: false,
				}],
			});
		}
	}

	addWhereSearch = (options, { meta, searchParts }) => {
		if (!searchParts?.length) return;
		const { where } = options;
		if (!where[Op.or]) where[Op.or] = [];
		// избавляемся от подзапросов, иначе не получится сделать поиск по полям
		options.subQuery = false;
		// для каждого поля добавляем поиск
		searchParts?.forEach(str => {
			where[Op.or].push({ id: { [Op.substring]: str } });
			where[Op.or].push({ lastName: { [Op.substring]: str } });
			where[Op.or].push({ firstName: { [Op.substring]: str } });
			where[Op.or].push({ middleName: { [Op.substring]: str } });
			where[Op.or].push({ role: { [Op.substring]: str } });
			where[Op.or].push({ email: { [Op.substring]: str } });
			where[Op.or].push({ phone: { [Op.substring]: str } });
			if (meta?.has('username')) {
				where[Op.or].push({ '$authCredential.username$': { [Op.substring]: str } });
			}
			if (meta?.has('shoproles')) {
				where[Op.or].push({ '$shopManagers->shopManagerRole.title$': { [Op.substring]: str } });
			}
		});
	}

	/**
	 * Обновление пользователя
	 * @param existUser
	 * @param data
	 * @param transaction
	 * @returns {Promise<*>}
	 */
	updateOne = async (existUser: any, data: any, transaction?: Transaction) => {
		const currentUser = this.req.getAuthorizedUser();
		const newData: any = {
			moderatorId: currentUser.id,
		};
		['externalId', 'role', 'username', 'lastName', 'firstName', 'middleName', 'email', 'phone', 'comment', 'shopId', 'dfId', 'tmId', 'upfId']
			.forEach(key => {
				if (data[key] !== undefined) newData[key] = data[key];
			});
		// проверка на уникальность логина
		const { username } = newData;
		const authCredentialService = new AuthCredentialService(this.req);
		if (username) {
			const existAuthCred = await authCredentialService.findOne({
				where: {
					username,
				},
			}, transaction);
			if (existAuthCred && existAuthCred.userId !== existUser.id) {
				throw ResponseError.validationInput('Пользователь с таким логином уже существует');
			}
		}
		// если изменилась роль, то зависимые пользователи и магазины должны обновиться
		const shopManagerService = new ShopManagerService(this.req);
		let shopManagersToRemove: any[] | undefined;
		if (newData.role !== undefined && existUser.role !== newData.role && existUser.role === UserRoles.MANAGER) {
			shopManagersToRemove = await shopManagerService.findAll({
				where: {
					userId: existUser.id,
				},
			}, transaction);
		}
		// update
		if (shopManagersToRemove?.length) {
			await Promise.all(shopManagersToRemove.map(i => shopManagerService
				.deleteWhere({
					id: i.id,
				}, transaction)));
		}
		return await existUser.update(newData, { transaction });
	}

	/**
	 * Вернет роли ДМ для пользователя
	 * @param userId
	 */
	findAllShopManagerRolesByUserId = async (userId: number): Promise<typeof ShopManagerRole[]> => {
		const shopManagerService = new ShopManagerService(this.req);
		const shopManagers = await shopManagerService.findAll({
			where: {
				userId,
			},
			attributes: ['shopId', 'roleId'],
			raw: true,
		});
		const roleIds = Array.from(new Set(shopManagers.map(i => i.roleId)));
		const shopManagerRoleService = new ShopManagerRoleService(this.req);
		return await shopManagerRoleService.findAllWithAccountId({
			where: {
				id: roleIds,
				isShopUser: true,
			},
		});
	}

	/**
	 * Поиск пользователя по auth токену
	 */
	static getUserByAuthTokenFromRequest(req) {
		const minDate = toUTCSqlDateTime(new Date(Date.now() - ONE_DAY_MS));
		const authToken = req.cookies[HEADER_HTTP_AUTH_TOKEN];
		if (!authToken) return null;
		return UserAuth.findOne({
			where: {
				authToken,
				createdAt: {
					[Op.gt]: minDate,
				},
			},
		});
	}

	/**
	 * Поиск пользователя по refresh токену
	 */
	static async getUserByRefreshTokenFromRequest(req) {
		const minDate = toUTCSqlDateTime(new Date(Date.now() - 5 * ONE_DAY_MS));
		const refreshToken = req.cookies[HEADER_HTTP_REFRESH_TOKEN];
		if (!refreshToken) return null;
		return UserAuth.findOne({
			where: {
				refreshToken,
				createdAt: {
					[Op.gt]: minDate,
				},
			},
		});
	}

	/**
	 * Регистрирует новый токен авторизации и заполняет Response
	 */
	static async registerAuthToken(req, res, user) {
		const logger = req.getLogger(moduleLogger);
		// создаем новый userAuth
		const authToken = md5(guid()) + md5(user.fullName) + md5(new Date().toString());
		const refreshToken = md5(guid()) + md5(user.fullName) + md5(new Date().toString());
		// https://stackoverflow.com/questions/10849687/express-js-how-to-get-remote-client-address
		const ipAddress = ((req.headers['x-forwarded-for'] || req.headers['x-real-ip'] || req.ip || req.connection.remoteAddress)
			?.match(/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/)?.[0] || '').substr(0, 15);
		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-ignore
		const userAuth = new UserAuth({
			userId: user.id,
			authToken,
			refreshToken,
			ipAddress,
			ownerId: user.id,
		});
		return userAuth.save()
			.then(() => res
				.status(200)
				// токен авторизации, httpOnly, по нему сервер проверяет авторизацию (актуален 1 сутки)
				.cookie(HEADER_HTTP_AUTH_TOKEN, authToken, { maxAge: ONE_DAY_MS, httpOnly: true })
				// токен авторизации, доступен для клиента, нужен только для проверки наличия авторизации,
				// используется как заглушка и не дает никакого доступа к северу
				.cookie(HEADER_WEB_AUTH_TOKEN, guid(), { maxAge: ONE_DAY_MS })
				// refresh токен, httpOnly, по нему сервер определяет возможность повторного входа (актуален 5 суток)
				.cookie(HEADER_HTTP_REFRESH_TOKEN, refreshToken, {
					maxAge: 5 * ONE_DAY_MS,
					httpOnly: true,
				})
				// refresh токен, доступен для клиента, нужен только для проверки наличия куакуыр njrtyf?
				// используется как заглушка и не дает никакого доступа к северу
				.cookie(HEADER_WEB_REFRESH_TOKEN, guid(), { maxAge: 5 * ONE_DAY_MS })
				.json({ ok: true }))
			.catch(error => {
				logger.error(fillError(Error, '[Ошибка во время авторизации]', error));
				res.sendHttpError(DATABASE_WRITE_ERROR);
			});
	}

	/**
	 * Пользователь выполняет разлогин.
	 * Чистим токен авторизации и заполняем Response.
	 * Если была выполнена авторизация под вторым пользователем, то сначала выходим под ним и
	 * возвращаем клиенту флаг logoutAsUser и redirectUrl.
	 */
	static async logout(req, res) {
		const logger = req.getLogger(moduleLogger);
		const currentUser = req.getAuthorizedUser();
		logger.http(`Пользователь #${currentUser.id} выполняет logout`);
		const userAuth = req.getAuthorizedUserAuth();
		if (userAuth.secondaryUserId) {
			const lastUrl = userAuth.lastUrl;
			return userAuth
				.update({ secondaryUserId: null })
				.catch(err => {
					logger.error(fillError(Error, util.format('[Ошибка logout] Ошибка при обновлении authToken: %o', userAuth), err));
					res.sendHttpError(DATABASE_WRITE_ERROR);
				})
				.then(() => res.json({ ok: true, redirectUrl: lastUrl }));
		} else {
			return userAuth
				.destroy()
				.catch(err => {
					logger.error(fillError(Error, `[Ошибка удаления userAuth из БД] При удалении authToken #${userAuth.id} возникла ошибка:`, err));
					res.sendHttpError(DATABASE_WRITE_ERROR);
				})
				.then(() => this.clearAuthCookie(res).json({ ok: true }));
		}
	}

	/**
	 * Вход под другим пользователем
	 */
	static async signAsUser(req, res, secondaryUserId, lastUrl) {
		const logger = req.getLogger(moduleLogger);
		const currentUser = req.getAuthorizedUser();
		// пользователь не должен заходить под собой
		if (secondaryUserId === currentUser.id) {
			return res.sendHttpError(AUTH_AS_CURRENT_USER);
		}
		const userAuth = req.getAuthorizedUserAuth();
		logger.http(util.format('Текущий пользователь #%d вошел под другим пользователем #%d', currentUser.id, secondaryUserId));
		return userAuth
			.update({ secondaryUserId, lastUrl })
			.then(() => res.json({ ok: true }))
			.catch(err => {
				logger.error(fillError(Error, `[Ошибка обновления userAuth] При входе под другим пользователем #${secondaryUserId} возникла ошибка:`, err));
				res.sendHttpError(DATABASE_WRITE_ERROR);
			});
	}

	static clearAuthCookie(res) {
		return res
			.cookie(HEADER_HTTP_AUTH_TOKEN, '', { maxAge: -1, httpOnly: true })
			.cookie(HEADER_WEB_AUTH_TOKEN, '', { maxAge: -1 })
			.cookie(HEADER_HTTP_REFRESH_TOKEN, '', { maxAge: -1, httpOnly: true })
			.cookie(HEADER_WEB_REFRESH_TOKEN, '', { maxAge: -1 });
	}
}
