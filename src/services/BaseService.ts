import { AppLogger } from '~lib/AppLogger';

/**
 * При создании сервиса передаем ему объект реквеста. Это дает возможности:
 * - получить текущего авторизованного пользователя, его id и accountId
 * - создать правильный логгер
 */
export default class BaseService
{
	private declare _logger: AppLogger;

	constructor(
		private readonly _req: any,
		private readonly _moduleLogger: AppLogger,
	) {}

	get req() {
		return this._req;
	}

	get logger(): AppLogger {
		if (!this._logger) {
			this._logger = this.req.getLogger(this._moduleLogger);
		}
		return this._logger!;
	}
}
