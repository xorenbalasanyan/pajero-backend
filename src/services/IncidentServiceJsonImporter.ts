import util from 'util';
import FirebaseAdmin from 'firebase-admin';
import { ONE_DAY_MS } from '~enums/DateEnum';
import { PARSE_INPUT_DATA_EXCEPTION } from '~errors/ResponseError';
import {
	GoodRepository,
	IncidentRepository,
	ShopRepository,
	UserRepository,
	UserSettingRepository,
} from '~repositories';
import { isFirebaseInitialized } from '~lib/initFirebaseAdmin';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import { Good, Incident, IncidentImport, Log } from '../stores/db/entity';
import { distinctListByField } from '~utils/list';

const moduleLogger = new AppLogger('IncidentServiceJsonImporter');

export default class IncidentServiceJsonImporter
{
	/**
	 * Выполняет импорт задач и заполняет Response
	 */
	static async importIncidentTasksFromJSON(req, res, date, incidentType, importData) {
		const user = req.getAuthorizedUser();
		const logger = req.getLogger(moduleLogger);
		// объект импорта
		const incidentImport = new IncidentImport({
			accountId: user.accountId,
			date,
			incidentType,
			addedCount: 0,
			ownerId: user.id,
		});
		// список предупреждений
		const warnings = {
			badCodes: [],
			noGoodName: [],
			newGood: [],
			noLostMarkup: [],
			noAvailBal: [],
			noPrice: [],
			notExistShop: [],
			existTask: [],
			noInserts: false,
		};
		return Promise.resolve({
			req,
			res,
			date,
			incidentType,
			importData,
			incidentImport,
			warnings,
			logger,
			user,
		})
			.then(readFromImportData)
			.then(filterData)
			.then(checkAndAddGoods)
			.then(createIncidents)
			.then(pushNotifiesAboutNewIncidents)
			.then(fillImportEvent)
			.catch(async (error) => {
				logger.error(fillError(Error, `Failed parse import file ${incidentImport.fileId} with error:`, error));
				const filledWarns = (makeWarnings(warnings) || []) as any[];
				filledWarns.push(`Catched exception:\\n${String(error)}`);
				const log = await makeLogForWarns(filledWarns, user, 'IMPORT_TASK_LIST'); // TODO change to LogTypeEnum values
				incidentImport.logId = log ? log.id : undefined;
				incidentImport.warnCount = -1;
				incidentImport.state = 'ERROR'; // TODO change to IncidentImportStateEnum values
				await incidentImport.save();
				return res.sendHttpError(PARSE_INPUT_DATA_EXCEPTION, { warnings: filledWarns });
			});
	}
}

async function readFromImportData(o) {
	const { logger, importData, warnings } = o;
	logger.info('Read import data');
	const data: any[] = [];
	importData.forEach((
		{
			shopExternalId,
			goodExternalId,
			goodName,
			ostatok,
			upPrib,
		},
		rowNumber,
	) => {
		const item = {
			rowNumber,
			shopExternalId,
			goodExternalId,
			goodName,
			data: {
				lostMarkup: Number(upPrib),
				availableBalances: Number(ostatok),
			},
		};
		// check
		if (!item.shopExternalId || !item.goodExternalId) {
			return warnings.badCodes.push(rowNumber);
		}
		if (!item.goodName || item.goodName === 'null') {
			return warnings.noGoodName.push(rowNumber);
		}
		if (!item.data.lostMarkup) {
			return warnings.noLostMarkup.push(rowNumber);
		}
		if (!item.data.availableBalances) {
			return warnings.noAvailBal.push(rowNumber);
		}
		// TODO добавить проверку на цену
		// if (!item.data.price) return warnings.noPrice.push(rowNumber);
		data.push(item);
	});
	// update o
	o.data = data;
	return o;
}

async function filterData(o) {
	const { incidentType, warnings, date, user } = o;
	let { data } = o;
	// проверка и фильтр по магазинам
	const shopExIds = Array.from(data.reduce((set, i) => set.add(i.shopExternalId), new Set()));
	const existShops = await ShopRepository.findAllByAccountIdAndExternalIds(user.accountId, shopExIds);
	const existShopExIdSet = new Set(existShops.map(i => i.externalId));
	data = data.filter(({ shopExternalId, rowNumber }) => {
		const has = existShopExIdSet.has(shopExternalId);
		if (!has) warnings.notExistShop.push(rowNumber);
		return has;
	});
	// проверка и фильтр от дублей инцидентов
	/* TODO xxx
	const existIncidentSet = await IncidentRepository
		.findAllWithShopAndGoodByDateAndShopIds(date, existShops.map(i => i.id))
		.then(items => items.reduce((set, i) => set.add(`${i.date}_${i.incidentTypeId}_${i.fk_shop.externalId}_${i.fk_good.externalId}`), new Set()));
	 */
	const existIncidentSet = new Set();
	data = data.filter(({ rowNumber, shopExternalId, goodExternalId }) => {
		const has = existIncidentSet.has(`${date}_${incidentType.id}_${shopExternalId}_${goodExternalId}`);
		if (has) warnings.existTask.push(rowNumber);
		return !has;
	});
	// update o
	o.data = data;
	o.existShops = existShops;
	return o;
}

async function checkAndAddGoods(o) {
	if (!o.data || !o.data.length) return o;
	const { logger, req, user, warnings, data } = o;
	// ищем номенклатуру, которая есть в файле импорта и отсутствует в БД
	const goodExIds: string[] = distinctListByField(data, 'goodExternalId');
	logger.debug(util.format('goodExIds = [%s]', goodExIds.join(',')));
	const goodRepository = new GoodRepository(req);
	const existGoods = await goodRepository.findAllByExternalIds(goodExIds);
	const existGoodExIdSet = new Set(existGoods.map(i => i.externalId));
	logger.debug(util.format('existGoodExIds = [%s]', Array.from(existGoodExIdSet).join(',')));
	const newGoods = data.reduce((map, i) => {
		const has = existGoodExIdSet.has(i.goodExternalId);
		if (!has) {
			map.set(i.goodExternalId, i.goodName);
			warnings.newGood.push(i.rowNumber);
		}
		return map;
	}, new Map());
	logger.info(util.format('newGoods = %o', newGoods));
	if (newGoods.size) {
		await Good.bulkCreate(Array.from(newGoods.keys()).map(externalId => ({
			externalId,
			accountId: user.accountId,
			name: newGoods.get(externalId),
			ownerId: user.id,
		})));
	}
	// update o
	o.goodExIds = goodExIds;
	return o;
}

async function createIncidents(o) {
	if (!o.data || !o.data.length) return o;
	const { logger, req, date, incidentType, data, user, goodExIds, existShops } = o;
	// fill the incidents, map the tables
	const goodRepository = new GoodRepository(req);
	const goods = await goodRepository.findAllByExternalIds(goodExIds);
	const shopExIdTable = existShops.reduce((map, i) => map.set(i.externalId, i.id), new Map());
	const goodExIdTable = goods.reduce((map, i) => map.set(i.externalId, i.id), new Map());
	// save incidents
	logger.debug(util.format('createIncidents import data = %o', data));
	// сохраняем инциденты
	await Incident.bulkCreate(data.map(item => {
		const {
			shopExternalId,
			goodExternalId,
		} = item;
		const inputData = { ...item };
		delete inputData.rowNumber;
		delete inputData.shopExternalId;
		delete inputData.goodExternalId;
		delete inputData.goodName;
		return {
			shopId: shopExIdTable.get(shopExternalId),
			incidentTypeId: incidentType.id,
			goodId: goodExIdTable.get(goodExternalId),
			date,
			inputData: JSON.stringify(inputData),
			ownerId: user.id,
		};
	}));
	// update o
	o.shopExIdTable = shopExIdTable;
	return o;
}

async function fillImportEvent(o) {
	const { data, warnings, user, incidentImport, logger } = o;
	// обогащаем и сохраняем IncidentImport событие
	const filledWarns = makeWarnings(warnings);
	const log = await makeLogForWarns(filledWarns, user, 'IMPORT_TASK_LIST'); // TODO change to LogTypeEnum values
	incidentImport.logId = log ? log.id : undefined;
	if (data && data.length) incidentImport.addedCount = data.length;
	incidentImport.warnCount = warnings.badCodes.length + warnings.noGoodName.length + warnings.noLostMarkup.length
		+ warnings.noAvailBal.length + warnings.noPrice.length + warnings.notExistShop.length;
	incidentImport.state = log ? 'WARN' : 'OK'; // TODO change to IncidentImportStateEnum values
	await incidentImport.save();
	logger.info(`Завершен импорт инцидентов. Результаты сохранены в IncidentImport #${incidentImport.id}.`);
	// update o
	o.log = log;
	return o;
}

async function pushNotifiesAboutNewIncidents(o) {
	if (!o.data || !o.data.length || !isFirebaseInitialized()) return o;
	const { logger, user, shopExIdTable } = o;
	// по дате определяем ДМ
	const shopToCountMap = o.data.reduce((map, { shopExternalId }) => {
		const shopId = shopExIdTable.get(shopExternalId);
		const count = (map.get(shopId) || 0) + 1;
		return map.set(shopId, count);
	}, new Map());
	logger.debug(util.format('shopToCountMap = %o', shopToCountMap));
	// список ДМ
	/* TODO xxx
	const shopUsers = await UserRepository.findAllDmAndZdmUsersByAccountIdAndShopIds(user.accountId, Array.from(shopToCountMap.keys()));
	 */
	const shopUsers: any[] = [];
	// список FCM
	const userFcms = await UserSettingRepository.findAllFcmTokensByUserIds(shopUsers.map(i => i.id));
	const userIdToFcmTable = userFcms.reduce((map, i) => map.set(i.userId, i.value), new Map());
	logger.debug(util.format('userIdToFcmTable = %o', userIdToFcmTable));
	// collect
	const notifications = shopUsers.reduce((arr, shopUser) => {
		const fcmToken = userIdToFcmTable.get(shopUser.id);
		if (!fcmToken) return arr;
		const addedCount = shopToCountMap.get(shopUser.shopId);
		const item = {
			token: fcmToken,
			notification: {
				title: '📋 Новые задачи',
				body: `В магазине появились новые задачи: ${addedCount}`,
			},
			android: {
				ttl: ONE_DAY_MS, // завтра уже не нужно отправлять
			},
		};
		arr.push(item);
		return arr;
	}, []);
	logger.debug(util.format('notifications = %o', notifications));
	// send notification messages
	if (notifications && notifications.length) {
		// firebase может отправлять за раз не более 500 уведомлений
		let part = 1;
		while (notifications.length > 0) {
			const cuttedNotifications = notifications.splice(0, 500);
			await FirebaseAdmin.messaging()
				.sendAll(cuttedNotifications)
				.then((response) => {
					logger.info(util.format('[Уведомление о новых инцидентах] Результат отправки (партия %d) в Firebase: %o', part++, response));
				})
				.catch(error => {
					logger.error(fillError(Error, '[Ошибка отправки уведомлений в Firebase]', error));
				});
		}
	}
	return o;
}

function makeWarnings(w) {
	const calcPeriods = (arr) => {
		if (arr.length === 1) {
			return `В строке ${arr[0]}`;
		}
		arr = Array.from(new Set(arr));
		arr.push('end');
		const res1 = arr.reduce((acc, i) => {
			if (i === 'end') {
				if (acc.first === null) {
					acc.items.push(acc.last);
				} else {
					acc.items.push(`${acc.first}-${acc.last}`);
				}
			} else if (acc.last === i - 1) {
				if (acc.first === null) {
					acc.first = acc.last;
				}
			} else if (acc.first !== null) {
				acc.items.push(`${acc.first}-${acc.last}`);
				acc.first = null;
			} else if (acc.last !== null) {
				acc.items.push(acc.last);
			}
			acc.last = i;
			return acc;
		}, { first: null, last: null, items: [] });
		return `В строках [${res1.items.join(', ')}]`;
	};
	const res: string[] = [];
	if (w.badCodes.length) {
		res.push(`${calcPeriods(w.badCodes)} неверно заполнены коды, эти записи не будут добавлены.`);
	}
	if (w.missingRequiredValues && Object.keys(w.missingRequiredValues).length) {
		Object.keys(w.missingRequiredValues).forEach(columnName => {
			res.push(`${calcPeriods(w.missingRequiredValues[columnName])} в столбце '${columnName}' `
				+ 'отсутствуют обязательные значения, эти записи не будут добавлены.');
		});
	}
	if (w.noGoodName.length) {
		res.push(`${calcPeriods(w.noGoodName)} не указана номенклатура, эти записи не будут добавлены.`);
	}
	if (w.newGood.length) {
		res.push(`${calcPeriods(w.newGood)} обнаружена номенклатура, которая отсутствовала и будет добавлена в базу.`);
	}
	if (w.noLostMarkup.length) {
		res.push(`${calcPeriods(w.noLostMarkup)} не указано значение для упущенной прибыли, эти записи не будут добавлены.`);
	}
	if (w.noAvailBal.length) {
		res.push(`${calcPeriods(w.noAvailBal)} не указано значение для остатков , эти записи не будут добавлены.`);
	}
	if (w.noPrice.length) {
		res.push(`${calcPeriods(w.noPrice)} не указано значение цена, эти записи не будут добавлены.`);
	}
	if (w.notExistShop.length) {
		res.push(`${calcPeriods(w.notExistShop)} указан магазин, которого нет в базе данных, эти записи не будут добавлены.`);
	}
	if (w.existTask.length) {
		res.push(`${calcPeriods(w.existTask)} указана задача, которая уже была раньше добавлена в базу, эти записи не будут добавлены.`);
	}
	if (w.noInserts) {
		res.push('Ни одна запись не была добавлена.');
	}
	return res.length ? res : null;
}

async function makeLogForWarns(warnList, user, type) {
	if (!warnList) return null;
	return await Log.create({
		accountId: user.accountId,
		userId: user.id,
		type,
		message: warnList.join('\n'),
		ownerId: user.id,
	});
}
