import { questionnaireExecutionToShopStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('QuestionnaireExecutionToShopService');

export default class QuestionnaireExecutionToShopService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, questionnaireExecutionToShopStore);
	}
}
