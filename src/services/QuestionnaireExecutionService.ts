import { questionnaireExecutionStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('QuestionnaireExecutionService');

export default class QuestionnaireExecutionService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, questionnaireExecutionStore);
	}
}
