import fs from 'fs';
import BaseEntityService from './BaseEntityService';
import { checklistTaskStore, fileTypeStore, shopStore } from '~stores';
import { FileService } from '~services';
import { AppLogger } from '~lib/AppLogger';
import { arrayToMapByField } from '~utils/list';
import sequelize from '~lib/sequelize';
import { ChecklistTask, File } from '../stores/db/entity';
import ResponseError from '~errors/ResponseError';

const moduleLogger = new AppLogger('ChecklistTaskImportService');

export default class ChecklistTaskImportService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, checklistTaskStore);
	}

	importAllChecklistTasks = async (checklistTasks: any[]) => {
		const currentUser = this.req.getAuthorizedUser();
		// check exist shops
		const shopExternalIds = new Set(checklistTasks.map(i => i.shopId));
		const existsShops = await shopStore.findAll({
			where: { externalId: Array.from(shopExternalIds) },
			raw: true,
			attributes: ['id', 'externalId'],
		});
		existsShops.forEach(({ externalId }) => shopExternalIds.delete(externalId));
		if (shopExternalIds.size) {
			throw ResponseError.notFound(
				`В базе отсутствуют следующие магазины: ${JSON.stringify(Array.from(shopExternalIds))}. Импорт задач не выполнен.`);
		}
		const existsShopMap = arrayToMapByField(existsShops, 'externalId');
		// get exist tasks
		const taskIds = checklistTasks.map(i => i.taskId);
		const existTasks = await this.store.findAll({
			where: { taskId: taskIds },
		});
		const existsTasksMap = arrayToMapByField(existTasks, 'taskId');
		const newTasks: any[] = [];
		const updateTasks = new Set<typeof ChecklistTask>();
		for (const importTask of checklistTasks) {
			const { taskId } = importTask;
			const existsTask = existsTasksMap.get(String(taskId));
			if (existTasks.length > 0) {
				// update
				if (importTask.departmentName !== undefined && importTask.departmentName !== existsTask.departmentName) {
					existsTask.departmentName = importTask.departmentName;
					updateTasks.add(existsTask);
				}
				if (importTask.checkListName !== undefined && importTask.checkListName !== existsTask.checkListName) {
					existsTask.checkListName = importTask.checkListName;
					updateTasks.add(existsTask);
				}
				if (importTask.checkListQuestion !== undefined && importTask.checkListQuestion !== existsTask.checkListQuestion) {
					existsTask.checkListQuestion = importTask.checkListQuestion;
					updateTasks.add(existsTask);
				}
				if (importTask.taskClosedAt !== undefined && importTask.taskClosedAt !== existsTask.taskCLosedAt) {
					existsTask.taskCLosedAt = importTask.taskCLosedAt;
					updateTasks.add(existsTask);
				}
				if (importTask.creatorFullname !== undefined && importTask.creatorFullname !== existsTask.creatorFullname) {
					existsTask.creatorFullname = importTask.creatorFullname;
					updateTasks.add(existsTask);
				}
				if (importTask.status !== undefined && importTask.status !== existsTask.status) {
					existsTask.status = importTask.status;
					updateTasks.add(existsTask);
				}
				if (importTask.taskDeadlineAt !== undefined && importTask.taskDeadlineAt !== existsTask.taskDeadlineAt) {
					existsTask.taskDeadlineAt = importTask.taskDeadlineAt;
					const nowDate = Date.now();
					const taskDeadlineAtMom = new Date(importTask.taskDeadlineAt).getTime();
					if (taskDeadlineAtMom < nowDate) {
						existsTask.isExpired = true;
					}
					updateTasks.add(existsTask);
				}
				if (importTask.executorComment !== undefined && importTask.executorComment !== existsTask.executorComment) {
					existsTask.executorComment = importTask.executorComment;
					updateTasks.add(existsTask);
				}
				if (importTask.taskClosedAt !== undefined && importTask.taskClosedAt !== existsTask.taskClosedAt) {
					existsTask.taskClosedAt = importTask.taskClosedAt;
					updateTasks.add(existsTask);
				}
			} else {
				// create new one
				newTasks.push({
					...importTask,
					shopId: existsShopMap.get(importTask.shopId).id,
					isNeedUpdateMedia: (importTask.questionPhotos?.length || 0) > 0,
					isReportSent: null,
				});
			}
		}
		// saving
		return sequelize.transaction(async transaction => {
			for (const item of Array.from(updateTasks.values())) {
				item.moderatorId = currentUser.id;
				await item.save({ transaction });
			}
			for (const item of newTasks) {
				await this.createOne(item, transaction);
			}
		});
	}

	importChecklistPhoto = async (photoData: any) => {
		const { taskId, photoId, contentType, contentData } = photoData;
		const task = await this.store.findOne({
			where: { taskId },
		});
		if (!task) {
			throw ResponseError.notFound(`Задача с номером "${taskId}" не существует`);
		}
		const bufferData = Buffer.from(contentData, 'base64');
		const svc = new FileService(this.req);
		const existPhoto = await svc.findOne({
			where: { externalId: photoId },
			attributes: ['id'],
			raw: true,
		});
		if (!existPhoto) {
			const currentUser = this.req.getAuthorizedUser();
			const imageFileType = await fileTypeStore.findIdByMimeType(contentType);
			if (!imageFileType) {
				throw ResponseError.notFound(`Тип файла c contentType "${contentType}" не существует.`);
			}
			const {
				fileName: name,
				pathName,
				fullPath,
			} = FileService.generateNewFileName('', `.png`);
			fs.writeFileSync(fullPath, bufferData);
			const size = fs.statSync(fullPath).size;
			await sequelize.transaction(async transaction => {
				await File.create({
					externalId: photoId,
					accountId: currentUser.accountId,
					name,
					fileTypeId: imageFileType.id,
					size,
					pathName,
					ownerId: currentUser.id,
				}, { transaction });
			});
		}
		this.checkMediaForTask(task);
	}

	private async checkMediaForTask(task: typeof ChecklistTask): Promise<void> {
		if (!task.isNeedUpdateMedia) return;
		const questionPhotos = JSON.parse(task.questionPhotos);
		if (!questionPhotos) return;
		const svc = new FileService(this.req);
		const files = await svc.findAll({
			where: {
				externalId: questionPhotos,
			},
			raw: true,
			attributes: ['externalId'],
		});
		const fileIds = new Set(files.map(i => i.externalId));
		if (fileIds.size === questionPhotos.length) {
			await sequelize.transaction(async transaction => {
				await task.update({ isNeedUpdateMedia: false }, { transaction });
			});
		}
	}
}
