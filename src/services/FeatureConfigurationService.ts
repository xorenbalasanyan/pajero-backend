import { Op } from 'sequelize';
import { AppLogger } from '~lib/AppLogger';
import { featureConfigurationStore } from '~stores';
import BaseEntityService from './BaseEntityService';

const moduleLogger = new AppLogger('FeatureConfigurationService');

export default class FeatureConfigurationService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, featureConfigurationStore);
	}

	addWhereSearch = ({ where }, { searchParts }) => {
		if (!searchParts?.length) return;
		if (!where[Op.or]) where[Op.or] = [];
		searchParts.forEach(str => {
			where[Op.or].push({ id: { [Op.substring]: str } });
			where[Op.or].push({ title: { [Op.substring]: str } });
		});
	}
}
