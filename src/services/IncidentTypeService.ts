import { Op } from 'sequelize';
import { incidentTypeStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('IncidentTypeService');

export default class IncidentTypeService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, incidentTypeStore);
	}

	addWhereSearch = ({ where }, { searchParts }) => {
		if (!searchParts?.length) return;
		if (!where[Op.or]) where[Op.or] = [];
		searchParts.forEach(str => {
			where[Op.or].push({ id: { [Op.substring]: str } });
			where[Op.or].push({ name: { [Op.substring]: str } });
			where[Op.or].push({ type: { [Op.substring]: str } });
		});
	}
}
