import fs from 'fs';
import path from 'path';
import JSZip from 'jszip';
import util from 'util';
import { Response } from 'express';
import contentDisposition from 'content-disposition';
import guid from '~utils/guid';
import config from '~lib/config';
import { fileStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { File, FileType } from '../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';
import { FileLocationEnum } from '~enums/FileLocationEnum';

const moduleLogger = new AppLogger('FileService');

export default class FileService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, fileStore);
	}

	addMeta = async (options, pageInfo) => {
		// нужно ли прикрепить пользователей к выгрузке
		if (pageInfo.meta.has('filetypes')) {
			if (!options.include) options.include = [];
			const { fileType } = pageInfo.customParams || {};
			const inc: any = { model: FileType, as: 'fk_fileType', required: true };
			if (fileType) {
				inc.where = { ext: fileType };
			}
			options.include.push(inc);
		}
	}

	getFullPath = (filePathName: string) => path.resolve(config.get('paths:userdata'), filePathName);

	moveFileToDeleted = (pathName: string, fileName: string) => {
		const fullPath = this.getFullPath(pathName);
		const p1 = pathName.substr(0, 5);
		const newDir = path.resolve(config.get('paths:userdata-deleted'), p1);
		const newPath = path.resolve(newDir, fileName);
		moduleLogger.info(util.format('Перемещаем файл "%s" в "%s"', fullPath, newDir));
		fs.mkdirSync(newDir, { recursive: true });
		// перемещение методом rename не работает если файлы располагаются в разных смонтированных
		// файловых системах, поэтому делаем копию и удаляем старый файл
		fs.copyFileSync(fullPath, newPath);
		if (!fs.existsSync(newPath)) {
			throw new Error(util.format('Не удалось скопировать временный файл "%s" по пути "%s"', fullPath, newPath));
		}
		fs.unlinkSync(fullPath);
		if (fs.existsSync(fullPath)) {
			throw new Error(util.format('Файл "%s" переместился в "%s", но не удалился по старому пути', fullPath, newPath));
		}
	}

	downloadOne = async (file: typeof File, res: Response, userId: number): Promise<void> => {
		this.logger.debug(`User #${userId} download file #${file.id}`);
		if (file.location === FileLocationEnum.CDN) {
			return res.redirect(301, file.pathName);
		}
		const fullPath = this.getFullPath(file.pathName);
		if (!fs.existsSync(fullPath)) {
			res.status(404).end();
			return;
		}
		// картинки мутабельны и для них не нужно подсчитывать количество скачиваний,
		// поэтому можно добавить заголовки с кешем на 7 суток
		if (file.name.match(/\.(png|jpg|jpeg|gif)$/)) {
			// похоже не работает, запросы все равно идут на бэк
			res.set('Cache-control', 'public, max-age=592200');
			res.set('Expires', new Date(Date.now() + 592200e3).toUTCString());
		}
		res.setHeader('Content-disposition', contentDisposition(file.name));
		res.sendFile(fullPath);
		return file.update({
			downloadCount: file.downloadCount + 1,
			moderatorId: userId,
		});
	}

	/**
	 * Генерирует имя файла с указанным префиксом и постфиксом файла.
	 * @param filePrefix
	 * @param filePostfix
	 */
	static generateNewFileName = (filePrefix: string | undefined, filePostfix = '.tmp') => {
		const fileGuid = guid();
		const fileName = `${filePrefix || ''}${fileGuid}${filePostfix}`;
		const p1 = fileGuid.substr(0, 2);
		const p2 = fileGuid.substr(2, 2);
		const _dirPath = path.resolve(config.get('paths:userdata'), p1, p2);
		fs.mkdirSync(_dirPath, { recursive: true });
		const pathName = path.join(p1, p2, fileName);
		const fullPath = path.resolve(config.get('paths:userdata'), pathName);
		return { fileGuid, fileName, pathName, fullPath };
	}

	/**
	 * Архивирует файл
	 * @param fileName Имя файла в архиве
	 * @param sourceFileName Расположение исходного файла
	 * @param destFileName Куда сохранить результат
	 * @returns {Promise<unknown>}
	 */
	static zipOneFile = async (fileName, sourceFileName, destFileName) => {
		const zip = new JSZip();
		zip.file(fileName, fs.readFileSync(sourceFileName));
		return new Promise(resolve => {
			zip
				.generateNodeStream({
					type: 'nodebuffer',
					streamFiles: true,
					compression: 'DEFLATE',
					compressionOptions: {
						level: 9,
					},
				})
				.pipe(fs.createWriteStream(destFileName))
				.on('finish', resolve);
		});
	}
}
