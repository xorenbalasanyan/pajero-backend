import { Transaction } from 'sequelize';
import { ChecklistTaskStatusEnum } from '~enums/ChecklistTaskStatusEnum';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS } from '~errors/ResponseError';
import { copyFields } from '~utils/copyFields';
import { dateAddDays, dateSetTimeForTimeZone } from '~utils/date';
import { validateInteger, validateIsPositiveNumber, validateString } from '~utils/validate';
import sequelize from '~lib/sequelize';
import { ShopService } from '~services';
import { checklistTaskStore, userStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';
import {User} from "../stores/db/entity";

const moduleLogger = new AppLogger('ChecklistTaskService');

function validateOneChecklistTaskData(checklistTaskData) {
	const availableKeys = ['title', 'description', 'executionPeriodDays', 'subordinateShopIds'];
	const data = copyFields(checklistTaskData, {}, availableKeys);
	if (!data) {
		throw NOT_ALL_REQUIRED_FIELDS;
	}
	if (validateString(5, 200)(data.title)) {
		throw ResponseError.validationInput('Поле title должно быть строкой длиной от 5 до 200');
	}
	if (data.description != null && validateString(0, 2000)(data.description)) {
		throw ResponseError.validationInput('Поле description может быть null или должно быть строкой длиной от 0 до 2000');
	}
	if (validateInteger(1, 15)(data.executionPeriodDays)) {
		throw ResponseError.validationInput('Поле executionPeriodDays должно быть целым числом от 1 до 15');
	}
	if (!data.subordinateShopIds || data.subordinateShopIds.constructor.name !== 'Array') {
		throw ResponseError.validationInput('Поле subordinateShopIds должно быть массивом');
	} else {
		const isPositive = validateIsPositiveNumber();
		if (data.subordinateShopIds.some(isPositive)) {
			throw ResponseError.validationInput('Поле subordinateShopIds должно быть массивом с целыми числами');
		}
	}
}

export default class ChecklistTaskService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, checklistTaskStore);
	}

	private async createOneWithTransaction(checklistTask: any, shopService: ShopService, transaction: Transaction) {
		const currentUser = this.req.getAuthorizedUser();
		checklistTask = {
			...checklistTask,
			accountId: currentUser.accountId,
			ownerId: currentUser.id,
			title: checklistTask.title.trim(),
			description: checklistTask.description?.trim() || null,
		};
		const { subordinateShopIds, ...checklistTaskParts } = checklistTask;
		// create ChecklistTask
		const savedItem = await this.store.createOne(checklistTaskParts, transaction);
		// проверяем магазины и получаем часовой пояс магазина, чтобы рассчитать dueDate
		const shops = await shopService.findAllByIds(subordinateShopIds, transaction);
		if (shops.length !== subordinateShopIds.length) {
			throw ResponseError.validationInput('Один из магазинов не найден');
		}
		// eslint-disable-next-line no-restricted-syntax
		const newSubs = shops.map(shop => ({
			checklistTaskId: savedItem.id,
			shopId: shop.id,
			status: ChecklistTaskStatusEnum.STARTED,
			dueDate: dateAddDays(dateSetTimeForTimeZone(new Date(), shop.timeOffset, 0, 0, 0, 0), checklistTask.executionPeriodDays),
		}));
		return savedItem;
	}
	findAllTasksForCurrentShop = async (options: any): Promise<any> => {
		options.where.isNeedUpdateMedia = false;
		const tasks = await this.store.findAll({
			...options,
			attributes: ['id', 'checkListName'],
			include: [
				{
					model: User,
					as: 'fk_executor',
					attributes: ['id', 'middleName', 'lastName', 'firstName'],
					required: false,
				},
			],
		});
		return tasks;
	}
}
