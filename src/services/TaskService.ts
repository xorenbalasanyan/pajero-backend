import { Transaction } from 'sequelize';
import { TaskStatusEnum } from '~enums/TaskStatusEnum';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS } from '~errors/ResponseError';
import { copyFields } from '~utils/copyFields';
import { dateAddDays, dateSetTimeForTimeZone } from '~utils/date';
import { validateInteger, validateIsPositiveNumber, validateString } from '~utils/validate';
import sequelize from '~lib/sequelize';
import { ShopService } from '~services';
import { freshTaskView, taskStore, taskToShopStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('TaskService');

function validateOneTaskData(taskData) {
	const availableKeys = ['title', 'description', 'executionPeriodDays', 'subordinateShopIds'];
	const data = copyFields(taskData, {}, availableKeys);
	if (!data) {
		throw NOT_ALL_REQUIRED_FIELDS;
	}
	if (validateString(5, 200)(data.title)) {
		throw ResponseError.validationInput('Поле title должно быть строкой длиной от 5 до 200');
	}
	if (data.description != null && validateString(0, 2000)(data.description)) {
		throw ResponseError.validationInput('Поле description может быть null или должно быть строкой длиной от 0 до 2000');
	}
	if (validateInteger(1, 15)(data.executionPeriodDays)) {
		throw ResponseError.validationInput('Поле executionPeriodDays должно быть целым числом от 1 до 15');
	}
	if (!data.subordinateShopIds || data.subordinateShopIds.constructor.name !== 'Array') {
		throw ResponseError.validationInput('Поле subordinateShopIds должно быть массивом');
	} else {
		const isPositive = validateIsPositiveNumber();
		if (data.subordinateShopIds.some(isPositive)) {
			throw ResponseError.validationInput('Поле subordinateShopIds должно быть массивом с целыми числами');
		}
	}
}

export default class TaskService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, taskStore);
	}

	private async createOneWithTransaction(task: any, shopService: ShopService, transaction: Transaction) {
		const currentUser = this.req.getAuthorizedUser();
		task = {
			...task,
			accountId: currentUser.accountId,
			ownerId: currentUser.id,
			title: task.title.trim(),
			description: task.description?.trim() || null,
		};
		const { subordinateShopIds, ...taskParts } = task;
		// create Task
		const savedItem = await this.store.createOne(taskParts, transaction);
		// проверяем магазины и получаем часовой пояс магазина, чтобы рассчитать dueDate
		const shops = await shopService.findAllByIds(subordinateShopIds, transaction);
		if (shops.length !== subordinateShopIds.length) {
			throw ResponseError.validationInput('Один из магазинов не найден');
		}
		// eslint-disable-next-line no-restricted-syntax
		const newSubs = shops.map(shop => ({
			taskId: savedItem.id,
			shopId: shop.id,
			status: TaskStatusEnum.IN_PROGRESS,
			dueDate: dateAddDays(dateSetTimeForTimeZone(new Date(), shop.timeOffset, 0, 0, 0, 0), task.executionPeriodDays),
		}));
		await taskToShopStore.createAll(newSubs, transaction);
		return savedItem;
	}

	/**
	 * Создание списка Task
	 */
	async createItems(tasks) {
		tasks.forEach(validateOneTaskData);
		return sequelize.transaction(async transaction => {
			const result: any[] = [];
			const shopService = new ShopService(this.req);
			for (const task of tasks) {
				result.push(await this.createOneWithTransaction(task, shopService, transaction));
			}
			return result;
		});
	}

	/**
	 * Создание Task
	 */
	createOne = async task => {
		validateOneTaskData(task);
		const shopService = new ShopService(this.req);
		return sequelize.transaction(async transaction => {
			return await this.createOneWithTransaction(task, shopService, transaction);
		});
	}

	/**
	 * Селектим только не протухшие задачи (прошло не более 30 дней с даты протухания).
	 * @param options
	 * @param transaction
	 * @returns {Promise<any>}
	 */
	findAllFreshTasks = async (options, transaction?: Transaction): Promise<any[]> => freshTaskView
		.findAll(this._fillOptions(options), transaction)
}
