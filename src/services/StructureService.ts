import util from 'util';
import fs from 'fs';
import { Transaction } from 'sequelize';
import { StructureImportStateEnum } from '~enums/StructureImportStatusEnum';
import ResponseError from '~errors/ResponseError';
import ValidationError from '~errors/ValidationError';
import { checkExcessKeys, validateIsPositiveNumber, validateString } from '~utils/validate';
import sequelize from '~lib/sequelize';
import {
	AuthCredentialService,
	FileService,
	RegionService,
	ShopManagerRoleService,
	ShopManagerService,
	ShopService,
	UserService,
} from '~services';
import { fileTypeStore } from '~stores';
import { File, Log, Shop, ShopManagerRole, StructureImport, User } from '../stores/db/entity';
import BaseService from './BaseService';
import { AppLogger } from '~lib/AppLogger';
import { UserRoles } from '~enums/UserRoleEnum';
import { fillError } from '~utils/errors';

const checkPN = validateIsPositiveNumber();
const checkIdString = validateString(1, 100);
const checkId = v => checkPN(v) && checkIdString(v);
const checkUsername = validateString(5, 100);
const checkNamePart = validateString(1, 100);

const USER_AVAILABLE_KEYS = ['user_id', 'role', 'username', 'first_name', 'last_name',
	'middle_name', 'phone', 'email'];
const SHOP_AVAILABLE_KEYS = ['shop_id', 'df_user_id', 'tm_user_id', 'upf_user_id', 'dm_user_id',
	'zdm_user_id', 'name', 'region', 'city', 'address', 'phones', 'emails'];

const moduleLogger = new AppLogger('StructureService');

export default class StructureService extends BaseService
{
	constructor(req) {
		super(req, moduleLogger);
	}

	importUsersAndShops = async ({ users, shops }) => {
		const currentUser = this.req.getAuthorizedUser();
		const log: any = {
			accountId: currentUser.accountId,
			userId: currentUser.id,
			type: 'STRUCTURE_IMPORT',
			ownerId: currentUser.id,
		};
		const structureImport: any = {
			accountId: currentUser.accountId,
			ownerId: currentUser.id,
		};

		// сохраним входной файл и поместим его в архив
		const {
			fileName: rawFileName,
			fullPath: rawFilePath,
		} = FileService.generateNewFileName('', '.json');
		fs.writeFileSync(rawFilePath, JSON.stringify({ users, shops }));
		// zip
		const {
			fileName: name,
			pathName,
			fullPath,
		} = FileService.generateNewFileName('', '.zip');
		await FileService.zipOneFile(rawFileName, rawFilePath, fullPath);
		const size = fs.statSync(fullPath).size;
		// save file entity
		const zipFileType = await fileTypeStore.findIdByExt('zip');
		const savedFile: any = await File.create({
			name,
			size,
			fileTypeId: zipFileType.id,
			pathName,
			ownerId: this.req.getAuthorizedUser().id,
			accountId: this.req.getAuthorizedUser().accountId,
		});
		fs.unlinkSync(rawFilePath);
		structureImport.fileId = savedFile.id;

		this.logger.info(`Выполняется импорт структуры для файла #${savedFile.id}`);

		const stats = { // будем собирать статистику
			createdUsers: new Set<number>(),
			updatedUsers: new Set<number>(),
			deletedUsers: new Set<number>(),
			createdShops: new Set<number>(),
			updatedShops: new Set<number>(),
			deletedShops: new Set<number>(),
			warns: [],
		};
		if (!users?.length && !shops?.length) {
			throw new ValidationError('В теле запроса должно быть заполнено хотя бы одно поле: users или shops');
		}

		try {
			// validate users
			if (users?.length) {
				users.forEach(u => {
					const excessKeys: any[] | false = checkExcessKeys(u, USER_AVAILABLE_KEYS);
					if (excessKeys && excessKeys.length) {
						throw new ValidationError(util.format(
							'У пользователя заданы избыточные поля %o в объекте %o',
							excessKeys,
							u));
					}
					if (checkId(u.user_id)) {
						throw new ValidationError(util.format(
							'У пользователя не указано или ошибочно заполнено поле user_id: %o',
							u.user_id));
					}
					if (u.username !== undefined) {
						if (checkUsername(u.username)) {
							throw new ValidationError(util.format(
								'У пользователя %o ошибочно заполнено поле username: %o',
								u.user_id,
								u.username));
						}
					}
					if (u.first_name !== undefined) {
						if (checkNamePart(u.first_name)) {
							throw new ValidationError(util.format(
								'У пользователя %o ошибочно заполнено поле first_name: %o',
								u.user_id,
								u.first_name));
						}
					}
					if (u.last_name !== undefined) {
						if (checkNamePart(u.last_name)) {
							throw new ValidationError(util.format(
								'У пользователя %o ошибочно заполнено поле last_name: %o',
								u.user_id,
								u.last_name));
						}
					}
					if (u.middle_name !== undefined) {
						if (checkNamePart(u.middle_name)) {
							throw new ValidationError(util.format(
								'У пользователя %o ошибочно заполнено поле middle_name: %o',
								u.user_id,
								u.middle_name));
						}
					}
					if (u.phone !== undefined) {
						if (checkNamePart(u.phone)) {
							throw new ValidationError(util.format(
								'У пользователя %o ошибочно заполнено поле phone: %o',
								u.user_id,
								u.phone));
						}
					}
					if (u.email !== undefined) {
						if (checkNamePart(u.email)) {
							throw new ValidationError(util.format(
								'У пользователя %o ошибочно заполнено поле email: %o',
								u.user_id,
								u.email));
						}
					}
				});
			}

			// validate shops
			if (shops?.length) {
				shops.forEach((s: any) => {
					const excessKeys: any[] | false = checkExcessKeys(s, SHOP_AVAILABLE_KEYS);
					if (excessKeys && excessKeys.length) {
						throw new ValidationError(util.format('У магазина заданы избыточные поля %o в объекте %o', excessKeys, s));
					}
					if (checkId(s.shop_id)) {
						throw new ValidationError(util.format('У магазина не указано или ошибочно заполнено поле shop_id: %o', s.shop_id));
					}
					if (s.df_user_id !== undefined) {
						if (checkId(s.df_user_id)) {
							throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле df_user_id: %o', s.shop_id, s.df_user_id));
						}
					}
					if (s.tm_user_id !== undefined) {
						if (checkId(s.tm_user_id)) {
							throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле tm_user_id: %o', s.shop_id, s.tm_user_id));
						}
					}
					if (s.upf_user_id !== undefined) {
						if (checkId(s.upf_user_id)) {
							throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле upf_user_id: %o', s.shop_id, s.upf_user_id));
						}
					}
					if (s.dm_user_id !== undefined) {
						if (checkId(s.dm_user_id)) {
							throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле dm_user_id: %o', s.shop_id, s.dm_user_id));
						}
					}
					if (s.zdm_user_id !== undefined) {
						if (checkId(s.zdm_user_id)) {
							throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле zdm_user_id: %o', s.shop_id, s.zdm_user_id));
						}
					}
					if (s.name !== undefined) {
						if (checkNamePart(s.name)) {
							throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле name: %o', s.shop_id, s.name));
						}
					}
					if (s.region !== undefined) {
						if (checkNamePart(s.region)) {
							throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле region: %o', s.shop_id, s.region));
						}
					}
					if (s.city !== undefined) {
						if (checkNamePart(s.city)) {
							throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле city: %o', s.shop_id, s.city));
						}
					}
					if (s.address !== undefined) {
						if (checkNamePart(s.address)) {
							throw new ValidationError(util.format('У магазина %o ошибочно заполнено поле address: %o', s.shop_id, s.address));
						}
					}
					if (s.phones !== undefined) {
						if (s.phones.length) {
							s.phones = s.phones.filter(s1 => !!s1);
						}
						if (s.phones.length) {
							const bad = s.phones.find(i => checkNamePart(i));
							if (bad) {
								throw new ValidationError(util.format('Телефон для магазина %o ошибочно заполнен: %o', s.shop_id, bad));
							}
						}
					}
					if (s.emails !== undefined) {
						if (s.emails.length) {
							s.emails = s.emails.filter(s1 => !!s1);
						}
						if (s.emails.length) {
							const bad = s.emails.find(i => checkNamePart(i));
							if (bad) {
								throw new ValidationError(util.format('Email для магазина %o ошибочно заполнен: %o', s.shop_id, bad));
							}
						}
					}
				});
			}
		} catch (error: any) {
			// сохраняем информацию о неудачном импорте
			this.logger.error(fillError(Error, '[Ошибка импорта структуры]', error));
			Object.assign(log, {
				message: String(error),
				reason: 'ERROR',
			});
			structureImport.status = StructureImportStateEnum.ERROR;
			// save
			const newLog: any = await Log.create(log);
			Object.assign(structureImport, {
				logId: newLog.id,
				createdUsersCount: stats.createdUsers.size,
				updatedUsersCount: stats.updatedUsers.size,
				deletedUsersCount: stats.deletedUsers.size,
				createdShopsCount: stats.createdShops.size,
				updatedShopsCount: stats.updatedShops.size,
				deletedShopsCount: stats.deletedShops.size,
				warnCount: stats.warns.length,
			});
			await StructureImport.create(structureImport);
			throw error;
		}

		return sequelize.transaction(async transaction => {
			// выполняем загрузку пользователей
			const userService = new UserService(this.req);
			const shopService = new ShopService(this.req);
			const authCredentialService = new AuthCredentialService(this.req);
			if (users?.length) {
				for (const user of users) {
					let existUser = await userService.findOneByExternalId(user.user_id, transaction);
					const update: any = {};
					if (!existUser && user.username) {
						const existAuthCred = await authCredentialService
							.findOne({ where: { username: user.username } }, transaction);
						if (existAuthCred) {
							// по имени пользователя нашли нужного пользователя
							existUser = await userService.findOneById(existAuthCred.userId, transaction);
							update.externalId = user.user_id;
						}
					}
					if (!existUser) {
						// если пользователя еще нет, то проверяем обязательные поля
						['user_id', 'role', 'first_name', 'last_name']
							.forEach(key => {
								if (!user[key]) {
									throw ResponseError.validationInput(util.format(
										'Пользователь #%s не найден в базе. Чтобы создать нового пользователя, необходимо заполнить поле %o. '
										+ 'Транзакция не сохранена, измените и повторите запрос.',
										user.user_id,
										key,
									));
								}
							});
					}
					if (user.first_name) {
						const fn = user.first_name.trim();
						if (!existUser || fn !== existUser.firstName) {
							update.firstName = fn;
						}
					}
					if (user.last_name) {
						const ln = user.last_name.trim();
						if (!existUser || ln !== existUser.lastName) {
							update.lastName = ln;
						}
					}
					if (user.middle_name) {
						const mn = user.middle_name.trim() || null;
						if (!existUser || mn !== existUser.middleName) {
							update.middleName = mn;
						}
					}
					if (user.username) {
						// это должен быть уникальный username
						const newUsername = user.username.trim();
						const existAuthCred = await authCredentialService
							.findOne({ where: { username: user.username } }, transaction);
						// логин не должен повторяться
						// поэтому нужно выполнить несколько проверок на уже созданного пользователя с таким
						// же логином
						const isBad = existAuthCred && (!existUser || existUser.id !== existAuthCred.userId);
						if (isBad) {
							throw ResponseError.validationInput(util.format(
								'Пользователь с логином \'%s\' уже есть в базе и это другой пользователь текущего аккаунта.'
								+ ' Транзакция не сохранена, измените и повторите запрос.',
								user.username,
							));
						}
						if (!existAuthCred) {
							update.username = newUsername;
						}
					}
					if (user.phone) {
						const phone = user.phone.trim() || null;
						if (!existUser || phone !== existUser.phone) {
							update.phone = phone;
						}
					}
					if (user.email) {
						const email = user.email.trim() || null;
						if (!existUser || email !== existUser.email) {
							update.email = email;
						}
					}
					if (existUser) {
						const { username } = update;
						delete update.username;
						if (Object.keys(update).length > 0) {
							this.logger.info(`В рамках импорта структуры обновляется пользователь #${existUser.id} (${JSON.stringify(update)})`);
							await userService.updateOne(existUser, {
								...update,
								moderatorId: currentUser.id,
							}, transaction);
							stats.updatedUsers.add(existUser.id);
						}
						// update username
						if (username) {
							const existAuthCred = await authCredentialService.findOne({ where: { userId: existUser.id } }, transaction);
							if (existAuthCred) {
								// update
								this.logger.info(`В рамках импорта структуры выполняется обновление кредов входа для пользователя #${existAuthCred.userId}`);
								await authCredentialService.updateOne(existAuthCred, {
									username,
									moderatorId: currentUser.id,
								}, transaction);
								stats.updatedUsers.add(existUser.id);
							}
						}
					} else {
						this.logger.info(`В рамках импорта структуры создается новый пользователь (${JSON.stringify(update)})`);
						const newUser = await userService.createOne({
							externalId: user.user_id,
							role: UserRoles.MANAGER,
							...update,
							accountId: currentUser.accountId,
							ownerId: currentUser.id,
						}, transaction);
						stats.createdUsers.add(newUser.id);
					}
				}
			}
			// выполняем загрузку магазинов
			const regionService = new RegionService(this.req);
			const shopManagerService = new ShopManagerService(this.req);
			const shopManagerRoleService = new ShopManagerRoleService(this.req);
			const roles = await shopManagerRoleService.findAllWithAccountId({ where: {} }, transaction);
			if (shops?.length) {
				for (const shop of shops) {
					const existsShop = (await Shop.unscoped().findAll({
						where: {
							accountId: currentUser.accountId,
							externalId: shop.shop_id,
						},
						transaction,
					}))[0];
					const update: Partial<{
						name: string,
						regionId: number,
						city: string,
						address: string,
						phone: string,
						email: string,
					}> = {};
					const shopManagers: Partial<{
						dfId: number,
						tmId: number,
						upfId: number,
						dmId: number,
						zdmId: number,
					}> = {};
					// если магазина еще нет, то проверяем обязательные поля
					if (!existsShop) {
						['shop_id', 'name', 'region', 'city', 'address']
							.forEach(key => {
								if (!shop[key]) {
									throw ResponseError.validationInput(util.format(
										'Магазин #%s не найден в базе. Чтобы новый магазин появился в базе,'
										+ ' необходимо заполнить поле %o. Транзакция не сохранена, измените и повторите запрос.',
										shop.shop_id,
										key,
									));
								}
							});
						// для нового магазина должно быть указано одно из следующих полей
						const keys = ['upf_user_id', 'tm_user_id', 'df_user_id'];
						const count = keys.map(key => shop[key]).filter(i => !!i).length;
						if (count !== 1) {
							throw ResponseError.validationInput(util.format(
								'Магазин #%s не найден в базе. Чтобы новый магазин появился в базе,'
								+ ' необходимо заполнить только одно из полей %s. Транзакция не сохранена, измените и повторите запрос.',
								shop.shop_id,
								`'${keys.join('\', \'')}'`,
							));
						}
					}
					// проверка ДФ
					if (shop.df_user_id) {
						const dfUser = await userService.findOneByExternalId(shop.df_user_id, transaction);
						if (!dfUser || dfUser.role !== UserRoles.MANAGER) {
							throw ResponseError.notFound(util.format(
								'Магазин #%s не удалось обновить, потому что ДФ #%s не найден в базе. '
								+ 'Транзакция не сохранена, измените и повторите запрос.',
								shop.shop_id,
								shop.df_user_id,
							));
						}
						shopManagers.dfId = dfUser.id;
					}
					// проверка ТМ
					if (shop.tm_user_id) {
						const tmUser = await userService.findOneByExternalId(shop.tm_user_id, transaction);
						if (!tmUser || tmUser.role !== UserRoles.MANAGER) {
							throw ResponseError.notFound(util.format(
								'Магазин #%s не удалось обновить, потому что ТМ #%s не найден в базе. '
								+ 'Транзакция не сохранена, измените и повторите запрос.',
								shop.shop_id,
								shop.tm_user_id,
							));
						}
						shopManagers.tmId = tmUser.id;
					}
					// проверка УПФ
					if (shop.upf_user_id) {
						const upfUser = await userService.findOneByExternalId(shop.upf_user_id, transaction);
						if (!upfUser || upfUser.role !== UserRoles.MANAGER) {
							throw ResponseError.notFound(util.format(
								'Магазин #%s не удалось обновить, потому что УПФ #%s не найден в базе. '
								+ 'Транзакция не сохранена, измените и повторите запрос.',
								shop.shop_id,
								shop.upf_user_id,
							));
						}
						shopManagers.upfId = upfUser.id;
					}
					// проверка ДМ
					if (shop.dm_user_id) {
						const dmUser = await userService.findOneByExternalId(shop.dm_user_id, transaction);
						if (!dmUser || dmUser.role !== UserRoles.MANAGER) {
							throw ResponseError.notFound(util.format(
								'Магазин #%s не удалось обновить, потому что ДМ #%s не найден в базе. '
								+ 'Транзакция не сохранена, измените и повторите запрос.',
								shop.shop_id,
								shop.dm_user_id,
							));
						}
						shopManagers.dmId = dmUser.id;
					}
					// проверка ЗДМ
					if (shop.zdm_user_id) {
						const zdmUser = await userService.findOneByExternalId(shop.zdm_user_id, transaction);
						if (!zdmUser || zdmUser.role !== UserRoles.MANAGER) {
							throw ResponseError.notFound(util.format(
								'Магазин #%s не удалось обновить, потому что ЗДМ #%s не найден в базе. '
								+ 'Транзакция не сохранена, измените и повторите запрос.',
								shop.shop_id,
								shop.zdm_user_id,
							));
						}
						shopManagers.zdmId = zdmUser.id;
					}
					if (shop.name) {
						const sname = shop.name.trim();
						if (!existsShop || sname !== existsShop.name) update.name = sname;
					}
					if (shop.region) {
						const regionName = shop.region.trim();
						const existsRegion = await regionService.findOneByName(regionName, transaction);
						if (existsRegion) {
							if (!existsShop || existsRegion.id !== existsShop.regionId) {
								update.regionId = existsRegion.id;
							}
						} else {
							// создаем новый
							this.logger.info(`В рамках импорта структуры создается новый регион "${regionName}"`);
							const newRegion = await regionService.createOne({
								accountId: currentUser.accountId,
								name: regionName,
								ownerId: currentUser.id,
							}, transaction);
							update.regionId = newRegion.id;
						}
					}
					if (shop.city) {
						const city = shop.city.trim();
						if (!existsShop || existsShop.city !== city) {
							update.city = city;
						}
					}
					if (shop.address) {
						const adr = shop.address.trim();
						if (!existsShop || existsShop.address !== adr) {
							update.address = adr;
						}
					}
					if (shop.phones?.length) {
						const phone = shop.phones.map(s => s?.trim()).filter(s => !!s).join(', ');
						if (!existsShop || existsShop.phone !== phone) {
							update.phone = phone;
						}
					}
					if (shop.emails?.length) {
						const email = shop.emails.map(s => s?.trim()).filter(s => !!s).join(', ');
						if (!existsShop || existsShop.email !== email) {
							update.email = email;
						}
					}
					let newShop: typeof Shop;
					if (existsShop) {
						if (Object.keys(update).length > 0) {
							// update shop
							this.logger.info(`В рамках импорта структуры обновляется магазин #${existsShop.id} (${JSON.stringify(update)})`);
							await shopService.updateOne(existsShop, {
								...update,
								moderatorId: currentUser.id,
							}, transaction);
							stats.updatedShops.add(existsShop.id);
						}
					} else {
						// create shop
						const data = {
							...(shop.shop_id && { externalId: shop.shop_id }),
							...update,
						};
						this.logger.info(`В рамках импорта структуры создается новый магазин (${JSON.stringify(data)})`);
						newShop = await shopService.createOne({
							...data,
							accountId: currentUser.accountId,
							ownerId: currentUser.id,
							timeOffset: 300,
						}, transaction);
						stats.createdShops.add(newShop.id);
					}
					// проверяем и назначаем менеджеров
					if (shopManagers.dfId) {
						const role = roles.find(i => i.engShortTitle === 'DF');
						await checkAndUpdateShopManagerRole(shopManagerService, existsShop || newShop,
							transaction, role, currentUser, shopManagers.dfId, this.logger);
					}
					if (shopManagers.tmId) {
						const role = roles.find(i => i.engShortTitle === 'TM');
						await checkAndUpdateShopManagerRole(shopManagerService, existsShop || newShop,
							transaction, role, currentUser, shopManagers.tmId, this.logger);
					}
					if (shopManagers.upfId) {
						const role = roles.find(i => i.engShortTitle === 'UPF');
						await checkAndUpdateShopManagerRole(shopManagerService, existsShop || newShop,
							transaction, role, currentUser, shopManagers.upfId, this.logger);
					}
					if (shopManagers.dmId) {
						const role = roles.find(i => i.engShortTitle === 'DM');
						await checkAndUpdateShopManagerRole(shopManagerService, existsShop || newShop,
							transaction, role, currentUser, shopManagers.dmId, this.logger);
					}
					if (shopManagers.zdmId) {
						const role = roles.find(i => i.engShortTitle === 'ZDM');
						await checkAndUpdateShopManagerRole(shopManagerService, existsShop || newShop,
							transaction, role, currentUser, shopManagers.zdmId, this.logger);
					}
					if (newShop) {
						// для новых магазинов проверяем наличие ДМов
						const dmRole = roles.find(i => i.engShortTitle === 'DM');
						const createdDm = await checkShopUsersForNewShop(shopManagerService, userService,
							authCredentialService, existsShop || newShop, transaction, dmRole, currentUser, this.logger);
						if (createdDm) {
							stats.createdUsers.add(createdDm.id);
						}
					}
				}
			}
			// запоминаем статистику
			Object.assign(log, {
				message: 'Обновление структуры выполнено',
				reason: 'OK',
			});
			structureImport.status = StructureImportStateEnum.OK;
		}) // transaction
			.catch(error => {
				this.logger.error(fillError(Error, '[Ошибка импорта структуры]', error));
				Object.assign(log, {
					message: String(error),
					reason: 'ERROR',
				});
				structureImport.status = StructureImportStateEnum.ERROR;
			})
			.finally(async () => {
				// если есть текст в объекте лога, то сохраняем полную связку
				if (log.message) {
					const newLog: any = await Log.create(log);
					Object.assign(structureImport, {
						logId: newLog.id,
						createdUsersCount: stats.createdUsers.size,
						updatedUsersCount: stats.updatedUsers.size,
						deletedUsersCount: stats.deletedUsers.size,
						createdShopsCount: stats.createdShops.size,
						updatedShopsCount: stats.updatedShops.size,
						deletedShopsCount: stats.deletedShops.size,
						warnCount: stats.warns.length,
					});
					await StructureImport.create(structureImport);
				}
				if (log.reason === 'ERROR') {
					throw ResponseError.validationInput(log.message);
				}
			});
	}
}

/**
 * Проверяет и обновляет менеджера в магазине
 */
async function checkAndUpdateShopManagerRole(
	shopManagerService: ShopManagerService,
	savedShop: typeof Shop,
	transaction: Transaction,
	role: typeof ShopManagerRole,
	currentUser: typeof User,
	managerUserId: number,
	logger: AppLogger,
) {
	const sms = await shopManagerService.findAllWithoutAccountId({
		where: {
			shopId: savedShop.id,
			roleId: role.id,
		},
	}, transaction);
	if (!role.isShopUser) {
		// для роли супервайзера будем переназначать пользователя на эту роль
		// удаляем лишних
		const other = sms.filter(i => i.userId !== managerUserId);
		if (other.length) {
			logger.info(`В рамках импорта структуры удаляются связи менеджеров для магазина #${savedShop.id} (${JSON.stringify(other)})`);
			await shopManagerService.deleteWhere({
				shopId: savedShop.id,
				userId: other.map(sm => sm.userId),
			}, transaction);
		}
	}
	// проверяем, прицеплен ли наш пользователь
	const existSm = sms.find(i => i.userId === managerUserId);
	if (!existSm) {
		const data = {
			shopId: savedShop.id,
			roleId: role.id,
			userId: managerUserId,
		};
		logger.info(`В рамках импорта структуры создается связь менеджера для магазина #${savedShop.id} (${JSON.stringify(data)})`);
		await shopManagerService.createOne({
			...data,
			ownerId: currentUser.id,
		}, transaction);
	}
}

/**
 * Для новых магазинов проверяем наличие ДМов.
 * Если нет ни одного ДМ, то создаем нового пользователя по номеру магазина.
 */
async function checkShopUsersForNewShop(
	shopManagerService: ShopManagerService,
	userService: UserService,
	authCredentialService: AuthCredentialService,
	savedShop: typeof Shop,
	transaction: Transaction,
	role: typeof ShopManagerRole,
	currentUser: typeof User,
	logger: AppLogger,
): Promise<typeof User | undefined> {
	const sms = await shopManagerService.findAllWithoutAccountId({
		where: {
			shopId: savedShop.id,
			roleId: role.id,
		},
	}, transaction);
	if (!sms.length && savedShop.externalId) {
		logger.info(`В рамках импорта структуры создается новый ДМ для магазина #${savedShop.id}`);
		const newUser = await userService.createOne({
			accountId: currentUser.accountId,
			role: UserRoles.MANAGER,
			firstName: `ДМ #${savedShop.externalId}`,
			lastName: `Директор магазина`,
			comment: `Создан автоматически при создании магазина при импорте структуры`,
			ownerId: currentUser.id,
		}, transaction);
		await shopManagerService.createOne({
			shopId: savedShop.id,
			roleId: role.id,
			userId: newUser.id,
			ownerId: currentUser.id,
		}, transaction);
		await authCredentialService.createOne({
			accountId: currentUser.accountId,
			userId: newUser.id,
			username: savedShop.externalId,
			key: `mon${savedShop.externalId}`,
			ownerId: currentUser.id,
		}, transaction);
		return newUser;
	}
}
