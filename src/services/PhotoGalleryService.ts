import fs from 'fs';
import ResponseError from '~errors/ResponseError';
import sequelize from '~lib/sequelize';
import { FileService } from '~services';
import { fileStore, fileTypeStore, photoGalleryStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { File } from '../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';
import { FileLocationEnum } from '~enums/FileLocationEnum';

const moduleLogger = new AppLogger('PhotoGalleryService');

const validatePhotoContent = photoContent => {
	if (!/^data:image\/(png|jpg|jpeg|gif);base64,/.test(photoContent)) {
		throw ResponseError.validationInput('Поле data фотографии должно содержать данные картинки в кодировке base64. '
			+ 'Поддерживаемые mime-типы: image/png, image/jpg, image/jpeg, image/gif.');
	}
};

export default class PhotoGalleryService extends BaseEntityService {
	constructor(req) {
		super(req, moduleLogger, photoGalleryStore);
	}

	/**
	 * Загрузка фотографии для текущего пользователя
	 */
	uploadOneForCurrentUser = async (photoContent) => {
		validatePhotoContent(photoContent);
		const currentUser = this.req.getAuthorizedUser();
		return await sequelize.transaction(async transaction => {
			const [found, mimeType, imageFileExt] = photoContent.match('^data:(image/(png|jpg|jpeg|gif));base64,');
			const photoBuf = Buffer.from(photoContent.substring(found.length), 'base64');
			const imageFileType = await fileTypeStore.findIdByMimeType(mimeType, transaction);
			// проверка на тип файла
			if (!imageFileType) {
				throw new Error('Тип файла обязательно должен быть указан');
			}
			// картинка в файл
			const {
				fileName: name,
				pathName,
				fullPath,
			} = FileService.generateNewFileName('', `.${imageFileExt}`);
			fs.writeFileSync(fullPath, photoBuf);
			// проверка на корректную запись файла
			if (!fs.existsSync(fullPath)) {
				throw new Error(`Ошибка записи файла по пути "${fullPath}"`);
			}
			const size = fs.statSync(fullPath).size;
			const file = await File.create({
				name,
				size,
				fileTypeId: imageFileType.id,
				pathName,
				ownerId: currentUser.id,
				accountId: currentUser.accountId,
			}, { transaction });
			await this.store.createOne({
				fileId: file.id,
				ownerId: currentUser.id,
				accountId: currentUser.accountId,
			}, transaction);
		});
	}

	/**
	 * Получение списка фотографий для текущего пользователя
	 */
	getPhotoListForCurrentUser = async (): Promise<any[]> => {
		const currentUser = this.req.getAuthorizedUser();
		const photos = await this.store.findAll({
			where: { ownerId: currentUser.id },
			attributes: ['fileId'],
			raw: true,
		});
		if (!photos.length) return [];
		const files = await fileStore.findAll({
			where: { id: Array.from(new Set(photos.map(i => i.fileId))) },
			attributes: ['id', 'createdAt'],
			raw: true,
			order: [['createdAt', 'desc']],
		});
		if (!files.length) return [];
		return files.map(f => ({ fileId: f.id, createdAt: f.createdAt }));
	}

	/**
	 * Удаление фотографии для текущего пользователя
	 * @param item
	 * @returns {Promise<void>}
	 */
	deleteOneForCurrentUser = async (item) => {
		const currentUser = this.req.getAuthorizedUser();
		return await sequelize.transaction(async transaction => {
			// удаляем фото из галереи
			await this.store.deleteWhere({ id: item.id }, transaction);
			// удаляем ссылку на файл
			const fileService = new FileService(this.req);
			// Принадлежность файла к текущему пользователю
			const file = await fileService.findOne({ where: { id: item.fileId, ownerId: currentUser.id } }, transaction);
			await file.update({ isDeleted: true }, { transaction });
		});
	}
}
