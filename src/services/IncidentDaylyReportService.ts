import { Op } from 'sequelize';
import { incidentDaylyReportStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';
import { File } from '../stores/db/entity';

const moduleLogger = new AppLogger('IncidentDaylyReportService');

export default class IncidentDaylyReportService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, incidentDaylyReportStore);
	}

	addMeta = async (options, pageInfo) => {
		// нужно ли прикрепить файлы к выгрузке
		if (pageInfo.meta.has('files')) {
			if (!options.include) options.include = [];
			// required позволяет делать LEFT JOIN вместо INNER JOIN
			options.include.push({ model: File, as: 'fk_file', required: false });
		}
	}

	addWhereSearch = (options, { searchParts }) => {
		if (!searchParts?.length) return;
		const { where } = options;
		if (!where[Op.or]) where[Op.or] = [];
		// избавляемся от подзапросов, иначе не получится сделать поиск по полям
		options.subQuery = false;
		// для каждого поля добавляем поиск
		searchParts?.forEach(str => {
			where[Op.or].push({ id: { [Op.substring]: str } });
		});
	}
}
