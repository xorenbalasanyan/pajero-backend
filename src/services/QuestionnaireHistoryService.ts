import { questionnaireHistoryStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('QuestionnaireHistoryService');

export default class QuestionnaireHistoryService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, questionnaireHistoryStore);
	}
}
