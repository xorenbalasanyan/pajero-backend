import path from 'path';
import moment from 'moment';
import nodemailer from 'nodemailer';
import { Op } from 'sequelize';
import config from '~lib/config';
import { File } from '../stores/db/entity';
import StructureAnalisysStats from '../background/backgroundTasks/StructureAnalysisStats';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const logger = new AppLogger('MailService');

async function sendMessage({ from, to, subject, html, text, attachments }) {
	return new Promise((resolve, reject) => {
		const transporter = nodemailer.createTransport(config.get('smtp-noreply:transport'));
		const mailOptions = {
			from: from || config.get('smtp-noreply:from'),
			to,
			subject,
			html,
			text,
			attachments,
		};
		transporter.sendMail(mailOptions, error => error ? reject(error) : resolve(undefined));
	});
}

function h1(text: string): string {
	return `<h1 style="font-family: Lato, Arial, Helvetica, sans-serif; font-size: 23px; font-weight: 500; color: #000000;">${text}</h1>`;
}

function h2(text: string): string {
	return `<h2 style="font-family: Lato, Arial, Helvetica, sans-serif; font-size: 20px; font-weight: 500; color: #111111;">${text}</h2>`;
}

const INCIDENT_DAYLY_REPORT_CONTENT_TEMPLATE = `
	${h1('Добрый день, %fullname%!')}
	<p>
		Для вас подготовлен отчет по инцидентам за %date%.
	</p>
	<p>
		Файл отчета прикреплен к письму.
	</p>`;

const QUESTIONNAIRE_REPORT_CONTENT_TEMPLATE = `
	${h1('Добрый день!')}
	<p>
		Для вас подготовлен отчет по опросу "%title%".
	</p>
	<p>
		Файл отчета прикреплен к письму.
	</p>`;

const STRUCTURE_ANALYSIS_REPORT_CONTENT_TEMPLATE = `
	${h1('Добрый день!')}
	<p>
		Для вас подготовлен отчет по результатам анализа струтуры за %reportDate%.
	</p>
	${h2('⛔️&nbsp;&nbsp;Ошибки')}
	<p>Количество ошибок: %errorCount%</p>
	<ol>%errorText%</ol>
	${h2('⚠️&nbsp;&nbsp;Предупреждения')}
	<p>Количество предупреждений: %warnCount%</p>
	<ol>%warnText%</ol>
	${h2('🚹️&nbsp;&nbsp;Уведомления')}
	<p>Количество уведомлений: %infoCount%</p>
	<ol>%infoText%</ol>`;

function emptyLine(height: number) {
	return `<div style="height: ${height}px; line-height: ${height}px; font-size: 1px;">&nbsp;</div>`;
}

// оборачивает контент в html
function decorateMessageContent(content: string): string {
	const MESSAGE_BGCOLOR = '#f5f5f5';
	const INNER_PADDING_HOR = 30;
	return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=0.7, minimum-scale=0.5, maximum-scale=1" /><!--[if !mso]><!-->
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,300" rel="stylesheet" type="text/css">
	<!--[if (gte mso 9)|(IE)]>
	<xml>
	<o:OfficeDocumentSettings>
	<o:AllowPNG/>
	<o:PixelsPerInch>96</o:PixelsPerInch>
	</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<!--[if (gte mso 9)|(IE)]>
	<style type="text/css">
	body {width: 700px;margin: 0 auto;}
	table {border-collapse: collapse;}
	table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
	img {-ms-interpolation-mode: bicubic;}
	</style>
	<![endif]-->
</head>
<body style="background: ${MESSAGE_BGCOLOR}; font-family: Lato, Arial, Helvetica, sans-serif;
	font-size: 16px; color: #333333;" bgcolor="${MESSAGE_BGCOLOR}">
	<table cellpadding="0" cellspacing="0" border="0" width="100%"
		style="background: ${MESSAGE_BGCOLOR}; min-width: 320px; font-size: 1px; line-height: normal;">
	<tr>
	<td align="center" valign="top">
		${emptyLine(10)}
		<a href="https://pajero-test.d87.ru" style="text-decoration:none">
			<img class="max-width" border="0" src="https://pajero-test.d87.ru/assets/logo.png"
				alt="" width="306" />
		</a>
		${emptyLine(30)}
		<table cellpadding="0" cellspacing="0" border="0" width="700" bgcolor="#ffffff"
			style="max-width: 700px; min-width: 320px; background: #ffffff; border-radius: 10px;
			border-top: 5px solid #FD8631;">
		<tr>
			<td width="${INNER_PADDING_HOR}" style="width: ${INNER_PADDING_HOR}px; max-width: ${INNER_PADDING_HOR}px; min-width: ${INNER_PADDING_HOR}px;">
			    &nbsp;
		    </td>
			<td valign="top" align="left" width="100%" style="font-family: Lato, Arial, Helvetica,
				sans-serif; font-size: 16px; color: #333333;">
				${emptyLine(20)}
				${content}
			</td>
			<td width="${INNER_PADDING_HOR}" style="width: ${INNER_PADDING_HOR}px; max-width: ${INNER_PADDING_HOR}px; min-width: ${INNER_PADDING_HOR}px;">
				&nbsp;
			</td>
		</tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" width="680" style="max-width: 680px;
			min-width: 320px;">
		<tr>
			<td valign="top" align="left" width="100%" style="color: #333333;
				font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal;
				font-weight: 400; font-stretch: normal; font-size: 16px; line-height: 25px;
				font-family: Lato, Arial, Helvetica, sans-serif;">
				${emptyLine(20)}
				<p style="font: 13px Arial; color: #222222; line-height: 13px;">
					Почтовая рассылка сервиса
					<a href="https://pajero-test.d87.ru" title="Клик переведет вас на сайт"
						style="color:inherit; text-decoration-color: #aaaaaa;">PAJERO</a>
					<br/>
				</p>
				${emptyLine(10)}
			</td>
		</tr>
		</table>
	</td>
	</tr>
	</table>
</body>
</html>`;
}

export default class MailService
{
	/**
	 * Выполняет отправку сообщения
	 * @param mailMessage
	 */
	async sendMailMessage(mailMessage) {
		logger.info(`Выполняется отправка MailMessage #${mailMessage.id}`);
		const { from, to, subject, text: html, fileIds } = mailMessage;
		let caughtError: any = false;
		const attachments = fileIds
			? await File
				.findAll({ where: { id: { [Op.in]: JSON.parse(fileIds) } } })
				.then((files: any[]) => files.map(file => ({
					path: path.resolve(config.get('paths:userdata'), file.pathName),
					filename: file.name,
				})))
				.catch(error => caughtError = error)
			: null;
		if (caughtError) {
			throw new Error('');
		}
		return sendMessage({ from, to, subject, html, text: undefined, attachments })
			.then(() => {
				mailMessage.status = 'SENDED'; // TODO change to MailMessageStatusEnum values
				return mailMessage.save();
			})
			.catch((err) => {
				logger.error(fillError(Error, `Error while sending email message #${mailMessage.id}`, err));
				mailMessage.status = 'ERROR'; // TODO change to MailMessageStatusEnum values
				mailMessage.errorCount++;
				return mailMessage.save();
			});
	}

	/**
	 * Создание нового письма для отправки отчета по инцидентам.
	 * @param from
	 * @param to { string | string[] }
	 * @param reportDate
	 * @param files
	 * @param owner
	 */
	static buildIncidentDaylyReportMessage(from, to, reportDate, files, owner) {
		const fileIds = (files && files.length) ? JSON.stringify(files.map(f => f.id)) : null;
		// замена полей в шаблоне
		const content = INCIDENT_DAYLY_REPORT_CONTENT_TEMPLATE.replace(/%(\w+)%/g, (_, key) => {
			switch (key) {
				case 'fullname': return owner.fullName;
				case 'date': return reportDate;
				default: return '';
			}
		});
		const text = decorateMessageContent(content);
		const subject = `Отчет за ${reportDate}`;
		if (to instanceof Array) {
			to = to.join(',');
		}
		return { from, to, subject, text, fileIds, ownerId: owner.id };
	}

	/**
	 * Создание нового письма с отчетом по опросу
	 * @param from
	 * @param to { string | string[] }
	 * @param subject
	 * @param questionnaireTitle
	 * @param files
	 * @param owner
	 */
	static buildQuestionnaireReportMessage(from, to: string | string[], subject: string, questionnaireTitle: string, files, owner) {
		const fileIds = (files && files.length) ? JSON.stringify(files.map(f => f.id)) : null;
		// замена полей в шаблоне
		const content = QUESTIONNAIRE_REPORT_CONTENT_TEMPLATE.replace(/%(\w+)%/g, (_, key) => {
			switch (key) {
				case 'title': return questionnaireTitle;
				default: return '';
			}
		});
		const text = decorateMessageContent(content);
		if (to instanceof Array) {
			to = to.join(',');
		}
		return { from, to, subject, text, fileIds, ownerId: owner.id };
	}

	/**
	 * Создание письма с отчетов анализа по структуре.
	 * @param stats
	 * @param from
	 * @param to
	 * @param owner
	 */
	static buildStructureAnalysisStatsMessage(stats: StructureAnalisysStats, from, to, owner) {
		const reportDate = moment().format('DD.MM.YYYY');
		const content = STRUCTURE_ANALYSIS_REPORT_CONTENT_TEMPLATE.replace(/%(\w+)%/g, (_, key) => {
			switch (key) {
				case 'errorCount':
					return String(stats.errorIdCount);
				case 'errorText':
					return stats.errorIdCount ? stats.errors.map(s => `<li style="margin-bottom: 1em">${s}</li>`).join('') : 'Нет ошибок';
				case 'warnCount':
					return String(stats.warnIdCount);
				case 'warnText':
					return stats.warnIdCount ? stats.warns.map(s => `<li style="margin-bottom: 1em">${s}</li>`).join('') : 'Нет предупреждений';
				case 'infoCount':
					return String(stats.infoIdCount);
				case 'infoText':
					return stats.infoIdCount ? stats.infos.map(s => `<li style="margin-bottom: 1em">${s}</li>`).join('') : 'Нет информационных сообщений';
				case 'reportDate':
					return reportDate;
				default:
					return '';
			}
		});
		const text = decorateMessageContent(content);
		const subject = `Анализ структуры ${reportDate}`;
		if (to instanceof Array) {
			to = to.join(',');
		}
		return { from, to, subject, text, ownerId: owner.id };
	}
}
