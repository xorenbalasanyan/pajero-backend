import { Op, Transaction } from 'sequelize';
import ResponseError from '~errors/ResponseError';
import sequelize from '~lib/sequelize';
import { infoMessageStore, infoMessageToAddresseeStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { FileService, InfoMessageToAddresseeService } from './index';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('InfoMessageService');

export default class InfoMessageService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, infoMessageStore);
	}

	addWhereSearch = ({ where }, { searchParts }) => {
		if (!searchParts?.length) return;
		if (!where[Op.or]) where[Op.or] = [];
		searchParts.forEach(str => {
			where[Op.or].push({ id: { [Op.substring]: str } });
			where[Op.or].push({ subject: { [Op.substring]: str } });
			where[Op.or].push({ externalMessageId: { [Op.substring]: str } });
		});
	}

	createOne = async (infoMessageDto, transaction?: Transaction) => {
		// set
		const currentUser = this.req.getAuthorizedUser();
		infoMessageDto.accountId = currentUser.accountId;
		infoMessageDto.ownerId = currentUser.id;
		infoMessageDto.isImportant = !!infoMessageDto.isImportant;
		const { linkAddressees } = infoMessageDto;
		delete infoMessageDto.linkAddressees;
		// create InfoMessage
		const savedItem = await this.store.createOne(infoMessageDto, transaction);
		// create addressee list
		if (linkAddressees?.length) {
			const newImas = linkAddressees.map(addresseeUserId => ({
				infoMessageId: savedItem.id,
				addresseeUserId,
				status: 'CREATED',
			}));
			await infoMessageToAddresseeStore.createAll(newImas, transaction);
		}
		return savedItem;
	}

	/**
	 * Выполняет обновление InfoMessage и/или обновляет список адресатов
	 * @param savedInfoMessage
	 * @param infoMessageDto
	 * @returns {*}
	 */
	updateOne = async (savedInfoMessage, infoMessageDto) => {
		// check
		if (infoMessageDto.linkAddressees?.length) {
			const imaId = savedInfoMessage.infoMessageToAddressees
				?.find(ima => infoMessageDto.linkAddressees.includes(ima.addresseeUserId));
			if (imaId) {
				throw ResponseError.validationInput(`Пользователя с id ${imaId} нельзя `
					+ 'добавить в список адресатов, потому что он уже добавлен');
			}
		}
		if (infoMessageDto.unlinkAddressees?.length) {
			const exists = savedInfoMessage.infoMessageToAddressees.map(ima => ima.addresseeUserId);
			const imaId = infoMessageDto.unlinkAddressees.find(id => !exists.includes(id));
			if (imaId) {
				throw ResponseError.validationInput(`Пользователя с id #${imaId} нельзя удалить из списка адресатов, `
					+ 'потому что он не числится в адресатах');
			}
		}
		// set
		const currentUser = this.req.getAuthorizedUser();
		infoMessageDto.moderatorId = currentUser.id;
		if (infoMessageDto.isImportant !== undefined) infoMessageDto.isImportant = !!infoMessageDto.isImportant;
		const { linkAddressees, unlinkAddressees } = infoMessageDto;
		delete infoMessageDto.linkAddressees;
		delete infoMessageDto.unlinkAddressees;
		return sequelize.transaction(async transaction => {
			// update InfoMessage
			const savedItem = Object.keys(infoMessageDto).length
				? await this.store.updateOne(savedInfoMessage, infoMessageDto, transaction)
				: savedInfoMessage;
			// update addressee list
			if (linkAddressees?.length) {
				const newImas = linkAddressees.map(addresseeUserId => ({
					infoMessageId: savedItem.id,
					addresseeUserId,
					status: 'CREATED',
				}));
				await infoMessageToAddresseeStore.createAll(newImas, transaction);
			}
			if (unlinkAddressees?.length) {
				await infoMessageToAddresseeStore.deleteWhere({
					infoMessageId: savedItem.id,
					addresseeUserId: unlinkAddressees,
				}, transaction);
			}
			return savedItem;
		});
	}

	findAddresseesByInfoMessageId = async infoMessageId => infoMessageToAddresseeStore
		.findAll({ where: { infoMessageId } })

	/**
	 * Удаление записи и ссылок адресатов
	 * @param item
	 * @param transaction
	 * @returns {Promise<void>}
	 */
	deleteOne = async (item, transaction) => {
		if (!transaction) {
			throw new Error('Для удаления нескольких объектов требуется открыть транзакцию');
		}
		// сначала удаляем адресатов
		await new InfoMessageToAddresseeService(this.req).deleteWhere({ infoMessageId: item.id }, transaction);
		// удаляем сообщение
		await this.deleteWhere({ id: item.id }, transaction);
		// в конце удаляем ссылку на файл
		const fileService = new FileService(this.req);
		const file = await fileService.findOne({ where: { id: item.fileId } }, transaction);
		await fileService.deleteWhere({ id: file.id }, transaction);
		// если все нормально, то перемещаем файл в папку с удаленными
		await fileService.moveFileToDeleted(file.pathName, file.name);
	}

	deleteAddressee = async infoMessageAddressee => infoMessageToAddresseeStore
		.deleteWhere({
			infoMessageId: infoMessageAddressee.infoMessageId,
			addresseeUserId: infoMessageAddressee.addresseeUserId,
		})
}
