import fs from 'fs';
import moment from 'moment';
import { Transaction } from 'sequelize';
import Exceljs, { Alignment } from 'exceljs';
import { AppLogger } from '~lib/AppLogger';
import sequelize from '~lib/sequelize';
import { fillError } from '~utils/errors';
import { arrayToMapByField, arrayToMapById, distinctListById } from '~utils/list';
import {
	fileTypeStore,
	questionnaireExecutionStore,
	questionnaireExecutionToShopStore,
	questionnaireStore,
	shopStore,
	userStore,
} from '~stores';
import { UserRoles } from '~enums/UserRoleEnum';
import { QuestionnaireExecutionStatusEnum } from '~enums/QuestionnaireExecutionStatusEnum';
import {
	QuestionnaireExecutionToShopStatusEnum,
} from '~enums/QuestionnaireExecutionToShopStatusEnum';
import {
	File,
	MailMessage,
	Questionnaire,
	QuestionnaireExecutionToShop,
	QuestionnaireHistory,
	User,
} from '../stores/db/entity';
import { FileService, MailService, QuestionnaireService } from './index';
import { QuestionnaireQuestionTypeEnum } from '~enums/QuestionnaireQuestionTypeEnum';

type ReportDataItem = {
	status: QuestionnaireExecutionToShopStatusEnum,
	shopId: number,
	respondingUserId: number,
	respondingData: any,
	respondingAt: Date,
};

export default class QuestionnaireBackgroundService
{
	private readonly logger = new AppLogger('QuestionnaireBackgroundService');

	constructor(private readonly accountId: number) {}

	/**
	 * Проверяет какие опросы можно поставить в очередь
	 */
	checkForSchedule = async (): Promise<void> => {
		const enabledQuests = await questionnaireStore.findAll({
			where: {
				accountId: this.accountId,
				enabled: true,
			},
		});
		if (!enabledQuests.length) return;
		const idleExecutions = await questionnaireExecutionStore.findAll({
			where: {
				questionnaireId: enabledQuests.map(i => i.id),
				status: QuestionnaireExecutionStatusEnum.IDLE,
			},
		});
		if (!idleExecutions.length) return;
		const today = moment().utcOffset(0).format('YYYY-MM-DD');
		for (const questExec of idleExecutions) {
			const questDate = moment(questExec.startAt).utcOffset(0).format('YYYY-MM-DD');
			if (today === questDate) {
				// ставим в расписание опросы на сегодня
				this.logger.info(`Опрос (QuestionnaireExecution.id: ${questExec.id}) переводится в статус запланированного`);
				await questExec.update({
					status: QuestionnaireExecutionStatusEnum.SCHEDULED,
				});
			}
		}
	}

	/**
	 * Проверяет какие опросы можно стартовать
	 */
	checkForRun = async (): Promise<void> => {
		const enabledQuests = await questionnaireStore.findAll({
			where: {
				accountId: this.accountId,
				enabled: true,
			},
		});
		if (!enabledQuests.length) return;
		const scheduledExecutions = await questionnaireExecutionStore.findAll({
			where: {
				questionnaireId: enabledQuests.map(i => i.id),
				status: QuestionnaireExecutionStatusEnum.SCHEDULED,
			},
		});
		if (!scheduledExecutions.length) return;
		// admin user
		const accountAdmin = await userStore.findOne({
			where: {
				accountId: this.accountId,
				role: UserRoles.ADMIN,
			},
		});
		if (!accountAdmin) {
			throw new Error(`Для аккаунта #${this.accountId} не удалось найти админа`);
		}
		const now = Date.now();
		for (const questExec of scheduledExecutions) {
			const questStartAt = new Date(questExec.startAt).getTime();
			if (now >= questStartAt) {
				questExec.status = QuestionnaireExecutionStatusEnum.RUNNING;
				questExec.moderatorId = accountAdmin.id;
				// заполняем адресатов
				const quest = await questionnaireStore.findOne({ where: { id: questExec.questionnaireId } });
				const { repeatCount = 1 } = JSON.parse(quest.executionConfig);
				const adr = JSON.parse(quest.addresseesShopIds);
				const questExecToShops: any[] = [];
				Array.from(new Set(adr)).forEach(shopId => {
					for (let i = 0; i < repeatCount; i++) {
						questExecToShops.push(new QuestionnaireExecutionToShop({
							questionnaireId: quest.id,
							questionnaireExecutionId: questExec.id,
							shopId,
							status: QuestionnaireExecutionToShopStatusEnum.RUNNING,
							startAt: questExec.startAt,
							finishAt: questExec.finishAt,
							ownerId: accountAdmin.id,
						}));
					}
				});
				await sequelize.transaction(async transaction => {
					this.logger.info(`Опрос (QuestionnaireExecution.id: ${questExec.id}) переводится в статус запущенного`);
					await questExec.save({ transaction });
					for (const questExecToShop of questExecToShops) {
						await questExecToShop.save({ transaction });
					}
				})
					.catch(err => {
						throw fillError(Error, `Ошибка создания одного из адресатов ${JSON.stringify(questExecToShops)} для опроса #${quest.id}: `, err);
					});
			}
		}
	}

	/**
	 * Проверяет какие опросы можно остановить
	 */
	checkForStop = async (): Promise<void> => {
		const enabledQuests = await questionnaireStore.findAll({
			where: {
				accountId: this.accountId,
				enabled: true,
			},
		});
		if (!enabledQuests.length) return;
		const ranExecutions = await questionnaireExecutionStore.findAll({
			where: {
				questionnaireId: enabledQuests.map(i => i.id),
				status: [
					QuestionnaireExecutionStatusEnum.IDLE,
					QuestionnaireExecutionStatusEnum.SCHEDULED,
					QuestionnaireExecutionStatusEnum.RUNNING,
				],
			},
		});
		if (!ranExecutions.length) return;
		// admin user
		const accountAdmin = await userStore.findOne({
			where: {
				accountId: this.accountId,
				role: UserRoles.ADMIN,
			},
		});
		if (!accountAdmin) {
			throw new Error(`Для аккаунта #${this.accountId} не удалось найти админа`);
		}
		const now = Date.now();
		for (const questExec of ranExecutions) {
			const questFinishAt = new Date(questExec.finishAt).getTime();
			if (now >= questFinishAt) {
				questExec.status = questExec.status === QuestionnaireExecutionToShopStatusEnum.RUNNING
					? QuestionnaireExecutionStatusEnum.STOPPED
					: QuestionnaireExecutionStatusEnum.CANCELED;
				questExec.moderatorId = accountAdmin.id;
				await sequelize.transaction(async transaction => {
					this.logger.info(`Опрос (QuestionnaireExecution.id: ${questExec.id}) переводится в статус остановленного/отмененного`);
					await questExec.save({ transaction });
					const shopExecs = await questionnaireExecutionToShopStore.findAll({
						where: {
							questionnaireExecutionId: questExec.id,
							status: QuestionnaireExecutionToShopStatusEnum.RUNNING,
						},
					}, transaction);
					for (const shopExec of shopExecs) {
						await shopExec.update({
							status: questExec.status === QuestionnaireExecutionStatusEnum.STOPPED
								? QuestionnaireExecutionToShopStatusEnum.FAILED
								: QuestionnaireExecutionToShopStatusEnum.CANCELED,
						}, { transaction });
					}
				})
					.catch(err => {
						throw fillError(Error, `[Ошибка остановки опроса]`, err);
					});
			}
		}
	}

	/**
	 * Проверяет какие опросы можно подытожить и составить отчет
	 * (!!!) Отчеты формируются по опросам для всех аккаунтов
	 */
	checkForReport = async (): Promise<void> => {
		const stoppedExecutions = await questionnaireExecutionStore.findAll({
			where: {
				status: QuestionnaireExecutionStatusEnum.STOPPED,
			},
		});
		if (!stoppedExecutions.length) return;
		// создаем отчеты для всех остановленных опросников
		for (const questExec of stoppedExecutions) {
			// опрос может быть удален
			const quest = await questionnaireStore.findOneUnscoped({
				where: {
					id: questExec.questionnaireId,
				},
			});
			// admin user
			const questAccountAdmin = await userStore.findOne({
				where: {
					accountId: quest.accountId,
					role: UserRoles.ADMIN,
				},
			});
			if (!questAccountAdmin) {
				throw new Error(`Для аккаунта #${quest.accountId} не удалось найти админа`);
			}
			await sequelize.transaction(async transaction => {
				this.logger.info(`Выполняется формирование отчета для опроса (QuestionnaireExecution.id: ${questExec.id})`);
				const shopExecs = await questionnaireExecutionToShopStore.findAll({
					where: {
						questionnaireExecutionId: questExec.id,
					},
				}, transaction);
				const answeredCount = shopExecs
					.filter(i => i.status === QuestionnaireExecutionToShopStatusEnum.FINISHED)
					.length;
				// составляем отчет
				const reportData: ReportDataItem[] = shopExecs.map(shopExec => ({
					status: shopExec.status,
					shopId: shopExec.shopId,
					respondingUserId: shopExec.respondingUserId,
					respondingData: shopExec.respondingData,
					respondingAt: shopExec.respondingAt,
				}));
				// составляем и сохраняем файл отчета
				const reportFile = await this.createQuestionnaireReportFile(quest, reportData, questAccountAdmin, transaction);
				// чтобы не забивалась таблица shop_execution_to_shop необходимо все записи
				// перенести в таблицу questionnaire_history
				const questHistory = new QuestionnaireHistory({
					questionnaireId: questExec.questionnaireId,
					questionnaireExecutionId: questExec.id,
					addresseesCount: questExec.addresseesCount,
					answeredCount,
					finishedAt: questExec.finishAt,
					reportData,
					reportFileId: reportFile.id,
					ownerId: questAccountAdmin.id,
				});
				await questHistory.save({ transaction });
				// удаляем записи из shop_execution_to_shop
				const shopExecIds = distinctListById(shopExecs);
				if (shopExecIds.length) {
					await questionnaireExecutionToShopStore.deleteWhere({
						id: shopExecIds,
					}, transaction);
				}
				// обновляем статус
				await questExec.update({
					status: QuestionnaireExecutionStatusEnum.REPORTED,
					moderatorId: questAccountAdmin.id,
				}, { transaction });
				// отправляем подписантам
				// TODO {"messageTitle":"Отчет по опросу","addressees":["user1@mail.ru","user2@mail.ru"]}
				const reportConfig = JSON.parse(quest.reportConfig);
				for (const toEmail of reportConfig.addressees) {
					const mailMessageData = MailService.buildQuestionnaireReportMessage(
						null,
						toEmail,
						reportConfig.messageTitle,
						quest.title,
						[reportFile],
						questAccountAdmin,
					);
					await MailMessage.create(mailMessageData, { transaction })
						.catch(error => {
							throw fillError(Error, `[Ошибка создания письма с уведомлением об отчете]: ${JSON.stringify({ mailMessageData })}`, error);
						});
				}
			})
				.catch(err => {
					throw fillError(Error, `[Ошибка формирования отчета по опросу] questionnaireExecutionId: ${questExec.id}`, err);
				});
		}
	}

	/**
	 * Рассчитывает время следующего старта опроса или останавливает опрос
	 */
	checkForNextStart = async (): Promise<void> => {
		// потенциальные запуски опросов, которые необходимо запустить снова
		// статусы запусков REPORTED и CANCELED это временные статусы
		const stoppedExecutions = await questionnaireExecutionStore.findAll({
			where: {
				status: [
					QuestionnaireExecutionStatusEnum.REPORTED,
					QuestionnaireExecutionStatusEnum.CANCELED,
				],
			},
		});
		if (!stoppedExecutions.length) return;
		const questRestartedSet = new Set<number>();
		const questService = new QuestionnaireService(null);
		for (const questExec of stoppedExecutions) {
			// check: опрос может быть удален
			const quest = await questionnaireStore.findOneUnscoped({
				where: {
					id: questExec.questionnaireId,
				},
			});
			// admin user
			const questAccountAdmin = await userStore.findOne({
				where: {
					accountId: quest.accountId,
					role: UserRoles.ADMIN,
				},
			});
			if (!questAccountAdmin) {
				throw new Error(`Для аккаунта #${quest.accountId} не удалось найти админа`);
			}
			let newQuestExecution;
			let questToSave;
			if (!quest.isDeleted && quest.enabled && !questRestartedSet.has(quest.id)) {
				// опрос не удален и активен
				const executionConfig = JSON.parse(quest.executionConfig);
				if (executionConfig.repeatEveryDays) {
					const now = Date.now();
					let nextRun = moment(questExec.startAt).utcOffset(300);
					let finishRun = moment(questExec.finishAt).utcOffset(300);
					// разница в сутках между стартом и финишем без учета времени
					const diffDays = finishRun.startOf('day').diff(nextRun.startOf('day'), 'days');

					while (nextRun.utcOffset(0).toDate().getTime() < now) {
						// расчет следующего запуска с учетом часового пояса
						// TODO добавить учет ЧП
						const next = moment(finishRun).add(executionConfig.repeatEveryDays, 'days');
						nextRun = moment(next.format('YYYY-MM-DD') + 'T' + executionConfig.startAtTime + '+05:00');
						const finish = moment(nextRun).add(diffDays, 'days');
						finishRun = moment(finish.format('YYYY-MM-DD') + 'T' + executionConfig.finishAtTime + '+05:00');
					}

					newQuestExecution = await questService.createQuestionnaireExecution(quest, questAccountAdmin.id);
					newQuestExecution.startAt = nextRun;
					newQuestExecution.finishAt = finishRun;
				} else {
					// опрос без настройки повторения, отключаем
					quest.enabled = false;
					questToSave = quest;
				}
				questRestartedSet.add(quest.id);
			}
			await sequelize.transaction(async transaction => {
				if (newQuestExecution) {
					await newQuestExecution.save({ transaction });
				}
				if (questExec.status === QuestionnaireExecutionStatusEnum.REPORTED) {
					// обновляем статус только для прошедших опросов
					await questExec.update({
						status: QuestionnaireExecutionStatusEnum.FINISHED,
						moderatorId: questAccountAdmin.id,
					}, { transaction });
				} else if (questExec.status === QuestionnaireExecutionStatusEnum.CANCELED) {
					await questionnaireExecutionStore.deleteWhere({ id: questExec.id }, transaction);
				}
				if (questToSave) {
					await questToSave.save({ transaction });
				}
			})
				.catch(error => {
					throw fillError(Error, `Ошибка рассчета следующего запуска опроса для QuestionnaireExecution #${questExec.id}`, error);
				});
		}
	}

	/**
	 * Формирование файла отчета
	 * @private
	 * @param quest
	 * @param reportData
	 * @param admin
	 * @param transaction
	 */
	private async createQuestionnaireReportFile(
		quest: typeof Questionnaire,
		reportData: ReportDataItem[],
		admin: typeof User,
		transaction: Transaction,
	): Promise<typeof File> {
		// формируем xlsx
		// создаем стрим для записи XLSX
		const { pathName, fullPath } = FileService.generateNewFileName('quest_report-', '.xlsx');
		const options = {
			filename: fullPath,
			useStyles: true,
			useSharedStrings: true,
		};
		const wb = new Exceljs.stream.xlsx.WorkbookWriter(options);
		// собираем пользователей и магазины
		const shops = await shopStore.findAll({
			where: {
				id: reportData.map(i => i.shopId),
			},
			attributes: ['id', 'externalId', 'name', 'city', 'address', 'timeOffset'],
		}, transaction);
		const shopMap = arrayToMapById(shops);
		const users = await userStore.findAll({
			where: {
				id: reportData.map(i => i.respondingUserId),
			},
			attributes: ['id', 'lastName', 'firstName', 'middleName'],
		}, transaction);
		const userMap = arrayToMapById(users);
		// готовим XLSX для записи
		const [ws, colCount] = prepareWorkbook(wb, quest, quest.title) as any;
		const { questions } = JSON.parse(quest.questionnaireConfig);
		let wsRow = 2;
		for (const reportItem of reportData) {
			const row = ws.getRow(wsRow++);
			const status = reportItem.status === QuestionnaireExecutionToShopStatusEnum.FINISHED
				? 'Пройден'
				: reportItem.status === QuestionnaireExecutionToShopStatusEnum.FAILED
					? 'Не пройден'
					: reportItem.status === QuestionnaireExecutionToShopStatusEnum.CANCELED
						? 'Опрос остановлен'
						: 'Статус неизвестен';
			const shop = shopMap.get(reportItem.shopId);
			const user = reportItem.respondingUserId ? userMap.get(reportItem.respondingUserId) : null;
			const date = reportItem.respondingAt ? moment(reportItem.respondingAt).utcOffset(shop.timeOffset).format('DD.MM.YYYY') : null;
			const time = reportItem.respondingAt ? moment(reportItem.respondingAt).utcOffset(shop.timeOffset).format('HH:mm') : null;
			row.values = [
				shop.externalId || '-',
				`${shop.name}${(shop.city || shop.address) ? ` (${[shop.city, shop.address].filter(Boolean).join(', ')})` : ''}`,
				user && [user.lastName, user.firstName, user.middleName].filter(Boolean).join(', ') || '-',
				date || null,
				time || null,
				status,
				...reportQuestionsAndAnswers(questions, reportItem.respondingData),
			];
			row.commit();
		}
		// добавляем автофильтр
		const rightColName = String.fromCharCode(64 + Math.min(colCount, 26));
		ws.autoFilter = { from: 'A1', to: `${rightColName}${wsRow - 1}` };
		// сохраняем файл
		await wb.commit();
		const size = fs.statSync(fullPath).size;
		const xlsxFileType = await fileTypeStore
			.findIdByExt('xlsx', transaction)
			.catch(error => {
				throw fillError(Error, `[Ошибка поиска типа файла]: ${JSON.stringify({ accountId: admin.accountId })}`, error);
			});
		const today = moment().format('DD.MM.YYYY');
		let fileName = replaceSpecChars(`${quest.title} ${today}`, '_');
		if (fileName.length > 40) {
			fileName = fileName.substring(0, 40);
		}
		const file = new File({
			accountId: admin.accountId,
			name: `${fileName}.xlsx`,
			size,
			fileTypeId: xlsxFileType.id,
			pathName,
			ownerId: admin.id,
		});
		return await file.save({ transaction });
	}
}

function prepareWorkbook(wb, quest: typeof Questionnaire, title: string) {
	// формируем XLSX отчет для каждого аккаунта.
	wb.creator = 'Pajero';
	wb.created = new Date();
	title = replaceSpecChars(title, ' ');
	if (title.length > 20) {
		title = title.substring(0, 20) + '...';
	}
	const ws = wb.addWorksheet(title, {
		views: [{
			state: 'frozen',
			ySplit: 1,
		}],
	});
	const alignment: Partial<Alignment> = {
		wrapText: true,
		vertical: 'middle',
		horizontal: 'center',
	};
	const questionnaireConfig = JSON.parse(quest.questionnaireConfig);
	ws.columns = [
		{ header: '# магазина', width: 15 },
		{ header: 'Магазин', width: 40 },
		{ header: 'ФИО ДМ', width: 30 },
		{ header: 'Дата', width: 12 },
		{ header: 'Время', width: 12 },
		{ header: 'Статус', width: 17 },
		...questionnaireConfig.questions.map(row => {
			const { text, isRequired } = row;
			return {
				header: `${text}${isRequired ? ' *' : ''}`,
				width: 30,
			};
		}),
	];
	ws.getRow(1).height = 24;
	ws.getRow(1).alignment = alignment;
	return [ws, ws.columns.length];
}

function reportQuestionsAndAnswers(questions: any[], answers: string | undefined): (string | number | '-')[] {
	const answerMap = answers ? arrayToMapByField(JSON.parse(answers), 'questionIndex') : undefined;
	return questions.map((question, index) => {
		const answer: any = answerMap?.get(index) || null;
		if (!answer || answer.value === null) return '-';
		switch (question.type) {
			case QuestionnaireQuestionTypeEnum.FLOAT:
				return Number(answer.value);
			case QuestionnaireQuestionTypeEnum.STRING:
				return answer.value;
			case QuestionnaireQuestionTypeEnum.RADIO:
				return question.options[Number(answer.value)];
			case QuestionnaireQuestionTypeEnum.CHECKERS:
				return answer.value.map(v => question.options[Number(v)]).join(', ') || '-';
			default:
				throw new Error(`Неизвестный тип вопроса: ${question.type}`);
		}
	});
}

function replaceSpecChars(s: string, repl: string) {
	return s
		.replace(/[^A-Za-zА-Яа-я0-9ёË.]+/g, repl)
		.replace(/[\s]+/g, repl)
		.trim();
}
