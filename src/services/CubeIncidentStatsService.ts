import { select } from '~lib/sequelize';
import BaseService from './BaseService';
import { AppLogger } from '~lib/AppLogger';
import { UserRoles } from '~enums/UserRoleEnum';
import { ShopManagerRoleService } from './index';
import ShopManagerRole from '../stores/db/entity/ShopManagerRole';

const moduleLogger = new AppLogger('CubeIncidentStatsService');

type UsersAndShopStat = {
	users: any[],
	shops: any[],
};

type IncidentTypeStat = any;

export default class CubeIncidentStatsService extends BaseService
{
	constructor(req) {
		super(req, moduleLogger);
	}

	/**
	 * Собираем статистику по инцидентам для текущего менеджера
	 */
	async getManagerStats(filterStates: string[]): Promise<UsersAndShopStat> {
		const currentUser = this.req.getAuthorizedUser();
		const shopManagerRoleService = new ShopManagerRoleService(this.req);
		const shopManagerRoles = await shopManagerRoleService.findAllWithAccountId({ where: {}, order: [['subLevel', 'asc']] });
		return await selectUsersAndShopsStat(currentUser.id,
			currentUser.role,
			filterStates,
			shopManagerRoles);
	}

	/**
	 * Собираем статистику по типам инцидентов для магазина
	 */
	getIncidentTypeStatByShopId(shopId: number, filterStates: string[]): Promise<IncidentTypeStat[]> {
		return selectIncidentTypeStatByShopId(shopId, [], filterStates);
	}
}

/**
 * Собирает статистику в виде дерева по заданному менеджеру
 */
async function selectUsersAndShopsStat(
	rootManagerId: number,
	role: UserRoles,
	filterStates: string[],
	shopManagerRoles: typeof ShopManagerRole[],
): Promise<UsersAndShopStat> {
	const rootUsers: any[] = [];
	const rootManagerIds: number[] = [rootManagerId];
	let managerIds: number[] = role === UserRoles.CHIEF ? [] : [rootManagerId];
	let hasSubUsers = true;

	// собираем дерево всех подчиненных пользователей
	function pushUser(user: any): void {
		rootUsers.push(user);
		rootManagerIds.push(user.userId);
		if (user.userId !== rootManagerId) {
			managerIds.push(user.userId);
		}
	}

	while (hasSubUsers) {
		const userStats = await selectCubeUserStats(managerIds, [], filterStates);
		managerIds = [];
		userStats.forEach(pushUser);
		hasSubUsers = managerIds.length > 0;
	}

	// собираем список магазинов и заполняем дерево статистики
	const shops = await selectCubeShopStats(rootManagerIds, [], filterStates);
	return { users: rootUsers, shops };
}

/**
 * Собирает статистику по типам инцидетнтов по заданному магазину
 */
async function selectIncidentTypeStatByShopId(shopId: number, filterIncidentTypes: number[], filterStates: string[]): Promise<IncidentTypeStat[]> {
	const replacements: any = {};
	const fields = 'c.incidentTypeId, c.completed, c.allCount, it.name AS incidentTypeName';
	const from = 'cube_incident_shop_stats c LEFT JOIN incident_type it ON it.id=c.incidentTypeId';
	const shopWhere = `shopId = ${shopId}`;
	let incidentTypeWhere = 'IS NOT NULL';
	if (filterIncidentTypes.length) {
		incidentTypeWhere = 'IN (:incidentTypes)'; // защита от инъекций
		replacements.incidentTypes = filterIncidentTypes;
	}
	let stateWhere = 'IS NULL';
	if (filterStates.length) {
		stateWhere = 'IN (:states)'; // защита от инъекций
		replacements.states = filterStates;
	}
	const where = `WHERE ${shopWhere} AND incidentTypeId ${incidentTypeWhere} AND state ${stateWhere}`;
	const query = `SELECT DISTINCT ${fields} FROM ${from} ${where}`;
	return await select(query, replacements);
}

function getNextRole(role: string): string | undefined {
	switch (role) {
		case UserRoles.CHIEF:
			return UserRoles.MANAGER;
		default:
			return undefined;
	}
}

function selectCubeUserStats(managersId: number[], filterIncidentTypes: number[], filterStates: string[]): Promise<any[]> {
	const fields = 'c.userId, c.completed, c.allCount';
	const from = 'cube_incident_user_stats c';

	return selectFromCube(fields, from, managersId, filterIncidentTypes, filterStates);
}

function selectCubeShopStats(managersId: number[], filterIncidentTypes: number[], filterStates: string[]): Promise<any[]> {
	const fields = 'c.shopId, c.completed, c.allCount';
	const from = 'cube_incident_shop_stats c';
	return selectFromCube(fields, from, managersId, filterIncidentTypes, filterStates);
}

function selectFromCube(fields: string, from: string, managersId: number[], filterIncidentTypes: number[], filterStates: string[]): Promise<any[]> {
	const replacements: any = {};
	const managerWhere = !managersId.length ? 'IS NULL' : `IN (${managersId.join(',')})`;
	let incidentTypeWhere = 'IS NULL';
	if (filterIncidentTypes.length) {
		incidentTypeWhere = 'IN (:incidentTypes)'; // защита от инъекций
		replacements.incidentTypes = filterIncidentTypes;
	}
	let stateWhere = 'IS NULL';
	if (filterStates.length) {
		stateWhere = 'IN (:states)'; // защита от инъекций
		replacements.states = filterStates;
	}
	const where = `WHERE managerId ${managerWhere} AND incidentTypeId ${incidentTypeWhere} AND state ${stateWhere}`;
	const query = `SELECT ${fields} FROM ${from} ${where}`;
	return select(query, replacements);
}
