import moment from 'moment';
import { Op, Transaction } from 'sequelize';
import {
	questionnaireExecutionStore,
	questionnaireExecutionToShopStore,
	questionnaireStore,
} from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';
import { Questionnaire, QuestionnaireExecution } from '../stores/db/entity';
import PageInfo from '~utils/PageInfo';
import { arrayToMapById, distinctListByField, distinctListById } from '~utils/list';
import { QuestionnaireExecutionService, QuestionnaireHistoryService, ShopService } from './index';
import sequelize, { select } from '~lib/sequelize';
import { QuestionnaireExecutionStatusEnum } from '~enums/QuestionnaireExecutionStatusEnum';
import ResponseError from '~errors/ResponseError';
import {
	QuestionnaireExecutionToShopStatusEnum,
} from '~enums/QuestionnaireExecutionToShopStatusEnum';
import QuestionnaireBackgroundTask
	from '../background/backgroundTasks/QuestionnaireBackgroundTask';

const moduleLogger = new AppLogger('QuestionnaireService');

export default class QuestionnaireService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, questionnaireStore);
	}

	/**
	 * Список опросов
	 * @param pageInfo
	 */
	getQuestList = async (pageInfo: PageInfo): Promise<any[]> => {
		const lastExecIds = await this.getLastExecutionsIds();
		const [list, pageInfo1] = await this.findAllWithPageInfo({
			where: {},
			include: {
				model: QuestionnaireExecution,
				where: {
					id: {
						[Op.or]: [
							null,
							lastExecIds,
						],
					},
				},
				required: false,
			},
			raw: true,
		}, pageInfo);
		const resList = list.map(quest => {
			const qConfig = JSON.parse(quest.questionnaireConfig);
			const executionConfig = JSON.parse(quest.executionConfig);
			const addressees = JSON.parse(quest.addresseesShopIds);
			const { 'questionnaireExecutions.id': execId } = quest;
			return {
				id: quest.id,
				title: quest.title,
				enabled: !!quest.enabled,
				...(!!execId && {
					status: quest['questionnaireExecutions.status'],
					startAt: quest['questionnaireExecutions.startAt'],
					finishAt: quest['questionnaireExecutions.finishAt'],
				}),
				questionCount: qConfig.questions.length,
				addresseesCount: addressees.length,
				answeredCount: quest['questionnaireExecutions.answeredCount'] || 0,
				executionConfig,
			};
		});
		return [resList, pageInfo1];
	}

	/**
	 * Один опрос
	 * @param quest
	 */
	getOneQuest = async (quest: typeof Questionnaire): Promise<any> => {
		const currentUser = this.req.getAuthorizedUser();
		const isShopUser = currentUser.isManager;
		return {
			id: quest.id,
			title: quest.title,
			...(!isShopUser && { enabled: !!quest.enabled }),
			questionnaireConfig: JSON.parse(quest.questionnaireConfig),
			...(!isShopUser && {
				addresseesShopIds: JSON.parse(quest.addresseesShopIds),
				reportConfig: JSON.parse(quest.reportConfig),
				executionConfig: JSON.parse(quest.executionConfig),
			}),
		};
	}

	/**
	 * Активирует опрос
	 * @param quest
	 */
	enableOne = async (quest: typeof Questionnaire): Promise<void> => {
		// проверяем статусы
		if (quest.enabled) {
			throw ResponseError.badRequest('Опрос уже запущен');
		}
		// првоеряем можно ли запустить второй раз для разовых
		const executionConfig = JSON.parse(quest.executionConfig);
		if (executionConfig) {
			const list = await questionnaireExecutionStore.findAll({
				where: {
					questionnaireId: quest.id,
					status: [
						QuestionnaireExecutionStatusEnum.STOPPED,
						QuestionnaireExecutionStatusEnum.REPORTED,
						QuestionnaireExecutionStatusEnum.FINISHED,
					],
				},
			});
			if (!executionConfig.repeatEveryDays && list.length) {
				// данный опрос уже был раньше запущен в период опроса
				throw ResponseError.badRequest('Опрос уже был запущен и завершен. Чтобы запустить этот опрос заново, необходимо изменить время запуска.');
			}
			const nowDate = new Date();
			const startAt = moment(`${executionConfig.startAtDate}T${executionConfig.startAtTime}:00Z`).add(-300, 'minutes').toDate();
			const finishAt = moment(`${executionConfig.finishAtDate}T${executionConfig.finishAtTime}:00Z`).add(-300, 'minutes').toDate();
			if (nowDate.getTime() < finishAt.getTime() && nowDate.getTime() > startAt.getTime() && list.length) {
				throw ResponseError.badRequest(('Опрос уже был запущен и завершен. Чтобы запустить этот опрос заново, необходимо изменить время запуска.'));
			}
		}
		const questExecution = await this.createQuestionnaireExecution(quest, this.req.getAuthorizedUser().id);
		return await sequelize.transaction(async transaction => {
			await this.updateOne(quest, { enabled: true }, transaction);
			await questExecution.save({ transaction });
		})
			.then(() => {
				// запускаем проверку статусов
				new QuestionnaireBackgroundTask().execute(this.req.getAuthorizedUser().accountId);
			});
	}

	/**
	 * Создание копии опроса
	 */
	createCopy = async (item, transaction?: Transaction): Promise<any> => {
		const currentUser = this.req.getAuthorizedUser();
		item.accountId = currentUser.accountId;
		item.ownerId = currentUser.id;
		const title = `${item.title} (копия)`;
		const reportConfig = JSON.parse(item.reportConfig);
		const questionnaireConfig = JSON.parse(item.questionnaireConfig);
		const addresseesShopIds = JSON.parse(item.addresseesShopIds);
		const executionConfig = JSON.parse(item.executionConfig);

		return this.store.createOne({
			...item,
			title,
			questionnaireConfig,
			addresseesShopIds,
			reportConfig,
			executionConfig,
		}, transaction);
	}

	createQuestionnaireExecution = async (quest: typeof Questionnaire, ownerId: number): Promise<typeof QuestionnaireExecution> => {
		const questExecutions = await this.getExecutionsByQuestionnaireIds([quest.id]);
		const ranExecution = questExecutions.find(q => [
			QuestionnaireExecutionStatusEnum.IDLE,
			QuestionnaireExecutionStatusEnum.SCHEDULED,
			QuestionnaireExecutionStatusEnum.RUNNING,
		].includes(q.status));
		if (ranExecution) {
			// уже запущено
			throw ResponseError.badRequest('Опрос уже запущен');
		}
		// запускаем новый опрос
		const addresseesShopIds = JSON.parse(quest.addresseesShopIds);
		const executionConfig = JSON.parse(quest.executionConfig);
		// TODO учитывать часовой пояс
		const startAt = moment(`${executionConfig.startAtDate}T${executionConfig.startAtTime}:00Z`).add(-300, 'minutes').toDate();
		const finishAt = moment(`${executionConfig.finishAtDate}T${executionConfig.finishAtTime}:00Z`).add(-300, 'minutes').toDate();
		return new QuestionnaireExecution({
			questionnaireId: quest.id,
			status: QuestionnaireExecutionStatusEnum.IDLE,
			startAt,
			finishAt,
			addresseesCount: addresseesShopIds.length,
			answeredCount: 0,
			ownerId,
		});
	}

	/**
	 * Деактивирует опрос
	 * @param quest
	 */
	disableOne = async (quest: typeof Questionnaire): Promise<void> => {
		// проверяем статусы
		if (!quest.enabled) {
			throw ResponseError.badRequest('Опрос не запущен');
		}
		const questExecutions = await this.getExecutionsByQuestionnaireIds([quest.id]);
		const ranExecution = questExecutions.find(q => [
			QuestionnaireExecutionStatusEnum.IDLE,
			QuestionnaireExecutionStatusEnum.SCHEDULED,
			QuestionnaireExecutionStatusEnum.RUNNING,
		].includes(q.status));
		if (!ranExecution) {
			// не найдено запущенных опросов, игнорируем
			return;
		}
		// останавливаем опрос
		quest.enabled = false;
		ranExecution.status = ranExecution.status === QuestionnaireExecutionStatusEnum.RUNNING
			? QuestionnaireExecutionStatusEnum.STOPPED
			: QuestionnaireExecutionStatusEnum.CANCELED;
		return await sequelize.transaction(async transaction => {
			await quest.save({ transaction });
			await ranExecution.save({ transaction });
			const shopRunningExecs = await questionnaireExecutionToShopStore.findAll({
				where: {
					questionnaireExecutionId: ranExecution.id,
					status: QuestionnaireExecutionToShopStatusEnum.RUNNING,
				},
			}, transaction);
			const updateIdsToCancel = shopRunningExecs.map(se => se.id);
			await questionnaireExecutionToShopStore
				.updateWhere({
					status: QuestionnaireExecutionToShopStatusEnum.CANCELED,
				}, {
					where: {
						id: updateIdsToCancel,
					},
				}, transaction);
		})
			.then(() => {
				// запускаем проверку статусов
				new QuestionnaireBackgroundTask().execute(this.req.getAuthorizedUser().accountId);
			});
	}

	/**
	 * Удаляет опрос
	 * @param quest
	 */
	deleteOne = async (quest: typeof Questionnaire): Promise<void> => {
		// проверяем статусы
		if (quest.enabled) {
			throw ResponseError.badRequest('Остановите опрос перед удалением');
		}
		const questExecutions = await this.getExecutionsByQuestionnaireIds([quest.id]);
		const ranExecution = questExecutions.find(q => [QuestionnaireExecutionStatusEnum.IDLE,
			QuestionnaireExecutionStatusEnum.SCHEDULED, QuestionnaireExecutionStatusEnum.RUNNING]
			.includes(q.status));
		if (ranExecution) {
			// уже запущено
			throw ResponseError.badRequest('Остановите опрос перед удалением');
		}
		return await quest.update({ isDeleted: true })
			.then(() => {
				// запускаем проверку статусов
				new QuestionnaireBackgroundTask().execute(this.req.getAuthorizedUser().accountId);
			});
	}

	/**
	 * Список отчетов
	 */
	getQuestReportList = async (pageInfo: PageInfo): Promise<any[]> => {
		const svc = new QuestionnaireHistoryService(this.req);
		const [list, pageInfo1] = await svc.findAllWithPageInfo({
			where: {},
			raw: true,
		}, pageInfo);
		const questIds = distinctListByField(list, 'questionnaireId');
		const quests = await this.findAllUnscoped({
			where: {
				id: questIds,
			},
		});
		const questMap = arrayToMapById(quests);
		return [
			list.map(historyItem => ({
				id: historyItem.id,
				title: questMap.get(historyItem.questionnaireId).title,
				addresseesCount: historyItem.addresseesCount,
				answeredCount: historyItem.answeredCount,
				finishedAt: historyItem.finishedAt,
				repeatCount: JSON.parse(questMap.get(historyItem.questionnaireId).executionConfig).repeatCount || 1,
			})),
			pageInfo1,
		];
	}

	/**
	 * Список опросов, доступных для сотрудника магазина
	 */
	getQuestListForManager = async (): Promise<any[]> => {
		const currentUser = this.req.getAuthorizedUser();
		const shopService = new ShopService(this.req);
		const shops = await shopService.findAllWhereUserIsShopUser(currentUser.id);
		if (!shops.length) return [];
		const shopIds = distinctListById(shops);
		const questExecs = await questionnaireExecutionToShopStore.findAll({
			where: {
				shopId: shopIds,
				status: [QuestionnaireExecutionToShopStatusEnum.RUNNING, QuestionnaireExecutionToShopStatusEnum.FINISHED],
			},
		});
		if (!questExecs.length) return [];
		const questIds = distinctListByField(questExecs, 'questionnaireId');
		const quests = await questionnaireStore.findAll({ where: { id: questIds } });
		if (!quests.length) return [];
		const questMap = arrayToMapById(quests);
		const execByQuestMap = new Map<number, any>();
		questExecs.forEach(qe => {
			let item = execByQuestMap.get(qe.questionnaireId);
			if (!item) {
				item = {
					id: qe.questionnaireId,
					title: questMap.get(qe.questionnaireId).title,
					questionCount: JSON.parse(questMap.get(qe.questionnaireId).questionnaireConfig).questions.length,
					repeatCount: JSON.parse(questMap.get(qe.questionnaireId).executionConfig).repeatCount || 1,
					finishedCount: 0,
				};
				execByQuestMap.set(qe.questionnaireId, item);
			}
			if (!item.status || qe.status === QuestionnaireExecutionToShopStatusEnum.RUNNING) {
				item.status = qe.status;
				item.startAt = qe.startAt;
				item.finishAt = qe.finishAt;
			}
			if (qe.status === QuestionnaireExecutionToShopStatusEnum.FINISHED) {
				item.finishedCount++;
			}
		});
		return Array.from(execByQuestMap.values()).filter(i => i.status === QuestionnaireExecutionToShopStatusEnum.RUNNING);
	}

	/**
	 * Создание ответа на опрос сотрудником магазина
	 */
	createQuestResponse = async (quest: typeof Questionnaire, response: any): Promise<void> => {
		const currentUser = this.req.getAuthorizedUser();
		// проверяем статусы
		if (!quest.enabled) {
			throw ResponseError.badRequest('Опрос остановлен');
		}
		// создаем транзакцию чтобы не сбросился счетчик отвеченных
		await sequelize.transaction(async transaction => {
			const questExecutions = await this.getExecutionsByQuestionnaireIds([quest.id], transaction);
			const ranExecution = questExecutions.find(q => q.status === QuestionnaireExecutionStatusEnum.RUNNING);
			if (!ranExecution) {
				throw ResponseError.badRequest('Опрос не запущен');
			}
			const shopService = new ShopService(this.req);
			const shops = await shopService.findAllWhereUserIsShopUser(currentUser.id, transaction);
			if (!shops.length) {
				// юзер не является ДМ
				throw ResponseError.badRequest('Пользователю запрещено проходить опросы');
			}
			const shopIds = distinctListById(shops);
			const questToShop = await questionnaireExecutionToShopStore.findOne({
				where: {
					questionnaireId: quest.id,
					questionnaireExecutionId: ranExecution.id,
					shopId: shopIds,
					status: QuestionnaireExecutionToShopStatusEnum.RUNNING,
				},
			}, transaction);
			if (!questToShop) {
				throw ResponseError.badRequest('Опрос завершен или уже был пройден');
			}
			await ranExecution.update({
				answeredCount: ranExecution.answeredCount + 1,
			}, { transaction });
			await questToShop.update({
				status: QuestionnaireExecutionToShopStatusEnum.FINISHED,
				respondingUserId: currentUser.id,
				respondingData: response,
				respondingAt: new Date(),
			}, { transaction });
		});
	}

	/**
	 * Выбирает только последние связанные запуски опросов
	 */
	private getLastExecutionsIds = async (): Promise<number[]> => {
		const execs = await select(`select max(id) as id
                                    from questionnaire_execution
                                    group by questionnaireId`);
		return distinctListById(execs);
	}

	/**
	 * Выбирает все связанные запуски опросов
	 * @param questIds
	 * @param transaction
	 */
	private getExecutionsByQuestionnaireIds = async (questIds: number[], transaction?: Transaction): Promise<typeof QuestionnaireExecution[]> => {
		if (!questIds.length) return [];
		const svc = new QuestionnaireExecutionService(this.req);
		return await svc.findAll({ where: { questionnaireId: questIds } }, transaction);
	}
}
