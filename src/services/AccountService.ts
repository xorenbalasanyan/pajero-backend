import { accountStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('AccountService');

export default class AccountService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, accountStore);
	}
}
