import { userAuthStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('UserAuthService');

export default class UserAuthService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, userAuthStore);
	}
}
