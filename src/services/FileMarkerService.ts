import { fileMarkerStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('FileMarkerService');

export default class FileMarkerService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, fileMarkerStore);
	}
}
