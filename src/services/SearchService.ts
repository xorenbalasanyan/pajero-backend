import { Op } from 'sequelize';
import { UserRoles } from '~enums/UserRoleEnum';
import BaseService from './BaseService';
import { shopStore, userStore } from '~stores';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('SearchService');

export default class SearchService extends BaseService
{
	constructor(req) {
		super(req, moduleLogger);
	}

	async findSubordinateManagers(text, offset = 0) {
		const currentUser = this.req.getAuthorizedUser();
		const { accountId, role } = currentUser;
		const where: any = { accountId };
		// для УПФ нет подчиненных менеджеров
		/* TODO xxx
		if (role === USER_ROLE_TM) where.tmId = currentUser.id;
		else if (role === USER_ROLE_DF) where.dfId = currentUser.id;
		else if (role !== USER_ROLE_DT) return [];
		 */
		// разбиваем строку поиска и добавляем в условия поиска
		const searchParts = String(text || '')
			.replace(/[^\w А-Яа-яёË]+/ig, '')
			.split(' ')
			.map(s => s.trim())
			.filter(s => !!s && s.length >= 2); // не будем искать короткие слова
		if (!searchParts.length) return [];
		const ors: any[] = [];
		searchParts.forEach(s => {
			ors.push({ lastName: { [Op.substring]: s } });
			ors.push({ firstName: { [Op.substring]: s } });
			ors.push({ middleName: { [Op.substring]: s } });
		});
		where[Op.or] = ors;
		// ищем только менеджеров
		// where.role = [USER_ROLE_DT, USER_ROLE_TM, USER_ROLE_UPF]; TODO xxx
		where.role = [UserRoles.CHIEF];
		const options: any = { where, limit: 51 };
		if (offset > 0) {
			options.offset = offset;
		}
		return userStore.findAll(options);
	}

	async findSubordinateShops(text: string, offset = 0) {
		const currentUser = this.req.getAuthorizedUser();
		const { accountId, role } = currentUser;
		const where: any = { accountId };
		/* TODO xxx
		if (role === USER_ROLE_UPF) where.upfId = currentUser.id;
		else if (role === USER_ROLE_TM) where.tmId = currentUser.id;
		else if (role === USER_ROLE_DF) where.dfId = currentUser.id;
		else if (role !== USER_ROLE_DT) return [];
		 */
		// разбиваем строку поиска и добавляем в условия поиска
		const searchParts = String(text || '')
			.replace(/[^\w А-Яа-яёË]+/ig, '')
			.split(' ')
			.map(s => s.trim())
			.filter(s => !!s && s.length >= 2); // не будем искать короткие слова
		if (!searchParts.length) return [];
		const ors: any[] = [];
		searchParts.forEach(s => {
			ors.push({ city: { [Op.substring]: s } });
			ors.push({ address: { [Op.substring]: s } });
			ors.push({ name: { [Op.substring]: s } });
		});
		where[Op.or] = ors;
		const options: any = { where, limit: 51 };
		if (offset > 0) {
			options.offset = offset;
		}
		return shopStore.findAll(options);
	}
}
