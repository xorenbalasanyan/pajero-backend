import fs from 'fs';
import util from 'util';
import FirebaseAdmin from 'firebase-admin';
import { ONE_DAY_MS } from '~enums/DateEnum';
import { PARSE_INPUT_DATA_EXCEPTION } from '~errors/ResponseError';
import { calcPeriods } from '~utils/calcPeriods';
import {
	arrayToMapByField,
	arrayToMapById,
	arrayToSetByField,
	distinctListByField,
	mappingArrayByFields,
} from '~utils/list';
import IncidentImporter from '~lib/IncidentImporter';
import { isFirebaseInitialized } from '~lib/initFirebaseAdmin';
import { select } from '~lib/sequelize';
import { UserSettingRepository } from '~repositories';
import { FileService, GoodService, ShopService } from '~services';
import { fileTypeStore, incidentStore } from '~stores';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import { File, Good, IncidentImport, Shop } from '../stores/db/entity';
import BaseEntityService from './BaseEntityService';
import IncidentImportService from './IncidentImportService';
import LogService from './LogService';
import ShopDiscountService from './ShopDiscountService';
import UserService from './UserService';
import IncidentStatsCuberBackgroundTask
	from '../background/backgroundTasks/IncidentStatsCuberBackgroundTask';

const moduleLogger = new AppLogger('IncidentService');

export default class IncidentService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, incidentStore);
	}

	findAll = options => this.store.findAll(this._fillOptions(options, false));

	findAllUnscoped = options => this.store.findAll(this._fillOptions(options, false));

	/**
	 * Метод выполняет raw query в куб и подтягивает по нему данные.
	 * @param pageInfo
	 */
	findAllForMarking = async (pageInfo) => {
		// защита от инъекций
		const fileMarkerFilter = pageInfo.customParams.fileMarker;
		const orderColumn = pageInfo.orderColumn;
		const orderDirection = pageInfo.orderDirection;
		// формируем запрос
		const from = `FROM cube_incident_to_file c
                      LEFT JOIN file_marker fm ON fm.fileId = c.fileId`;
		const replacements: any = { fileMarkerFilter };
		const where = fileMarkerFilter === 'unmarked' ? 'WHERE fm.marker IS NULL'
			: fileMarkerFilter === 'marked' ? 'WHERE fm.marker IS NOT NULL'
				: fileMarkerFilter === 'unscored' ? 'WHERE fm.score IS NULL'
					: fileMarkerFilter === 'scored' ? 'WHERE fm.score IS NOT NULL'
						: fileMarkerFilter ? 'WHERE fm.marker=:fileMarkerFilter'
							: '';
		const allCount = await select(`SELECT count(*) kount ${from} ${where}`, replacements)
			.then((res: any[]) => res[0].kount);
		const limit = `LIMIT ${pageInfo.pageIndex * pageInfo.pageSize}, ${pageInfo.pageSize}`;
		const orderBy = orderColumn ? `ORDER BY c.${orderColumn} ${orderDirection || 'asc'}` : '';
		const query = `SELECT c.id, c.incidentId, c.goodId, c.fileId fileId, fm.id fileMarkerId,
							  fm.marker fileMarker, fm.score fileScore
				       ${from}
				       ${where}
				       ${orderBy}
				       ${limit}`;
		const items = await select(query, replacements);
		if (items.length) {
			// fill goods
			const goodIds = Array.from(items.reduce((set, i) => set.add(i.goodId), new Set()));
			const goods = await select(`SELECT id, externalId, name FROM good WHERE id IN (${goodIds.join(',')})`);
			const goodMap = arrayToMapById(goods);
			items.forEach(i => {
				const good = goodMap.get(i.goodId) as any;
				if (good) {
					i.goodExternalId = good.externalId;
					i.goodName = good.name;
				}
			});
			// fill files
			const fileIds = Array.from(items.reduce((set, i) => set.add(i.fileId), new Set()));
			const files = await select(`SELECT id, pathName, location, size FROM file WHERE id IN (${fileIds.join(',')})`);
			const fileMap = arrayToMapById(files);
			items.forEach(i => {
				const file = fileMap.get(i.fileId) as any;
				if (file) {
					i.fileLocation = file.location;
					i.filePathName = file.pathName;
					i.fileSize = file.size;
				}
			});
		}
		return [items, {
			pageIndex: pageInfo.pageIndex,
			pageSize: pageInfo.pageSize,
			pageCount: Math.floor((allCount - 1) / pageInfo.pageSize + 1),
			allCount,
			orderColumn: pageInfo.orderColumn,
			orderDirection: pageInfo.orderDirection,
		}];
	}

	/**
	 * Выполняет импорт задач из CSV файла и заполняет Response
	 */
	importIncidentTasksFromCSV = async (res, date, incidentType, csvTmpFileName) => {
		const currentUser = this.req.getAuthorizedUser();
		const logger = this.req.getLogger(moduleLogger);
		// объект импорта
		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-ignore
		const incidentImport = new IncidentImport({
			accountId: currentUser.accountId,
			date,
			incidentTypeId: incidentType.id,
			addedCount: 0,
			ownerId: currentUser.id,
		});
		// список предупреждений
		const warnings = {
			missingRequiredValues: {},
			missingRequiredCounts: {},
			badCodes: [],
			noGoodName: [],
			newGood: [],
			notExistShop: [],
			existTask: [],
			newDoubles: [],
		};

		function mergeWarnings(newWarnings) {
			Object.keys(warnings).forEach(key => {
				if (warnings[key] instanceof Array) {
					// максимум 100 индексов в каждом типе ошибки
					if (newWarnings[key]?.length) {
						const maxAdd = Math.max(0, 100 - warnings[key].length);
						warnings[key] = warnings[key].concat(newWarnings[key].slice(0, maxAdd));
						const countKey = `${key}Count`;
						warnings[countKey] = (warnings[countKey] || 0) + newWarnings[key].length;
					}
				}
			});
			const mrv = warnings.missingRequiredValues;
			const mrc = warnings.missingRequiredCounts;
			const newMrv = newWarnings.missingRequiredValues;
			Object.keys(newMrv).forEach(key => {
				if (newMrv[key].length) {
					// максимум 100 индексов в каждом типе ошибки
					const maxAdd = Math.max(0, 100 - (mrv[key]?.length || 0));
					mrv[key] = (mrv[key] || []).concat(newMrv[key].slice(0, maxAdd));
					mrc[key] = (mrc[key] || 0) + newMrv[key].length;
				}
			});
		}

		// создаем сервисы для работы
		const incidentImporter = new IncidentImporter(incidentType);
		const shopService = new ShopService(this.req);
		const shopDiscountService = new ShopDiscountService(this.req);
		const goodService = new GoodService(this.req);
		const incidentService = new IncidentService(this.req);
		const userService = new UserService(this.req);
		const incidentImportService = new IncidentImportService(this.req);
		const logService = new LogService(this.req);
		const fileService = new FileService(this.req);
		try {
			// перемещаем csv файл из temp папки в userdata
			const file = await this._movePayloadCsvFile(csvTmpFileName);
			logger.info(util.format('Перемещаем файл из %s в %s', csvTmpFileName, file.pathName));
			file.name = `${date}_${incidentType.type}_${file.name}`;
			await file.save();
			incidentImport.fileId = file.id;
			// id магазинов, которые получили новые инциденты
			const shopSetWithNewIncidents = new Set();
			// сколько новых инцидентов добавлено
			let newIncidentsCount = 0;
			const timers = { [Date.now()]: 'начало импорта' };
			// читаем CSV файл в несколько заходов (постранично)
			let pageIndex = 0;
			let lastReadRowNumber = 0;
			const maxReadRows = 10e3; // размер одной страницы
			const existShopsMap = new Map();
			const existShopExIdSet = new Set();
			while (pageIndex * maxReadRows < 1e6) {
				const importResult: any = await incidentImporter
					.importFromFile(file, this.req, lastReadRowNumber, maxReadRows);
				timers[Date.now()] = `прочитано строк = ${pageIndex}`;
				// прочитали часть строчек, отправляем эту пачку в обработку
				let rows = importResult[0];
				const readRowsInCycle = rows.length; // сколько прочитали на этой странице в CSV
				mergeWarnings(importResult[1]);
				lastReadRowNumber = importResult[2] - 1; // минусуем шапку
				// 1) для начала фильтруем по магазинам и уже созданным инцидентам
				rows = await filterByShops(rows, date, incidentType, existShopsMap, existShopExIdSet, shopService, warnings);
				if (rows?.length) {
					// 2) проверяем акции и добавим если есть новые
					await fillAndAddWithShopDiscounts(rows, existShopsMap, shopDiscountService);
					// 3) фильтр новых инцидентов по уже добавленным
					rows = await filterByExistIncidents(rows, date, incidentType, existShopsMap, incidentService, warnings);
					if (rows?.length) {
						// 4) проверяем номенклатуру и добавляем если нужно
						const goodExIds = await checkAndAddGoods(rows, goodService, warnings);
						// 5) создаем новые инциденты
						await createIncidents(rows, date, incidentType, goodExIds, existShopsMap, currentUser, goodService, this);
						timers[Date.now()] = `создано инцидентов = ${pageIndex}`;
						// запоминаем магазины, в которых появились новые инциденты
						Array.from(existShopsMap.values()).forEach(id => shopSetWithNewIncidents.add(id));
						// увеличиваем счетчик новых инцидентов
						newIncidentsCount += rows.length;
					}
				}
				if (readRowsInCycle < maxReadRows) break; // признак для остановки while
				pageIndex++; // увеличиваем счетчик страниц
			}
			// отправляем пуши о новых инцидентах
			await pushNotifiesAboutNewIncidents(Array.from(shopSetWithNewIncidents), userService, logger);
			// дозаполняем IncidentImport
			const warnLogLines = await fillImportEvent(incidentImport, newIncidentsCount, warnings, incidentImportService, logService);
			// архивируем исходный CSV файл
			await this._archivateInputFile(file, fileService);
			timers[Date.now()] = 'входной файл заархивирован';
			// log timers
			logger.info('[Подсчет времени] ' + Object.keys(timers).map(time => {
				const date1 = new Date(Number(time)).toISOString();
				return `${timers[time]} (${date1})`;
			}).join('; '));
			// результат
			res.sendOk({ warnings: warnLogLines?.length ? warnLogLines : undefined });
			// запускаем обновление кубов
			new IncidentStatsCuberBackgroundTask().execute(currentUser.accountId);
		} catch (error: any) {
			logger.error(fillError(Error, `[Ошибка при импорте инцидентов, файл id=${incidentImport.fileId}]`, error));
			const filledWarns: string[] = (makeWarnings(warnings) || []);
			filledWarns.push(util.format('Catched exception:\n%s', String(error)));
			const log = await makeLogForWarns(filledWarns, 'IMPORT_TASK_LIST', logService);
			incidentImport.logId = log ? log.id : undefined;
			incidentImport.warnCount = -1;
			incidentImport.state = 'ERROR'; // TODO change to IncidentImportStateEnum values
			res.sendHttpError(PARSE_INPUT_DATA_EXCEPTION, { warnings: filledWarns });
		}
		return await incidentImport.save();
	}

	/**
	 * Перемещает входной файл в userdata и записывает ссылку на файл в БД.
	 * Возвращает не сохраненный в БД объект File.
	 */
	_movePayloadCsvFile = async csvTmpFileName => {
		const {
			fileName: name,
			pathName,
			fullPath,
		} = FileService.generateNewFileName(undefined, '.csv');
		// перемещение методом rename не работает если файлы располагаются в разных смонтированных
		// файловых системах, поэтому делаем копию и удаляем временный файл
		fs.copyFileSync(csvTmpFileName, fullPath);
		if (!fs.existsSync(fullPath)) {
			throw new Error(util.format('Не удалось скопировать временный файл "%s" по пути "%s"', csvTmpFileName, fullPath));
		}
		fs.unlinkSync(csvTmpFileName);
		const size = fs.statSync(fullPath).size;
		const csvFileType = await fileTypeStore.findIdByExt('csv');
		const currentUser = this.req.getAuthorizedUser();
		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-ignore
		return new File({
			accountId: currentUser.accountId,
			name,
			size,
			fileTypeId: csvFileType.id,
			pathName,
			ownerId: currentUser.id,
		});
	}

	/**
	 * Архивируем исходный CSV файл.
	 */
	_archivateInputFile = async (file, fileService) => {
		// располагаем zip файл рядом с csv
		const sourceFileName = fileService.getFullPath(file.pathName);
		const fullPath = `${sourceFileName}.zip`;
		await FileService.zipOneFile(file.name, sourceFileName, fullPath);
		const size = fs.statSync(fullPath).size;
		const zipFileType = await fileTypeStore.findIdByExt('zip');
		// обновляем File
		Object.assign(file, {
			name: `${file.name}.zip`,
			fileTypeId: zipFileType.id,
			size,
			pathName: `${file.pathName}.zip`,
		});
		await file.save();
		// если все успешно и ошибок нет, грохаем исходный CSV
		fs.unlinkSync(sourceFileName);
	}
}

/**
 * Фильтр данных из CSV по существующим магазинам и уже созданным инцидентам.
 * Возвращает отфильтрпованные строки и список магазинов.
 */
async function filterByShops(rows, date, incidentType, existShopsMap, existShopExIdSet, shopService, warnings) {
	if (!rows?.length) return [];
	// проверяем, какие магазины уже есть в мапе и какие нужно догрузить из базы
	const shopExIdsToLoad = Array.from(rows.reduce((set, i) => (existShopExIdSet.has(i.shopExternalId)) ? set : set.add(i.shopExternalId), new Set()));
	return shopService
		.findAll({
			where: { externalId: shopExIdsToLoad },
			attributes: ['id', 'externalId'],
			raw: true,
		})
		.then((existShops: any[]) => {
			// дозаполняем set и map
			existShops.forEach(i => {
				existShopExIdSet.add(i.externalId);
				existShopsMap.set(i.externalId, i.id);
			});
			rows = rows.filter(({ shopExternalId, rowNumber }) => {
				const has = existShopExIdSet.has(shopExternalId);
				if (!has) {
					if (warnings.notExistShop.length < 100) {
						warnings.notExistShop.push(rowNumber);
					}
					warnings.notExistShopCount = (warnings.notExistShopCount || 0) + 1;
				}
				return has;
			});
			return rows;
		});
}

/**
 * Создание отсутствующих акций для магазинов и заполнение полей shopDiscountId в новых инцидентах.
 */
function fillAndAddWithShopDiscounts(rows, existShopsMap, shopDiscountService) {
	if (!rows?.length) return;
	// ищем новые акции, которые есть в файле импорта и отсутствуют в БД
	const discountNames = distinctListByField(rows, 'discountName').filter(s => !!s);
	if (!discountNames?.length) return;
	return shopDiscountService
		.findAllByNames(discountNames)
		.then(async (existDiscounts: any[]) => {
			const existDiscountNameSet = arrayToSetByField(existDiscounts, 'name');
			const newDiscounts = rows.filter(i => !!i.discountName).reduce((map, i) => {
				const has = existDiscountNameSet.has(i.discountName);
				if (!has) {
					map.set(i.discountName, existShopsMap.get(i.shopExternalId));
				}
				return map;
			}, new Map());
			// создаем новые акции
			if (newDiscounts.size) {
				await shopDiscountService
					.createAll(Array.from(newDiscounts.keys()).map(discountName => ({
						name: discountName,
						shopId: newDiscounts.get(discountName),
					})));
			}
			// обогащаем новые инциденты id-шниками с акциями
			return shopDiscountService
				.findAllByNames(discountNames)
				.then((discounts: any[]) => {
					const discountsTable = arrayToMapByField(discounts, 'name');
					rows.forEach(row => row.shopDiscountId = discountsTable.get(row.discountName).id);
				});
		});
}

/**
 * Фильтрация новых инцидентов по уже созданным с таким же ключом.
 * Возвращает отфильтрованный список новых инцидентов.
 */
async function filterByExistIncidents(rows, date, incidentType, existShopsMap, incidentService, warnings) {
	// фильтр от дублей инцидентов
	const existShopIds = Array.from(existShopsMap.values());
	// создаем карту будущих инцидентов
	const newIncidentMap = rows.reduce((map, i) => {
		const { shopDiscountId, shopExternalId, goodExternalId } = i;
		const key = `${shopDiscountId || ''}_${shopExternalId}_${goodExternalId}`;
		if (map.has(key)) {
			if (warnings.newDoubles.length < 100) {
				warnings.newDoubles.push(i.rowNumber);
			}
			warnings.newDoublesCount = (warnings.newDoublesCount || 0) + 1;
			return map;
		} else {
			return map.set(key, i);
		}
	}, new Map());
	// постранично ищем уже существующие задачи
	let pageIndex = 0;
	const PAGE_SIZE = 30e3;
	while (pageIndex * PAGE_SIZE < 1e6) {
		const existIncidents = await incidentService
			.findAllUnscoped({
				where: {
					date,
					incidentTypeId: incidentType.id,
					shopId: existShopIds,
				},
				attributes: ['shopDiscountId'],
				include: [
					{ model: Shop, as: 'fk_shop', attributes: ['externalId'] },
					{ model: Good, as: 'fk_good', attributes: ['externalId'] },
				],
				offset: pageIndex * PAGE_SIZE,
				limit: PAGE_SIZE,
				raw: true,
			});
		pageIndex++;
		if (!existIncidents?.length) break;
		// фильтруем дубли задач
		existIncidents.forEach(i => {
			const key = `${i.shopDiscountId || ''}_${i['fk_shop.externalId']}_${i['fk_good.externalId']}`;
			const incident = newIncidentMap.get(key);
			if (incident) {
				newIncidentMap.delete(key);
				if (warnings.existTask.length < 100) {
					warnings.existTask.push(incident.rowNumber);
				}
				warnings.existTaskCount = (warnings.existTaskCount || 0) + 1;
			}
		});
	}
	return Array.from(newIncidentMap.values());
}

/**
 * Создает несуществующую номенклутуру.
 * Вовзращает список внешних id номенклатуры из rows.
 */
async function checkAndAddGoods(rows, goodService, warnings) {
	if (!rows?.length) return [];
	// ищем новую номенклатуру, которая есть в файле импорта и отсутствует в БД
	const goodExIds = distinctListByField(rows, 'goodExternalId');
	const existGoods = await goodService
		.findAllByExternalIds(goodExIds);
	const existGoodExIdSet = arrayToSetByField(existGoods, 'externalId');
	const newGoods = rows.reduce((map, i) => {
		const has = existGoodExIdSet.has(i.goodExternalId);
		if (!has) {
			map.set(i.goodExternalId, i.goodName);
			if (warnings.newGood.length < 100) {
				warnings.newGood.push(i.rowNumber);
			}
			warnings.newGoodCount = (warnings.newGoodCount || 0) + 1;
		}
		return map;
	}, new Map());
	// создаем новую номенклатуру
	if (newGoods.size) {
		await goodService
			.createAll(Array.from(newGoods.keys()).map(externalId => ({
				externalId,
				name: newGoods.get(externalId),
			})));
	}
	return goodExIds;
}

/**
 * Создает новые инциденты по данным из списка.
 */
function createIncidents(rows, date, incidentType, goodExIds, existShopsMap, currentUser, goodService, incidentService) {
	if (!rows?.length) return;
	return goodService
		.findAllByExternalIds(goodExIds)
		.then((goods: any[]) => {
			const goodExIdTable = goods.reduce((map, i) => map.set(i.externalId, i.id), new Map());
			const newIncidentDatas = rows.map(item => {
				const {
					shopExternalId,
					shopDiscountId,
					goodExternalId,
				} = item;
				const inputData = { ...item };
				delete inputData.rowNumber;
				delete inputData.shopExternalId;
				delete inputData.shopDiscountId;
				delete inputData.goodExternalId;
				delete inputData.goodName;
				return {
					shopId: existShopsMap.get(shopExternalId),
					shopDiscountId,
					goodId: goodExIdTable.get(goodExternalId),
					incidentTypeId: incidentType.id,
					date,
					inputData: JSON.stringify(inputData),
					ownerId: currentUser.id,
				};
			});
			// сохраняем инциденты
			return incidentService.createAll(newIncidentDatas);
		});
}

/**
 * Отправляет пуши о новых инцидентах.
 */
async function pushNotifiesAboutNewIncidents(shopIdsWithNewIncidents, userService, logger) {
	if (!isFirebaseInitialized()) return;
	// определяем ДМ
	/* TODO xxx
	const shopUsers = await userService
		.findAllDmAndZdmByShopIds(shopIdsWithNewIncidents)
		.catch(error => caughtError = error);
	if (caughtError) throw caughtError;
	 */
	const shopUsers: any[] = [];
	// список FCM
	const userFcms = await UserSettingRepository.findAllFcmTokensByUserIds(shopUsers.map(i => i.id));
	if (!userFcms.length) return undefined;
	const userIdToFcmTable = mappingArrayByFields(userFcms, 'userId', 'value');
	// collect
	const notifications = shopUsers.reduce((arr, shopUser) => {
		const fcmToken = userIdToFcmTable.get(shopUser.id);
		if (!fcmToken) return arr;
		const item = {
			token: fcmToken,
			notification: {
				title: '📋 Новые задачи',
				body: `В магазине появились новые задачи`,
			},
			android: {
				ttl: ONE_DAY_MS, // завтра уже не нужно отправлять
			},
		};
		arr.push(item);
		return arr;
	}, []);
	// send notification messages
	if (notifications && notifications.length) {
		// firebase может отправлять за раз не более 500 уведомлений
		let part = 1;
		while (notifications.length > 0) {
			const cuttedNotifications = notifications.splice(0, 500);
			await FirebaseAdmin.messaging()
				.sendAll(cuttedNotifications)
				.then((response) => {
					logger.info(util.format('[Уведомление о новых инцидентах] Результат отправки (партия %d) в Firebase: %o', part++, response));
				})
				.catch(error => {
					logger.error(fillError(Error, '[Ошибка отправки уведомлений в Firebase]', error));
				});
		}
	}
}

/**
 * Заполняем и сохраняем IncidentImport.
 */
function fillImportEvent(incidentImport, newIncidentsCount, warnings, incidentImportService, logService) {
	// обогащаем и сохраняем IncidentImport событие
	const filledWarns = makeWarnings(warnings);
	return makeLogForWarns(filledWarns, 'IMPORT_TASK_LIST', logService)
		.then((log: any) => {
			incidentImport.logId = log?.id;
			incidentImport.addedCount = newIncidentsCount || 0;
			incidentImport.warnCount = warnings.badCodes.length + warnings.noGoodName.length
				+ warnings.notExistShop.length + warnings.newDoubles.length;
			incidentImport.state = log ? 'WARN' : 'OK'; // TODO change to IncidentImportStateEnum values
			return filledWarns;
		});
}

function makeWarnings(w) {
	const res: string[] = [];
	if (w.badCodes.length) {
		res.push(`${calcPeriods(w.badCodes)} неверно заполнены коды, эти записи не будут добавлены.`
			+ (w.badCodes.length < w.badCodesCount
				? ` Выведены не все номера строк, всего предупреждений: ${w.badCodesCount}.`
				: ''));
	}
	const mrv = w.missingRequiredValues;
	const mrc = w.missingRequiredCounts;
	if (mrv && Object.keys(mrv).length) {
		Object.keys(mrv).forEach(columnName => {
			res.push(`${calcPeriods(mrv[columnName])} в столбце '${columnName}' отсутствуют `
				+ `обязательные значения или не удовлетворяют формату, эти записи не будут добавлены.`
				+ (mrv[columnName].length < mrc[columnName]
					? ` Выведены не все номера строк, всего предупреждений: ${mrc[columnName]}.`
					: ''));
		});
	}
	if (w.noGoodName.length) {
		res.push(`${calcPeriods(w.noGoodName)} не указана номенклатура, эти записи не будут добавлены.`
			+ (w.noGoodName.length < w.noGoodNameCount
				? ` Выведены не все номера строк, всего предупреждений: ${w.noGoodNameCount}.`
				: ''));
	}
	if (w.newGood.length) {
		res.push(`${calcPeriods(w.newGood)} обнаружена номенклатура, которая отсутствовала и будет добавлена в базу.`
			+ (w.newGood.length < w.newGoodCount
				? ` Выведены не все номера строк, всего предупреждений: ${w.newGoodCount}.`
				: ''));
	}
	if (w.notExistShop.length) {
		res.push(`${calcPeriods(w.notExistShop)} указан магазин, которого нет в базе, эти записи не будут добавлены.`
			+ (w.notExistShop.length < w.notExistShopCount
				? ` Выведены не все номера строк, всего предупреждений: ${w.notExistShopCount}.`
				: ''));
	}
	if (w.existTask.length) {
		res.push(`${calcPeriods(w.existTask)} указана задача, которая уже была раньше добавлена в базу, эти записи не будут добавлены.`
			+ (w.existTask.length < w.existTaskCount
				? ` Выведены не все номера строк, всего предупреждений: ${w.existTaskCount}.`
				: ''));
	}
	if (w.newDoubles.length) {
		res.push(`${calcPeriods(w.newDoubles)} указан дубль для другой задачи в этом файле импорта, эти записи не будут добавлены.`
			+ (w.newDoubles.length < w.newDoublesCount
				? ` Выведены не все номера строк, всего предупреждений: ${w.newDoublesCount}.`
				: ''));
	}
	return res.length ? res : null;
}

function makeLogForWarns(warnList, type, logService): Promise<any | null> {
	if (!warnList) return Promise.resolve(null);
	return logService.createOne({
		type,
		message: warnList.join('\n'),
	});
}
