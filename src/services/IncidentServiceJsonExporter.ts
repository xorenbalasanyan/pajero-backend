import { IncidentService } from './index';
import { Good, Shop } from '../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('IncidentServiceJsonImporter');

export default class IncidentServiceJsonExporter
{
	/**
	 * Выбирает завершенные задачи и заполняет Response.
	 */
	static async exportIncidentResultData(req, res, date, incidentType) {
		const incidentService = new IncidentService(req);
		const incidents = await incidentService.findAll({
			where: {
				isReported: true,
				date,
				incidentTypeId: incidentType.id,
			},
			attributes: ['id', 'report', 'inputData', 'state'],
			include: [
				{ model: Shop, as: 'fk_shop', attributes: ['externalId'] },
				{ model: Good, as: 'fk_good', attributes: ['externalId'] },
			],
			raw: true,
		});
		const { outputData: meta } = JSON.parse(incidentType.data);
		const result = incidents.map(incident => {
			const inputData = JSON.parse(incident.inputData || '{}');
			const report = JSON.parse(incident.report || '{}');
			const values = {};
			meta.forEach(field => {
				const [section, keyName] = (field.from || '').split('.');
				const isPhotoExists = field.valueType === 'PHOTO_EXISTS';
				let value;
				if (section === 'inputData') {
					value = inputData[keyName];
				} else if (section === 'report') {
					value = report[keyName];
				}
				if (isPhotoExists) {
					value = !!(value && value.fileId);
				}
				values[field.id] = value;
			});
			return ({
				task_id: incident.id,
				shop_id: incident['fk_shop.externalId'],
				vendor_code: incident['fk_good.externalId'],
				executed: incident.state === 'DONE', // TODO change to IncidentStateEnum
				...values,
			});
		});
		return res.json(result);
	}
}
