import { Op, Transaction } from 'sequelize';
import { shopStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { ShopManager, ShopManagerRole, User } from '../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';
import { ShopManagerRoleService, ShopManagerService } from './index';
import Shop from '../stores/db/entity/Shop';

const moduleLogger = new AppLogger('ShopService');

export default class ShopService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, shopStore);
	}

	findAllByExternalIds = async (externalIds, transaction?: Transaction) => this.findAll({ where: { externalId: externalIds } }, transaction);

	findAllByRegionIds = async (regionIds, transaction?: Transaction) => this.findAll({ where: { regionId: regionIds } }, transaction);

	addMeta = async (options, pageInfo) => {
		// нужно ли прикрепить пользователей к выгрузке
		if (pageInfo.meta?.has('users')) {
			if (!options.include) options.include = [];
			// required позволяет делать LEFT JOIN вместо INNER JOIN
			options.include.push({
				model: ShopManager,
				attributes: ['id', 'shopId', 'roleId', 'userId'],
				required: false,
				include: [{
					model: ShopManagerRole,
					as: 'shopManagerRole',
					attributes: ['id', 'title', 'shortTitle', 'engShortTitle', 'subLevel', 'isShopUser'],
					required: false,
				}, {
					model: User,
					as: 'user',
					attributes: ['id', 'firstName', 'lastName', 'middleName'],
					required: false,
				}],
			});
		}
	}

	addWhereSearch = (options, { meta, searchParts }) => {
		if (!searchParts?.length) return;
		const { where } = options;
		if (!where[Op.or]) where[Op.or] = [];
		// избавляемся от подзапросов, иначе не получится сделать поиск по полям
		options.subQuery = false;
		// для каждого поля добавляем поиск
		searchParts?.forEach(str => {
			where[Op.or].push({ id: { [Op.substring]: str } });
			where[Op.or].push({ externalId: { [Op.substring]: str } });
			where[Op.or].push({ name: { [Op.substring]: str } });
			if (meta?.has('users')) {
				where[Op.or].push({ '$shopManagers.user.firstName$': { [Op.substring]: str } });
				where[Op.or].push({ '$shopManagers.user.lastName$': { [Op.substring]: str } });
				where[Op.or].push({ '$shopManagers.user.middleName$': { [Op.substring]: str } });
			}
		});
	}

	/**
	 * Выбираем магазины, в которых пользователь является директором магазина
	 * @param userId
	 * @param transaction
	 */
	findAllWhereUserIsShopUser = async (userId: number, transaction?: Transaction): Promise<typeof Shop[]> => {
		const shopManagerService = new ShopManagerService(this.req);
		const shopManagers = await shopManagerService.findAll({
			where: {
				userId,
			},
			attributes: ['shopId', 'roleId'],
			raw: true,
		}, transaction);
		const shopManagerRoleService = new ShopManagerRoleService(this.req);
		const roles = await shopManagerRoleService.findAllWithAccountId({
			where: {
				id: shopManagers.map(i => i.roleId),
				isShopUser: true,
			},
			attributes: ['id'],
			raw: true,
		}, transaction);
		const roleIds = roles.map(i => i.id);
		const shopIds = shopManagers.filter(i => roleIds.includes(i.roleId)).map(i => i.shopId);
		const shopService = new ShopService(this.req);
		return await shopService.findAllWithAccountId({
			where: {
				id: shopIds,
			},
		}, transaction);
	}
}
