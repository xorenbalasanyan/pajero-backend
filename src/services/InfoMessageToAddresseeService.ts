import { Op } from 'sequelize';
import { infoMessageToAddresseeStore } from '~stores';
import BaseEntityService from './BaseEntityService';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('InfoMessageToAddresseeService');

export default class InfoMessageToAddresseeService extends BaseEntityService
{
	constructor(req) {
		super(req, moduleLogger, infoMessageToAddresseeStore);
	}

	findAllByInfoMessageIds = async ids => this.store.findAll({
		where: {
			infoMessageId: {
				[Op.in]: ids,
			},
		},
	})

	countByStatusAndInfoMessageIds = async (status, ids) => this.store.count({
		where: {
			status,
			infoMessageId: { [Op.in]: ids },
		},
	})
}
