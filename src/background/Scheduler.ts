import moment from 'moment';
import { ScheduleJobUnitEnum } from '~enums/ScheduleJobUnitEnum';
import { ONE_DAY_MS, ONE_HOUR_MS, ONE_MINUTE_MS } from '~enums/DateEnum';
import BaseScheduleJob from './scheduleJobs/BaseScheduleJob';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import { toRoundFixed } from '~utils/numbers';

// рамки времени для срабатывания шедулера
const TIMER_TICK_MIN = 30_000; // шедулер срабатывает не раньше чем через 30 секунд
const TIMER_TICK_MAX = 300_000; // шедулер срабатывает не позднее чем через 5 минут

// порог превышения времени выполнения джобы, 2 минуты
// если джоба затратит больше времени на выполнение, в логе выведется ошибка
const WARNING_EXECUTION_LENGTH = 120_000;

type JobStatus = {
	nextRunTime: number
	isRunning: boolean
	executedAt: number
};

/**
 * @Singleton
 * Шедулер выполняет фоновые задачи по расписанию
 */
export default class Scheduler
{
	private static _instance: Scheduler;
	private readonly logger = new AppLogger(Scheduler.name);

	private timerId: NodeJS.Timeout | undefined = undefined;
	private jobs: BaseScheduleJob[] = [];
	private jobStatus = new Map<BaseScheduleJob, JobStatus>();

	private constructor() {}

	static getInstance(): Scheduler {
		return this._instance || (this._instance = new this());
	}

	// старт шедулера
	start() {
		this.tick();
	}

	// прекращение работы шедулера
	stop() {
		if (this.timerId) {
			clearTimeout(this.timerId);
		}
		// сообщаем каждой джобе о завершении
		this.jobs.forEach(job => job.stop());
	}

	// добавляет джобу в список
	addJob(job: BaseScheduleJob) {
		this.jobs.push(job);
		this.jobStatus.set(job, {
			nextRunTime: Infinity, // счетчик будет пересчитан
			isRunning: false,
			executedAt: 0,
		});
		// сразу рассчитываем время следующего запуска
		this.calculateJobNextRun(job);
		// если джобу нужно запустить при старте, запускаем
		if (job.schedule.executeOnStart) {
			this.executeJob(job);
		}
	}

	// рассчитывает для джобы время следующего срабатывания
	private calculateJobNextRun(job: BaseScheduleJob) {
		const now = moment().utcOffset(0);
		const jobStatus = this.jobStatus.get(job)!;
		if (job.schedule.every) {
			const mult = job.schedule.every.unit === ScheduleJobUnitEnum.MINUTE ? ONE_MINUTE_MS
				: job.schedule.every.unit === ScheduleJobUnitEnum.HOUR ? ONE_HOUR_MS
					: ONE_DAY_MS;
			jobStatus.nextRunTime = now.valueOf() + job.schedule.every.value * mult;
		} else if (job.schedule.launch) {
			// текущее время в часах и минутах
			const nowMins = now.hours() * 60 + now.minutes();
			const launchMins = job.schedule.launch.hour * 60 + job.schedule.launch.minute;
			if (nowMins >= launchMins) {
				// запуск будет выполнен на следующий день
				jobStatus.nextRunTime = now.endOf('day').valueOf() + launchMins * 60_000;
			} else {
				// запуск будет выполнен в текущий день
				jobStatus.nextRunTime = now.startOf('day').valueOf() + launchMins * 60_000;
			}
		} else {
			throw new Error(`Для джобы "${job.jobName}" не указано расписание`);
		}
	}

	// выполняет джобу
	private executeJob = (job: BaseScheduleJob) => {
		const checkStop = () => {
			const jobStatus1 = this.jobStatus.get(job)!;
			jobStatus1.isRunning = false;
			const jobLength1 = Date.now() - jobStatus1.executedAt;
			const jobLengthS1 = `${toRoundFixed(jobLength1 / 1000, 2)} сек.`;
			if (jobLength1 > WARNING_EXECUTION_LENGTH) {
				this.logger.warn(`Джоба "${job.jobName}" работала ${jobLengthS1}, что превышает порог длительности выполнения для джобы`);
			} else {
				this.logger.info(`Джоба "${job.jobName}" завершилась, продолжительность выполнения: ${jobLengthS1}`);
			}
			this.calculateJobNextRun(job);
		};

		const jobStatus = this.jobStatus.get(job)!;
		// если джоба еще выполняется, то запускать не будем
		if (jobStatus.isRunning) {
			const jobLength2 = Date.now() - jobStatus.executedAt;
			const jobLengthS2 = `${toRoundFixed(jobLength2 / 1000, 2)} сек.`;
			// дадим этой джобе еще 3 минуты на завершение работы
			this.logger.warn(`Старт джобы "${job.jobName}" отложен на 3 минуты, потому что она все еще работает. Текущее время работы джобы: ${jobLengthS2}`);
			jobStatus.nextRunTime = Date.now() + 3 * ONE_MINUTE_MS;
			return;
		}
		jobStatus.isRunning = true;
		jobStatus.executedAt = Date.now();
		try {
			// запускаем джобу
			this.logger.info(`Шедулер стартует джобу '${job.jobName}'`);
			const promise = job.execute();
			// если джоба возвращает промис, то придется его дождаться
			if (promise !== null && promise instanceof Promise) {
				promise
					.catch(error => {
						const jobLength = Date.now() - jobStatus.executedAt;
						const jobLengthS = `${toRoundFixed(jobLength / 1000, 2)} сек.`;
						this.logger.error(fillError(Error,
							`[Ошибка в работе Scheduler] Джоба "${job.jobName}" отработала ${jobLengthS} и вернула ошибку`,
							error));
					})
					.finally(checkStop);
			} else {
				checkStop();
			}
		} catch (error: any) {
			const jobLength = Date.now() - jobStatus.executedAt;
			const jobLengthS = `${toRoundFixed(jobLength / 1000, 2)} сек.`;
			this.logger.error(fillError(Error,
				`[Ошибка в работе Scheduler] Джоба "${job.jobName}" отработала ${jobLengthS} и завершилась с ошибкой`,
				error));
			checkStop();
		}
	}

	// циклический метод, проверяет джобы, выполняет и перезапускает
	private tick() {
		if (this.timerId) {
			clearTimeout(this.timerId);
		}
		let now = Date.now();
		this.jobs.forEach(job => {
			const nextTime = (this.jobStatus.get(job) as any).nextRunTime;
			// если по какой-то причине расписание еще не было создано, то все равно выполняем джобу
			if (!nextTime || now > nextTime) {
				this.executeJob(job);
			}
		});
		// вычисляем время следующего срабатывания шедулера
		now = Date.now();
		const jobNextTimes = Array.from(this.jobStatus.values()).map(i => i.nextRunTime - now);
		const nextRunTime = Math.max(TIMER_TICK_MIN, Math.min(TIMER_TICK_MAX, ...jobNextTimes));
		this.logger.info(`Шедулер ушел спать на ${(nextRunTime / 1000).toFixed(2)} сек.`);
		this.timerId = setTimeout(() => this.tick(), nextRunTime);
	}
}
