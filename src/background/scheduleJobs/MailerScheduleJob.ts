import BaseScheduleJob from './BaseScheduleJob';
import MailerBackgroundTask from '../backgroundTasks/MailerBackgroundTask';

export default class MailerScheduleJob extends BaseScheduleJob
{
	async execute() {
		return new MailerBackgroundTask().execute();
	}
}
