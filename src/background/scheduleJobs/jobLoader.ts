import util from 'util';
import { ScheduleJobUnitEnum } from '~enums/ScheduleJobUnitEnum';
import { validateInteger, validateIsBoolean } from '~utils/validate';
import config from '~lib/config';
import BaseScheduleJob, { JobTimetable } from './BaseScheduleJob';
import {
	FileClouderScheduleJob,
	IncidentReporterScheduleJob,
	IncidentStatsCuberScheduleJob,
	IncidentToFileCuberScheduleJob,
	InfoMessageReaderScheduleJob,
	MailerScheduleJob,
	OldDataCleanScheduleJob,
	QuestionnaireScheduleJob,
	StructureAnalysisScheduleJob,
} from './index';
import { BackgroundTaskExecuteOptions } from '../backgroundTasks/BaseBackgroundTask';

export const JOB_CLASSES = {
	OldDataCleanScheduleJob,
	IncidentReporterScheduleJob,
	FileClouderScheduleJob,
	MailerScheduleJob,
	InfoMessageReaderScheduleJob,
	IncidentToFileCuberScheduleJob,
	IncidentStatsCuberScheduleJob,
	QuestionnaireScheduleJob,
	StructureAnalysisScheduleJob,
};

const CONFIG_SECTION = 'schedulerJobs';

/**
 * Загружает джобы из конфига
 */
export function loadJobsFromConfig(): BaseScheduleJob[] | undefined {
	const jobsConfig = config.get(CONFIG_SECTION);
	if (!jobsConfig?.length) {
		return undefined;
	}
	return jobsConfig
		.map((jobConfig: any): BaseScheduleJob | null => {
			const {
				jobClassName,
				every: {
					value: everyValue,
					unit: everyUnit,
				} = {} as any,
				launch: {
					hour: launchHour,
					minute: launchMinute,
				} = {} as any,
				options,
				executeOnStart,
				enabled,
			} = jobConfig;
			if (enabled !== undefined) {
				const checkEnabled = validateIsBoolean()(enabled);
				if (checkEnabled) {
					throw new Error(`Значение 'enabled' указано с ошибкой (указано ${util.format('%o', enabled)}): ${checkEnabled}. Проверьте раздел конфигурации '${CONFIG_SECTION}'.`);
				}
				if (enabled === false) {
					return null;
				}
			}
			const jobClass = JOB_CLASSES[jobClassName];
			if (!jobClass) {
				throw new Error(`Значение 'jobClassName' указано с ошибкой: Класс ${util.format('%o', jobClassName)} не реализован. Проверьте раздел конфигурации '${CONFIG_SECTION}'.`);
			}
			const checkExec = validateIsBoolean()(executeOnStart);
			if (checkExec) {
				throw new Error(`Значение 'executeOnStart' указано с ошибкой (указано ${util.format('%o', executeOnStart)}): ${checkExec}. Проверьте раздел конфигурации '${CONFIG_SECTION}'.`);
			}
			// заполним объект настроек, дальше будем его обогащать
			const jobSettings: JobTimetable = {
				executeOnStart,
			};
			let executeOptions: BackgroundTaskExecuteOptions;
			// заполняем опции для задачи
			if (options) {
				if (options instanceof Array) {
					executeOptions = options;
				} else if (typeof options === 'object') {
					executeOptions = new Map();
					for (const key in options) {
						executeOptions.set(key, options[key]);
					}
				} else {
					executeOptions = options;
				}
			}
			if (everyUnit && everyValue) {
				const everyUnit1: ScheduleJobUnitEnum = ScheduleJobUnitEnum[everyUnit];
				if (!everyUnit1) {
					throw new Error(`Значение 'unit' указано с ошибкой: Временная константа '${util.format('%o', everyUnit)}' не существует. Проверьте раздел конфигурации '${CONFIG_SECTION}'.`);
				}
				const checkValue = validateInteger(1, 100)(everyValue);
				if (checkValue) {
					throw new Error(`Значение 'value' указано с ошибкой (указано ${util.format('%o', everyValue)}): ${checkValue}. Проверьте раздел конфигурации '${CONFIG_SECTION}'.`);
				}
				jobSettings.every = {
					unit: everyUnit1,
					value: everyValue,
				};
			} else if (launchHour >= 0 && launchMinute >= 0) {
				const check1 = validateInteger(0, 23)(launchHour);
				if (check1) {
					throw new Error(`Значение 'hour' указано с ошибкой: ${check1}`);
				}
				const check2 = validateInteger(0, 59)(launchMinute);
				if (check2) {
					throw new Error(`Значение 'minute' указано с ошибкой: ${check1}`);
				}
				jobSettings.launch = {
					hour: launchHour,
					minute: launchMinute,
				};
			}
			return new jobClass(jobSettings, executeOptions);
		})
		.filter(i => !!i);
}
