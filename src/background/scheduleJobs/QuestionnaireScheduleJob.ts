import BaseScheduleJob from './BaseScheduleJob';
import QuestionnaireBackgroundTask from '../backgroundTasks/QuestionnaireBackgroundTask';

export default class QuestionnaireScheduleJob extends BaseScheduleJob
{
	async execute() {
		// TODO пока только один аккаунт, поэтому обновляем куб для первого аккаунта
		return new QuestionnaireBackgroundTask().execute(100);
	}
}
