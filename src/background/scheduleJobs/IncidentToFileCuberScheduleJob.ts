import BaseScheduleJob from './BaseScheduleJob';
import IncidentToFileCuberBackgroundTask
	from '../backgroundTasks/IncidentToFileCuberBackgroundTask';

export default class IncidentToFileCuberScheduleJob extends BaseScheduleJob
{
	async execute() {
		// TODO пока только один аккаунт, поэтому обновляем куб для первого аккаунта
		return new IncidentToFileCuberBackgroundTask().execute(100);
	}
}
