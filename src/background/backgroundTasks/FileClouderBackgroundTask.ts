import path from 'path';
import { Op } from 'sequelize';
import { fileStore, fileTypeStore } from '~stores';
import { FileLocationEnum } from '~enums/FileLocationEnum';
import SelectelRequester from '~lib/SelectelRequester';
import config from '~lib/config';
import { FileService } from '~services';
import BaseBackgroundTask, {
	BackgroundTaskExecuteOptions,
	BackgroundTaskReturnType,
} from './BaseBackgroundTask';
import { fillError } from '~utils/errors';

const USERDATA_PATH = config.get('paths:userdata');
const SELECTEL_CONFIG = config.get('selectelCloud');

const DEFAULT_FILE_UPLOAD_MAX = 50; // количество файлов, загружаемых в облако за один запуск по умолчанию

/**
 * Задача загружает файлы в облачное хранилище.
 * В опциях принимает Map с полями:
 * - uploadLimit: Integer (not required, [1..Infinity]) - ограничение по аплоаду файлов за один запуск
 * - extensions: String[] (not required) - выгружать только указанные типы файлов
 */
export default class FileClouderBackgroundTask extends BaseBackgroundTask
{
	protected override async start(options?: BackgroundTaskExecuteOptions): Promise<BackgroundTaskReturnType> {
		// определяем лимит из настроек или берем дефолтный/sa
		const tempLimit: number | undefined = (options instanceof Map && options.has('uploadLimit'))
			? Number(options.get('uploadLimit')) : undefined;
		const uploadLimit = tempLimit && Number.isInteger(tempLimit) && tempLimit > 0
			? tempLimit : DEFAULT_FILE_UPLOAD_MAX;
		// определяем какие типы файлов будем выгружать
		const tempExts: any = (options instanceof Map && options.has('extensions'))
			? options.get('extensions') : undefined;
		const onlyExtensions: string[] | undefined = (tempExts instanceof Array)
			? tempExts.map(s => String(s)) : undefined;
		const extIds: number[] | undefined = onlyExtensions?.length
			? await fileTypeStore
				.findAll({
					where: { ext: onlyExtensions },
					attributes: ['id'],
				})
				.then(rows => rows.map((i: any) => i.id))
				.catch(error => {
					throw fillError(Error, `[Ошибка получения списка типов файлов]: ${JSON.stringify({ onlyExtensions })}`, error);
				})
			: undefined;
		// получаем список свежих файлов, которые еще не в облаке
		const where: any = {
			location: FileLocationEnum.FS,
			createdAt: {
				// FIXME временное решение для удаленных файлов, чтобы не сыпались ошибки
				// на проде были удалены старые файлы физически, но остались записи в file
				// как только пройдет три месяца, можно будет снять ограничение
				[Op.gt]: '2022-04-01',
			},
		};
		if (extIds?.length) {
			where.fileTypeId = extIds;
		}
		const files: any[] = await fileStore
			.findAll({
				where,
				limit: uploadLimit,
				order: [['createdAt', 'DESC']],
			})
			.catch(error => {
				throw fillError(Error, `[Ошибка получения списка файлов]: ${JSON.stringify({ where, limit: uploadLimit })}`, error);
			});

		// авторизуемся в Selectel
		const selectelRequester = new SelectelRequester(
			SELECTEL_CONFIG.username,
			SELECTEL_CONFIG.password,
			SELECTEL_CONFIG.accountPath,
			SELECTEL_CONFIG.containerName);
		await selectelRequester.auth()
			.catch(error => {
				throw fillError(Error, `[Ошибка подключения к Selectel]`, error);
			});

		const fileService = new FileService(null);

		for (const file of files) {
			// ошибки игнорируем, просто выводим в лог
			await uploadFile(selectelRequester, fileService, file)
				.catch(error => {
					throw fillError(Error, `[Ошибка загрузки файла в Selectel]: ${JSON.stringify({ fileId: file.id })}`, error);
				});
		}
	}
}

function uploadFile(selectelRequester: SelectelRequester, fileService: FileService, file: any) {
	const fsPath = path.resolve(USERDATA_PATH, file.pathName);
	const oldPathName = file.pathName;
	return selectelRequester.sendFile(fsPath, file.pathName)
		.then(() => {
			// обновляем файл в БД
			file.location = FileLocationEnum.CDN;
			file.pathName = SELECTEL_CONFIG.pathBase + file.pathName;
			file.moderatorId = -1;
			return file.save();
		})
		.then(() => {
			// перемещаем локальный файл в мусорку
			fileService.moveFileToDeleted(oldPathName, file.name);
		});
}
