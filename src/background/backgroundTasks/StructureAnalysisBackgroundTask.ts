import util from 'util';
import { validateInteger } from '~utils/validate';
import BaseBackgroundTask, { BackgroundTaskReturnType } from './BaseBackgroundTask';
import {
	accountSettingStore,
	shopManagerRoleStore,
	shopManagerStore,
	shopStore,
	userStore,
} from '~stores';
import StructureAnalisysStats from './StructureAnalysisStats';
import { UserRoles } from '~enums/UserRoleEnum';
import { arrayToMapById, distinctListByField, distinctListById } from '~utils/list';
import { MailService } from '~services';
import { MailMessage, Shop, ShopManager, ShopManagerRole } from '../../stores/db/entity';
import { fillError } from '~utils/errors';
import User from '../../stores/db/entity/User';

/**
 * Задача проверяет структуру на предмет соответствия привязок и ролей.
 */
export default class StructureAnalysisBackgroundTask extends BaseBackgroundTask
{
	protected override start(accountId: number): Promise<BackgroundTaskReturnType> {
		return new Promise((resolve, reject) => {
			// обновляем куб для одного аккаунта
			const check = validateInteger(1, Number.MAX_SAFE_INTEGER)(accountId);
			if (check) {
				throw new Error(util.format(
					'При запуске задачи "StructureAnalysisBackgroundTask" передано значение accountId=%o, ошибка валидации: %s',
					accountId,
					check));
			}
			checkStructureForAccountId(accountId)
				.then(() => resolve(undefined))
				.catch(error => reject(fillError(Error, 'Ошибка анализа структуры', error)));
		});
	}

	// перепишу метод чтобы ограничить входные данные
	override execute(accountId: number): Promise<BackgroundTaskReturnType> {
		return super.execute(accountId);
	}
}

type Shops = (typeof Shop & {
	_managers: {
		user: typeof User,
		role: typeof ShopManagerRole,
	}[],
})[];
type ShopManagers = (typeof ShopManager)[];
type ShopManagerRoles = (typeof ShopManagerRole)[];
type Users = (typeof User & {
	_management: {
		shop: typeof Shop,
		role: typeof ShopManagerRole,
	}[],
})[];

async function checkStructureForAccountId(accountId: number) {
	const shops: Shops = await shopStore.findAll({ where: { accountId }, raw: true });
	const shopManagers: ShopManagers = await shopManagerStore.findAll({
		where: { shopId: distinctListById(shops) },
		raw: true,
	});
	const shopManagerRoles: ShopManagerRoles = await shopManagerRoleStore.findAll({
		where: { id: distinctListByField(shopManagers, 'roleId') },
		raw: true,
	});
	const users: Users = await userStore.findAll({ where: { accountId }, raw: true });
	// привязываем ссылки к моделям.
	fillLinks(shops, shopManagers, shopManagerRoles, users);
	// проверяем поля магазинов и пользователей
	const stats = new StructureAnalisysStats();
	checkShops(shops, stats);
	checkUsers(users, stats);
	// создаем письмо с отчетом
	return createStatsMessage(stats, accountId);
}

// привязка ссылок к моделям
function fillLinks(shops: Shops, shopManagers: ShopManagers, shopManagerRoles: ShopManagerRoles, users: Users) {
	const shopMap = arrayToMapById(shops);
	const userMap = arrayToMapById(users);
	const shopManagerRoleMap = arrayToMapById(shopManagerRoles);
	shopManagers.forEach(sm => {
		const shop = shopMap.get(sm.shopId);
		const user = userMap.get(sm.userId);
		const role = shopManagerRoleMap.get(sm.roleId);
		if (!shop._managers) shop._managers = [];
		shop._managers.push({ user, role });
		if (user) {
			if (!user._management) user._management = [];
			user._management.push({ shop, role });
		}
	});
}

// проверка заполенных полей магазинов
function checkShops(shops: Shops, stats: StructureAnalisysStats): void {
	const emptyExternalId: Shops = [];
	const emptyCity: Shops = [];
	const emptyName: Shops = [];
	const emptyRegion: Shops = [];
	const emptyAddress: Shops = [];
	const emptyPhone: Shops = [];
	const emptyEmail: Shops = [];
	const emptyTimeOffset: Shops = [];
	const emptyDf: Shops = [];
	const emptyTm: Shops = [];
	const emptyUpf: Shops = [];
	const emptyDmButHasZdm: Shops = [];
	const emptyDm: Shops = [];
	const manyDf: Shops = [];
	const manyTm: Shops = [];
	const manyUpf: Shops = [];
	const manyDm: Shops = [];
	const deadShopManagers: Shops = [];
	const timeOffsets: Record<string, number> = {};
	shops.forEach(shop => {
		if (!shop.externalId) emptyExternalId.push(shop);
		if (!shop.name) emptyName.push(shop);
		if (!shop.city) emptyCity.push(shop);
		if (!shop.regionId) emptyRegion.push(shop);
		if (!shop.address) emptyAddress.push(shop);
		if (!shop.phone) emptyPhone.push(shop);
		if (!shop.email) emptyEmail.push(shop);
		const dfCount = shop._managers.filter(m => m.role.engShortTitle === 'DF' && m.user).length;
		const tmCount = shop._managers.filter(m => m.role.engShortTitle === 'TM' && m.user).length;
		const upfCount = shop._managers.filter(m => m.role.engShortTitle === 'UPF' && m.user).length;
		const dmCount = shop._managers.filter(m => m.role.engShortTitle === 'DM' && m.user).length;
		const zdmCount = shop._managers.filter(m => m.role.engShortTitle === 'ZDM' && m.user).length;
		if (!dfCount) emptyDf.push(shop);
		if (!tmCount) emptyTm.push(shop);
		if (!upfCount) emptyUpf.push(shop);
		if (!dmCount && zdmCount) emptyDmButHasZdm.push(shop);
		if (!dmCount && !zdmCount) emptyDm.push(shop);
		if (dfCount > 1) manyDf.push(shop);
		if (tmCount > 1) manyTm.push(shop);
		if (upfCount > 1) manyUpf.push(shop);
		if (dmCount > 1) manyDm.push(shop);
		const deadUsers = shop._managers.filter(m => !m.user).length;
		if (deadUsers) deadShopManagers.push(shop);
		if (!shop.timeOffset) {
			emptyTimeOffset.push(shop);
		} else {
			timeOffsets[shop.timeOffset] = (timeOffsets[shop.timeOffset] || 0) + 1;
		}
	});
	if (emptyExternalId.length) stats.addError('У следующих магазинов не указан ВНЕШНИЙ ИДЕНТИФИКАТОР', emptyExternalId);
	if (emptyName.length) stats.addError('У следующих магазинов не указано НАЗВАНИЕ', emptyName);
	if (emptyCity.length) stats.addError('У следующих магазинов не указан ГОРОД', emptyCity);
	if (emptyRegion.length) stats.addError('У следующих магазинов не указан РЕГИОН', emptyRegion);
	if (emptyAddress.length) stats.addError('У следующих магазинов не указан АДРЕС', emptyAddress);
	if (emptyTimeOffset.length) stats.addError('У магазинов не указан ЧАСОВОЙ ПОЯС', emptyTimeOffset);
	if (emptyDf.length) stats.addError('У следующих магазинов ОТСУТСТВУЕТ ДФ', emptyDf, true);
	if (emptyTm.length) stats.addWarn('У следующих магазинов ОТСУТСТВУЕТ ТМ', emptyTm, true);
	if (emptyUpf.length) stats.addWarn('У следующих магазинов ОТСУТСТВУЕТ УПФ', emptyUpf, true);
	if (emptyDmButHasZdm.length) stats.addError('У следующих магазинов ОТСУТСТВУЕТ ДМ, НО ЕСТЬ ЗДМ', emptyDmButHasZdm, true);
	if (emptyDm.length) stats.addError('У следующих магазинов ОТСУТСТВУЕТ ДМ И ЗДМ', emptyDm, true);
	if (manyDf.length) stats.addError('У следующих магазинов НЕСКОЛЬКО ДФ', manyDf, true);
	if (manyTm.length) stats.addError('У следующих магазинов НЕСКОЛЬКО ТМ', manyTm, true);
	if (manyUpf.length) stats.addError('У следующих магазинов НЕСКОЛЬКО УПФ', manyUpf, true);
	if (manyDm.length) stats.addInfo('У следующих магазинов НЕСКОЛЬКО ДМ', manyDm, true);
	if (deadShopManagers.length) stats.addError('У следующих магазинов есть ссылки на УДАЛЕННЫХ СОТРУДНИКОВ', deadShopManagers);
	if (emptyPhone.length) stats.addWarn('У следующих магазинов не указан ТЕЛЕФОН', emptyPhone);
	if (emptyEmail.length) stats.addWarn('У следующих магазинов не указан EMAIL', emptyEmail);

	const tos = Object.keys(timeOffsets).map(key => `${key} мин = ${timeOffsets[key]} шт.`).join(', ');
	stats.addInfo(`Часовые пояса в магазинах: ${tos}`);
}

// проверка заполненных полей пользователей
function checkUsers(users: Users, stats: StructureAnalisysStats): void {
	const emptyExternalId: Users = [];
	const emptyLastName: Users = [];
	const emptyFirstName: Users = [];
	const emptyEmail: Users = [];
	const managerEmptyPhone: Users = [];
	const chiefEmptyPhone: Users = [];
	const hasNoRoles: Users = [];
	const hasManyRoles: Users = [];
	const rolesCount: any = {};
	users.forEach(user => {
		if (!user.externalId) emptyExternalId.push(user);
		if (!user.lastName) emptyLastName.push(user);
		if (!user.firstName) emptyFirstName.push(user);
		if (!user.email) emptyEmail.push(user);
		if (!user.phone && user.role === UserRoles.MANAGER) managerEmptyPhone.push(user);
		if (!user.phone && user.role === UserRoles.CHIEF) chiefEmptyPhone.push(user);
		if (!user._management) {
			if (![UserRoles.ADMIN, UserRoles.SUPPORT, UserRoles.ROBOT, UserRoles.CHIEF].includes(user.role)) {
				hasNoRoles.push(user);
			}
		} else {
			const roleIds = new Set(user._management.map(m => m.role.id)).size;
			if (!roleIds) hasNoRoles.push(user);
			if (roleIds > 1) hasManyRoles.push(user);
		}
		rolesCount[user.role] = (rolesCount[user.role] || 0) + 1;
	});
	if (emptyExternalId.length) stats.addWarn('У следующих пользователей не указан ВНЕШНИЙ ИДЕНТИФИКАТОР', emptyExternalId);
	if (emptyLastName.length) stats.addError('У следующих пользователей не указана ФАМИЛИЯ', emptyLastName);
	if (emptyFirstName.length) stats.addWarn('У следующих пользователей не указано ИМЯ', emptyFirstName);
	if (emptyEmail.length) stats.addWarn('У следующих пользователей не указан EMAIL', emptyEmail);
	if (managerEmptyPhone.length) stats.addWarn('У следующих сотрудников с ролью МНЕДЖЕР не указан ТЕЛЕФОН', managerEmptyPhone);
	if (chiefEmptyPhone.length) stats.addWarn('У следующих сотрудников с ролью ДТ не указан ТЕЛЕФОН', chiefEmptyPhone);
	if (hasNoRoles.length) stats.addError('У следующих сотрудников не назначено НИ ОДНОЙ РОЛИ', hasNoRoles);
	if (hasManyRoles.length) stats.addError('У следующих сотрудников назначено НЕСКОЛЬКО РОЛЕЙ', hasManyRoles);
	const tos = Object.keys(rolesCount).map(key => `${key} = ${rolesCount[key]} шт.`).join(', ');
	stats.addInfo(`Назначено ролей пользователям: ${tos}`);
}

// создаем информационное письмо с результатами анализа
async function createStatsMessage(stats: StructureAnalisysStats, accountId: number) {
	// файл и отчет создает один из админов аккаунта
	const currentUser = await userStore.findOne({ where: { accountId, role: UserRoles.ADMIN } });
	// кому будем отправлять очтет
	const addresseesSetting = await accountSettingStore.findOneStructureAnalysisReportAddresseesByAccountId(accountId);
	const toEmails = addresseesSetting?.value.split(',').map(s => s.trim()).filter(s => !!s);
	if (!toEmails?.length) {
		// не кому отправлять отчет, значит нет смысла его формировать
		return false;
	}
	const mailMessageData = MailService.buildStructureAnalysisStatsMessage(stats, null, toEmails, currentUser);
	return MailMessage.create(mailMessageData);
}
