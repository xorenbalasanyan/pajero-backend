import { isProdServer, isReleaseServer } from '~utils/env';
import { Shop, User } from '../../stores/db/entity';

type Item = typeof Shop | typeof User;

const serverUrl = isProdServer ? 'https://pajero.ru'
	: isReleaseServer ? 'https://pajero-release.d87.ru'
		: 'https://pajero-test.d87.ru';

export default class StructureAnalisysStats
{
	errors: string[] = [];
	errorIdCount = 0;
	warns: string[] = [];
	warnIdCount = 0;
	infos: string[] = [];
	infoIdCount = 0;

	addError(msg: string, items?: Item[], printManagers = false) {
		if (items?.length) {
			this.errorIdCount += items.length;
			this.errors.push(this.formatMessage(msg, items, printManagers, 10, 100));
		} else {
			this.errorIdCount++;
			this.errors.push(msg);
		}
	}

	addWarn(msg: string, items?: Item[], printManagers = false) {
		if (items?.length) {
			this.warnIdCount += items.length;
			this.warns.push(this.formatMessage(msg, items, printManagers, 10, 50));
		} else {
			this.warnIdCount++;
			this.warns.push(msg);
		}
	}

	addInfo(msg: string, items?: Item[], printManagers = false) {
		if (items?.length) {
			this.infoIdCount += items.length;
			this.infos.push(this.formatMessage(msg, items, printManagers, 10, 50));
		} else {
			this.infoIdCount++;
			this.infos.push(msg);
		}
	}

	private formatMessage(msg: string, items: Item[], printManagers: boolean, maxLinks: number, maxItems: number): string {
		const parts = [`<b>${msg}</b> (${items.length})`];
		const links = items.slice(0, maxLinks).map(item => {
			if (item.regionId || item.city || item.address || item.timeOffset) {
				let line = `<a href="${serverUrl}/admin/shops?search=${item.id}">#${item.id} ${item.name}</a>`;
				if (printManagers && item._managers.length) {
					line += `<ol>${item._managers.map(man => `<li>${man.role.shortTitle} ${printUser(man.user)}</li>`).join('')}</ol>`;
				}
				return line;
			} else {
				return printUser(item);
			}
		});
		if (links.length) {
			parts.push(':<ol>', ...links.map(s => `<li>${s}</li>`), '</ol>');
		}
		if (items.length > maxLinks) {
			const ids = items.slice(0, maxItems).map(i => i.id);
			parts.push(`<i>(выведено только первые ${maxItems} индексов)</i> ${ids.join(', ')}`);
		}
		return parts.join('');
	}
}

function printUser(user: typeof User): string {
	if (!user) return '(empty)';
	return `<a href="${serverUrl}/admin/users/${user.id}">#${user.id} ${[user.lastName, user.firstName, user.middleName]
		.filter(Boolean)
		.join(' ').trim()}</a>`;
}
