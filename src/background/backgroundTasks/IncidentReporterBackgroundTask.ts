import fs from 'fs';
import util from 'util';
import moment from 'moment';
import Exceljs, { Alignment } from 'exceljs';
import { arrayToMapById } from '~utils/list';
import { fromSqlDateToHumanDate } from '~utils/date';
import sequelize from '~lib/sequelize';
import { FileService, MailService } from '~services';
import { UserRoles } from '~enums/UserRoleEnum';
import {
	accountStore,
	fileStore,
	fileTypeStore,
	goodStore,
	incidentDaylyReportStore,
	incidentStore,
	incidentTypeStore,
	shopStore,
	userStore,
} from '~stores';
import BaseBackgroundTask, {
	BackgroundTaskExecuteOptions,
	BackgroundTaskReturnType,
} from './BaseBackgroundTask';
import { MailMessage } from '../../stores/db/entity';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const logger = new AppLogger('IncidentReporterBackgroundTask');

/**
 * Задача выполняет проверку инцидентов, по которым еще не был составлен суточный отчет.
 * Выполняется поиск инцидентов по всей сети.
 * Чтобы составить очтет, должен сначала закончится день выполнения инцидентов, с учетом часового
 *  пояса магазинов.
 */
export default class IncidentReporterBackgroundTask extends BaseBackgroundTask
{
	protected override async start(options?: BackgroundTaskExecuteOptions): Promise<BackgroundTaskReturnType> {
		const accounts: any[] = await accountStore
			.findAll({
				where: {},
				attributes: ['id'],
				raw: true,
			})
			.catch(error => {
				throw fillError(Error, '[Ошибка поиска аккаунтов]', error);
			});
		const accountIds: number[] = accounts.map(i => i.id);
		const serverDate = moment().utcOffset(0); // текущее время UTC
		for (const accountId of accountIds) {
			const shops: any[] = await shopStore
				.findAll({
					where: { accountId },
					attributes: ['id', 'timeOffset', 'externalId', 'name'],
					raw: true,
				})
				.catch(error => {
					throw fillError(Error, `[Ошибка поиска магазинов аккаунта]: ${JSON.stringify({ accountId })}`, error);
				});
			const shopMap = arrayToMapById(shops);
			// проверяем текущий и предыдущие несколько дней
			for (let daysDec = -5; daysDec <= 0; daysDec++) {
				let needReportShops = 0;
				let noNeedReportShops = 0;
				const checkDate = serverDate.clone().add(daysDec, 'day');
				const sqlDate = checkDate.format('YYYY-MM-DD');
				const sqlDateNum = sqlDate.replace(/-/g, '') as any * 1;
				const hasDailyReport: number = await incidentDaylyReportStore
					.count({
						where: {
							accountId,
							date: sqlDate,
						},
					})
					.catch(error => {
						throw fillError(Error, `[Ошибка поиска отчетов по инцидентам]: ${JSON.stringify({ accountId, sqlDate })}`, error);
					});
				if (!hasDailyReport) {
					logger.info(util.format('Для аккаунта #%d нет отчета на дату %s', accountId, sqlDate));
					for (const shop of shops) {
						// проверяем, наступили следующие сутки в магазине или нет
						const shopDate = moment(serverDate).utcOffset(shop.timeOffset);
						const checkIncidentForDay = moment(shopDate).format('YYYYMMDD') as any * 1;
						// сейчас проверим, сколько магазинов перешли в новый день
						if (checkIncidentForDay <= sqlDateNum) {
							noNeedReportShops++; // в магазине еще не наступил следующий день
						} else {
							needReportShops++; // в магазине уже наступил следующий день
						}
					}
					logger.info(util.format('Сутки в магазинах сменились: %d, не сменились: %d', needReportShops, noNeedReportShops));
					// когда все магазины аккаунта перешли в новые сутки, только тогда формируем отчет
					if (needReportShops > 0 && !noNeedReportShops) {
						await reportShopsForDate(accountId, shopMap, sqlDate)
							.catch(error => {
								throw fillError(Error, `[Ошибка формирования отчета]: ${JSON.stringify({ accountId, sqlDate })}`, error);
							});
					} else {
						logger.info('Ничего не делаем. Нужно дождаться пока во всей сети сменятся сутки');
					}
				}
			}
		}
	}
}

/**
 * Формируем отчет за конкретные сутки.
 * @param accountId
 * @param shopMap
 * @param sqlDate
 */
async function reportShopsForDate(accountId, shopMap, sqlDate) {
	logger.info(util.format(
		'Запущено формирование отчета по инцидентам для аккаунта #%d на дату %s, количество магазинов: %d',
		accountId,
		sqlDate,
		shopMap.size));
	// готовим все типы инцидентов
	const incidentTypes: any[] = await incidentTypeStore
		.findAll({
			where: { accountId },
			attributes: ['id', 'name', 'data'],
			raw: true,
		})
		.catch(error => {
			throw fillError(Error, `[Ошибка поиска типов инцидентов]: ${JSON.stringify({ accountId })}`, error);
		});
	const incidentTypesMap = incidentTypes.reduce((map, i) => map
		.set(i.id, { name: i.name, incidentTypeData: JSON.parse(i.data) }), new Map());
	// кто создатель файла
	const accountAdmin = await userStore
		.findOne({
			where: {
				accountId,
				role: UserRoles.ADMIN,
			},
		})
		.catch(error => {
			throw fillError(Error, `[Ошибка поиска пользователей аккаунта]: ${JSON.stringify({ accountId })}`, error);
		});
	// создаем стрим для записи XLSX
	const { pathName, fullPath } = FileService.generateNewFileName('', '.xlsx');
	const options = {
		filename: fullPath,
		useStyles: true,
		useSharedStrings: true,
	};
	const wb = new Exceljs.stream.xlsx.WorkbookWriter(options);
	// готовим XLSX для записи
	const humanDate = fromSqlDateToHumanDate(sqlDate);
	const ws = prepareWorkbook(wb, humanDate) as any;
	let wsRow = 2;
	// инциденты будем читать по странично
	let offset = 0;
	const LIMIT = 20e3; // размер страницы
	// номер инцидентов будем запоминать чтобы потом обновить статус
	const incidentIdSet = new Set();
	while (offset < 4_000_000) { // больше 4M строк в XLSX не влезет
		// получаем инциденты по всем магазинам
		const incidents = await incidentStore
			.findAll({
				where: {
					shopId: Array.from(Array.from(shopMap.keys())),
					date: sqlDate,
				},
				attributes: ['id', 'date', 'shopId', 'goodId', 'incidentTypeId', 'state', 'inputData', 'report'],
				order: [['id', 'DESC']],
				raw: true,
				offset,
				limit: LIMIT,
			})
			.catch(error => {
				throw fillError(Error, `[Ошибка поиска инцидентов]: ${JSON.stringify({ accountId, sqlDate })}`, error);
			});
		logger.info(util.format('Для аккаунта #%d на дату %s найдено инцидентов: %d', accountId, sqlDate, incidents.length));
		if (!incidents?.length) {
			break;
		}
		// узнаем связанные товары
		const goodIds = new Set(incidents.map(i => i.goodId));
		const goods = await goodStore
			.findAll({
				where: { id: Array.from(goodIds) },
				attributes: ['id', 'externalId', 'name'],
				raw: true,
			})
			.catch(error => {
				throw fillError(Error, `[Ошибка поиска товаров]: ${JSON.stringify({ accountId })}`, error);
			});
		logger.info(util.format('Для аккаунта #%d на дату %s найдено товаров: %d', accountId, sqlDate, goods.length));
		const goodMap = arrayToMapById(goods);
		incidents.forEach(incident => {
			const { id, state } = incident;
			incidentIdSet.add(id);
			const { externalId: shopExId, name: shopName } = shopMap.get(incident.shopId);
			const { externalId: goodExId, name: goodName } = goodMap.get(incident.goodId) as any;
			const {
				ostatok: ost,
				upush_prib: upprib,
			} = JSON.parse(incident.inputData || '{}');
			let { radio_options: value } = JSON.parse(incident.report || '{}');
			const incidentType = incidentTypesMap.get(incident.incidentTypeId);
			const { name: incidentTypeName } = incidentType;
			// маппим результат
			if (value) {
				// FIXME грязный хак
				const { solutionData } = incidentType.incidentTypeData;
				value = (solutionData.find(sd => sd.type === 'RADIO_OPTIONS')?.options?.find(o => o.value === value) || {}).reportValue || value;
			}
			const row = ws.getRow(wsRow++);
			row.values = [
				id,
				humanDate,
				shopName,
				shopExId,
				goodExId,
				goodName,
				incidentTypeName,
				ost,
				upprib,
				state === 'DONE' ? 1 : 0,
				value,
			];
			row.commit();
		});
		//
		offset += incidents.length;
	}
	// добавляем автофильтр
	ws.autoFilter = { from: 'A1', to: `K${wsRow - 1}` };
	// сохраняем файл
	await wb.commit();
	const size = fs.statSync(fullPath).size;
	const xlsxFileType = await fileTypeStore
		.findIdByExt('xlsx')
		.catch(error => {
			throw fillError(Error, `[Ошибка поиска типа файла]: ${JSON.stringify({ accountId })}`, error);
		});
	return sequelize.transaction(async transaction => {
		const file = await fileStore
			.createOne({
				accountId,
				name: `Отчет - ${humanDate}.xlsx`,
				size,
				fileTypeId: xlsxFileType.id,
				pathName,
				ownerId: accountAdmin.id,
			}, transaction)
			.catch(error => {
				throw fillError(Error, `[Ошибка создания файла отчета]: ${JSON.stringify({ accountId, pathName })}`, error);
			});
		logger.info(util.format('Для аккаунта #%d на дату %s файл отчета сохранен: %s', accountId, sqlDate, pathName));
		await incidentDaylyReportStore
			.createOne({
				accountId,
				fileId: file.id,
				date: sqlDate,
				ownerId: accountAdmin.id,
			}, transaction)
			.catch(error => {
				throw fillError(Error, `[Ошибка создания отчета]: ${JSON.stringify({ accountId, fileId: file.id, sqlDate })}`, error);
			});
		// обновляем отчет для инцидентов
		const incidentIds = Array.from(incidentIdSet);
		await incidentStore.updateWhere(
			{ isReported: true, moderatorId: accountAdmin.id },
			{ where: { id: incidentIds } },
			transaction,
		)
			.catch(error => {
				throw fillError(Error, `[Ошибка обновления инцидентов]: ${JSON.stringify({ accountId, incidentIds })}`, error);
			});
		// формируем e-mail сообщение
		const to = `${accountAdmin.fullName} <${accountAdmin.email}>`;
		const mailMessageData = MailService.buildIncidentDaylyReportMessage(
			null,
			to,
			humanDate,
			[file],
			accountAdmin,
		);
		await MailMessage.create(mailMessageData, { transaction })
			.catch(error => {
				throw fillError(Error, `[Ошибка создания письма с уведомлением об отчете]: ${JSON.stringify({ accountId, mailMessageData })}`, error);
			});
		logger.info(util.format('Для аккаунта #%d на дату %s e-mail письмо готово к отправке', accountId, sqlDate));
	})
		.catch(error => {
			fs.unlinkSync(fullPath); // remove report file
			throw fillError(Error, '[Ошибка при сохранении отчета]', error);
		});
}

function prepareWorkbook(wb, humanDate) {
	// формируем XLSX отчет для каждого аккаунта
	wb.creator = 'Pajero';
	wb.created = new Date();
	const ws = wb.addWorksheet(`Отчет - ${humanDate}`, {
		views: [{
			state: 'frozen',
			ySplit: 1,
		}],
	});
	const alignment: Partial<Alignment> = {
		wrapText: true,
		vertical: 'middle',
		horizontal: 'center',
	};
	ws.columns = [
		{ header: 'ИД инцидента', width: 12 },
		{ header: 'Дата', width: 12 },
		{ header: 'Магазин', width: 32 },
		{ header: 'ИД магазина', width: 12 },
		{ header: 'Артикул', width: 12 },
		{ header: 'Наименование артикула', width: 42 },
		{ header: 'Тип задачи', width: 12 },
		{ header: 'Доступный остаток, шт', width: 12 },
		{ header: 'Упущенная выручка, руб.', width: 22 },
		{ header: 'Исполнена (0/1)', width: 12 },
		{ header: 'Отметка магазина', width: 22, style: { alignment } },
	];
	ws.getRow(1).height = 24;
	ws.getRow(1).alignment = alignment;
	return ws;
}
