import moment from 'moment';
import util from 'util';
import { Transaction } from 'sequelize';
import { IncidentStateEnum } from '~enums/IncidentStateEnum';
import { arrayToMapById, distinctListById } from '~utils/list';
import { validateInteger } from '~utils/validate';
import sequelize, { deleteQuery, insert, select, update } from '~lib/sequelize';
import BaseBackgroundTask, { BackgroundTaskReturnType } from './BaseBackgroundTask';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import { shopStore } from '~stores';
import { sortDescNumeric } from '~utils/sort';

const logger = new AppLogger('IncidentStatsCuberBackgroundTask');

const CUBE_NAME1 = 'cube_incident_user_stats';
const CUBE_NAME2 = 'cube_incident_shop_stats';

/**
 * Задача чистит и заполняет куб статистики по инцидентам для менеджеров.
 */
export default class IncidentStatsCuberBackgroundTask extends BaseBackgroundTask
{
	private started = Date.now();

	protected override start(accountId: number): Promise<BackgroundTaskReturnType> {
		const date = moment().format('YYYY-MM-DD'); // TODO опредлелять сутки сети по аккаунту
		// обновляем куб для одного аккаунта
		const check = validateInteger(1, Number.MAX_SAFE_INTEGER)(accountId);
		if (check) {
			throw new Error(util.format(
				'При запуске задачи "IncidentStatsCuberBackgroundTask" передано значение accountId=%o, ошибка валидации: %s',
				accountId,
				check));
		}
		return sequelize.transaction(async transaction => {
			// определяем, нужно ли чистить кубы, для этого делаем выборку по дате
			const counts1 = await select(`SELECT date, count(*)
                                          FROM cube_incident_user_stats
                                          GROUP BY date`, undefined, transaction);
			const hasToday = counts1.find(i => i.date === date);
			// если статов за сегодня нет, но есть за другие дни, чистим кубы для аккаунта
			if (!hasToday && counts1.length) {
				await clearCubeForAccount(accountId, transaction);
			}
			const queries: Queries = await generateCubesForAccountId(accountId, date, transaction);
			const { inserts, updates, deletes } = queries;
			for (const query of deletes) {
				await deleteQuery(query, undefined, transaction);
			}
			for (const query of updates) {
				await update(query, undefined, transaction);
			}
			for (const query of inserts) {
				await insert(query, undefined, transaction);
			}
			const length = (Date.now() - this.started) / 1000;
			logger.info(`Задача работала ${length.toFixed(2)} секунд`);
		});
	}

	// перепишу метод чтобы ограничить входные данные
	override execute(accountId: number): Promise<BackgroundTaskReturnType> {
		return super.execute(accountId);
	}
}

/**
 * Чистка кубов для одного аккаунта
 */
async function clearCubeForAccount(accountId: number, transaction: Transaction): Promise<void> {
	logger.debug(util.format('Выполняется чистка кубов "%s" и "%s" для аккаунта #%d', CUBE_NAME1, CUBE_NAME2, accountId));
	await sequelize.query(`DELETE
                           FROM ${CUBE_NAME1}
                           WHERE accountId = ${accountId}`, { transaction });
	await sequelize.query(`DELETE
                           FROM ${CUBE_NAME2}
                           WHERE accountId = ${accountId}`, { transaction });
}

type Stat = {
	shopId: number,
	state: string,
	incidentTypeId: number,
	kount: number,
};

type LevelStat = {
	ALL: number,
	[IncidentStateEnum.TODO]?: number,
	[IncidentStateEnum.DONE]?: number,
};

type ManagerStat = {
	managerId?: number,
	managerMaxRoleSubLevel: number,
	incidentTypeStats: Map<number, LevelStat>,
	subUsers: ManagerStat[],
	shopIds: number[],
};

type Queries = {
	inserts: string[],
	updates: string[],
	deletes: string[],
};

type ShopStats = Map<number, Map<number, LevelStat>>;

type Context = {
	shopManagers: any[],
	shopStats: ShopStats,
	roles: any[],
	roleLevels: number[],
};

// заполняет куб для одного аккаунта
async function generateCubesForAccountId(accountId: number, date: string, transaction: Transaction): Promise<Queries> {
	logger.debug(util.format('Начато заполнение кубов "%s" и "%s" для аккаунта #%d', CUBE_NAME1, CUBE_NAME2, accountId));
	// получаем список ролей для аккаунта
	const shopManagerRoles: any[] = await select(`SELECT *
                                                  FROM shop_manager_role
                                                  WHERE accountId=:accountId`, { accountId }, transaction)
		.catch(error => {
			throw fillError(Error, '[Ошибка при получении списка ролей]', error);
		});
	const roleMap = arrayToMapById(shopManagerRoles);
	// загружаем связки менеджеров и магазинов
	const shopManagers: any[] = await select(`SELECT shopId, roleId, userId
                                              FROM shop_manager
                                              WHERE roleId IN (${Array.from(roleMap.keys()).join(',')})`, undefined, transaction)
		.catch(error => {
			throw fillError(Error, '[Ошибка при получении списка менеджеров]', error);
		});
	shopManagers.forEach(i => i.role = roleMap.get(i.roleId));
	let shopIds = Array.from(new Set(shopManagers.map(i => i.shopId)));
	// вытаскиваем магазины с фильтром по удаленным
	{
		const shops = await shopStore.findAllByIds(shopIds, transaction);
		shopIds = distinctListById(shops);
	}
	// загружаем статы по инцидентам для магазинов аккаунта
	const incidentStats: Stat[] = await select(`SELECT shopId,
                                                       incidentTypeId,
                                                       state,
                                                       COUNT(*) AS kount
                                                FROM incident
                                                WHERE date = '${date}'
                                                  AND shopId IN (${shopIds.join(',')})
                                                GROUP BY shopId, incidentTypeId, state`, undefined, transaction)
		.catch(error => {
			throw fillError(Error, '[Ошибка при получении списка инцидентов]', error);
		});
	// также загружаем текущие кубы
	const existUserStats = await select(`SELECT id,
                                                managerId,
                                                userId,
                                                incidentTypeId,
                                                state,
                                                completed,
                                                allCount
                                         FROM ${CUBE_NAME1}
                                         WHERE date = '${date}'
                                           AND accountId = ${accountId}`, undefined, transaction)
		.catch(error => {
			throw fillError(Error, '[Ошибка при получении куба пользователей]', error);
		});
	const existShopStats = await select(`SELECT id,
                                                managerId,
                                                shopId,
                                                incidentTypeId,
                                                state,
                                                completed,
                                                allCount
                                         FROM ${CUBE_NAME2}
                                         WHERE date = '${date}'
                                           AND accountId = ${accountId}`, undefined, transaction)
		.catch(error => {
			throw fillError(Error, '[Ошибка при получении куба магазинов]', error);
		});
	// формируем карту статистики по статусам для каждого магазина
	const shopStats: ShopStats = new Map<number, Map<number, LevelStat>>();
	for (const stat of incidentStats) {
		if (!shopStats.has(stat.shopId)) {
			const map = new Map<number, LevelStat>();
			map.set(0, { ALL: 0 }); // статы по всем типам инцидентов
			shopStats.set(stat.shopId, map);
		}
		const shopStat = shopStats.get(stat.shopId)!;
		// счетчик инцидентов по всем типам в магазине
		const allIncidentTypeStat = shopStat.get(0)!;
		allIncidentTypeStat[stat.state] = (allIncidentTypeStat[stat.state] || 0) + stat.kount;
		allIncidentTypeStat.ALL += stat.kount;
		// счетчик по конкретному типу инцидента
		if (!shopStat.has(stat.incidentTypeId)) {
			shopStat.set(stat.incidentTypeId, { ALL: 0 });
		}
		const shopIncidentTypeStat = shopStat.get(stat.incidentTypeId)!;
		shopIncidentTypeStat[stat.state] = stat.kount;
		shopIncidentTypeStat.ALL += stat.kount;
	}
	// формируем дерево статистики по менедежерам, начиная с уровня DF
	const context: Context = {
		shopManagers,
		shopStats,
		roles: shopManagerRoles,
		roleLevels: Array.from(new Set(shopManagerRoles.map(i => i.subLevel))),
	};
	const statsTree = fillManagerLevel(context);
	// для каждого уровня и пользователя записываем статы в куб
	const cubeStats: CubeQueries = {
		shopInserts: [],
		shopUpdates: [],
		shopDeletes: [],
		userInserts: [],
		userUpdates: [],
		userDeletes: [],
	};
	fillCubeData(cubeStats, existUserStats, existShopStats, accountId, date, statsTree, shopManagerRoles, shopStats);
	// формируем и выполняем запросы
	const inserts: string[] = [];
	const updates: string[] = [];
	const deletes: string[] = [];
	if (cubeStats.userInserts.length) {
		const chunkSize = 20_000;
		const chunks = Math.floor(cubeStats.userInserts.length / chunkSize) + 1;
		for (let i = 0; i < chunks; i++) {
			const chunk = cubeStats.userInserts.slice(i * chunkSize, (i + 1) * chunkSize);
			if (chunk.length) {
				inserts.push(`INSERT INTO ${CUBE_NAME1} (accountId, managerId, userId, date,
                                                         incidentTypeId, state, completed,
                                                         allCount)
                              VALUES (${chunk.join('),(')})`);
			}
		}
	}
	cubeStats.userUpdates.forEach(setQuery => {
		updates.push(`UPDATE ${CUBE_NAME1} SET ${setQuery}`);
	});
	if (cubeStats.userDeletes.length) {
		const chunkSize = 50_000;
		const chunks = Math.floor(cubeStats.userDeletes.length / chunkSize) + 1;
		for (let i = 0; i < chunks; i++) {
			const chunk = cubeStats.userDeletes.slice(i * chunkSize, (i + 1) * chunkSize);
			if (chunk.length) {
				deletes.push(`DELETE FROM ${CUBE_NAME1} WHERE id IN (${chunk.join(',')})`);
			}
		}
	}
	if (cubeStats.shopInserts.length) {
		const chunkSize = 20_000;
		const chunks = Math.floor(cubeStats.shopInserts.length / chunkSize) + 1;
		for (let i = 0; i < chunks; i++) {
			const chunk = cubeStats.shopInserts.slice(i * chunkSize, (i + 1) * chunkSize);
			if (chunk.length) {
				inserts.push(`INSERT INTO ${CUBE_NAME2} (accountId, managerId, shopId, date,
                                                         incidentTypeId, state, completed,
                                                         allCount)
                              VALUES (${chunk.join('),(')})`);
			}
		}
	}
	cubeStats.shopUpdates.forEach(setQuery => {
		updates.push(`UPDATE ${CUBE_NAME2}
                      SET ${setQuery}`);
	});
	if (cubeStats.shopDeletes.length) {
		const chunkSize = 50_000;
		const chunks = Math.floor(cubeStats.shopDeletes.length / chunkSize) + 1;
		for (let i = 0; i < chunks; i++) {
			const chunk = cubeStats.shopDeletes.slice(i * chunkSize, (i + 1) * chunkSize);
			if (chunk.length) {
				deletes.push(`DELETE FROM ${CUBE_NAME2} WHERE id IN (${chunk.join(',')})`);
			}
		}
	}
	return {
		inserts: inserts.filter(Boolean),
		updates: updates.filter(Boolean),
		deletes: deletes.filter(Boolean),
	};
}

// метод заполняет статистикой дерево менеджеров
function fillManagerLevel(context: Context): ManagerStat[] {
	const { shopManagers, shopStats, roles } = context;
	const managerIds = Array.from(new Set(shopManagers.map(i => i.userId)));
	const result: ManagerStat[] = [];
	// сначала по кааждому пользователю заполняем полностью счетчики по всем типам инцидентов
	for (const managerId of managerIds) {
		const shopIds = Array.from(new Set(shopManagers.filter(i => i.userId === managerId).map(i => i.shopId)));
		const shopStatItems = shopIds.map(shopId => shopStats.get(shopId));
		if (shopStatItems) {
			const incidentTypeStats = new Map<number, LevelStat>();
			for (const shopStatItem of shopStatItems) {
				if (shopStatItem) {
					const incidentTypeIds = Array.from(shopStatItem.keys());
					for (const incidentTypeId of incidentTypeIds) {
						if (!incidentTypeStats.has(incidentTypeId)) {
							incidentTypeStats.set(incidentTypeId, { ALL: 0 });
						}
						const incidentTypeStat = incidentTypeStats.get(incidentTypeId)!;
						const shopStatIncident = shopStatItem.get(incidentTypeId)!;
						for (const key in shopStatIncident) {
							incidentTypeStat[key] = (incidentTypeStat[key] || 0) + shopStatIncident[key];
						}
					}
				}
			}
			// рассчет верхней роли у пользователи
			const managerRoleIds = Array.from(new Set(shopManagers.filter(i => i.userId === managerId).map(i => i.roleId)));
			const managerMaxRoleSubLevel = managerRoleIds
				.map(id => roles.find(i => i.id === id)?.subLevel)
				.sort(sortDescNumeric)[0];
			result.push({
				managerId,
				incidentTypeStats,
				subUsers: [],
				managerMaxRoleSubLevel,
				shopIds,
			});
		}
	}
	// из этого списка заполняем подчиненных менеджеров, которые привязаны к тем же магазинам что и сам менеджер
	result.forEach(managerStat => {
		const mansWithShop = new Set<ManagerStat>();
		managerStat.shopIds.forEach(shopId => {
			const mansInShop = result.filter(manStat1 => manStat1.shopIds.includes(shopId));
			mansInShop
				.filter(m => m.managerMaxRoleSubLevel < managerStat.managerMaxRoleSubLevel)
				.forEach(i => mansWithShop.add(i));
		});
		managerStat.subUsers.push(...Array.from(mansWithShop));
	});
	return result;
}

type CubeQueries = {
	userInserts: string[],
	userUpdates: string[],
	userDeletes: string[],
	shopInserts: string[],
	shopUpdates: string[],
	shopDeletes: string[],
};

// заполняет данные для куба
function fillCubeData(
	cubeQueries: CubeQueries, cubeUserStats: any[], cubeShopStats: any[], accountId: number,
	date: string, managerStats: ManagerStat[], roles: any[], shopStats: ShopStats,
): void {
	// сначала все текущие записи в кубах маркируем под удаление
	const markDeleteUserStats = new Set(cubeUserStats);
	for (const managerStat of managerStats) {
		// заполняем статы по подчиненным пользователям
		const managerId = managerStat.managerId || null;
		const managedCubeUserStats1 = cubeUserStats.filter(i => i.managerId === managerId);
		for (const subUser of managerStat.subUsers) {
			const managedCubeUserStats2 = managedCubeUserStats1.filter(i => i.userId === subUser.managerId);
			const first = `${accountId},${managerId},${subUser.managerId},'${date}'`;
			for (const [incidentTypeIdV, incidentTypeStat] of subUser.incidentTypeStats) {
				const incidentTypeId = incidentTypeIdV || null;
				// для статуса "TODO"
				if (incidentTypeStat.TODO) {
					const exUserStat = managedCubeUserStats2.find(i => i.incidentTypeId === incidentTypeId && i.state === 'TODO');
					if (exUserStat) {
						if (exUserStat.completed !== 0 || exUserStat.allCount !== incidentTypeStat.TODO) {
							cubeQueries.userUpdates.push(`completed=0,allCount=${incidentTypeStat.TODO} WHERE id=${exUserStat.id}`);
						} // иначе: статы в кубе актуальны, обновлять не надо
						markDeleteUserStats.delete(exUserStat); // снимаем флаг об удалении
					} else {
						cubeQueries.userInserts.push(`${first},${incidentTypeId},'TODO',0,${incidentTypeStat.TODO}`);
					}
				}
				// для статуса "DONE"
				if (incidentTypeStat.DONE) {
					const exUserStat = managedCubeUserStats2.find(i => i.incidentTypeId === incidentTypeId && i.state === 'DONE');
					if (exUserStat) {
						if (exUserStat.completed !== incidentTypeStat.DONE || exUserStat.allCount !== incidentTypeStat.DONE) {
							cubeQueries.userUpdates.push(`completed=${incidentTypeStat.DONE},allCount=${incidentTypeStat.DONE} WHERE id=${exUserStat.id}`);
						} // иначе: статы в кубе актуальны, обновлять не надо
						markDeleteUserStats.delete(exUserStat); // снимаем флаг об удалении
					} else {
						cubeQueries.userInserts.push(`${first},${incidentTypeId},'DONE',${incidentTypeStat.DONE},${incidentTypeStat.DONE}`);
					}
				}
				// дял суммарного счетчика
				const completed = incidentTypeStat.DONE || 0;
				const exUserStat1 = managedCubeUserStats2.find(i => i.incidentTypeId === incidentTypeId && i.state === null);
				if (exUserStat1) {
					if (exUserStat1.completed !== completed || exUserStat1.allCount !== incidentTypeStat.ALL) {
						cubeQueries.userUpdates.push(`completed=${completed},allCount=${incidentTypeStat.ALL} WHERE id=${exUserStat1.id}`);
					} // иначе: статы в кубе актуальны, обновлять не надо
					markDeleteUserStats.delete(exUserStat1); // снимаем флаг об удалении
				} else {
					cubeQueries.userInserts.push(`${first},${incidentTypeId},NULL,${completed},${incidentTypeStat.ALL}`);
				}
			}
		}
	}
	// для неснятых отметок об удалении готовим запрос на зачистку
	if (markDeleteUserStats.size) {
		const statIds = Array.from(markDeleteUserStats.values()).map(i => i.id);
		cubeQueries.userDeletes.push(...statIds);
	}
	// кубы по магазинам заполняются только для директоров магазинов
	const dmRoleSubLevels = roles.filter(i => i.isShopUser).map(i => i.subLevel);
	// сначала все текущие записи в кубах маркируем под удаление
	const markDeleteShopStats = new Set(cubeShopStats);
	for (const managerStat of managerStats) {
		const managedCubeShopStats1 = cubeShopStats.filter(i => i.managerId === managerStat.managerId);
		if (dmRoleSubLevels.includes(managerStat.managerMaxRoleSubLevel)) {
			// заполняем статы по подчиненным магазинам
			for (const shopId of managerStat.shopIds) {
				const managedCubeShopStats2 = managedCubeShopStats1.filter(i => i.managerId === managerStat.managerId && i.shopId === shopId);
				const first = `${accountId},${managerStat.managerId},${shopId},'${date}'`;
				const shopStat = shopStats.get(shopId);
				if (shopStat) {
					for (const [incidentTypeIdV, incidentTypeStat] of shopStat) {
						const incidentTypeId = incidentTypeIdV || null;
						// для статуса "TODO"
						if (incidentTypeStat.TODO) {
							const exShopStat = managedCubeShopStats2.find(i => i.incidentTypeId === incidentTypeId && i.state === 'TODO');
							if (exShopStat) {
								if (exShopStat.completed !== 0 || exShopStat.allCount !== incidentTypeStat.TODO) {
									cubeQueries.shopUpdates.push(`completed=0,allCount=${incidentTypeStat.TODO} WHERE id=${exShopStat.id}`);
								} // иначе: статы в кубе актуальны, обновлять не надо
								markDeleteShopStats.delete(exShopStat); // снимаем флаг об удалении
							} else {
								cubeQueries.shopInserts.push(`${first},${incidentTypeId},'TODO',0,${incidentTypeStat.TODO}`);
							}
						}
						// для статуса "DONE"
						if (incidentTypeStat.DONE) {
							const exShopStat = managedCubeShopStats2.find(i => i.incidentTypeId === incidentTypeId && i.state === 'DONE');
							if (exShopStat) {
								if (exShopStat.completed !== incidentTypeStat.DONE || exShopStat.allCount !== incidentTypeStat.DONE) {
									cubeQueries.shopUpdates
										.push(`completed=${incidentTypeStat.DONE},allCount=${incidentTypeStat.DONE} WHERE id=${exShopStat.id}`);
								} // иначе: статы в кубе актуальны, обновлять не надо
								markDeleteShopStats.delete(exShopStat); // снимаем флаг об удалении
							} else {
								cubeQueries.shopInserts.push(`${first},${incidentTypeId},'DONE',${incidentTypeStat.DONE},${incidentTypeStat.DONE}`);
							}
						}
						// для суммарного счетчика
						const completed = incidentTypeStat.DONE || 0;
						const exShopStat1 = managedCubeShopStats2.find(i => i.incidentTypeId === incidentTypeId && i.state === null);
						if (exShopStat1) {
							if (exShopStat1.completed !== completed || exShopStat1.allCount !== incidentTypeStat.ALL) {
								cubeQueries.shopUpdates.push(`completed=${completed},allCount=${incidentTypeStat.ALL} WHERE id=${exShopStat1.id}`);
							} // иначе: статы в кубе актуальны, обновлять не надо
							markDeleteShopStats.delete(exShopStat1); // снимаем флаг об удалении
						} else {
							cubeQueries.shopInserts.push(`${first},${incidentTypeId},NULL,${incidentTypeStat.DONE || 0},${incidentTypeStat.ALL}`);
						}
					}
				}
			}
		}
	}
	// для неснятых отметок об удалении готовим запрос на зачистку
	if (markDeleteShopStats.size) {
		const statIds = Array.from(markDeleteShopStats.values()).map(i => i.id);
		cubeQueries.shopDeletes.push(...statIds);
	}
}
