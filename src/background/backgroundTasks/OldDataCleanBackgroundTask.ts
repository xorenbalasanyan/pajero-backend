import util from 'util';
import moment from 'moment';
import { Op } from 'sequelize';
import { validateInteger } from '~utils/validate';
import sequelize, { deleteQuery, insert, select, update } from '~lib/sequelize';
import BaseBackgroundTask, { BackgroundTaskExecuteOptions, BackgroundTaskReturnType } from './BaseBackgroundTask';
import { FileLocationEnum } from '~enums/FileLocationEnum';
import { FileService } from '~services';
import config from '~lib/config';
import SelectelRequester from '~lib/SelectelRequester';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import { fileStore, photoGalleryStore } from '~stores';

const logger = new AppLogger('OldDataCleanBackgroundTask');

const SELECTEL_CONFIG = config.get('selectelCloud');

/**
 * Задача выполняет чистку устаревших данных
 */
export default class OldDataCleanBackgroundTask extends BaseBackgroundTask
{
	protected override start(options?: BackgroundTaskExecuteOptions): Promise<BackgroundTaskReturnType> {
		// определяем лимит из настроек или берем дефолтный
		const accountId: number | undefined = (options instanceof Map && options.has('accountId'))
			? Number(options.get('accountId')) : undefined;
		let check = validateInteger(1, Number.MAX_SAFE_INTEGER)(accountId);
		if (!accountId || check) {
			throw new Error(util.format(
				'[Ошибка при запуске задачи "OldDataCleanBackgroundTask"] Передано значение accountId=%o, ошибка валидации: %s',
				accountId,
				check));
		}
		// сколько дней храним данные
		const safeDaysPeriod: number | undefined = (options instanceof Map && options.has('safeDaysPeriod'))
			? Number(options.get('safeDaysPeriod')) : undefined;
		check = validateInteger(1, 365)(safeDaysPeriod);
		if (!safeDaysPeriod || check) {
			throw new Error(util.format(
				'[Ошибка при запуске задачи "OldDataCleanBackgroundTask"] Передано значение safeDaysPeriod=%o, ошибка валидации: %s',
				accountId,
				check));
		}
		// сколько сообщений удаляем за один запуск джобы
		const cleanInfoMessagesLimit: number | undefined = (options instanceof Map && options.has('cleanInfoMessagesLimit'))
			? Number(options.get('cleanInfoMessagesLimit')) : undefined;
		if (!cleanInfoMessagesLimit && cleanInfoMessagesLimit !== 0) {
			logger.warn([
				'[Предупреждение при запуске задачи "OldDataCleanBackgroundTask"] Не передано значение cleanInfoMessagesLimit.',
				'Очистка информационных сообщений будет пропущена.',
			].join(' '));
		}
		// через сколько дней инциденты переезжают в таблицу истории
		const putIncidentsToHistoryDaysPeriod: number | undefined = (options instanceof Map && options.has('putIncidentsToHistoryDaysPeriod'))
			? Number(options.get('putIncidentsToHistoryDaysPeriod')) : undefined;
		if (!putIncidentsToHistoryDaysPeriod && putIncidentsToHistoryDaysPeriod !== 0) {
			logger.warn([
				'[Предупреждение при запуске задачи "OldDataCleanBackgroundTask"] Не передано значение putIncidentsToHistoryDaysPeriod.',
				'Перемещение инцидентов в историю будет пропущено.',
			].join(' '));
		}
		// сколько инцидентов перемещаем в историю за один запуск джобы
		const putIncidentsToHistoryLimit: number | undefined = (options instanceof Map && options.has('putIncidentsToHistoryLimit'))
			? Number(options.get('putIncidentsToHistoryLimit')) : undefined;
		if (!putIncidentsToHistoryLimit && putIncidentsToHistoryLimit !== 0) {
			logger.warn([
				'[Предупреждение при запуске задачи "OldDataCleanBackgroundTask"] Не передано значение putIncidentsToHistoryLimit.',
				'Перемещение инцидентов в историю будет пропущено.',
			].join(' '));
		}
		// сколько файлов удаляем за один запуск джобы
		// сам процесс выполняет удаление связанных с файлами объектов, а сами файлы помечаются как iDeleted
		const markDeletedFileLimit: number | undefined = (options instanceof Map && options.has('markDeletedFileLimit'))
			? Number(options.get('markDeletedFileLimit')) : undefined;
		if (!markDeletedFileLimit && markDeletedFileLimit !== 0) {
			logger.warn([
				'[Предупреждение при запуске задачи "OldDataCleanBackgroundTask"] Не передано значение markDeletedFileLimit.',
				'Пометка файлов на удаление будет пропущено.',
			].join(' '));
		}
		// сколько помеченных на удаление файлов удаляем за один запуск джобы
		// удаляются файлы с флагом isDeleted, сначала из CDN, потом из таблицы
		const dropFileLimit: number | undefined = (options instanceof Map && options.has('dropFileLimit'))
			? Number(options.get('dropFileLimit')) : undefined;
		if (!dropFileLimit && dropFileLimit !== 0) {
			logger.warn([
				'[Предупреждение при запуске задачи "OldDataCleanBackgroundTask"] Не передано значение dropFileLimit.',
				'Удаление отмеченных на удаление файлов будет пропущено.',
			].join(' '));
		}
		return clearOldInformationMessages(accountId, safeDaysPeriod, cleanInfoMessagesLimit)
			.then(() => moveIncidentsToHistory(accountId, putIncidentsToHistoryDaysPeriod, putIncidentsToHistoryLimit))
			.catch(error => {
				logger.error(fillError(Error, '[Ошибка чистки инцидентов]', error));
			})
			.then(() => markFileAsDeleted(accountId, safeDaysPeriod, markDeletedFileLimit))
			.catch(error => {
				logger.error(fillError(Error, '[Ошибка пометки старых файлов]', error));
			})
			.then(() => cleanGallery(accountId, dropFileLimit))
			.catch(error => {
				logger.error(fillError(Error, '[Ошибка чистки галереи]', error));
			})
			.then(() => dropFiles(accountId, dropFileLimit))
			.catch(error => {
				logger.error(fillError(Error, '[Ошибка удаления старых файлов]', error));
			})
			.then(() => undefined);
	}
}

/**
 * Удаляем информационные сообщения, которые были созданы более *safeDaysPeriod* дней назад.
 * При удалении сообщений также удаляются адресаты и файлы помечаются на удаление.
 * @param accountId
 * @param safeDaysPeriod
 * @param cleanInfoMessagesLimit
 */
async function clearOldInformationMessages(accountId: number, safeDaysPeriod: number, cleanInfoMessagesLimit: number | undefined): Promise<void> {
	// LIMIT строк на один SELECT
	const COUNT_PER_SELECT = 10;
	if (!cleanInfoMessagesLimit) {
		return;
	}
	let msgDeleteCount = 0;
	let fileDeleteCount = 0;
	const oldDate = moment().utcOffset(0).add(-safeDaysPeriod, 'days').format('YYYY-MM-DD');
	while (cleanInfoMessagesLimit > 0) {
		// вытаскиваем пачку старых сообщений
		const infoMessages = await select(`SELECT id, fileId
                                           FROM info_message
                                           WHERE createdAt < '${oldDate}'
                                             AND accountId = ${accountId}
                                           ORDER BY id
                                           LIMIT ${Math.min(cleanInfoMessagesLimit, COUNT_PER_SELECT)}`)
			.catch(error => {
				throw fillError(Error, '[Ошибка загрузки старых сообщений]', error);
			});
		cleanInfoMessagesLimit -= COUNT_PER_SELECT;
		if (!infoMessages.length) {
			break;
		}
		for (const infoMessage of infoMessages) {
			// сначала удаляем адресатов
			const delAdrMsgRepl = { infoMessageId: infoMessage.id };
			await deleteQuery(`DELETE
                               FROM info_message_to_addressee
                               WHERE infoMessageId = :infoMessageId`, delAdrMsgRepl)
				.catch(error => {
					throw fillError(Error, `[Ошибка удаления адресатов из старых сообщений]: ${JSON.stringify(delAdrMsgRepl)}`, error);
				});
			// удаляем сообщение
			const delInfMsgRepl = { id: infoMessage.id };
			await deleteQuery(`DELETE
                               FROM info_message
                               WHERE id = :id`, delInfMsgRepl)
				.catch(error => {
					throw fillError(Error, `[Ошибка удаления старых сообщений]: ${JSON.stringify(delInfMsgRepl)}`, error);
				});
			msgDeleteCount++;
			// ищем файл и удаляем
			const delFilRepl = { delFileId: infoMessage.fileId };
			const files = await select(`SELECT *
                                        FROM file
                                        WHERE id = :delFileId`, delFilRepl)
				.catch(error => {
					throw fillError(Error, `[Ошибка удаления файлов старых сообщений]: ${JSON.stringify(delFilRepl)}`, error);
				});
			const file = files[0];
			if (file) {
				// помечаем файл как удаленный
				const updFilRepl = { id: file.id };
				await update(`UPDATE file
                              SET isDeleted=1
                              WHERE id = :id`, updFilRepl)
					.catch(error => {
						throw fillError(Error, `[Ошибка пометки файла как удаленного]: ${JSON.stringify(updFilRepl)}`, error);
					});
				fileDeleteCount++;
			}
		}
	}
	logger.info(`Чистка старых сообщений завершена. Удалено сообщений: ${msgDeleteCount}, помечено на удаление файлов: ${fileDeleteCount}`);
}

/**
 * Инциденты страше *putIncidentsToHistoryDaysPeriod* дней перемещаем в таблицу истории
 * @param accountId
 * @param putIncidentsToHistoryDaysPeriod
 * @param putIncidentsToHistoryLimit
 */
async function moveIncidentsToHistory(
	accountId: number,
	putIncidentsToHistoryDaysPeriod: number | undefined,
	putIncidentsToHistoryLimit: number | undefined,
): Promise<void> {
	// LIMIT строк на один SELECT
	const COUNT_PER_SELECT = 1000;
	if (!putIncidentsToHistoryDaysPeriod || !putIncidentsToHistoryLimit) {
		return;
	}
	let incidentMovedCount = 0;
	const oldDate = moment()
		.utcOffset(0)
		.add(-putIncidentsToHistoryDaysPeriod, 'days')
		.format('YYYY-MM-DD');
	while (putIncidentsToHistoryLimit > 0) {
		// вытаскиваем пачку старых инцидентов
		const selDateRepl = { createdAt: oldDate };
		const incidents = await select(`SELECT *
                                        FROM incident
                                        WHERE createdAt < :createdAt
                                        ORDER BY id
                                        LIMIT ${Math.min(putIncidentsToHistoryLimit, COUNT_PER_SELECT)}`, selDateRepl)
			.catch(error => {
				throw fillError(Error, `[Ошибка получения старых инцидентов]: ${JSON.stringify(selDateRepl)}`, error);
			});
		putIncidentsToHistoryLimit -= COUNT_PER_SELECT;
		if (!incidents.length) {
			break;
		}
		// закидываем старые инциденты в таблицу истории
		for (const incident of incidents) {
			const replacements = {
				id: incident.id,
				externalId: incident.externalId,
				shopId: incident.shopId,
				shopDiscountId: incident.shopDiscountId,
				incidentTypeId: incident.incidentTypeId,
				incidentTypeVersion: incident.incidentTypeVersion,
				goodId: incident.goodId,
				date: incident.date,
				state: incident.state,
				inputData: incident.inputData,
				report: incident.report,
				isReported: incident.isReported,
				createdAt: incident.createdAt,
				ownerId: incident.ownerId,
				updatedAt: incident.updatedAt,
				moderatorId: incident.moderatorId,
			};
			await insert(`INSERT INTO incident_history VALUE (:id, :externalId,
                                                              :shopId, :shopDiscountId,
                                                              :incidentTypeId,
                                                              :incidentTypeVersion, :goodId,
                                                              :date, :state, :inputData, :report,
                                                              :isReported, :createdAt, :ownerId,
                                                              :updatedAt,
                                                              :moderatorId)`, replacements)
				// eslint-disable-next-line no-loop-func
				.catch(error => {
					if (error) {
						// проверка на уникальность
						// ошибку про уникальные записи пропускаем
						let isUniqueValidationError = false;
						if (error.name === 'SequelizeUniqueConstraintError') {
							if (error?.errors?.length) {
								if (error.errors[0].message === 'PRIMARY must be unique') {
									isUniqueValidationError = true;
								}
							}
						}
						if (!isUniqueValidationError) {
							throw fillError(Error, `[Ошибка перемещения старых  инцидентов в таблицу истории]: ${JSON.stringify(replacements)}`, error);
						}
					}
				});
			const delIncRepl = { id: incident.id };
			await deleteQuery(`DELETE
                               FROM incident
                               WHERE id = :id`, delIncRepl)
				.catch(error => {
					throw fillError(Error, `[Ошибка удаления инцидентов после перемещения]: ${JSON.stringify(delIncRepl)}`, error);
				});
			incidentMovedCount++;
		}
	}
	logger.info(`Перемещение старых инцидентов завершено. Перемещено инцидентов в историю: ${incidentMovedCount}`);
}

/**
 * Помечаем файлы на удаление, которые были созданы более *safeDaysPeriod* дней назад
 * @param accountId
 * @param safeDaysPeriod
 * @param markDeletedFileLimit
 */
async function markFileAsDeleted(accountId: number, safeDaysPeriod: number, markDeletedFileLimit: number | undefined): Promise<void> {
	// LIMIT строк на один SELECT
	const COUNT_PER_SELECT = 200;
	if (!markDeletedFileLimit) {
		return;
	}
	let fileMarkedCount = 0;
	const oldDate = moment().utcOffset(0).add(-safeDaysPeriod, 'days').format('YYYY-MM-DD');
	while (markDeletedFileLimit > 0) {
		// вытаскиваем пачку старых файлов
		const selDateRepl = { createdAt: oldDate };
		const files = await select(`SELECT id
                                    FROM file
                                    WHERE createdAt < :createdAt
                                      AND accountId = ${accountId}
                                      AND isDeleted = 0
                                    ORDER BY id
                                    LIMIT ${Math.min(markDeletedFileLimit, COUNT_PER_SELECT)}`, selDateRepl)
			.catch(error => {
				throw fillError(Error, `[Ошибка выборки старых файлов]: ${JSON.stringify(selDateRepl)}`, error);
			});
		markDeletedFileLimit -= COUNT_PER_SELECT;
		if (!files.length) {
			break;
		}
		const fileIds = files.map(f => f.id);
		// помечаем файлы на удаление
		const updFilRepl = { ids: fileIds };
		await update(`UPDATE file
                      SET isDeleted=1
                      WHERE id IN (:ids)`, updFilRepl)
			// eslint-disable-next-line no-loop-func
			.then(([_, affectedCount]) => {
				fileMarkedCount += affectedCount;
			})
			.catch(error => {
				throw fillError(Error, `[Ошибка пометки старых фаайлов как удаленных]: ${JSON.stringify(updFilRepl)}`, error);
			});
	}
	logger.info(`Маркировка старых файлов завершена. Помечено на удаление файлов: ${fileMarkedCount}`);
}

/**
 * Удалем файлы, помеченные на удаление.
 * Процесс удаления:
 * 1. Удаляем связь файла и записи в incident_import
 * 2. Удаляется файл из CDN, если файл был отправлен на CDN.
 * 3. Удаляется файл из FS, если файл не был отправлен на CDN.
 * @param accountId
 * @param dropFileLimit
 */
async function dropFiles(accountId: number, dropFileLimit: number | undefined): Promise<void> {
	// LIMIT строк на один SELECT
	const COUNT_PER_SELECT = 50;
	if (!dropFileLimit) {
		return;
	}
	let dropedFsFiles = 0;
	let dropedCdnFiles = 0;
	let dropedFilesTotal = 0;
	let dropedInfoMessages = 0;
	let dropedIncidentImports = 0;
	let dropedIncidentDaylyReports = 0;
	let dropedStructureImports = 0;
	let dropedFileMarkers = 0;

	// авторизуемся в Selectel
	const selectelRequester = new SelectelRequester(
		SELECTEL_CONFIG.username,
		SELECTEL_CONFIG.password,
		SELECTEL_CONFIG.accountPath,
		SELECTEL_CONFIG.containerName);
	await selectelRequester.auth()
		.catch(error => {
			throw fillError(Error, '[Ошибкаа подключения к СУБД с целью удалить помеченные файлы]', error);
		});
	const fileService = new FileService(null);
	while (dropFileLimit > 0) {
		// вытаскиваем пачку файлов, отмеченных на удаление
		const files = await select(`SELECT id, location, name, pathName
                                    FROM file
                                    WHERE accountId = :accountId
                                      AND isDeleted = 1
                                    ORDER BY id
                                    LIMIT ${Math.min(dropFileLimit, COUNT_PER_SELECT)}`, { accountId })
			.catch(error => {
				throw fillError(Error, `[Ошибка получения помеченных на удаление файлов]: ${JSON.stringify({ accountId })}`, error);
			});
		dropFileLimit -= COUNT_PER_SELECT;
		if (!files.length) {
			break;
		}
		for (const file of files) {
			// удаляем связи файлов из info_message
			await deleteItemByFileIdFromTable(accountId, 'info_message', file.id)
				.then(affectedRows => dropedInfoMessages += affectedRows);
			// удаляем связи файлов из incident_import
			await deleteItemByFileIdFromTable(accountId, 'incident_import', file.id)
				.then(affectedRows => dropedIncidentImports += affectedRows);
			// удаляем связи файлов из incident_dayly_report
			await deleteItemByFileIdFromTable(accountId, 'incident_dayly_report', file.id)
				.then(affectedRows => dropedIncidentDaylyReports += affectedRows);
			// удаляем связи файлов из structure_import
			await deleteItemByFileIdFromTable(accountId, 'structure_import', file.id)
				.then(affectedRows => dropedStructureImports += affectedRows);
			// удаляем связи файлов из file_marker
			await deleteItemByFileIdFromTable(null, 'file_marker', file.id)
				.then(affectedRows => dropedFileMarkers += affectedRows);
			// удаляем файл из хранилища FS или CDN
			if (file.location === FileLocationEnum.FS) {
				// удаляем файл из FS
				try {
					fileService.moveFileToDeleted(file.pathName, file.name);
				} catch (error) {
					// не удалось удалить файл, не критично, выведем ворнинг
					logger.warn(`[Ошибка при удалении файла из FS] Номер файла #${file.id}, путь до файла "${file.pathName}", ошибка:`, error);
				}
				dropedFsFiles++;
			} else if (file.location === FileLocationEnum.CDN) {
				// удаляем файл из CDN
				const p1 = file.name.substr(0, 2);
				const p2 = file.name.substr(2, 2);
				await selectelRequester.deleteFile(`${p1}/${p2}/${file.name}`)
					.catch(error => {
						// не удалось удалить файл, не критично, выведем ворнинг
						logger.warn(`[Ошибка при удалении файла из CDN] Номер файла #${file.id}, путь до файла "${file.pathName}", ошибка:`, error);
					});
				dropedCdnFiles++;
			}
			// удаляем запись о файле
			const delFileRepl = { id: file.id };
			await deleteQuery(`DELETE
                               FROM file
                               WHERE id = :id`, delFileRepl)
				.catch(error => {
					throw fillError(Error, `[Ошибка удаления старых файлов]: ${JSON.stringify(delFileRepl)}`, error);
				});
			dropedFilesTotal++;
		}
	}
	logger.info([
		`Удаление отмеченных на удаление файлов завершено.`,
		`Удалено файлов: ${dropedFilesTotal} (из FS: ${dropedFsFiles}, из CDN: ${dropedCdnFiles}),`,
		`удалено информационных сообщений: ${dropedInfoMessages},`,
		`удалено отчетов импорта инцидентов: ${dropedIncidentImports},`,
		`удалено суточных отчетов по инцидентам: ${dropedIncidentDaylyReports}`,
		`удалено отчетов импорта структуры: ${dropedStructureImports}`,
		`удалено файловых маркеров: ${dropedFileMarkers}`,
	].join(' '));
}

/**
 * Удалем файлы, помеченные на удаление.
 * Процесс удаления:
 * 1. Удаляем связь файла и записи в incident_import
 * 2. Удаляется файл из CDN, если файл был отправлен на CDN.
 * 3. Удаляется файл из FS, если файл не был отправлен на CDN.
 */
async function cleanGallery(accountId: number, dropFileLimit: number | undefined): Promise<void> {
	// LIMIT строк на один SELECT
	const PHOTO_DAYS_LIFE = 2;
	if (!dropFileLimit) return;

	const rottenDate = moment().add(-PHOTO_DAYS_LIFE, 'days').format('YYYY-MM-DD 00:00:00');

	const galleryPhotos = await photoGalleryStore.findAll({
		where: {
			accountId,
			createdAt: {
				[Op.lt]: rottenDate,
			},
		},
		raw: true,
		limit: dropFileLimit,
	});
	const fileIds = Array.from(new Set(galleryPhotos.map(i => i.fileId)));
	await sequelize.transaction(async transaction => {
		await fileStore.updateWhere({
			isDeleted: true,
		}, {
			where: {
				id: fileIds,
			},
		}, transaction);
		await photoGalleryStore.deleteWhere({
			id: Array.from(new Set(galleryPhotos.map(i => i.id))),
		}, transaction);
	});

	logger.info([
		`Удаление фотографий:`,
		`удалено записей из галереи: ${galleryPhotos.length},`,
	].join(' '));
}

/**
 * Ищет связи в таблице по fileId и выполняет удаление этих связей
 * @param accountId
 * @param tableName
 * @param fileId
 * @return Возвращает количество удаленных связей
 */
function deleteItemByFileIdFromTable(accountId: number | null, tableName: string, fileId: number) {
	return select(`SELECT id
                   FROM ${tableName}
                   WHERE fileId = :fileId`, { fileId })
		.then((items: any[]) => items.length
			? deleteQuery(`DELETE
                           FROM ${tableName}
                           WHERE id IN (:ids)
                               ${accountId ? `AND accountId = :accountId` : ''}`, {
				ids: items.map(i => i.id),
				accountId,
			})
				.then(() => items.length)
			: 0);
}
