import { QuestionnaireBackgroundService } from '~services';
import BaseBackgroundTask, { BackgroundTaskReturnType } from './BaseBackgroundTask';

/**
 * Задача управляет расписанием опросов
 */
export default class QuestionnaireBackgroundTask extends BaseBackgroundTask
{
	// перепишу метод чтобы ограничить входные данные
	override execute(accountId: number): Promise<BackgroundTaskReturnType> {
		return super.execute(accountId);
	}

	protected override start(accountId: number): Promise<BackgroundTaskReturnType> {
		const backService = new QuestionnaireBackgroundService(accountId);
		return backService.checkForSchedule()
			.then(backService.checkForRun)
			.then(backService.checkForStop)
			.then(backService.checkForReport)
			.then(backService.checkForNextStart);
	}
}
