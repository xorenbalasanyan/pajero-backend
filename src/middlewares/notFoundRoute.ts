import { ROUTE_NOT_FOUND } from '~errors/ResponseError';

export default function notFoundRoute(req, res) {
	res.sendHttpError(ROUTE_NOT_FOUND);
}
