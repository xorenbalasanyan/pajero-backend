import util from 'util';
import {
	AUTH_TOKEN_NOT_FOUND,
	AUTH_USER_NOT_FOUND,
	SERVER_ERROR,
	USER_HAS_NO_ACCESS,
} from '~errors/ResponseError';
import { UserService } from '~services';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import { ExRequest, ExResponse } from '~lib/WebServer';

const moduleLogger = new AppLogger('applyRoutes');

export default function (routes, router) {
	routes.forEach(({
		method,
		path,
		headPath,
		getPath,
		postPath,
		putPath,
		deletePath,
		accessRoles,
		publicAccess,
		anyAuthorizedRole,
		doNotLogNormalStatus,
		handler,
	}) => {
		const opt = {
			accessRoles,
			publicAccess,
			anyAuthorizedRole,
			path: path || headPath || getPath || postPath || putPath || deletePath,
			doNotLogNormalStatus,
		};
		if (doNotLogNormalStatus !== undefined && doNotLogNormalStatus !== true) {
			throw Error(`Конфигурация роута '${opt.path}' настроена с ошибкой. Значение для doNotLogNormalStatus может быть true или undefined.`);
		}
		if (method && path) router[method](path, checkAccess(opt, handler));
		else if (headPath) router.head(headPath, checkAccess(opt, handler));
		else if (getPath) router.get(getPath, checkAccess(opt, handler));
		else if (postPath) router.post(postPath, checkAccess(opt, handler));
		else if (putPath) router.put(putPath, checkAccess(opt, handler));
		else if (deletePath) router.delete(deletePath, checkAccess(opt, handler));
		else {
			throw Error(`Конфигурация роута '${opt.path}' настроена с ошибкой.`);
		}
	});
	return router;
}

/**
 * Проверка доступа по ролям
 */
function checkAccess(options, cb) {
	const { path, accessRoles, publicAccess, anyAuthorizedRole } = options;
	if (!accessRoles && !publicAccess && !anyAuthorizedRole) {
		throw new Error(`Инициализация роута '${path}' провалена, необходимо указать дополнительные опции проверки доступа.`);
	}
	// проверка смешения опций: нельзя чтобы роут был публичный и с проверкой ролей
	if (publicAccess && (accessRoles || anyAuthorizedRole)) {
		throw new Error(`Роут '${path}' не может быть сконфигурирован как приватный и публичный одновременно.`);
	}
	// tslint:disable-next-line:variable-name
	return async (req: ExRequest, res: ExResponse, ...any: any[]) => {
		const logger = req.getLogger(moduleLogger);
		if (accessRoles || anyAuthorizedRole) {
			const currentUserAuth: any = await UserService.getUserByAuthTokenFromRequest(req);
			if (!currentUserAuth) {
				return res.sendHttpError(AUTH_TOKEN_NOT_FOUND);
			}
			// пользователь мог авторизоваться под другим пользователем, поэтому будет заполнено поле secondaryUserId
			// авторизация под вторым пользователем более приоритетная
			const currentUser = await (currentUserAuth.secondaryUserId
				? currentUserAuth.getFk_secondaryUser()
				: currentUserAuth.getFk_user()
			)
				.catch(error => {
					res.sendHttpError(AUTH_USER_NOT_FOUND);
					throw fillError(Error, '[Ошибка определения текущего пользователя]', error);
				});
			if (!currentUser) {
				return res.sendHttpError(AUTH_USER_NOT_FOUND);
			}
			if (accessRoles && !accessRoles.includes(currentUser.role)) {
				return res.sendHttpError(USER_HAS_NO_ACCESS);
			}
			Object.assign(req, {
				getAuthorizedUser: () => currentUser,
				getAuthorizedUserAuth: () => currentUserAuth,
				__authorizedUserAccount: null,
				getAuthorizedUserAccount: async () => {
					if (!req.__authorizedUserAccount) {
						req.__authorizedUserAccount = await req.getAuthorizedUser().getFk_account();
					}
					return req.__authorizedUserAccount;
				},
			});
		} else if (!publicAccess) {
			return res.sendHttpError(USER_HAS_NO_ACCESS);
		}
		if (options.doNotLogNormalStatus === true) {
			req.doNotLogNormalStatus = true;
		}
		return cb(req, res, ...any)
			.catch(err => {
				logger.error(fillError(
					Error,
					util.format('Something went wrong with request: %o', {
						method: req.method,
						url: req.url,
						query: req.query,
						params: req.params,
						headers: req.headers,
						cookies: req.cookies,
						hostname: req.hostname,
						xhr: req.xhr,
						ip: req.ip,
					}),
					err,
				));
				res.sendHttpError(SERVER_ERROR);
			});
	};
}
