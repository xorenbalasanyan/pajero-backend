import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, {
	DATABASE_WRITE_ERROR,
	NOT_ALL_REQUIRED_FIELDS,
	REQUESTED_ITEM_NOT_FOUND,
	SERVER_ERROR,
} from '~errors/ResponseError';
import { AppLogger } from '~lib/AppLogger';
import { ExRequest, ExResponse } from '~lib/WebServer';
import { PhotoGalleryService } from '~services';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('PhotoGalleryController');

export default [
	{
		/**
		 * Получение списка фотографий для текущего пользователя
		 */
		getPath: '/api/photo_gallery/list',
		accessRoles: [UserRoles.MANAGER],
		doNotLogNormalStatus: true,
		handler: async (req: ExRequest, res: ExResponse) => {
			const svc = new PhotoGalleryService(req);
			return await svc.getPhotoListForCurrentUser()
				.then((list) => {
					res.sendOk(list);
				})
				.catch((error) => {
					req
						.getLogger(moduleLogger)
						.error(fillError(Error, '[Ошибка при получении списка фотографий]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Загрузка фотографии
		 */
		postPath: '/api/photo_gallery/upload',
		accessRoles: [UserRoles.MANAGER],
		handler: async (req: ExRequest, res: ExResponse) => {
			const photoItem = req.body;
			if (!photoItem?.data) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`Пользователь ${req.getAuthorizedUser().id} загружает фотографию в личную галерею`);
			const svc = new PhotoGalleryService(req);
			await svc.uploadOneForCurrentUser(photoItem.data)
				.then(() => {
					res.sendOk();
				})
				.catch(error => {
					logger.error(fillError(Error, '[Произошла ошибка при загрузке фотографии в личную галерею]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Удаление фотографии
		 */
		deletePath: '/api/photo_gallery/one/:fileId',
		accessRoles: [UserRoles.MANAGER],
		handler: async (req: ExRequest, res: ExResponse) => {
			// check input
			const { fileId } = req.params;
			if (!fileId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			// check exist item
			const svc = new PhotoGalleryService(req);
			const photoItem = await svc.findOne({
				where: { fileId },
			});
			if (!photoItem) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			// delete
			const logger = req.getLogger(moduleLogger);
			logger.info(`Удаление файла #${photoItem}`);
			await svc.deleteOneForCurrentUser(photoItem)
				.then(() => res.sendOk())
				.catch(error => {
					logger.error(fillError(Error, `Произошла ошибка при удалении photoItem #${photoItem}:`, error));
					res.sendHttpError(error instanceof ResponseError ? error : DATABASE_WRITE_ERROR);
				});
		},
	},
];
