import ResponseError, {
	FILE_NOT_FOUND,
	NOT_ALL_REQUIRED_FIELDS,
	SERVER_ERROR,
} from '~errors/ResponseError';
import { FileService } from '~services';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import { ExRequest, ExResponse } from '~lib/WebServer';

const moduleLogger = new AppLogger('fileDownloadController');

export default [
	/**
	 * Пользователь скачивает файл.
	 * При скачивании увеличивается счетчик скачиваний для файла.
	 */
	{
		getPath: '/api/file/download/:fileId',
		anyAuthorizedRole: true,
		doNotLogNormalStatus: true,
		handler: async (req: ExRequest, res: ExResponse) => {
			const fileId = +req.params.fileId;
			if (!fileId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const currentUser = req.getAuthorizedUser();
			const fileService = new FileService(req);
			const file = await fileService.findOneById(fileId);
			if (!file) {
				return res.sendHttpError(FILE_NOT_FOUND);
			}
			return fileService.downloadOne(file, res, currentUser.id)
				.catch(error => {
					req.getLogger(moduleLogger)
						.error(fillError(Error, '[Ошибка при чтении файла]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
];
