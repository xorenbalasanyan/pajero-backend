import ResponseError, { SERVER_ERROR } from '~errors/ResponseError';
import { UserRoles } from '~enums/UserRoleEnum';
import { IncidentDaylyReportService } from '~services';
import PageInfo from '~utils/PageInfo';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('incidentReportFileContainer');

export default [
	{
		/**
		 * Выгружает список суточных отчетов по инцидентам.
		 */
		getPath: '/api/incident/report/list',
		accessRoles: [UserRoles.ADMIN],
		handler: async (req: any, res: any) => {
			const options = { where: {} };
			const incidentDaylyReportService = new IncidentDaylyReportService(req);
			const pageInfo = PageInfo.parseFromObject(req.query);
			incidentDaylyReportService.findAllWithPageInfo(options, pageInfo)
				.then(([items, pageInfo1]) => res.sendOk(items, pageInfo1))
				.catch(error => {
					const logger = req.getLogger(moduleLogger);
					logger.error(fillError(Error, '[Ошибка при чтении списка IncidentDaylyReport]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
];
