export default [
	{
		/**
		 * Получение данных об аккаунте.
		 */
		getPath: '/api/account',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			await req.getAuthorizedUserAccount()
				.then(res.sendOk);
		},
	},
];
