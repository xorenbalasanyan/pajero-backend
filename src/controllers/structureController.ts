import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, {
	DATABASE_READ_ERROR,
	NOT_ALL_REQUIRED_FIELDS,
} from '~errors/ResponseError';
import { StructureService, UserService } from '~services';
import { GoodRepository } from '~repositories';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('structureController');

export default [
	{
		/**
		 * Получение списка товаров
		 */
		getPath: '/api/structure/goods/:ids',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const { ids } = req.params;
			if (!ids) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const goodRepository = new GoodRepository(req);
			return goodRepository.findAllByIds(req.getAuthorizedUser(), ids.split(','))
				.then(goods => res.json(goods.map(i => i.toDto())))
				.finally(() => res.end());
		},
	},
	{
		/**
		 * Загрузка новой структуры
		 */
		postPath: '/api/structure/import',
		accessRoles: [UserRoles.ROBOT],
		handler: async (req: any, res: any) => {
			const logger = req.getLogger(moduleLogger);
			try {
				await (new StructureService(req)).importUsersAndShops(req.body);
				res.sendOk();
			} catch (error: any) {
				logger.error(fillError(Error, '[Произошла ошибка при импорте структуры]', error));
				res.sendHttpError(error);
			}
		},
	},
	{
		/**
		 * Получение подчиненных магазинов списком
		 */
		getPath: '/api/structure/subordinate_shops/list',
		accessRoles: [UserRoles.CHIEF, UserRoles.MANAGER],
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const currentUser = req.getAuthorizedUser();
			return new UserService(req).getSubordinateShopList(currentUser)
				.then(subordinateShops => {
					res.sendOk(subordinateShops.map(i => i.toDto()));
				})
				.catch(error => {
					req.getLogger(moduleLogger).error('[Произошла ошибка при получении списка подчиненных магазинов]', error);
					res.sendHttpError(error instanceof ResponseError ? error : DATABASE_READ_ERROR);
				});
		},
	},
	{
		/**
		 * Получение подчиненных магазинов в виде дерева
		 */
		getPath: '/api/structure/subordinate_shops/tree',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT, UserRoles.CHIEF, UserRoles.MANAGER],
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const currentUser = req.getAuthorizedUser();
			return new UserService(req).getSubordinateShopTree(currentUser)
				.then((tree: any) => {
					const { subordinateUsers, subordinateShops } = tree;
					const data = {
						subordinateUsers: subordinateUsers?.length ? subordinateUsers.map(i => i.toDto()) : undefined,
						subordinateShops: subordinateShops?.length ? subordinateShops.map(i => i.toDto()) : undefined,
					};
					res.sendOk(data);
				})
				.catch(error => {
					req.getLogger(moduleLogger).error('[Произошла ошибка при получении дерева подчиненных магазинов]', error);
					res.sendHttpError(error instanceof ResponseError ? error : DATABASE_READ_ERROR);
				});
		},
	},
];
