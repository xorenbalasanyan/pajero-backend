import util from 'util';
import { UserRoles } from '~enums/UserRoleEnum';
import { QuestionnaireQuestionTypeEnum } from '~enums/QuestionnaireQuestionTypeEnum';
import {
	FileService,
	QuestionnaireHistoryService,
	QuestionnaireService,
	ShopService,
} from '~services';
import ResponseError, {
	FILE_NOT_FOUND,
	NOT_ALL_REQUIRED_FIELDS,
	REQUESTED_ITEM_NOT_FOUND,
	SERVER_ERROR,
} from '~errors/ResponseError';
import { AppLogger } from '~lib/AppLogger';
import { validateInteger, validateIsBoolean, validateString } from '~utils/validate';
import { EMAIL_REGEX } from '~enums/RegexEnum';
import { checkSqlDate, checkSqlTime } from '~utils/date';
import PageInfo from '~utils/PageInfo';
import { fillError } from '~utils/errors';
import { ExRequest, ExResponse } from '~lib/WebServer';

const moduleLogger = new AppLogger('questionnaireController');

export default [
	{
		/**
		 * Получение списка опросов в ЛК администратора
		 */
		getPath: '/api/questionnaires/list',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: ExRequest, res: ExResponse) => {
			const pageInfo = PageInfo.parseFromObject(req.query);
			const svc = new QuestionnaireService(req);
			return svc.getQuestList(pageInfo)
				.then(([list, pageInfo1]) => {
					res.sendOk(list, pageInfo1);
				})
				.catch(error => {
					req.getLogger(moduleLogger)
						.error(fillError(Error, '[Ошибка при чтении списка опросов]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение опроса
		 */
		getPath: '/api/questionnaires/one/:questId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT, UserRoles.MANAGER],
		handler: async (req: ExRequest, res: ExResponse) => {
			const questId = +req.params.questId;
			const svc = new QuestionnaireService(req);
			const quest = await svc.findOneById(questId);
			if (!quest) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			await svc.getOneQuest(quest)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error(error, '[Ошибка получения опроса]');
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
	 	 * Создание копии опроса
	 	 */
		postPath: '/api/questionnaires/duplicate/:questId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: ExRequest, res: ExResponse) => {
			// check
			const questId = +req.params.questId;
			const svc = new QuestionnaireService(req);
			const quest = await svc.findOneById(questId);
			if (!quest) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			const {
				title,
				questionnaireConfig,
				addresseesShopIds,
				reportConfig,
				executionConfig,
			} = quest
			if (!title || !questionnaireConfig || !addresseesShopIds?.length || !reportConfig
				|| !executionConfig) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			// создаём копию опроса
			await svc.createCopy({
				title,
				questionnaireConfig,
				addresseesShopIds,
				reportConfig,
				executionConfig,
			}).then(() => res.sendOk())
				.catch(error => {
					req.getLogger(moduleLogger).error(error, '[Ошибка при создании Questionnaire]');
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Создание опроса
		 */
		postPath: '/api/questionnaires/create',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: ExRequest, res: ExResponse) => {
			// check
			const data = req.body;
			if (!data || !Object.keys(data).length) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(util.format('Создание Questionnaire: %o', data));
			// check body
			const {
				title,
				questionnaireConfig,
				addresseesShopIds,
				reportConfig,
				executionConfig,
				...other
			} = data;
			if (!title || !questionnaireConfig || !addresseesShopIds?.length || !reportConfig
				|| !executionConfig) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			if (Object.keys(other).length) {
				return res.sendHttpError(ResponseError.validationInput(`Лишние поля: ${Object.keys(other)}`));
			}
			if (validateString(1, 200)(title)) {
				return res.sendHttpError(ResponseError.validationInput(`Поле title должно быть длиной от 1 до 200 символов`));
			}
			let check = validateQuestionnaireConfig(questionnaireConfig);
			if (check) {
				return res.sendHttpError(ResponseError.validationInput(`Ошибка заполнения questionnaireConfig: ${check}`));
			}
			check = validateReportConfig(reportConfig);
			if (check) {
				return res.sendHttpError(ResponseError.validationInput(`Ошибка заполнения reportConfig: ${check}`));
			}
			check = validateExecutionConfig(executionConfig);
			if (check) {
				return res.sendHttpError(ResponseError.validationInput(`Ошибка заполнения executionConfig: ${check}`));
			}
			// проверка магазинов-респондентов
			const shopIds = await new ShopService(req).findAll({ where: { id: addresseesShopIds } });
			const shopIdMap = shopIds.map(o => o.id).reduce((map, id) => {
				map[id] = true;
				return map;
			}, {});
			const notFoundShopIds = addresseesShopIds.filter(id => !shopIdMap[id]);
			if (notFoundShopIds.length) {
				return res.sendHttpError(ResponseError.validationInput(`Не найдены магазины: ${notFoundShopIds}`));
			}
			// данные в порядке, создаем новый опрос
			const svc = new QuestionnaireService(req);
			await svc.createOne({
				title,
				questionnaireConfig,
				addresseesShopIds: Array.from(new Set(addresseesShopIds)),
				reportConfig,
				executionConfig,
			})
				.then(() => res.sendOk())
				.catch(error => {
					req.getLogger(moduleLogger).error(error, '[Ошибка при создании Questionnaire]');
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Включение опроса
		 */
		postPath: '/api/questionnaires/enable/:questId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: ExRequest, res: ExResponse) => {
			const questId = +req.params.questId;
			const svc = new QuestionnaireService(req);
			const quest = await svc.findOneById(questId);
			if (!quest) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			if (quest.enabled) {
				return res.sendHttpError(ResponseError.validationInput(`Опрос уже запущен`));
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(util.format('Активация Questionnaire: %d', questId));
			await svc.enableOne(quest)
				.then(() => res.sendOk())
				.catch(error => {
					req.getLogger(moduleLogger).error(error, '[Ошибка активации Questionnaire]');
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Выключение опроса
		 */
		postPath: '/api/questionnaires/disable/:questId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: ExRequest, res: ExResponse) => {
			const questId = +req.params.questId;
			const svc = new QuestionnaireService(req);
			const quest = await svc.findOneById(questId);
			if (!quest) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			if (!quest.enabled) {
				return res.sendHttpError(ResponseError.validationInput(`Опрос уже остановлен`));
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(util.format('Деактивация Questionnaire: %d', questId));
			await svc.disableOne(quest)
				.then(() => res.sendOk())
				.catch(error => {
					req.getLogger(moduleLogger).error(error, '[Ошибка деактивации Questionnaire]');
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Удаление опроса
		 */
		deletePath: '/api/questionnaires/one/:questId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: ExRequest, res: ExResponse) => {
			const questId = +req.params.questId;
			const svc = new QuestionnaireService(req);
			const quest = await svc.findOneById(questId);
			if (!quest) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(util.format('Удаление Questionnaire: %d', questId));
			await svc.deleteOne(quest)
				.then(() => res.sendOk())
				.catch(error => {
					req.getLogger(moduleLogger).error(error, '[Ошибка удаления Questionnaire]');
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение спика отчетов
		 */
		getPath: '/api/questionnaires/reports/list',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: ExRequest, res: ExResponse) => {
			const pageInfo = PageInfo.parseFromObject(req.query);
			const svc = new QuestionnaireService(req);
			return svc.getQuestReportList(pageInfo)
				.then(([list, pageInfo1]) => {
					res.sendOk(list, pageInfo1);
				})
				.catch(error => {
					req.getLogger(moduleLogger)
						.error(fillError(Error, '[Ошибка при чтении списка отчетов по опросам]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Скачивание отчета
		 */
		getPath: '/api/questionnaires/reports/download/:questHistoryId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: ExRequest, res: ExResponse) => {
			const questHistoryId = +req.params.questHistoryId;
			const svc = new QuestionnaireHistoryService(req);
			const questHistory = await svc.findOneByIdWithoutAccountId(questHistoryId);
			if (!questHistory) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			const fileSvc = new FileService(req);
			const file = await fileSvc.findOneById(questHistory.reportFileId);
			if (!file) {
				return res.sendHttpError(FILE_NOT_FOUND);
			}
			return fileSvc.downloadOne(file, res, req.getAuthorizedUser().id)
				.catch(error => {
					req.getLogger(moduleLogger)
						.error(fillError(Error, '[Ошибка при скачивании файла]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Список доступных опросов для ЛК сотрудника магазина
		 */
		getPath: '/api/questionnaires/running',
		accessRoles: [UserRoles.MANAGER, UserRoles.CHIEF],
		handler: async (req: ExRequest, res: ExResponse) => {
			const svc = new QuestionnaireService(req);
			return svc.getQuestListForManager()
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger)
						.error(fillError(Error, '[Ошибка при чтении списка опросов для менеджера]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Отправка ответа на опрос
		 */
		postPath: '/api/questionnaires/answer/:questId',
		accessRoles: [UserRoles.MANAGER],
		handler: async (req: ExRequest, res: ExResponse) => {
			const questId = +req.params.questId;
			// check
			const data = req.body;
			if (!data || !Object.keys(data).length) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const svc = new QuestionnaireService(req);
			const quest = await svc.findOneById(questId);
			if (!quest) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			if (quest.disabled) {
				return res.sendHttpError(ResponseError.validationInput(`Опрос не запущен`));
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(util.format('Отправка ответа на опрос #%d: %o', questId, data));
			await svc.createQuestResponse(quest, data)
				.then(() => res.sendOk())
				.catch(error => {
					req.getLogger(moduleLogger).error(error, '[Ошибка отправки ответа на опрос]');
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
];

/**
 * Проверка конфигурации опросника
 * @param config
 */
function validateQuestionnaireConfig(config: any): string | false {
	if (!config?.questions?.length) return `Список questions должен быть заполнен`;
	for (const questionConfig of config.questions) {
		const { text, type, isRequired, visibleOptions } = questionConfig;
		if (validateString(1, 200)(text)) return `Длина text должна быть от 1 до 200`;
		if (validateIsBoolean()(isRequired)) return `isRequired должно быть булевым значением`;
		if (type === QuestionnaireQuestionTypeEnum.RADIO || type === QuestionnaireQuestionTypeEnum.CHECKERS) {
			// проверяем варианты ответов
			if (!questionConfig.options?.length) return `Список options не может быть пустой`;
			questionConfig.options = questionConfig.options.map(s => s.trim());
			if (questionConfig.options.some(s => !s)) return `Значения в поле options не должны быть пустыми`;
		} else if (type !== QuestionnaireQuestionTypeEnum.FLOAT && type !== QuestionnaireQuestionTypeEnum.STRING) {
			return `Значение type "${type}" указано с ошибкой`;
		}
		// настройки скрытого вопроса
		if (visibleOptions) {
			const { questionIndex, answerIndex } = visibleOptions;
			const q = config.questions[questionIndex];
			if (!q) {
				return `Скрытый вопрос ссылается на несуществующий вопрос`;
			} else if (q.type === QuestionnaireQuestionTypeEnum.RADIO || q.type === QuestionnaireQuestionTypeEnum.CHECKERS) {
				const a = q.options?.[answerIndex];
				if (!a) return `Скрытый вопрос ссылается на несуществующий вариант ответа`;
			} else {
				return `Скрытый вопрос может ссылаться только на вопросы типа список`;
			}
		}
	}
	return false;
}

/**
 * Проверка конфигурации отчета
 * @param config
 */
function validateReportConfig(config: any): string | false {
	const { messageTitle } = config;
	if (validateString(1, 200)(messageTitle)) return `Длина messageTitle должна быть от 1 до 200`;
	config.addressees = config.addressees.map(s => s.trim()).filter(Boolean);
	for (const email of config.addressees) {
		// проверяем адреса почты по шаблону
		if (!EMAIL_REGEX.test(email)) return `Email "${email}" не является валидным`;
	}
	return false;
}

/**
 * Проверка конфигурации запуска
 * @param config
 */
function validateExecutionConfig(config: any): string | false {
	const { startAtDate, startAtTime, finishAtDate, finishAtTime, repeatEveryDays } = config;
	if (!checkSqlDate(startAtDate)) return `Поле startAtDate указано с ошибкой`;
	if (!checkSqlTime(startAtTime)) return `Поле startAtTime указано с ошибкой`;
	if (!checkSqlDate(finishAtDate)) return `Поле finishAtDate указано с ошибкой`;
	if (!checkSqlTime(finishAtTime)) return `Поле finishAtTime указано с ошибкой`;
	if (repeatEveryDays) {
		if (validateInteger(1, 100)(repeatEveryDays)) return `Значение repeatEveryDays должно быть от 1 до 100`;
	} else {
		// вдруг там null или 0
		delete config.repeatEveryDays;
	}
	// проверка даты
	const len = new Date(startAtDate).getTime()
		- new Date(new Date().toISOString().substring(0, 10)).getTime();
	if (len < 0) return `Время для запуска уже вышло`;
	return false;
}
