import { ShopService, UserService } from '~services';
import { ExRequest, ExResponse } from '~lib/WebServer';

export default [
	{
		/**
		 * Получение конфигурации для текущего пользователя
		 */
		getPath: '/api/me/config',
		anyAuthorizedRole: true,
		doNotLogNormalStatus: true,
		handler: async (req: ExRequest, res: ExResponse) => {
			const currentUser = req.getAuthorizedUser();
			const userService = new UserService(req);
			const dmRoles = await userService.findAllShopManagerRolesByUserId(currentUser.id);
			const shopService = new ShopService(req);
			const shops = await shopService.findAllWhereUserIsShopUser(currentUser.id);
			res.sendOk({
				currentUser: {
					...currentUser.toDto(),
					...(dmRoles.length && {
						shopManagerRoles: dmRoles.map(i => i.toDto()),
					}),
					...(shops.length && {
						shops: shops.map(shop => ({
							id: shop.id,
							name: shop.name,
							city: shop.city,
							address: shop.address,
						})),
					}),
				},
			});
		},
	},
];
