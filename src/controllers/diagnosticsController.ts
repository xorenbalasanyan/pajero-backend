import ResponseError, { NOT_ALL_REQUIRED_FIELDS, SERVER_ERROR } from '~errors/ResponseError';
import PageInfo from '~utils/PageInfo';
import { FileMarkerService, FileService, IncidentService } from '~services';
import { AppLogger } from '~lib/AppLogger';

const moduleLogger = new AppLogger('diagnosticsController');

export default [
	{
		/**
		 * Получение списка файлов
		 */
		getPath: '/api/diagnostics/file-gallery/list/:fileIds?',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const pageInfo = PageInfo.parseFromObject(req.query);
			const { fileIds } = req.params;
			const options: any = { where: {} };
			if (fileIds) {
				options.where.id = fileIds.split(',');
			}
			return new FileService(req).findAllWithPageInfo(options, pageInfo)
				.then(([items, pageInfo1]) => res.sendOk(items, pageInfo1))
				.catch(error => {
					req.getLogger(moduleLogger).error('[Ошибка при чтении списка File]', error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение списка картинок и привязанных к ним инцидентов
		 */
		getPath: '/api/diagnostics/incident-marking/list',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const pageInfo = PageInfo.parseFromObject(req.query);
			return new IncidentService(req).findAllForMarking(pageInfo)
				.then(([items, pageInfo1]) => res.sendOk(items, pageInfo1))
				.catch(error => {
					req.getLogger(moduleLogger).error('[Ошибка при чтении списка инцидентов и их картинок]', error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Создание маркера для файла
		 */
		postPath: '/api/diagnostics/incident-marking/one',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const { fileId, marker } = req.body || {};
			if (!fileId || !marker) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			return new FileMarkerService(req).createOne({ fileId, marker })
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error('[Ошибка при создании FileMarker]', error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Обновление маркера для файла
		 */
		putPath: '/api/diagnostics/incident-marking/one',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const fileId = Number(req.query.id);
			const { marker } = req.body || {};
			if (!fileId || marker === undefined) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const fileMarkerService = new FileMarkerService(req);
			return fileMarkerService
				.findOneWithoutAccountId({ where: { fileId } })
				.then(item => fileMarkerService.updateOne(item, { marker }))
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error('[Ошибка при обновлении FileMarker]', error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Удаление маркера для файла
		 */
		deletePath: '/api/diagnostics/incident-marking/one',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const fileId = Number(req.query.id);
			if (!fileId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			return new FileMarkerService(req).deleteWhere({ fileId })
				.then(() => res.sendOk())
				.catch(error => {
					req.getLogger(moduleLogger).error('[Ошибка при удалении FileMarker]', error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
];
