import fs from 'fs';
import util from 'util';
import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, { NOT_ALL_REQUIRED_FIELDS, SERVER_ERROR } from '~errors/ResponseError';
import { distinctListByField } from '~utils/list';
import PageInfo from '~utils/PageInfo';
import { ChecklistTaskImportService, ChecklistTaskService, FileService, UserService } from '~services';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import { ChecklistTask, ChecklistTaskImport, User, Shop, File, Log } from '../stores/db/entity';
import { select } from '~lib/sequelize';
import ValidationError from '~errors/ValidationError';
import { ExRequest, ExResponse } from '~lib/WebServer';
import { fileTypeStore } from '~stores';
import { ChecklistTasksImportStatusEnum } from '~enums/ChecklistTasksImportStatusEnum';
import { validateIsBoolean, validateIsIso8601, validateIsPositiveNumber, validateString } from '~utils/validate';
import { ChecklistTaskStatusEnum } from '~enums/ChecklistTaskStatusEnum';

const moduleLogger = new AppLogger('checklistTaskController');

const checkPN = validateIsPositiveNumber();
const checkIdString = validateString(1, 100);
const checkId = v => checkPN(v) && checkIdString(v);
const checkString255 = validateString(1, 255);
const checkString32 = validateString(1, 32);
const checkString2000 = validateString(1, 2000);
const checkIso8601 = validateIsIso8601();

const checkBool = validateIsBoolean();

export default [
	{
		/**
		 * Импорт списка задач по ЧЛ
		 */
		postPath: '/api/checklist_tasks/import/list',
		anyAuthorizedRole: true,
		doNotLogNormalStatus: true,
		handler: async (req: ExRequest, res: ExResponse) => {
			const currentUser = req.getAuthorizedUser();
			const log: any = {
				accountId: currentUser.accountId,
				userId: currentUser.id,
				type: 'CHECKLIST_TASKS_IMPORT',
				ownerId: currentUser.id,
			};
			const checklistTasksImport: any = {
				accountId: currentUser.accountId,
				ownerId: currentUser.id,
			};
			// сохраним входной файл и поместим его в архив
			const {
				fileName: rawFileName,
				fullPath: rawFilePath,
			} = FileService.generateNewFileName('', '.json');
			fs.writeFileSync(rawFilePath, JSON.stringify(req.body));
			// zip
			const {
				fileName: name,
				pathName,
				fullPath,
			} = FileService.generateNewFileName('', '.zip');
			await FileService.zipOneFile(rawFileName, rawFilePath, fullPath);
			const size = fs.statSync(fullPath).size;
			// save file entity
			const zipFileType = await fileTypeStore.findIdByExt('zip');
			const savedFile: any = await File.create({
				name,
				size,
				fileTypeId: zipFileType.id,
				pathName,
				ownerId: currentUser.id,
				accountId: currentUser.accountId,
			});
			fs.unlinkSync(rawFilePath);
			checklistTasksImport.fileId = savedFile.id;

			const logger = req.getLogger(moduleLogger);
			logger.info(`Выполняется импорт задач по ЧЛ для файла #${savedFile.id}`);

			let checklistTasks: any[] | undefined;
			try {
				checklistTasks = parseChecklistTasks(req.body);
			} catch (inputError: any) {
				// сохраняем информацию о неудачном импорте
				const message = `Ошибка валидации запроса: ${inputError.message}. Импорт задач не выполнен.`;
				const error = ResponseError.validationInput(message, { error: inputError });
				logger.error(error);
				Object.assign(log, { message, reason: 'ERROR' });
				checklistTasksImport.status = ChecklistTasksImportStatusEnum.ERROR;
				// save
				const newLog = await Log.create(log);
				Object.assign(checklistTasksImport, { logId: newLog.id });
				await ChecklistTaskImport.create(checklistTasksImport);
				res.sendHttpError(error);
				return;
			}
			// import data
			const svc = new ChecklistTaskImportService(req);
			return svc.importAllChecklistTasks(checklistTasks)
				.then(() => {
					res.sendOk();
					// запоминаем статистику
					Object.assign(log, {
						message: 'Импорт задач по ЧЛ выполнен',
						reason: 'OK',
					});
					checklistTasksImport.status = ChecklistTasksImportStatusEnum.OK;
				})
				.catch(error => {
					req
						.getLogger(moduleLogger)
						.error(fillError(Error, '[Ошибка импорта списка ChecklistTask]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				})
				.finally(async () => {
					// если есть текст в объекте лога, то сохраняем полную связку
					if (log.message) {
						const newLog: any = await Log.create(log);
						Object.assign(checklistTasksImport, { logId: newLog.id });
						await ChecklistTaskImport.create(checklistTasksImport);
					}
				});
		},
	},
	{
		/**
		 *  Получение фотографии к задаче по ЧЛ
		 */
		postPath: '/api/checklist_tasks/import/photo',
		anyAuthorizedRole: true,
		doNotLogNormalStatus: true,
		handler: async (req: ExRequest, res: ExResponse) => {
			const logger = req.getLogger(moduleLogger);
			logger.info(`Выполняется импорт фото для задач по ЧЛ`);

			let checklistTasksPhoto: any;
			try {
				checklistTasksPhoto = parsePhotoImport(req.body);
			} catch (inputError: any) {
				// сохраняем информацию о неудачном импорте
				const message = `Ошибка валидации запроса: ${inputError.message}. Импорт не выполнен.`;
				const error = ResponseError.validationInput(message, { error: inputError });
				logger.error(error);
				res.sendHttpError(error);
				return;
			}
			// import data
			const svc = new ChecklistTaskImportService(req);
			return svc.importChecklistPhoto(checklistTasksPhoto)
				.then(() => {
					res.sendOk();
				})
				.catch(error => {
					req
						.getLogger(moduleLogger)
						.error(fillError(Error, '[Ошибка импорта фотографии]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение списка задач (всех, либо по номерам)
		 */
		getPath: '/api/checklist_tasks/list/:checklistTaskIds?',
		anyAuthorizedRole: true,
		doNotLogNormalStatus: true,
		handler: async (req: any, res: any) => {
			const { checklistTaskIds } = req.params;
			const pageInfo = PageInfo.parseFromObject(req.query);
			const { shopId } = pageInfo.customParams || {};
			const options: any = { where: {} };
			const filterStatuses = (pageInfo.customParams?.statuses || '').split(',').filter(s => !!s);
			if (checklistTaskIds?.length) {
				options.where.id = checklistTaskIds.split(',');
			}
			// набор данных зависит от роли пользователя
			const currentUser = req.getAuthorizedUser();
			const userService = new UserService(req);
			await chargeOptionsByShopIdAndUserRole(options, shopId, currentUser, userService, filterStatuses);
			return new ChecklistTaskService(req)
				.findAllTasksForCurrentShop(options)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error('[Ошибка при чтении списка ChecklistTask]', error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение задачи по номеру
		 */
		getPath: '/api/checklist_tasks/one/:checklistTaskId',
		anyAuthorizedRole: true,
		handler: async (req: any, res: any) => {
			const { checklistTaskId } = req.params;
			if (!checklistTaskId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const pageInfo = PageInfo.parseFromObject(req.query);
			const { shopId } = pageInfo.customParams || {};
			const options = { where: { taskId: checklistTaskId } };
			// набор данных зависит от роли пользователя
			const currentUser = req.getAuthorizedUser();
			const userService = new UserService(req);
			await chargeOptionsByShopIdAndUserRole(options, shopId, currentUser, userService);
			return new ChecklistTaskService(req).findOne(options)
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger).error(`[Произошла ошибка при чтении ChecklistTask]`, error);
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Сохранение решения по задаче
		 */
		putPath: '/api/checklist_tasks/execute/:checklistTaskToShopId',
		// accessRoles: [USER_ROLE_DM, USER_ROLE_ZDM], TODO xxx
		accessRoles: [UserRoles.MANAGER],
		handler: async (req: any, res: any) => {
			const data = req.body;
			if (!data) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(`[Сохраняется решение ChecklistTaskToShop] ${JSON.stringify({
				data,
			})}`);
		},
	},
];

async function chargeOptionsByShopIdAndUserRole(options, shopId, user, userService, filterStatuses?: string[]) {
	const shopManagers = await select(`SELECT *
                                       FROM shop_manager sm
                                                INNER JOIN shop_manager_role smr ON smr.id = sm.roleId
                                       WHERE sm.userId = :userId`, {
		userId: user.id,
	});
	const isShopUser = !!shopManagers.find(s => s.isShopUser);
	if (user.isChief || (user.isManager && !isShopUser)) {
		// определяем список подчиненных магазинов
		let shopIds;
		if (shopId) {
			shopIds = [shopId];
		} else {
			shopIds = distinctListByField(await userService.getSubordinateShopList(user), 'id');
		}
		if (filterStatuses?.length) {
			options.where.status = filterStatuses;
		}
	} else if (user.isManager && isShopUser) {
		options.where.shopId = shopManagers.map(i => i.shopId);
		if (filterStatuses?.length) {
			options.where.status = filterStatuses;
		}
	}
}

function parseChecklistTasks(data: any) {
	const availableFields = ['shopId', 'taskId', 'taskCreatedAt', 'departmentName', 'checkListName',
		'checkListProcessName', 'checkListQuestion', 'creatorComment', 'executorComment', 'questionPhotos',
		'isPhotoRequired', 'isCommentRequired', 'taskDeadlineAt', 'taskClosedAt', 'status', 'creatorFullname',
		'creatorPhone'];
	const { checklistTasks } = data || {};
	if (!checklistTasks?.length) {
		throw new ValidationError('Не заполнен массив checklistTasks');
	}
	const result: any[] = [];
	const uniqTaskIds = new Set<string>();
	for (const item of checklistTasks) {
		const fields = Object.keys(item);
		for (const field of fields) {
			if (!availableFields.includes(field)) {
				throw new ValidationError(`В импорте указано лишнее поле "${field}"`);
			}
		}
		// check required fields
		if (!item.shopId) throw new ValidationError('Не заполнено поле shopId');
		if (checkId(item.shopId)) throw new ValidationError('Ошибочно заполнено поле shopId');
		item.shopId = String(item.shopId);
		if (!item.taskId) throw new ValidationError('Не заполнено поле taskId');
		if (checkId(item.taskId)) throw new ValidationError('Ошибочно заполнено поле taskId');
		item.taskId = String(item.taskId);
		if (uniqTaskIds.has(item.taskId)) {
			throw new ValidationError(util.format(
				'Задача с номером %o уже обработана в этом запросе. В рамках одного запроса номера задач не должны повторяться',
				item.taskId));
		}
		uniqTaskIds.add(item.taskId);
		if (!item.taskCreatedAt) throw new ValidationError('Не заполнено поле taskCreatedAt');
		if (checkString32(item.taskCreatedAt)) throw new ValidationError('Ошибочно заполнено поле taskCreatedAt');
		item.taskCreatedAt = new Date(item.taskCreatedAt).toISOString();
		if (checkIso8601(item.taskCreatedAt)) throw new ValidationError('Значение в поле taskCreatedAt не соответствует стандарту ISO-8601');
		if (!item.checkListQuestion) throw new ValidationError('Не заполнено поле checkListQuestion');
		if (checkString2000(item.checkListQuestion)) throw new ValidationError('Ошибочно заполнено поле checkListQuestion');
		if (!item.taskDeadlineAt) throw new ValidationError('Не заполнено поле taskDeadlineAt');
		if (checkString32(item.taskDeadlineAt)) throw new ValidationError('Ошибочно заполнено поле taskDeadlineAt');
		if (checkIso8601(item.taskDeadlineAt)) throw new ValidationError('Значение в поле taskDeadlineAt не соответствует стандарту ISO-8601');
		item.taskDeadlineAt = new Date(item.taskDeadlineAt).toISOString();
		if (!item.creatorFullname) throw new ValidationError('Не заполнено поле creatorFullname');
		if (checkString255(item.creatorFullname)) throw new ValidationError('Ошибочно заполнено поле creatorFullname');
		// check nullable fields
		if (item.departmentName !== undefined && item.departmentName !== null) {
			if (checkString255(item.departmentName)) {
				throw new ValidationError('Ошибочно указано поле departmentName');
			}
		}
		if (item.checkListName !== undefined && item.checkListName !== null) {
			if (checkString255(item.checkListName)) {
				throw new ValidationError('Ошибочно указано поле checkListName');
			}
		}
		if (item.checkListProcessName !== undefined && item.checkListProcessName !== null) {
			if (checkString255(item.checkListProcessName)) {
				throw new ValidationError('Ошибочно указано поле checkListProcessName');
			}
		}
		if (item.creatorComment !== undefined && item.creatorComment !== null) {
			if (checkString2000(item.creatorComment)) {
				throw new ValidationError('Ошибочно заполнено поле creatorComment');
			}
		}
		if (item.executorComment !== undefined && item.executorComment !== null) {
			if (checkString2000(item.executorComment)) {
				throw new ValidationError('Ошибочно заполнено поле executorComment');
			}
		}
		if (item.taskClosedAt !== undefined && item.taskClosedAt !== null) {
			if (checkString32(item.taskClosedAt)) {
				throw new ValidationError('Ошибочно заполнено поле taskClosedAt');
			}
			if (checkIso8601(item.taskClosedAt)) throw new ValidationError('Значение в поле taskClosedAt не соответствует стандарту ISO-8601');
			item.taskClosedAt = new Date(item.taskClosedAt).toISOString();
		}
		if (item.checkListProcessName !== undefined && item.checkListProcessName !== null) {
			if (checkString255(item.checkListProcessName)) {
				throw new ValidationError('Ошибочно указано поле checkListProcessName');
			}
		}
		if (item.creatorPhone !== undefined && item.creatorPhone !== null) {
			if (checkString255(item.creatorPhone)) {
				throw new ValidationError('Ошибочно указано поле creatorPhone');
			}
		}
		// check array []
		if (item.questionPhotos !== undefined && item.questionPhotos !== null) {
			if (!item.questionPhotos?.length) {
				throw new ValidationError('Ошибочно заполнено поле questionPhotos');
			}
			const fail = item.questionPhotos.some(i => typeof i !== 'string');
			if (fail) {
				throw new ValidationError('Элементы массива questionPhotos должны быть типа string');
			}
		}
		// check boolean
		if (item.isPhotoRequired !== undefined) {
			if (checkBool(item.isPhotoRequired)) {
				throw new ValidationError('Ошибочно заполнено поле isPhotoRequired');
			}
		}
		if (item.isCommentRequired !== undefined) {
			if (checkBool(item.isCommentRequired)) {
				throw new ValidationError('Ошибочно заполнено поле isCommentRequired');
			}
		}
		// check status
		if (!item.status) throw new ValidationError('Не заполнено поле status');
		if (item.status !== ChecklistTaskStatusEnum.PLANNED && item.status !== ChecklistTaskStatusEnum.STARTED
			&& item.status !== ChecklistTaskStatusEnum.COMPLETED) {
			throw new ValidationError('Ошибочно заполнено поле status');
		}
		result.push(item);
	}
	return result;
}

function parsePhotoImport(data: any) {
	const availableFields = ['taskId', 'photoId', 'contentType', 'contentData'];
	if (!data) {
		throw new ValidationError('Тело запроса должно быть заполнено');
	}
	for (const item in data) {
		if (!availableFields.includes(item)) {
			throw new ValidationError(`В импорте указано лишнее поле "${item}"`);
		}
	}
	if (!data.taskId) throw new ValidationError('Не заполнено поле taskId');
	if (checkId(data.taskId)) throw new ValidationError('Ошибочно заполнено поле taskId');
	if (!data.photoId) throw new ValidationError('Не заполнено поле photoId');
	if (typeof data.photoId !== 'string') throw new ValidationError('Ошибочно заполнено поле photoId');
	if (data.contentType !== 'image/png') throw new ValidationError('Ошибочно заполнено поле contentType');
	if (!data.contentData) throw new ValidationError('Ошибочно заполнено поле contentData');
	if (!data.contentData.match(/^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/)) {
		throw ResponseError.notFound('Значение в поле contentData должно быть закодировано в кодировке base64');
	}
	return data;
}
