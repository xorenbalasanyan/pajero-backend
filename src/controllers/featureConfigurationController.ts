import util from 'util';
import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, {
	DATABASE_WRITE_ERROR,
	REQUESTED_ITEM_NOT_FOUND,
	SERVER_ERROR,
} from '~errors/ResponseError';
import { AppLogger } from '~lib/AppLogger';
import { ExRequest, ExResponse } from '~lib/WebServer';
import { FeatureConfigurationService, ShopService } from '~services';
import PageInfo from '~utils/PageInfo';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('featureConfigurationController');

export default [
	{
		/**
		 * Получение списка конфигураций в ЛК администратора
		 */
		getPath: '/api/feature_configurations/list',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		doNotLogNormalStatus: true,
		handler: async (req: ExRequest, res: ExResponse) => {
			const pageInfo = PageInfo.parseFromObject(req.query);
			const svc = new FeatureConfigurationService(req);
			return svc
				.findAllWithPageInfo({ where: {} }, pageInfo)
				.then(([list, pageInfo1]) => {
					res.sendOk(list, pageInfo1);
				})
				.catch((error) => {
					req
						.getLogger(moduleLogger)
						.error(fillError(Error, '[Ошибка при чтении списка опросов]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
	{
		/**
		 * Получение конфигурации
		 */
		getPath: '/api/feature_configurations/one/:featureConfigurationId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: ExRequest, res: ExResponse) => {
			const featureConfigurationId = +req.params.featureConfigurationId;
			const svc = new FeatureConfigurationService(req);
			const featureConfiguration = await svc.findOneById(featureConfigurationId);
			if (!featureConfiguration) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			res.sendOk(featureConfiguration);
		},
	},
	{
		/**
		 * Редактирование конфигурации
		 */
		postPath: '/api/feature_configurations/edit/:featureConfigurationId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: ExRequest, res: ExResponse) => {
			const featureConfigurationId = +req.params.featureConfigurationId;
			const svc = new FeatureConfigurationService(req);
			const featureConfiguration = await svc.findOneById(featureConfigurationId);
			if (!featureConfiguration) {
				return res.sendHttpError(REQUESTED_ITEM_NOT_FOUND);
			}
			const { enabledAll, shopIds } = req.body;
			if (typeof enabledAll !== 'boolean') {
				return res.sendHttpError(ResponseError.badRequest('Field enabledAll must be boolean'));
			}
			// validation
			if (enabledAll === false) {
				if (!(shopIds instanceof Array)) {
					return res.sendHttpError(ResponseError.badRequest('Field shopIds must be list of numbers because enabledAll is false'));
				}
				const isNumArray = shopIds.every((n) => typeof n === 'number');
				if (!isNumArray) {
					return res.sendHttpError(ResponseError.badRequest('Field shopIds must be list of numbers because enabledAll is false'));
				}
				if (shopIds.length) {
					const shopSvc = new ShopService(req);
					const shops = await shopSvc.findAll({
						where: {
							id: shopIds,
						},
						attributes: ['id'],
						raw: true,
					});
					if (shops.length !== shopIds.length) {
						return res.sendHttpError(
							ResponseError.badRequest('You must select shops from your account'),
						);
					}
				}
			}
			// update
			const newData = {
				enabledAll,
				shopIds: enabledAll ? null : shopIds,
			};
			const logger = req.getLogger(moduleLogger);
			logger.info(`Updating FeatureConfiguration #${featureConfigurationId} with body ${JSON.stringify(newData)}`);
			await svc
				.updateOne(featureConfiguration, newData)
				.then(res.sendOk)
				.catch((error) => {
					logger.error(fillError(Error, util.format('Error when update FeatureConfiguration'), error));
					res.sendHttpError(DATABASE_WRITE_ERROR);
				});
		},
	},
];
