import { UserRoles } from '~enums/UserRoleEnum';
import {
	DATABASE_READ_ERROR,
	NOT_ALL_REQUIRED_FIELDS,
	UNHANDLED_EXCEPTION,
	USER_NOT_FOUND,
} from '~errors/ResponseError';
import { AppLogger } from '~lib/AppLogger';
import { UserRepository, UserStatsRepository } from '~repositories';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('userStatsController');

export default [
	{
		/**
		 * Статистика авторизаций пользователя
		 */
		getPath: '/api/user-stats/auth-history/:userId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			// check
			const { userId } = req.params;
			if (!userId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const currentUser = req.getAuthorizedUser();
			const user = await UserRepository.findOneByAccountIdAndId(currentUser.accountId, userId);
			if (!user) {
				return res.sendHttpError(USER_NOT_FOUND);
			}
			const logger = req.getLogger(moduleLogger);
			try {
				UserStatsRepository.find100AuthsByUser(user)
					.then(stats => {
						res.sendOk(stats.map((i: any) => i.toDto()));
					})
					.catch(error => {
						logger.error(fillError(Error, 'Failed new filial saving in database', error));
						res.sendHttpError(DATABASE_READ_ERROR);
					});
			} catch (error: any) {
				logger.error(fillError(Error, 'Failed create filial', error));
				res.sendHttpError(UNHANDLED_EXCEPTION);
			}
		},
	},
];
