import ResponseError, { SERVER_ERROR } from '~errors/ResponseError';
import { ShopManagerRoleService } from '~services';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';

const moduleLogger = new AppLogger('shopManagerRoleController');

export default [
	{
		/**
		 * Получение списка ролей менеджеров в магазинах
		 */
		getPath: '/api/shop-roles',
		anyAuthorizedRole: true,
		handler: async (req, res) => {
			const service = new ShopManagerRoleService(req);
			return await service.findAllWithAccountId({ where: {} })
				.then(res.sendOk)
				.catch(error => {
					req.getLogger(moduleLogger)
						.error(fillError(Error, '[Ошибка при чтении списка ролей пользователей магазинов]', error));
					res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
				});
		},
	},
];
