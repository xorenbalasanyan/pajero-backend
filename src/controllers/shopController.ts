import util from 'util';
import { Op } from 'sequelize';
import { UserRoles } from '~enums/UserRoleEnum';
import ResponseError, {
	DATABASE_WRITE_ERROR,
	NOT_ALL_REQUIRED_FIELDS,
	SERVER_ERROR,
	SHOP_NOT_FOUND,
	UNHANDLED_EXCEPTION,
	UNIQUE_CONSTRAINT_ERROR,
} from '~errors/ResponseError';
import sequelize from '~lib/sequelize';
import { ShopService } from '~services';
import { ShopRepository } from '~repositories';
import PageInfo from '~utils/PageInfo';
import { AppLogger } from '~lib/AppLogger';
import { fillError } from '~utils/errors';
import { shopManagerStore, shopStore } from '~stores';
import { Shop } from '../stores/db/entity';

const moduleLogger = new AppLogger('shopController');

export default [
	{
		/**
		 * Получение списка магазинов
		 */
		getPath: '/api/shops/:shopIds?',
		anyAuthorizedRole: true,
		handler: async (req, res) => {
			const currentUser = req.getAuthorizedUser();
			const options: any = { where: {} };
			const { shopIds } = req.params;
			if (shopIds) {
				options.where.id = shopIds.split(',');
			}
			const shopService = new ShopService(req);
			const pageInfo = PageInfo.parseFromObject(req.query);
			// набор данных зависит от роли пользователя
			if (currentUser.isAdmin || currentUser.isSupport || currentUser.isDt) {
				// оставляем как есть
			} else if (currentUser.isDf) {
				options.where.dfId = currentUser.id;
			} else if (currentUser.isTm) {
				options.where.tmId = currentUser.id;
			} else if (currentUser.isUpf) {
				options.where.upfId = currentUser.id;
			} else if (currentUser.isDm || currentUser.isZdm) {
				options.where.id = currentUser.shopId;
			}
			try {
				let result = await shopService.findAllWithPageInfo(options, pageInfo);
				const pageInfo1 = result[1];
				// если указан поиск, то делаем повторный поиск без условий поиска, но с указанными shop.id
				if (pageInfo.search) {
					pageInfo.search = undefined;
					options.where = { id: result[0].map(s => s.id) };
					result = await shopService.findAllWithPageInfo(options, pageInfo);
				}
				res.sendOk(result[0], pageInfo1);
			} catch (error: any) {
				req.getLogger(moduleLogger)
					.error(fillError(Error, '[Ошибка при чтении списка магазинов]', error));
				res.sendHttpError(error instanceof ResponseError ? error : SERVER_ERROR);
			}
		},
	},
	{
		/**
		 * Получение магазинов по списку externalId
		 */
		getPath: '/api/shops/by-externalIds/:externalIds',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT, UserRoles.ROBOT],
		handler: (req, res) => {
			const externalIds = req.params.externalIds?.split(',');
			if (!externalIds?.length) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			return new ShopService(req)
				.findAll({ where: { externalId: { [Op.in]: externalIds } } })
				.then(shops => res.json(shops?.map(s => s.toDto())));
		},
	},
	{
		/**
		 * Получение магазина по id
		 * ex: ?meta=users
		 */
		getPath: '/api/shop/:id',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT, UserRoles.CHIEF, UserRoles.MANAGER],
		handler: async (req: any, res: any) => {
			const { id } = req.params;
			if (!id) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const shopService = new ShopService(req);
			const pageInfo = PageInfo.parseFromObject(req.query);
			const options = { where: { id } };
			await shopService.addMeta(options, pageInfo);
			// TODO: добавить проверку доступа для разных ролей
			//  например, ДМ не может получать инфу по любому из магазинов
			const shop: any = await shopService.findOne(options);
			if (!shop) {
				return res.sendHttpError(SHOP_NOT_FOUND);
			}
			res.sendOk(shop);
		},
	},
	{
		/**
		 * Получение магазина по externalId
		 */
		getPath: '/api/shop/by-externalId/:externalId',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT, UserRoles.ROBOT],
		handler: (req, res) => {
			const { externalId } = req.params;
			if (!externalId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			return new ShopService(req).findOne({ where: { externalId } })
				.then(shop => res.json(shop?.toDto()));
		},
	},
	{
		/**
		 * Создание магазина
		 */
		postPath: '/api/shop',
		accessRoles: [UserRoles.ADMIN, UserRoles.SUPPORT],
		handler: async (req: any, res: any) => {
			// check
			const currentUser = req.getAuthorizedUser();
			const data = req.body;
			if (!data) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(util.format('Create shop with body %o', data));
			// check body
			const { externalId, name, regionId, city, address, phone, email, timeOffset, shopManagers } = data;
			const newShopData: any = {
				accountId: currentUser.accountId,
				externalId,
				name,
				regionId,
				city,
				address,
				phone,
				email,
				timeOffset,
				ownerId: currentUser.id,
			};
			// прицепляем менеджеров
			const addShopManagers = shopManagers?.map(({ roleId, userId }) => {
				return { roleId, userId };
			})
				.filter(Boolean);
			// save
			await sequelize
				.transaction(async transaction => await shopStore
					.createOne(newShopData, transaction)
					.then(async newShop => {
						if (addShopManagers?.length) {
							await Promise.all(
								addShopManagers.map(async newShopManager => {
									const newManagerData = {
										roleId: newShopManager.roleId,
										userId: newShopManager.userId,
										shopId: newShop.id,
										ownerId: currentUser.id,
									};
									logger.http(util.format('Create shopManager %o', newManagerData));
									return await shopManagerStore.createOne(newManagerData, transaction);
								}),
							);
						}
					}))
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Ошибка создания магазина]', error));
					if (error.constructor.name === 'UniqueConstraintError') {
						// ошибка уникальной записи
						res.sendHttpError(UNIQUE_CONSTRAINT_ERROR);
					} else {
						res.sendHttpError(UNHANDLED_EXCEPTION);
					}
				});
		},
	},
	{
		/**
		 * Обновление магазина
		 */
		putPath: '/api/shop/:shopId',
		accessRoles: [UserRoles.ADMIN],
		handler: async (req: any, res: any) => {
			// check
			const { shopId } = req.params;
			if (!shopId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const currentUser = req.getAuthorizedUser();
			const existShop: any = await ShopRepository.findOneByAccountIdAndId(currentUser.accountId, shopId);
			if (!existShop) {
				return res.sendHttpError(SHOP_NOT_FOUND);
			}
			const data = req.body;
			if (!data) {
				return res.sendHttpError(ResponseError.requiredFields('Отсутствует тело запроса'));
			}
			const logger = req.getLogger(moduleLogger);
			logger.http(util.format('Update shop #%d with body %o', shopId, data));
			// check body
			const { externalId, name, regionId, city, address, phone, email, timeOffset,
				shopManagers } = data;
			const newData: any = {
				externalId, // может прийти пустая строка
				name,
				regionId, // может прийти null
				city,
				address,
				phone,
				email,
				timeOffset,
				moderatorId: currentUser.id,
			};
			// прицепляем менеджеров
			const existShopManagers = await shopManagerStore.findAll({ where: { shopId: existShop.id } });
			// каких добавить
			const addShopManagers = shopManagers.map(({ roleId, userId }) => {
				const ex = existShopManagers.find(i => i.roleId === roleId && i.userId === userId);
				if (ex) return; // Тот же пользователь
				return { roleId, userId };
			})
				.filter(Boolean);
			// каких удалить
			const removeShopManagers = existShopManagers.map(sm => {
				const ex = shopManagers.find(i => i.roleId === sm.roleId && i.userId === sm.userId);
				if (ex) return; // оставляем
				return sm;
			})
				.filter(Boolean);
			// save
			await sequelize
				.transaction(async transaction => await existShop
					.update(newData, { transaction })
					.then(async updatedShop => {
						const promises: Promise<void>[] = [];
						if (removeShopManagers.length) {
							// remove old managers
							const removeShopManagerIds = removeShopManagers.map(i => i.id);
							logger.http(util.format('Remove shopManager with ids %o', removeShopManagerIds));
							promises.push(shopManagerStore.deleteWhere({ id: removeShopManagerIds }, transaction));
						}
						// add new managers
						if (addShopManagers.length) {
							promises.push(...addShopManagers
								.map(async newShopManager => {
									const newManagerData = {
										roleId: newShopManager.roleId,
										userId: newShopManager.userId,
										shopId: updatedShop.id,
										ownerId: currentUser.id,
									};
									logger.http(util.format('Update shopManager %o', newManagerData));
									return shopManagerStore.createOne(newManagerData, transaction);
								}));
						}
						return await Promise.all(promises);
					}),
				)
				.then(res.sendOk)
				.catch(error => {
					logger.error(fillError(Error, '[Ошибка обновления магазина]', error));
					if (error.constructor.name === 'UniqueConstraintError') {
						// ошибка уникальной записи
						res.sendHttpError(UNIQUE_CONSTRAINT_ERROR);
					} else {
						res.sendHttpError(UNHANDLED_EXCEPTION);
					}
				});
		},
	},
	{
		/**
		 * Удаление магазина
		 */
		deletePath: '/api/shop/:shopId',
		accessRoles: [UserRoles.ADMIN],
		handler: async (req: any, res: any) => {
			// check
			const { shopId } = req.params;
			if (!shopId) {
				return res.sendHttpError(NOT_ALL_REQUIRED_FIELDS);
			}
			const currentUser = req.getAuthorizedUser();
			const shop: typeof Shop = await ShopRepository.findOneByAccountIdAndId(currentUser.accountId, shopId);
			if (!shop) {
				return res.sendHttpError(SHOP_NOT_FOUND);
			}
			const shopManagers = await shopManagerStore.findAll({
				where: {
					shopId: shop.id,
				},
			});

			const logger = req.getLogger(moduleLogger);
			await sequelize.transaction(async transaction => {
				try {
					logger.http(util.format('[Удаление магазина] Номер #%d', shopId));
					for (const shopManager of shopManagers) {
						await shopManager.destroy({ transaction });
					}
					await shop.update({
						isDeleted: true,
						moderatorId: currentUser.id,
					}, { transaction });
					await transaction.commit();
					res.sendOk(shop.toDto());
				} catch (error: any) {
					logger.error(fillError(Error, `[Ошибка при удалении магазина] #${shopId}:`, error));
					await transaction.rollback();
					res.sendHttpError(DATABASE_WRITE_ERROR);
				}
			});
		},
	},
];
