import { Op } from 'sequelize';
import { IncidentType } from '../stores/db/entity';

export default class IncidentTypeRepository
{
	/**
	 * Поиск по аккаунту и названию
	 */
	static async findOneByAccountIdAndType(accountId, type) {
		return IncidentType.findOne({
			where: {
				accountId,
				type,
			},
		});
	}

	/**
	 * Вытаскивает последнюю версию описания типа инцидента
	 * @param accountId
	 * @param type
	 * @returns {Promise<*>}
	 */
	static async findOneWithLastVersionByAccountIdAndType(accountId, type) {
		return IncidentType.findOne({
			where: {
				accountId,
				type,
			},
			order: [['version', 'DESC']],
		});
	}

	static findOneByAccountAndId = (accountId, id) => IncidentType.findOne({
		where: {
			accountId,
			id,
		},
	});

	static findAllByAccountAndIds = (accountId, ids) => IncidentType.findAll({
		where: {
			accountId,
			id: {
				[Op.in]: ids,
			},
		},
	});
}
