import BaseStore from './BaseStore';
import FileTypeDbStore from './db/FileTypeDbStore';
import { Transaction } from 'sequelize';

export default class FileTypeStore extends BaseStore
{
	constructor() {
		super(FileTypeDbStore);
	}

	findIdByExt = async (ext: string, transaction?: Transaction) => this.findOne({
		where: { ext },
		attributes: ['id'],
		raw: true,
	}, transaction)

	findIdByMimeType = async (mimeType: string, transaction?: Transaction) => this.findOne({
		where: { mimeType },
		attributes: ['id'],
		raw: true,
	}, transaction)
}
