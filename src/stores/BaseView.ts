import { Op, Transaction } from 'sequelize';

/**
 * Объект для выполнения SELECT запросов к хранилищу модели.
 */
export default class BaseView
{
	protected readonly dbStore: any;

	constructor(DbStoreType) {
		this.dbStore = new DbStoreType();
	}

	count = async (options: any, transaction?: Transaction): Promise<any> => this
		.dbStore.count(options, transaction)

	findOne = async (options: any, transaction?: Transaction): Promise<any> => this
		.dbStore.findOne(options, transaction)

	findOneUnscoped = async (options: any, transaction?: Transaction): Promise<any> => this
		.dbStore.findOneUnscoped(options, transaction)

	findOneById = async (id: number, transaction?: Transaction): Promise<any> => this
		.dbStore.findOne({ where: { id } }, transaction)

	findAll = async (options: any, transaction?: Transaction): Promise<any[]> => this
		.dbStore.findAll(options, transaction)

	findAllUnscoped = async (options: any, transaction?: Transaction): Promise<any[]> => this
		.dbStore.findAllUnscoped(options, transaction)

	findAllByIds = async (ids: number[], transaction?: Transaction): Promise<any[]> => this
		.findAll({ where: { id: { [Op.in]: ids } } }, transaction)
}
