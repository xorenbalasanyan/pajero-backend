import BaseStore from './BaseStore';
import QuestionnaireHistoryDbStore from './db/QuestionnaireHistoryDbStore';

export default class QuestionnaireHistoryStore extends BaseStore
{
	constructor() {
		super(QuestionnaireHistoryDbStore);
	}
}
