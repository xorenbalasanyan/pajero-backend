import BaseStore from './BaseStore';
import ChecklistTaskDbStore from './db/ChecklistTaskDbStore';

export default class ChecklistTaskStore extends BaseStore
{
	constructor() {
		super(ChecklistTaskDbStore);
	}
}
