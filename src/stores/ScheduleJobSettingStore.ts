import BaseStore from './BaseStore';
import ScheduleJobSettingDbStore from './db/ScheduleJobSettingDbStore';

export default class ScheduleJobSettingStore extends BaseStore
{
	constructor() {
		super(ScheduleJobSettingDbStore);
	}
}
