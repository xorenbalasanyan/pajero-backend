import util from 'util';
import { Transaction } from 'sequelize';
import BaseDbView from './BaseDbView';

/**
 * Объект для выполнения всех типов запросов к БД.
 */
export default class BaseDbStore extends BaseDbView
{
	constructor(entityType: any) {
		super(entityType);
	}

	createOne = async (item: any, transaction?: Transaction): Promise<any> => this
		.entityType.create(item, { transaction })

	createAll = async (items: any, transaction?: Transaction): Promise<any[]> => this
		.entityType.bulkCreate(items, { transaction })

	updateOne = async (item: any, newData: any, transaction?: Transaction): Promise<any> => item
		.update(newData, { transaction })

	// @Deprecated
	updateAll = async (items: any[], newData: any, transaction?: Transaction): Promise<any[]> => items
		.map(i => i.update(newData, { transaction }))

	/**
	 * Обновление записей по условию.
	 * @param newData
	 * @param options может содержать transaction
	 */
	updateWhere = async (newData: any, options: any) => this
		.entityType.update(newData, options)

	deleteWhere = async (where: any, transaction?: Transaction) => {
		const options = { where: where || {}, transaction };
		// check for truncate
		if (!Object.keys(options.where).length) {
			throw new Error(util.format(
				'Запрещено удаление записей из таблицы без условий! Метод deleteWhere вызван с атрибутами (%o)',
				where));
		}
		return this.entityType.destroy(options, transaction); // FIXME transaction проверить
	}
}
