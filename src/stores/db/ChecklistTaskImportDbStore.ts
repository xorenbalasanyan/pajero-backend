import { ChecklistTaskImport } from '../../stores/db/entity';
import BaseDbStore from './BaseDbStore';

export default class ChecklistTaskImportDbStore extends BaseDbStore
{
	constructor() {
		super(ChecklistTaskImport);
	}
}
