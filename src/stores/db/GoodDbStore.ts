import BaseDbStore from './BaseDbStore';
import { Good } from './entity';

export default class GoodDbStore extends BaseDbStore
{
	constructor() {
		super(Good);
	}
}
