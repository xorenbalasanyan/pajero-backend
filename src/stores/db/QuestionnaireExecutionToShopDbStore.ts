import BaseDbStore from './BaseDbStore';
import { QuestionnaireExecutionToShop } from './entity';

export default class QuestionnaireExecutionToShopDbStore extends BaseDbStore
{
	constructor() {
		super(QuestionnaireExecutionToShop);
	}
}
