import BaseDbStore from './BaseDbStore';
import { PhotoGallery } from './entity';

export default class PhotoGalleryDbStore extends BaseDbStore {
	constructor() {
		super(PhotoGallery);
	}
}
