import BaseDbStore from './BaseDbStore';
import { ShopManagerRole } from './entity';

export default class ShopManagerRoleDbStore extends BaseDbStore
{
	constructor() {
		super(ShopManagerRole);
	}
}
