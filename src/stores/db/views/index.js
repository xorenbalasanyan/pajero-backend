/**
 * Данные модели ссылаются на view, поэтому предназначены только для селекта.
 */
import FreshTask from './FreshTask';
import { Account, TaskToShop, User } from '../entity';

const OWNER_ID_TO_USER_ID = { as: 'fk_owner', foreignKey: 'ownerId', targetKey: 'id' };
const MODERATOR_ID_TO_USER_ID = { as: 'fk_moderator', foreignKey: 'moderatorId', targetKey: 'id' };

FreshTask.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
FreshTask.hasMany(TaskToShop, { as: 'fk_taskToShops', sourceKey: 'id', foreignKey: 'taskId' });
FreshTask.belongsTo(User, OWNER_ID_TO_USER_ID);
FreshTask.belongsTo(User, MODERATOR_ID_TO_USER_ID);

export {
	FreshTask,
};
