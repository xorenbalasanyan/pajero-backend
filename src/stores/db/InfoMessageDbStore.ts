import BaseDbStore from './BaseDbStore';
import { InfoMessage } from '../../stores/db/entity';

export default class InfoMessageDbStore extends BaseDbStore
{
	constructor() {
		super(InfoMessage);
	}
}
