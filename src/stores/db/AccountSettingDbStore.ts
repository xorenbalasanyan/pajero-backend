import BaseDbStore from './BaseDbStore';
import { AccountSetting } from './entity';

export default class AccountSettingDbStore extends BaseDbStore
{
	constructor() {
		super(AccountSetting);
	}
}
