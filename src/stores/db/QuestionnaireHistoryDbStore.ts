import BaseDbStore from './BaseDbStore';
import { QuestionnaireHistory } from './entity';

export default class QuestionnaireHistoryDbStore extends BaseDbStore
{
	constructor() {
		super(QuestionnaireHistory);
	}
}
