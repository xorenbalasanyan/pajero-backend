import BaseDbStore from './BaseDbStore';
import { AuthCredential } from './entity';

export default class AuthCredentialDbStore extends BaseDbStore
{
	constructor() {
		super(AuthCredential);
	}
}
