import BaseDbStore from './BaseDbStore';
import { UserAuth } from './entity';

export default class UserAuthDbStore extends BaseDbStore
{
	constructor() {
		super(UserAuth);
	}
}
