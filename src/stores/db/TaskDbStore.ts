import BaseDbStore from './BaseDbStore';
import { Task } from './entity';

export default class TaskDbStore extends BaseDbStore
{
	constructor() {
		super(Task);
	}
}
