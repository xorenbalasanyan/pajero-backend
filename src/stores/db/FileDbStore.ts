import BaseDbStore from './BaseDbStore';
import { File } from './entity';

export default class FileDbStore extends BaseDbStore
{
	constructor() {
		super(File);
	}
}
