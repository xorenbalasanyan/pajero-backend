import BaseDbStore from './BaseDbStore';
import { QuestionnaireExecution } from './entity';

export default class QuestionnaireExecutionDbStore extends BaseDbStore
{
	constructor() {
		super(QuestionnaireExecution);
	}
}
