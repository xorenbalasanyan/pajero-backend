import BaseDbStore from './BaseDbStore';
import { ChecklistTask } from './entity';

export default class ChecklistTaskDbStore extends BaseDbStore
{
	constructor() {
		super(ChecklistTask);
	}
}
