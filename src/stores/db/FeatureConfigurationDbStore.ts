import BaseDbStore from './BaseDbStore';
import { FeatureConfiguration } from './entity';

export default class FeatureConfigurationDbStore extends BaseDbStore {
	constructor() {
		super(FeatureConfiguration);
	}
}
