import { Transaction } from 'sequelize';

/**
 * Объект для выполнения SELECT запросов к БД.
 * Используется для декорации view моделей.
 */
export default class BaseDbView
{
	protected readonly entityType: any;

	constructor(entityType: any) {
		this.entityType = entityType;
	}

	/**
	 * Подсчет количества может выполняться с include условием.
	 * Поэтому необходимо подсчитывать количество строк по группе.
	 * @param options
	 * @param transaction
	 * @returns {Promise<*>}
	 */
	count = async (options: any, transaction?: Transaction) => this
		.entityType.count({
			...options,
			group: [`${this.entityType.name}.id`],
		}, transaction)
		.then(res => res.length)

	findOne = async (options: any, transaction?: Transaction): Promise<any> => this
		.entityType.findOne({ ...options, transaction })

	findOneUnscoped = async (options: any, transaction?: Transaction): Promise<any> => this
		.entityType.unscoped().findOne({ ...options, transaction })

	findAll = async (options: any, transaction?: Transaction): Promise<any[]> => this
		.entityType.findAll({ ...options, transaction })

	findAllUnscoped = async (options: any, transaction?: Transaction): Promise<any[]> => this
		.entityType.unscoped().findAll({ ...options, transaction })
}
