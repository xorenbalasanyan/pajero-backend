import BaseDbStore from './BaseDbStore';
import { Questionnaire } from './entity';

export default class QuestionnaireDbStore extends BaseDbStore
{
	constructor() {
		super(Questionnaire);
	}
}
