import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const IncidentType = DefineLoggedEntity(
	'incidentType',
	'incident_type',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		name: {
			type: Sequelize.STRING(100),
			allowNull: false,
		},
		type: {
			type: Sequelize.STRING(50),
			allowNull: false,
		},
		version: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		data: {
			type: Sequelize.TEXT,
			allowNull: false,
		},
	});

IncidentType.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		name: this.name,
		type: this.type,
		data: this.data,
		version: this.version,
		updatedAt: this.updatedAt,
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default IncidentType;
