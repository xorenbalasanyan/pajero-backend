import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';
import { ChecklistTaskStatusEnum } from "~enums/ChecklistTaskStatusEnum";

const ChecklistTask = DefineLoggedEntity(
	'checklistTask',
	'checklist_task',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		shopId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		taskId: {
			type: Sequelize.STRING(255),
			allowNull: false,
		},
		status: {
			type: Sequelize.ENUM(
				ChecklistTaskStatusEnum.PLANNED,
				ChecklistTaskStatusEnum.STARTED,
				ChecklistTaskStatusEnum.COMPLETED,
			),
			allowNull: false,
		},
		taskCreatedAt: {
			type: Sequelize.DATE,
			allowNull: false,
		},
		departmentName: {
			type: Sequelize.STRING(255),
			defaultValue: null,
		},
		checkListName:  {
			type: Sequelize.STRING(255),
			defaultValue: null,
		},
		checkListProcessName:  {
			type: Sequelize.STRING(255),
			defaultValue: null,
		},
		checkListQuestion: {
			type: Sequelize.TEXT,
			allowNull: false,
		},
		creatorComment: {
			type: Sequelize.TEXT,
			defaultValue: null,
		},
		questionPhotos: {
			type: Sequelize.JSON,
			defaultValue: null,
		},
		isPhotoRequired: {
			type: Sequelize.BOOLEAN,
			defaultValue: null,
		},
		isCommentRequired: {
			type: Sequelize.BOOLEAN,
			defaultValue: null,
		},
		taskDeadlineAt: {
			type: Sequelize.DATE,
			defaultValue: null,
		},
		creatorFullname: {
			type: Sequelize.STRING(255),
			allowNull: false,
		},
		creatorPhone: {
			type: Sequelize.STRING(255),
			defaultValue: null,
		},
		isNeedUpdateMedia: {
			type: Sequelize.BOOLEAN,
			allowNull: false,
			defaultValue: true,
		},
		taskClosedAt: {
			type: Sequelize.DATE,
			defaultValue: null,
		},
		executorUserId: {
			type: Sequelize.INTEGER,
			defaultValue: null,
		},
		executorShopId: {
			type: Sequelize.INTEGER,
			defaultValue: null,
		},
		executorComment: {
			type: Sequelize.TEXT,
			defaultValue: null,
		},
		executorPhotoIds: {
			type: Sequelize.JSON,
			defaltValue: null,
		},
		isExpired: {
			type: Sequelize.BOOLEAN,
			defaultValue: null,
		},
		isReportSent: {
			type: Sequelize.BOOLEAN,
			defaultValue: null,
		},
	});

ChecklistTask.prototype.toDto = function toDto() {
	return {
		id: this.id,
		shopId: this.shopId,
		taskId: this.taskId,
		status: this.status,
		taskCreatedAt: this.taskCreatedAt,
		departmentName: this.departmentName,
		checkListName: this.checkListName,
		checkListProcessName: this.checkListProcessName,
		checkListQuestion: this.checkListQuestion,
		creatorComment: this.creatorComment,
		questionPhotos: this.questionPhotos,
		isPhotoRequired: this.isPhotoRequired,
		isCommentRequired: this.isCommentRequired,
		taskDeadlineAt: this.taskDeadlineAt,
		creatorFullname: this.creatorFullname,
		creatorPhone: this.creatorPhone,
		isNeedUpdateMedia: this.isNeedUpdateMedia,
		taskClosedAt: this.taskClosedAt,
		executorUserId: this.executorUserId,
		executorShopId: this.executorShopId,
		executorComment: this.executorComment,
		executorPhotoIds: this.executorPhotoIds,
		isExpired: this.isExpired,
		isReportSent: this.isReportSent,
	};
};

export default ChecklistTask;
