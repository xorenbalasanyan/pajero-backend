import Sequelize from 'sequelize';
import { DefineNotUpdatedEntity } from './tools';
import { ChecklistTasksImportStatusEnum } from '~enums/ChecklistTasksImportStatusEnum';

const ChecklistTaskImport = DefineNotUpdatedEntity(
	'checklistTaskImport',
	'checklist_task_import',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		fileId: Sequelize.INTEGER,
		logId: Sequelize.INTEGER,
		status: {
			type: Sequelize.ENUM(
				ChecklistTasksImportStatusEnum.OK,
				ChecklistTasksImportStatusEnum.WARN,
				ChecklistTasksImportStatusEnum.ERROR,
			),
			allowNull: false,
			defaultValue: ChecklistTasksImportStatusEnum.OK,
		},
	});

ChecklistTaskImport.prototype.toDto = function toDto() {
	return {
		id: this.id,
		fileId: this.fileId,
		logId: this.logId,
		status: this.status,
		ownerId: this.ownerId,
		createdAt: this.createdAt,
	};
};

export default ChecklistTaskImport;
