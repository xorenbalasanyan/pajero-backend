import md5 from 'md5';
import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

/**
 * Метод хеширования пароля
 * @param username
 * @param key
 * @returns {string|undefined}
 */
function generatePasswordHash(username: string, key: string): string | undefined {
	if (!username || !key) return;
	return md5(`${username}${md5(key)}`);
}

const AuthCredential = DefineLoggedEntity(
	'authCredential',
	'auth_credential',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		userId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		username: {
			type: Sequelize.STRING(100),
			allowNull: false,
			set(value) {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				this.setDataValue('username', value);
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				this.setDataValue('passwordHash', generatePasswordHash(this.username, this.key));
			},
		},
		passwordHash: {
			type: Sequelize.STRING(100),
			set() {
				// это поле нельзя менять вручную
				throw new Error('Hey, buddy! You cannot change passwordHash manually. Change fields username and key for it.');
			},
		},
		key: {
			type: Sequelize.STRING(100),
			allowNull: false,
			set(value) {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				this.setDataValue('key', value);
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				this.setDataValue('passwordHash', generatePasswordHash(this.username, this.key));
			},
		},
	});

AuthCredential.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		...(this.id && { id: this.id }),
		...(this.userId && { userId: this.userId }),
		...(this.username && { username: this.username }),
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default AuthCredential;
