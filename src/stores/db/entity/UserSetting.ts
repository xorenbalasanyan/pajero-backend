import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const UserSetting = DefineLoggedEntity(
	'userSetting',
	'user_setting',
	{
		userId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		key: {
			type: Sequelize.STRING(50),
			allowNull: false,
		},
		value: {
			type: Sequelize.STRING,
			allowNull: false,
		},
	},
	{
		setterMethods: {
			user(value) {
				if (!value || !value.id) {
					throw new Error('User.id must be not null when set user in UserSetting entity');
				}
				// @ts-ignore
				this.setDataValue('userId', value.id);
			},
		},
	});

UserSetting.prototype.toDto = function toDto() {
	throw new Error('Hey buddy! Method UserSetting.toDto not released');
};

export default UserSetting;
