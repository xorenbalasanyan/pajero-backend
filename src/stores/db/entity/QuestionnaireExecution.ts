import Sequelize from 'sequelize';
import { QuestionnaireExecutionStatusEnum } from '~enums/QuestionnaireExecutionStatusEnum';
import { DefineLoggedEntity } from './tools';

const QuestionnaireExecution = DefineLoggedEntity(
	'questionnaireExecution',
	'questionnaire_execution',
	{
		questionnaireId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		status: {
			type: Sequelize.ENUM(
				QuestionnaireExecutionStatusEnum.IDLE,
				QuestionnaireExecutionStatusEnum.SCHEDULED,
				QuestionnaireExecutionStatusEnum.RUNNING,
				QuestionnaireExecutionStatusEnum.STOPPED,
				QuestionnaireExecutionStatusEnum.REPORTED,
				QuestionnaireExecutionStatusEnum.FINISHED,
				QuestionnaireExecutionStatusEnum.CANCELED,
			),
			allowNull: false,
			defaultValue: QuestionnaireExecutionStatusEnum.IDLE,
		},
		startAt: {
			type: Sequelize.DATE,
			allowNull: false,
		},
		finishAt: {
			type: Sequelize.DATE,
			allowNull: false,
		},
		addresseesCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		answeredCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	});

QuestionnaireExecution.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		test: 'TESTING DTO FILED',
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default QuestionnaireExecution;
