import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const Questionnaire = DefineLoggedEntity(
	'questionnaire',
	'questionnaire',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		title: {
			type: Sequelize.STRING(200),
			allowNull: false,
		},
		enabled: {
			type: Sequelize.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},
		questionnaireConfig: {
			type: Sequelize.JSON,
			allowNull: false,
		},
		addresseesShopIds: {
			type: Sequelize.JSON,
			allowNull: false,
		},
		reportConfig: {
			type: Sequelize.JSON,
			allowNull: false,
		},
		executionConfig: {
			type: Sequelize.JSON,
			allowNull: false,
		},
		isDeleted: {
			type: Sequelize.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},
	},
	{
		defaultScope: {
			where: {
				isDeleted: false,
			},
		},
	});

Questionnaire.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		test: 'TESTING DTO FILED',
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default Questionnaire;
