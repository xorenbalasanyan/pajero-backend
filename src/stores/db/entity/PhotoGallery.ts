import Sequelize from 'sequelize';
import { DefineNotUpdatedEntity } from './tools';

const PhotoGallery = DefineNotUpdatedEntity(
	'photoGallery', // название модели
	'photo_gallery', // название таблицы
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		fileId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	},
);

PhotoGallery.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		fileId: this.fileId,
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default PhotoGallery;
