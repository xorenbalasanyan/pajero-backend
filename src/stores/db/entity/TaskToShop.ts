import Sequelize from 'sequelize';
import { TaskStatusEnum } from '~enums/TaskStatusEnum';
import sequelize from '../../../lib/sequelize'; // FIXME импорт по алиасу

const TaskToShop = sequelize.define(
	'taskToShop',
	{
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			allowNull: false,
			autoIncrement: true,
		},
		taskId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		shopId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		status: {
			type: Sequelize.ENUM(
				TaskStatusEnum.IN_PROGRESS,
				TaskStatusEnum.DONE,
				TaskStatusEnum.REJECTED,
				TaskStatusEnum.EXPIRED,
			),
			allowNull: false,
		},
		dueDate: {
			type: Sequelize.DATE,
			allowNull: false,
		},
		executorUserId: {
			type: Sequelize.INTEGER,
			defaultValue: null,
		},
		executionComment: {
			type: Sequelize.TEXT,
			defaultValue: null,
		},
		executionInfo: {
			type: Sequelize.TEXT,
			defaultValue: null,
		},
		executedAt: {
			type: Sequelize.DATE,
			defaultValue: null,
		},
		rejectorUserId: {
			type: Sequelize.INTEGER,
			defaultValue: null,
		},
		rejectionComment: {
			type: Sequelize.TEXT,
			defaultValue: null,
		},
		rejectedAt: {
			type: Sequelize.DATE,
			defaultValue: null,
		},
		updatedAt: {
			type: Sequelize.DATE,
			defaultValue: null,
		},
		moderatorId: {
			type: Sequelize.INTEGER,
			defaultValue: null,
		},
	}, {
		tableName: 'task_to_shop',
		setterMethods: {
			shop(value) {
				// @ts-ignore
				this.setDataValue('shopId', value === null ? null : value.id);
			},
			executor(value) {
				// @ts-ignore
				this.setDataValue('executorUserId', value === null ? null : value.id);
			},
			rejector(value) {
				// @ts-ignore
				this.setDataValue('rejectorUserId', value === null ? null : value.id);
			},
			moderator(value) {
				// @ts-ignore
				this.setDataValue('moderatorId', value === null ? null : value.id);
			},
		},
		createdAt: false,
	},
);

TaskToShop.prototype.toDto = function toDto() {
	return {
		id: this.id,
		taskId: this.taskId,
		shopId: this.shopId,
		status: this.status,
		dueDate: this.dueDate,
		executorUserId: this.executorUserId || undefined,
		executionComment: this.executionComment || undefined,
		executionInfo: this.executionInfo ? JSON.parse(this.executionInfo) : undefined,
		executedAt: this.executedAt || undefined,
		rejectorUserId: this.rejectorUserId || undefined,
		rejectionComment: this.rejectionComment || undefined,
		rejectedAt: this.rejectedAt || undefined,
	};
};

export default TaskToShop;
