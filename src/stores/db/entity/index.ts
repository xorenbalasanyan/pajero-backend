import Account from './Account';
import AccountSetting from './AccountSetting';
import AuthCredential from './AuthCredential';
import FeatureConfiguration from './FeatureConfiguration';
import File from './File';
import FileMarker from './FileMarker';
import FileType from './FileType';
import Good from './Good';
import Incident from './Incident';
import IncidentDaylyReport from './IncidentDaylyReport';
import IncidentImport from './IncidentImport';
import IncidentType from './IncidentType';
import InfoMessage from './InfoMessage';
import InfoMessageToAddressee from './InfoMessageToAddressee';
import Log from './Log';
import MailMessage from './MailMessage';
import Questionnaire from './Questionnaire';
import QuestionnaireExecution from './QuestionnaireExecution';
import QuestionnaireExecutionToShop from './QuestionnaireExecutionToShop';
import QuestionnaireHistory from './QuestionnaireHistory';
import PhotoGallery from "./PhotoGallery";
import Region from './Region';
import ScheduleJobSetting from './ScheduleJobSetting';
import Shop from './Shop';
import ShopManager from './ShopManager';
import ShopManagerRole from './ShopManagerRole';
import ShopDiscount from './ShopDiscount';
import StructureImport from './StructureImport';
import Task from './Task';
import TaskToShop from './TaskToShop';
import TodoItem from './TodoItem';
import User from './User';
import UserAuth from './UserAuth';
import UserSetting from './UserSetting';
import ChecklistTask from './ChecklistTask';
import ChecklistTaskImport from './ChecklistTaskImport';

/*
 * Пример связи и использования:
 *
 * Account.hasMany(User)
 * -> const user = await account.getUsers()
 *
 * Account.belongsTo(User, { as: 'fk_owner', foreignKey: 'ownerId', targetKey: 'id' })
 * -> const owner = await account.getOwner()
 *
 * User.belongsTo(Account)
 * -> const account = await user.getAccount()
 */

const OWNER_ID_TO_USER_ID = { as: 'fk_owner', foreignKey: 'ownerId', targetKey: 'id' };
const MODERATOR_ID_TO_USER_ID = { as: 'fk_moderator', foreignKey: 'moderatorId', targetKey: 'id' };

Account.hasMany(User);
Account.hasMany(File);
Account.hasMany(FeatureConfiguration);
Account.hasMany(Log);
Account.hasMany(Shop);
Account.hasMany(ShopManagerRole);
Account.hasMany(ShopDiscount);
Account.hasMany(IncidentType);
Account.hasMany(InfoMessage);
Account.hasMany(Good);
Account.hasMany(IncidentDaylyReport);
Account.hasMany(IncidentImport);
Account.hasMany(Task);
Account.hasMany(StructureImport);
Account.hasMany(ChecklistTaskImport);
Account.hasMany(TodoItem);
Account.hasMany(PhotoGallery);
Account.hasMany(Region);
Account.hasMany(Questionnaire);
Account.hasMany(ChecklistTask);
Account.belongsTo(User, OWNER_ID_TO_USER_ID);
Account.belongsTo(User, MODERATOR_ID_TO_USER_ID);

AuthCredential.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
AuthCredential.belongsTo(User, { as: 'user', sourceKey: 'id', foreignKey: 'userId' });
AuthCredential.belongsTo(User, OWNER_ID_TO_USER_ID);
AuthCredential.belongsTo(User, MODERATOR_ID_TO_USER_ID);

AccountSetting.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
AccountSetting.belongsTo(User, OWNER_ID_TO_USER_ID);
AccountSetting.belongsTo(User, MODERATOR_ID_TO_USER_ID);

User.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
User.hasMany(UserAuth);
User.hasOne(AuthCredential);
User.hasMany(ShopManager);
User.hasMany(UserSetting);
User.hasMany(Log);
User.belongsTo(User, OWNER_ID_TO_USER_ID);
User.belongsTo(User, MODERATOR_ID_TO_USER_ID);

UserAuth.belongsTo(User, { as: 'fk_user', foreignKey: 'userId', targetKey: 'id' });
UserAuth.belongsTo(User, { as: 'fk_secondaryUser', foreignKey: 'secondaryUserId', targetKey: 'id' });
UserAuth.belongsTo(User, OWNER_ID_TO_USER_ID);
UserAuth.belongsTo(User, MODERATOR_ID_TO_USER_ID);

UserSetting.belongsTo(User, { as: 'fk_user', foreignKey: 'userId', targetKey: 'id' });
UserSetting.belongsTo(User, OWNER_ID_TO_USER_ID);
UserSetting.belongsTo(User, MODERATOR_ID_TO_USER_ID);

FeatureConfiguration.belongsTo(Account, { as: 'fk_account', foreignKey: 'accountId', targetKey: 'id' });
FeatureConfiguration.belongsTo(User, OWNER_ID_TO_USER_ID);
FeatureConfiguration.belongsTo(User, MODERATOR_ID_TO_USER_ID);

File.belongsTo(Account, { as: 'fk_account', foreignKey: 'accountId', targetKey: 'id' });
File.hasMany(FileMarker, { as: 'fk_fileMarker', foreignKey: 'fileId', targetKey: 'id' });
File.belongsTo(FileType, { as: 'fk_fileType', foreignKey: 'fileTypeId', targetKey: 'id' });
File.hasOne(IncidentDaylyReport);
File.hasOne(IncidentImport);
File.hasOne(InfoMessage);
File.hasOne(StructureImport);
File.hasOne(ChecklistTaskImport);
File.belongsTo(User, OWNER_ID_TO_USER_ID);
File.belongsTo(User, MODERATOR_ID_TO_USER_ID);

FileMarker.belongsTo(File, { as: 'fk_file', foreignKey: 'fileId', targetKey: 'id' });

Log.belongsTo(Account, { as: 'fk_account', foreignKey: 'accountId', targetKey: 'id' });
Log.belongsTo(Shop, { as: 'fk_shop', foreignKey: 'shopId', targetKey: 'id' });
Log.belongsTo(User, { as: 'fk_user', foreignKey: 'userId', targetKey: 'id' });
Log.belongsTo(Incident, { as: 'fk_incident', foreignKey: 'incidentId', targetKey: 'id' });
Log.hasOne(IncidentImport);
Log.hasOne(StructureImport);
Log.hasOne(ChecklistTaskImport);
Log.belongsTo(User, OWNER_ID_TO_USER_ID);

PhotoGallery.belongsTo(Account, { as: 'fk_account', foreignKey: 'accountId', targetKey: 'id' });
PhotoGallery.belongsTo(File, { as: 'fk_file', foreignKey: 'fileId', targetKey: 'id' });
PhotoGallery.belongsTo(User, OWNER_ID_TO_USER_ID);

Region.belongsTo(Account, { as: 'fk_account', foreignKey: 'accountId', targetKey: 'id' });
Region.hasMany(Shop);
Region.belongsTo(User, OWNER_ID_TO_USER_ID);
Region.belongsTo(User, MODERATOR_ID_TO_USER_ID);

Questionnaire.belongsTo(Account, { as: 'fk_account', foreignKey: 'accountId', targetKey: 'id' });
Questionnaire.hasMany(QuestionnaireExecution);
Questionnaire.hasMany(QuestionnaireExecutionToShop);
Questionnaire.hasMany(QuestionnaireHistory);
Questionnaire.belongsTo(User, OWNER_ID_TO_USER_ID);
Questionnaire.belongsTo(User, MODERATOR_ID_TO_USER_ID);

QuestionnaireExecution.belongsTo(Questionnaire, { as: 'fk_questionnaire', foreignKey: 'questionnaireId', targetKey: 'id' });
QuestionnaireExecution.hasMany(QuestionnaireExecutionToShop);
QuestionnaireExecution.belongsTo(User, OWNER_ID_TO_USER_ID);
QuestionnaireExecution.belongsTo(User, MODERATOR_ID_TO_USER_ID);

QuestionnaireExecutionToShop.belongsTo(Questionnaire, { as: 'fk_questionnaire', foreignKey: 'questionnaireId', targetKey: 'id' });
QuestionnaireExecutionToShop.belongsTo(QuestionnaireExecution, { as: 'fk_questionnaireExecution', foreignKey: 'questionnaireExecutionId', targetKey: 'id' });
QuestionnaireExecutionToShop.belongsTo(Shop, { as: 'fk_shop', foreignKey: 'shopId', targetKey: 'id' });
QuestionnaireExecutionToShop.belongsTo(User, { as: 'fk_respondingUser', foreignKey: 'respondingUserId', targetKey: 'id' });
QuestionnaireExecutionToShop.belongsTo(User, OWNER_ID_TO_USER_ID);
QuestionnaireExecutionToShop.belongsTo(User, MODERATOR_ID_TO_USER_ID);

Shop.belongsTo(Account, { as: 'fk_account', foreignKey: 'accountId', targetKey: 'id' });
Shop.hasMany(Incident);
Shop.hasMany(ShopManager);
Shop.hasMany(ShopDiscount);
Shop.hasMany(Log);
Shop.hasMany(ShopManager);
Shop.belongsTo(Region, { as: 'region', foreignKey: 'regionId', targetKey: 'id' });
Shop.belongsTo(User, OWNER_ID_TO_USER_ID);
Shop.belongsTo(User, MODERATOR_ID_TO_USER_ID);

ShopManager.belongsTo(Shop, { as: 'shop', foreignKey: 'shopId', targetKey: 'id' });
ShopManager.belongsTo(ShopManagerRole, { as: 'shopManagerRole', foreignKey: 'roleId', targetKey: 'id' });
ShopManager.belongsTo(User, { as: 'user', foreignKey: 'userId', targetKey: 'id' });
ShopManager.belongsTo(User, OWNER_ID_TO_USER_ID);
ShopManager.belongsTo(User, MODERATOR_ID_TO_USER_ID);

ShopManagerRole.belongsTo(Account, { as: 'account', foreignKey: 'accountId', targetKey: 'id' });
ShopManagerRole.belongsTo(User, OWNER_ID_TO_USER_ID);
ShopManagerRole.belongsTo(User, MODERATOR_ID_TO_USER_ID);

ShopDiscount.belongsTo(Account, { as: 'fk_account', foreignKey: 'accountId', targetKey: 'id' });
ShopDiscount.belongsTo(Shop, { as: 'fk_shop', foreignKey: 'shopId', targetKey: 'id' });
ShopDiscount.belongsTo(User, OWNER_ID_TO_USER_ID);
ShopDiscount.belongsTo(User, MODERATOR_ID_TO_USER_ID);

Good.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
Good.hasMany(Incident);
Good.belongsTo(User, OWNER_ID_TO_USER_ID);
Good.belongsTo(User, MODERATOR_ID_TO_USER_ID);

IncidentType.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
IncidentType.hasMany(Incident);
IncidentType.hasMany(IncidentImport);
IncidentType.belongsTo(User, OWNER_ID_TO_USER_ID);
IncidentType.belongsTo(User, MODERATOR_ID_TO_USER_ID);

Incident.belongsTo(Shop, { as: 'fk_shop', sourceKey: 'id', foreignKey: 'shopId' });
Incident.belongsTo(ShopDiscount, { as: 'fk_shopDiscount', sourceKey: 'id', foreignKey: 'shopDiscountId' });
Incident.belongsTo(IncidentType, { as: 'fk_incidentType', sourceKey: 'id', foreignKey: 'incidentTypeId' });
Incident.belongsTo(Good, { as: 'fk_good', sourceKey: 'id', foreignKey: 'goodId' });
Incident.hasMany(Log);
Incident.belongsTo(User, OWNER_ID_TO_USER_ID);
Incident.belongsTo(User, MODERATOR_ID_TO_USER_ID);

IncidentDaylyReport.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
IncidentDaylyReport.belongsTo(File, { as: 'fk_file', sourceKey: 'id', foreignKey: 'fileId' });
IncidentDaylyReport.belongsTo(User, OWNER_ID_TO_USER_ID);
IncidentDaylyReport.belongsTo(User, MODERATOR_ID_TO_USER_ID);

IncidentImport.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
IncidentImport.belongsTo(File, { as: 'fk_file', sourceKey: 'id', foreignKey: 'fileId' });
IncidentImport.belongsTo(IncidentType, { as: 'fk_incidentType', sourceKey: 'id', foreignKey: 'incidentTypeId' });
IncidentImport.belongsTo(Log, { as: 'fk_log', sourceKey: 'id', foreignKey: 'logId' });
IncidentImport.belongsTo(User, OWNER_ID_TO_USER_ID);

MailMessage.belongsTo(User, OWNER_ID_TO_USER_ID);
MailMessage.belongsTo(User, MODERATOR_ID_TO_USER_ID);

InfoMessage.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
InfoMessage.belongsTo(File, { as: 'fk_file', sourceKey: 'id', foreignKey: 'fileId' });
InfoMessage.hasMany(InfoMessageToAddressee, { as: 'fk_infoMessageToAddressees', sourceKey: 'id', foreignKey: 'infoMessageId' });
InfoMessage.belongsToMany(User, { as: 'fk_addressee', through: InfoMessageToAddressee, foreignKey: 'infoMessageId', otherKey: 'addresseeUserId' });
InfoMessage.belongsTo(User, OWNER_ID_TO_USER_ID);
InfoMessage.belongsTo(User, MODERATOR_ID_TO_USER_ID);

InfoMessageToAddressee.belongsTo(InfoMessage, { as: 'fk_infoMessage', foreignKey: 'infoMessageId', targetKey: 'id' });
InfoMessageToAddressee.belongsTo(User, { as: 'fk_addressee', foreignKey: 'addresseeUserId', targetKey: 'id' });
InfoMessageToAddressee.belongsTo(User, MODERATOR_ID_TO_USER_ID);

StructureImport.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
StructureImport.belongsTo(File, { as: 'fk_file', sourceKey: 'id', foreignKey: 'fileId' });
StructureImport.belongsTo(Log, { as: 'fk_log', sourceKey: 'id', foreignKey: 'logId' });
StructureImport.belongsTo(User, OWNER_ID_TO_USER_ID);

Task.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
Task.hasMany(TaskToShop, { as: 'fk_taskToShops', sourceKey: 'id', foreignKey: 'taskId' });
Task.belongsTo(User, OWNER_ID_TO_USER_ID);
Task.belongsTo(User, MODERATOR_ID_TO_USER_ID);

TaskToShop.belongsTo(Task, { as: 'fk_task', targetKey: 'id', foreignKey: 'taskId' });
TaskToShop.belongsTo(User, { as: 'fk_executor', targetKey: 'id', foreignKey: 'executorUserId' });
TaskToShop.belongsTo(User, { as: 'fk_rejector', targetKey: 'id', foreignKey: 'rejectorUserId' });
TaskToShop.belongsTo(Shop, { as: 'fk_shop', targetKey: 'id', foreignKey: 'shopId' });
TaskToShop.belongsTo(User, MODERATOR_ID_TO_USER_ID);

TodoItem.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
TodoItem.belongsTo(User, { as: 'fk_addressee', sourceKey: 'id', foreignKey: 'addresseeId' });
TodoItem.belongsTo(User, OWNER_ID_TO_USER_ID);
TodoItem.belongsTo(User, MODERATOR_ID_TO_USER_ID);

ChecklistTask.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
ChecklistTask.belongsTo(User, { as: 'fk_executor', sourceKey: 'id', foreignKey: 'executorUserId' });
ChecklistTask.belongsTo(Shop, { as: 'fk_shop', sourceKey: 'id', foreignKey: 'shopId' });
ChecklistTask.belongsTo(Shop, { as: 'fk_executor_shop', sourceKey: 'id', foreignKey: 'executorShopId' });
ChecklistTask.belongsTo(User, OWNER_ID_TO_USER_ID);
ChecklistTask.belongsTo(User, MODERATOR_ID_TO_USER_ID);

ChecklistTaskImport.belongsTo(Account, { as: 'fk_account', sourceKey: 'id', foreignKey: 'accountId' });
ChecklistTaskImport.belongsTo(File, { as: 'fk_file', sourceKey: 'id', foreignKey: 'fileId' });
ChecklistTaskImport.belongsTo(Log, { as: 'fk_log', sourceKey: 'id', foreignKey: 'logId' });
ChecklistTaskImport.belongsTo(User, OWNER_ID_TO_USER_ID);

export {
	Account,
	AccountSetting,
	AuthCredential,
	User,
	UserAuth,
	UserSetting,
	Log,
	FeatureConfiguration,
	File,
	FileMarker,
	FileType,
	PhotoGallery,
	Region,
	ScheduleJobSetting,
	Shop,
	ShopDiscount,
	ShopManager,
	ShopManagerRole,
	Good,
	IncidentType,
	Incident,
	IncidentDaylyReport,
	IncidentImport,
	MailMessage,
	InfoMessage,
	InfoMessageToAddressee,
	Questionnaire,
	QuestionnaireExecution,
	QuestionnaireExecutionToShop,
	QuestionnaireHistory,
	Task,
	TaskToShop,
	TodoItem,
	StructureImport,
	ChecklistTask,
	ChecklistTaskImport,
};
