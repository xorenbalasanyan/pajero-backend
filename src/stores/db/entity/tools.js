import merge from 'deepmerge';
import Sequelize from 'sequelize';
import sequelize from '~lib/sequelize'; // FIXME импорт по алиасу

export function DefineLoggedEntity(entityName, tableName, config, properties) {
	return sequelize.define(
		entityName,
		merge.all([
			{
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					allowNull: false,
					autoIncrement: true,
				},
			},
			config,
			{
				createdAt: Sequelize.DATE,
				ownerId: {
					type: Sequelize.INTEGER,
					allowNull: false,
				},
				updatedAt: Sequelize.DATE,
				moderatorId: Sequelize.INTEGER,
			},
		]),
		merge(
			{
				tableName,
				setterMethods: {
					owner(value) {
						if (!value || !value.id) {
							throw new Error('Owner.id undefined when setup owner in entity');
						}
						// @ts-ignore
						this.setDataValue('ownerId', value.id);
					},
					moderator(value) {
						// @ts-ignore
						this.setDataValue('moderatorId', value === null ? null : value.id);
					},
				},
			},
			properties || {},
		),
	);
}

export function DefineNotUpdatedEntity(entityName, tableName, config, properties) {
	return sequelize.define(
		entityName,
		merge.all([
			{
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					allowNull: false,
					autoIncrement: true,
				},
			},
			config,
			{
				createdAt: Sequelize.DATE,
				ownerId: {
					type: Sequelize.INTEGER,
					allowNull: false,
				},
			},
		]),
		merge(
			{
				tableName,
				setterMethods: {
					owner(value) {
						if (!value?.id) {
							throw new Error('Owner.id undefined when setup owner in entity');
						}
						// @ts-ignore
						this.setDataValue('ownerId', value.id);
					},
				},
				updatedAt: false,
			},
			properties || {},
		),
	);
}
