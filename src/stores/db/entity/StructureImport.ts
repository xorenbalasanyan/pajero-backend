import Sequelize from 'sequelize';
import { DefineNotUpdatedEntity } from './tools';
import { StructureImportStateEnum } from '~enums/StructureImportStatusEnum';

const StructureImport = DefineNotUpdatedEntity(
	'structureImport',
	'structure_import',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		fileId: Sequelize.INTEGER,
		logId: Sequelize.INTEGER,
		status: {
			type: Sequelize.ENUM(
				StructureImportStateEnum.OK,
				StructureImportStateEnum.WARN,
				StructureImportStateEnum.ERROR,
			),
			allowNull: false,
			defaultValue: StructureImportStateEnum.OK,
		},
		createdUsersCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		updatedUsersCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		deletedUsersCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		createdShopsCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		updatedShopsCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		deletedShopsCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		warnCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	});

StructureImport.prototype.toDto = function toDto() {
	return {
		id: this.id,
		fileId: this.fileId,
		logId: this.logId,
		status: this.status,
		createdUsersCount: this.createdUsersCount,
		updatedUsersCount: this.updatedUsersCount,
		deletedUsersCount: this.deletedUsersCount,
		createdShopsCount: this.createdShopsCount,
		updatedShopsCount: this.updatedShopsCount,
		deletedShopsCount: this.deletedShopsCount,
		warnCount: this.warnCount,
		ownerId: this.ownerId,
		createdAt: this.createdAt,
	};
};

export default StructureImport;
