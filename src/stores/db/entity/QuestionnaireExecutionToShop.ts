import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';
import { QuestionnaireExecutionToShopStatusEnum } from '~enums/QuestionnaireExecutionToShopStatusEnum';

const QuestionnaireExecutionToShop = DefineLoggedEntity(
	'questionnaireExecutionToShop',
	'questionnaire_execution_to_shop',
	{
		questionnaireId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		questionnaireExecutionId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		shopId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		status: {
			type: Sequelize.ENUM(
				QuestionnaireExecutionToShopStatusEnum.RUNNING,
				QuestionnaireExecutionToShopStatusEnum.FINISHED,
				QuestionnaireExecutionToShopStatusEnum.FAILED,
				QuestionnaireExecutionToShopStatusEnum.CANCELED,
			),
			allowNull: false,
			defaultValue: QuestionnaireExecutionToShopStatusEnum.RUNNING,
		},
		startAt: {
			type: Sequelize.DATE,
			allowNull: false,
		},
		finishAt: {
			type: Sequelize.DATE,
			allowNull: false,
		},
		respondingUserId: {
			type: Sequelize.INTEGER,
		},
		respondingData: {
			type: Sequelize.JSON,
		},
		respondingAt: {
			type: Sequelize.DATE,
		},
	});

QuestionnaireExecutionToShop.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		test: 'TESTING DTO FILED',
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default QuestionnaireExecutionToShop;
