import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const Region = DefineLoggedEntity(
	'region',
	'region',
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		name: {
			type: Sequelize.STRING(100),
			allowNull: false,
		},
		email: {
			type: Sequelize.STRING(100),
		},
	});

Region.prototype.toDto = function () {
	return {
		id: this.id,
		name: this.name,
		email: this.email || undefined,
	};
};

export default Region;
