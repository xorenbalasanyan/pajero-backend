import Sequelize from 'sequelize';
import { DefineNotUpdatedEntity } from './tools';

const QuestionnaireHistory = DefineNotUpdatedEntity(
	'questionnaireHistory',
	'questionnaire_history',
	{
		questionnaireId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		questionnaireExecutionId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		addresseesCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		answeredCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		finishedAt: {
			type: Sequelize.DATE,
			allowNull: false,
		},
		reportData: {
			type: Sequelize.JSON,
			allowNull: false,
		},
		reportFileId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
	});

QuestionnaireHistory.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		test: 'TESTING DTO FILED',
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default QuestionnaireHistory;
