import Sequelize from 'sequelize';
import { FileLocationEnum } from '~enums/FileLocationEnum';
import { DefineLoggedEntity } from './tools';

const File = DefineLoggedEntity(
	'file',
	'file',
	{
		accountId: Sequelize.INTEGER,
		externalId: {
			type: Sequelize.STRING(255),
		},
		name: {
			type: Sequelize.STRING(100),
			allowNull: false,
		},
		fileTypeId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		size: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		location: {
			type: Sequelize.ENUM(
				FileLocationEnum.FS,
				FileLocationEnum.CDN,
			),
			allowNull: false,
			defaultValue: FileLocationEnum.FS,
		},
		pathName: {
			type: Sequelize.STRING(500),
			allowNull: false,
		},
		downloadCount: {
			type: Sequelize.INTEGER,
			allowNull: false,
			defaultValue: 0,
		},
		isDeleted: {
			type: Sequelize.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},
	},
	{
		defaultScope: {
			where: {
				isDeleted: false,
			},
		},
		setterMethods: {
			account(value) {
				if (!value || !value.id) {
					throw new Error('Account.id must be not null when set account in File entity');
				}
				// @ts-ignore
				this.setDataValue('accountId', value.id);
			},
		},
	});

File.prototype.toDto = function toDto() {
	return {
		id: this.id,
		name: this.name,
		fileTypeId: this.fileTypeId,
		size: this.size,
		// если файл в облаке, то отдает ссылку на файл
		path: this.location === FileLocationEnum.CDN ? this.pathName : undefined,
		downloadCount: this.downloadCount,
	};
};

export default File;
