import Sequelize from 'sequelize';
import { UserRoles } from '~enums/UserRoleEnum';
import { DefineLoggedEntity } from './tools';

const User = DefineLoggedEntity(
	'user',
	'user',
	{
		externalId: {
			type: { type: Sequelize.STRING(100) },
		},
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		role: {
			type: Sequelize.ENUM(
				UserRoles.ADMIN,
				UserRoles.ROBOT,
				UserRoles.SUPPORT,
				UserRoles.CHIEF,
				UserRoles.MANAGER,
			),
			allowNull: false,
		},
		firstName: {
			type: Sequelize.STRING(100),
		},
		lastName: {
			type: Sequelize.STRING(100),
		},
		middleName: {
			type: Sequelize.STRING(100),
		},
		email: {
			type: Sequelize.STRING(100),
		},
		phone: {
			type: Sequelize.STRING(100),
		},
		skype: {
			type: Sequelize.STRING(100),
		},
		telegram: {
			type: Sequelize.STRING(100),
		},
		comment: Sequelize.TEXT,
		isDeleted: {
			type: Sequelize.BOOLEAN,
			allowNull: false,
			defaultValue: false,
		},
	},
	{
		defaultScope: {
			where: {
				isDeleted: false,
			},
		},
		getterMethods: {
			fullName() {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				return [this.lastName, this.firstName, this.middleName]
					.filter(Boolean)
					.join(' ').trim();
			},
			isAdmin() {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				return this.role === UserRoles.ADMIN;
			},
			isRobot() {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				return this.role === UserRoles.ROBOT;
			},
			isSupport() {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				return this.role === UserRoles.SUPPORT;
			},
			isChief() {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				return this.role === UserRoles.CHIEF;
			},
			isManager() {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				return this.role === UserRoles.MANAGER;
			},
		},
		setterMethods: {
			fullName(value) {
				const parts = value.split(' ');
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				this.setDataValue('lastName', parts[0]);
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				this.setDataValue('firstName', (parts.length > 1) ? parts[1] : null);
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				this.setDataValue('middleName', (parts.length > 2) ? parts[2] : null);
			},
		},
	});

User.prototype.toDto = function toDto(mixerCb, userSet?: Set<number>) {
	let dto = {
		id: this.id,
		externalId: this.externalId || undefined,
		role: this.role,
		firstName: this.firstName,
		lastName: this.lastName || undefined,
		middleName: this.middleName || undefined,
		fullName: this.fullName,
		email: this.email || undefined,
		phone: this.phone || undefined,
		comment: this.comment || undefined,
		subordinateUsers: undefined, // FIXME удалить когда будет нормальная модель
		subordinateShops: undefined, // FIXME удалить когда будет нормальная модель
		shopManagerRole: undefined,
	};

	if (this.subordinateUsers?.length) {
		if (!userSet) {
			dto.subordinateUsers = this.subordinateUsers.map(i => i.toDto(undefined, new Set([this.id])));
		} else if (!userSet.has(this.id)) {
			dto.subordinateUsers = this.subordinateUsers.map(i => i.toDto(undefined, userSet.add(this.id)));
		}
	}

	if (this.subordinateShops?.length) {
		dto.subordinateShops = this.subordinateShops.map(i => i.toDto());
	}

	if (this.shopManagerRole) {
		dto.shopManagerRole = this.shopManagerRole.toDto();
	}

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default User;
