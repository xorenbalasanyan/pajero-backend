import Sequelize from 'sequelize';
import { DefineLoggedEntity } from './tools';

const FeatureConfiguration = DefineLoggedEntity(
	'featureConfiguration', // название модели
	'feature_configuration', // название таблицы
	{
		accountId: {
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		name: {
			type: Sequelize.STRING(255),
			allowNull: false,
		},
		title: {
			type: Sequelize.STRING(255),
			allowNull: false,
		},
		enabledAll: {
			type: Sequelize.BOOLEAN,
			allowNull: false,
		},
		shopIds: {
			type: Sequelize.JSON,
			get() {
				// @ts-ignore
				const value = this.getDataValue('shopIds');
				if (typeof value !== 'string') return value;
				return JSON.parse(value);
			},
		},
	},
);

FeatureConfiguration.prototype.toDto = function toDto(mixerCb) {
	let dto = {
		id: this.id,
		name: this.name,
		title: this.title,
		enabledAll: this.enabledAll,
		shopIds: this.shopIds,
	};

	if (mixerCb) {
		dto = mixerCb(this, dto);
	}

	return dto;
};

export default FeatureConfiguration;
