import BaseStore from './BaseStore';
import ShopDbStore from './db/ShopDbStore';

export default class ShopStore extends BaseStore
{
	constructor() {
		super(ShopDbStore);
	}
}
