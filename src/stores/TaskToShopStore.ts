import BaseStore from './BaseStore';
import TaskToShopDbStore from './db/TaskToShopDbStore';

export default class TaskToShopStore extends BaseStore
{
	constructor() {
		super(TaskToShopDbStore);
	}
}
