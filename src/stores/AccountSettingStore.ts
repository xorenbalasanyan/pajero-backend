import BaseStore from './BaseStore';
import { AccountSettingKeyEnum } from '~enums/AccountSettingKeyEnum';
import AccountSettingDbStore from './db/AccountSettingDbStore';

export default class AccountSettingStore extends BaseStore
{
	constructor() {
		super(AccountSettingDbStore);
	}

	/**
	 * Ищет адресатов для отправки очтета анализа структуры.
	 * @param accountId
	 */
	findOneStructureAnalysisReportAddresseesByAccountId = async accountId => this.findOne({
		where: {
			accountId,
			key: AccountSettingKeyEnum.STRUCTURE_ANALISYS_REPORT_ADDRESSEES,
		},
	})
}
