import BaseStore from './BaseStore';
import QuestionnaireDbStore from './db/QuestionnaireDbStore';

export default class QuestionnaireStore extends BaseStore
{
	constructor() {
		super(QuestionnaireDbStore);
	}
}
