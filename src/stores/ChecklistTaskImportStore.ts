import BaseStore from './BaseStore';
import ChecklistTaskImportDbStore from './db/ChecklistTaskImportDbStore';

export default class ChecklistTaskImportStore extends BaseStore
{
	constructor() {
		super(ChecklistTaskImportDbStore);
	}
}
