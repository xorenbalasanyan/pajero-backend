import BaseStore from './BaseStore';
import PhotoGalleryDbStore from './db/PhotoGalleryDbStore';

export default class PhotoGalleryStore extends BaseStore {
	constructor() {
		super(PhotoGalleryDbStore);
	}
}
