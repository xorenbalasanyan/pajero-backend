import BaseStore from './BaseStore';
import FileDbStore from './db/FileDbStore';

export default class FileStore extends BaseStore
{
	constructor() {
		super(FileDbStore);
	}
}
