import BaseStore from './BaseStore';
import FeatureConfigurationDbStore from './db/FeatureConfigurationDbStore';

export default class FeatureConfigurationStore extends BaseStore {
	constructor() {
		super(FeatureConfigurationDbStore);
	}
}
