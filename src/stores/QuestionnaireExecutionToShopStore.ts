import BaseStore from './BaseStore';
import QuestionnaireExecutionToShopDbStore from './db/QuestionnaireExecutionToShopDbStore';

export default class QuestionnaireExecutionToShopStore extends BaseStore
{
	constructor() {
		super(QuestionnaireExecutionToShopDbStore);
	}
}
