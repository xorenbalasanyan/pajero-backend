import { Transaction } from 'sequelize';
import BaseView from './BaseView';

/**
 * Объект для выполнения всех типов запросов к хранилищу модели.
 */
export default class BaseStore extends BaseView
{
	constructor(DbStoreType) {
		super(DbStoreType);
	}

	createOne = async (item: any, transaction?: Transaction): Promise<any> => this
		.dbStore.createOne(item, transaction)

	createAll = async (items: any[], transaction?: Transaction): Promise<any[]> => this
		.dbStore.createAll(items, transaction)

	updateOne = async (item: any, newData: any, transaction?: Transaction): Promise<any> => this
		.dbStore.updateOne(item, newData, transaction)

	updateAll = async (items: any[], newData: any, transaction?: Transaction): Promise<any[]> => this
		.dbStore.updateAll(items, newData, transaction)

	updateWhere = async (newData: any, options: any, transaction? :Transaction) => {
		// объект транзакции запихиваем в options
		if (transaction) {
			options.transaction = transaction;
		}
		return this.dbStore.updateWhere(newData, options);
	}

	deleteWhere = async (where, transaction?: Transaction): Promise<void> => this
		.dbStore.deleteWhere(where, transaction)
}
