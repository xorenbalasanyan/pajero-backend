import BaseStore from './BaseStore';
import UserSettingDbStore from './db/UserSettingDbStore';

export default class UserSettingStore extends BaseStore
{
	constructor() {
		super(UserSettingDbStore);
	}
}
