import BaseStore from './BaseStore';
import QuestionnaireExecutionDbStore from './db/QuestionnaireExecutionDbStore';

export default class QuestionnaireExecutionStore extends BaseStore
{
	constructor() {
		super(QuestionnaireExecutionDbStore);
	}
}
