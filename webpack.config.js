const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const TerserPlugin = require('terser-webpack-plugin');
const WebpackShellPluginNext = require('webpack-shell-plugin-next');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const packageInfo = require('./package');
const CleanWebpackPlugin = require('clean-webpack-plugin').CleanWebpackPlugin;

const commonSrc = path.resolve(__dirname, '..', 'pajero-common', 'src');
const srcRoot = path.resolve(__dirname, 'src');
const distRoot = path.resolve(__dirname, 'dist');
const nodeEnv = (process.env.NODE_ENV || '').toLowerCase();
const serverEnvironment = (process.env.SERVER_ENV || '').toLowerCase();
const isProd = nodeEnv === 'production';

// При запуске приложения необходимо указать переменную среды NODE_ENV.
// При сборке вебпаком этого делать не требуется, потому что вебпак зашьет переменную в билд.
const availableNodeEnvs = ['development', 'test', 'production'];
if (!availableNodeEnvs.includes(nodeEnv || '')) {
	console.error(`[Ошибка сборки] env NODE_ENV может принимать одно значение из ${availableNodeEnvs}.\nProcess will exit.`);
	process.exit(1);
}

// Проверка SERVER_ENV - переменная, указывающая приложению на каком стенде выполняется его работа.
// При сборке приложения необходимо указать переменную среды SERVER_ENV.
// При сборке вебпаком этого делать не требуется, потому что вебпак зашьет переменную в билд.
// Эта переменная имеет смысл только при сборке (когда NODE_ENV=production').
if (!serverEnvironment) {
	console.error(`[Ошибка сборки] Нужно установить env SERVER_ENV.\nProcess will exit.`);
	process.exit(1);
}
const availableServerEnvs = ['local', 'test', 'release', 'prod'];
// local - стенд разработчика
// test - тестовый стенд
// release - релизный стенд
// prod - продуктовый стенд
if (!availableServerEnvs.includes(serverEnvironment)) {
	console.error(`[Ошибка сборки] env SERVER_ENV может принимать одно значение из ${availableServerEnvs}.\nProcess will exit.`);
	process.exit(1);
}

// Опредеялем собираемые модули
const { ONE_ENTRY } = process.env;
const entry = ONE_ENTRY
	? { [ONE_ENTRY]: path.resolve(srcRoot, `${ONE_ENTRY}`) }
	: ['backend']
		.reduce((acc, key) => {
			acc[key] = path.resolve(srcRoot, `${key}`);
			return acc;
		}, {});

const config = {
	mode: nodeEnv,
	target: 'node',
	node: {
		__filename: true,
		__dirname: true,
	},
	devtool: 'source-map', // для логов на проде тоже нужен
	entry,
	output: {
		path: distRoot,
		filename: '[name].js',
	},
	// под dev - облегчаем работу вебпаку, собираем только чистый код
	// в этом случае вебпак будет брать депенденси из node_modules
	// под prod - исключаем либы, с которыми не будем работать, потому что sequilize их подтягивает
	externals: isProd
		? ['pg', 'sqlite3', 'tedious', 'pg-hstore']
		: [nodeExternals()],
	optimization: {
		// при сборке прода в модуле mysql2 обращение к полям в каком-то месте выполняется через жопу,
		// поэтому для сборки на прод отключим обфускацию полей
		minimizer: isProd
			? [new TerserPlugin({ terserOptions: { keep_fnames: true } })]
			: undefined,
	},
	resolve: {
		extensions: ['.ts', '.js'],
		modules: [
			srcRoot,
			'node_modules',
		],
		alias: {
			// common source
			'~enums': path.resolve(commonSrc, 'enums'),
			'~errors': path.resolve(commonSrc, 'errors'),
			'~types': path.resolve(commonSrc, 'types'),
			'~utils': path.resolve(commonSrc, 'utils'),
			// backend source
			'~middlewares': path.resolve(srcRoot, 'middlewares'),
			'~stores': path.resolve(srcRoot, 'stores'),
			'~lib': path.resolve(srcRoot, 'lib'),
			'~jobs': path.resolve(srcRoot, 'jobs'),
			'~controllers': path.resolve(srcRoot, 'controllers'),
			'~services': path.resolve(srcRoot, 'services'),
			'~repositories': path.resolve(srcRoot, 'repositories'),
		},
	},
	plugins: [
		new CleanWebpackPlugin(),
		new MomentLocalesPlugin({ // разрешаем moment загрузить только en (базовая) и ru локали
			localesToKeep: ['ru'],
		}),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify(nodeEnv),
			'process.env.SERVER_ENV': JSON.stringify(serverEnvironment),
			'process.env.PACKAGE_NAME': JSON.stringify(packageInfo.name),
			'process.env.PACKAGE_VERSION': JSON.stringify(packageInfo.version),
		}),
	],
	module: {
		rules: [
			{
				test: /\.ts$/,
				enforce: 'pre',
				loader: 'ts-loader',
				options: {
					configFile: isProd ? 'tsconfig.prod.json' : 'tsconfig.json',
				},
			},
		],
	},
	watchOptions: {
		aggregateTimeout: 300, // delay before rebuilding once the first file changed
		// исключить из мониторинга за изменениями
		ignored: [
			'deploy',
			'dist',
			'migrations',
			'scripts',
			'node_modules',
			'config*.js',
		],
		poll: 1000, // check for changes every ms
	},
	stats: {
		// errorDetails: true, // выводим подробные ошибки
	}
};

if (!isProd && ONE_ENTRY) {
	// после сборки в режиме разработки будем запускать nodemon
	config.plugins.push(
		new WebpackShellPluginNext({
			onBuildEnd: {
				scripts: [`nodemon -w ${distRoot} ${distRoot}/${ONE_ENTRY}.js`],
				blocking: false,
				parallel: true,
			},
		}),
	);
}

module.exports = config;
